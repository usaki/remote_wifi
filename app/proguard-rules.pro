# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\android-sdks/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#指定代码的压缩级别
-optimizationpasses 5
#包名不混合大小写
-dontusemixedcaseclassnames

#优化  不优化输入的类文件
-dontoptimize
#预校验
-dontpreverify
#混淆时是否记录日志
-verbose
# 混淆时所采用的算法
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
#保护注解
-keepattributes *Annotation*
#忽略警告
-ignorewarning

# 抛出异常时保留代码行号
-keepattributes SourceFile,LineNumberTable

# 不做预校验，preverify是proguard的四个步骤之一，Android不需要preverify，去掉这一步能够加快混淆速度。
-dontpreverify

-libraryjars <java.home>/lib/rt.jar


# -------------系统类不需要混淆 --------------------------
-keep public class * extends android.app.Fragment
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.preference.Preference
-keep public class * extends android.support.**

#保持 native 方法不被混淆
-keepclasseswithmembernames class * {
  native <methods>;
}
#保持自定义控件类不被混淆
-keepclasseswithmembers class * {
  public <init>(android.content.Context, android.util.AttributeSet);
}
-keepclassmembers class * {
      public void *ButtonClicked(android.view.View);
}
#保持自定义控件类不被混淆
-keepclassmembers class * extends android.app.Activity {
  public void *(android.view.View);
}

# Glide 保护
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
**[] $VALUES;
public *;
}

# 保留support下的所有类及其内部类
-keep class android.support.** {*;}

# 保留继承的
-keep public class * extends android.support.v4.**
-keep public class * extends android.support.v7.**
-keep public class * extends android.support.annotation.**

#不混淆资源类
-keep class **.R$* {
    *;
}
# 泛型与反射

-keepattributes *Annotation*,InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
-keepattributes EnclosingMethod
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}


#第三方
-keep class org.xmlpull.v1.** {*;}
-keep class android.support.**{*;}
-keep class android.support.v7.**{*;}
-keep class android.support.v4.**{*;}
-keep class java.nio.**{*;}
-keep class org.codehaus.**{*;}
-keep class com.bumptech.glide.**{*;}
-keep class com.intuit.**{*;}
-keep public class com.google.** {*;}
-keep com.lee.** {*;}
-keep com.mylhyl.** {*;}
-keep com.android.support.** {*;}
-keep class com.squareup.** {*;}
-keep class com.yanzhenjie.** {*;}
-keep class com.mylhyl.** {*;}
-keep class com.github.** {*;}
-keep class okio.** {*;}
-keep public class org.codehaus.**{*;}
-keep class javax.annotation.**{*;}
-keep class android.support.annotation.Nullable.**{*;}
-keep class com.mcxiaoke.** {*;}
-keep public class com.android.util.APSettingDialog.** {*;}

-dontwarn android.support.v7.media.MediaRouter.*
-dontwarn android.support.**
-dontwarn class org.codehaus.**
-dontwarn javax.annotation.**
-dontwarn android.support.annotation.Nullable.**

-dontwarn org.codehaus.**
-dontwarn java.lang.invoke.**
-dontwarn android.app.Notification.**
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement.**

-dontreport android.app.Notification.**
-dontnote android.net.http.**
-dontnote org.apache.**

-dontwarn org.apache.**
-dontwarn javax.annotation.Nullable
-dontwarn javax.annotation.ParametersAreNonnullByDefault


-dontwarn  com.iflytek.sunflower.**
-keep class  com.iflytek.sunflower.** { *; }
# 保留Parcelable序列化类不被混淆
-keep class * implements android.os.Parcelable {
public static final android.os.Parcelable$Creator *;
}


#權限申請
-keepclassmembers class ** {
    @com.yanzhenjie.permission.PermissionYes <methods>;
}
-keepclassmembers class ** {
    @com.yanzhenjie.permission.PermissionNo <methods>;
}

# 对于带有回调函数的onXXEvent、**On*Listener的，不能被混淆
-keepclassmembers class * {
void *(**On*Event);
void *(**On*Listener);
}

-assumenosideeffects class android.util.Log {
    public static *** v(...);
    public static *** i(...);
    public static *** d(...);
    public static *** w(...);
    public static *** e(...);
}

