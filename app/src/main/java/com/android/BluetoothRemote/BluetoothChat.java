/*
 *
 *
 *
 *
 *
 */

package com.android.BluetoothRemote;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGestureListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.RemoteWiFi.R;
import com.android.util.APSettingDialog;
import com.android.util.MenuDialog;
import com.android.util.MySocketThread;
import com.lee.gcm.QuickstartPreferences;
import com.mylhyl.circledialog.CircleDialog;
import com.mylhyl.circledialog.callback.ConfigDialog;
import com.mylhyl.circledialog.params.DialogParams;

import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static com.android.util.MySocketThread.StopSocket;
import static com.android.util.ResultCode.SEND_FAILED;
import static com.android.util.ResultCode.SEND_OK;
import static com.android.util.ResultCode.SEND_RECIEVE;
import static com.android.util.Validator.isValidIPAddr;

public class BluetoothChat extends AppCompatActivity implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener, Handler.Callback {
    // Debugging
    private static final String TAG = "WifiChat";
    public static final boolean D = true;

    private static final int REQUEST_PERMISSION = 1;


    //menu
    private String changeMenu = "none";//YILINEDIT-voiceRecg改成字串
    private static final int st = Menu.FIRST;
    protected static final int close = Menu.FIRST + 1;
    private static final int scan = Menu.FIRST + 2;
    //YILINEDIT-voiceRecg
    private static final int record = Menu.FIRST + 3;
    private static final int record_learning = Menu.FIRST + 4;
    private static final int record_learning_stop = Menu.FIRST + 5;
    private static final int privace_text = Menu.FIRST + 6;
    //YILINEDIT
    private SharedPreferences OPenCam;
    private static final String OpenCamData = "OPENCAMDATA";
    private static final String CAM = "CAM";


    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    //dialog
    private static final int DIALOG_MESSAGE = 1;
    private static final int DIALOG_MENU = 2;


    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int LAMP_PIC1 = 3;
    private static final int LAMP_PIC2 = 4;
    private static final int LAMP_PIC3 = 5;
    private static final int LAMP_PIC4 = 6;
    private static final int LAMP_PIC5 = 7;
    private static final int LAMP_PIC6 = 8;
    private static final int LAMP_PIC7 = 9;
    private static final int LAMP_PIC8 = 10;
    private static final int LAMP_PIC9 = 11;
    private static final int LAMP_PIC10 = 12;
    private static final int LAMP_PIC11 = 13;
    private static final int LAMP_PIC12 = 14;
    private static final int RECORD = 15;//YILINEDIT-voiceRecg
    private static final int RETRUN_MAIN = 16;
    //YILINEDIT－情境
    public Handler handler_sendMacro = new Handler(this);
    private static final int SEND_MESSAGE_MACRO = 1;
    // Layout Views
    public TextView mTitle;
    public ListView mConversationView;
    private Spinner spinner, spinner2, spinner3;
    private boolean avoidSpinnerListenerCheck = false;
    public Button mButton01, mButton02, mButton03, mButton04, mButton05, mButton06, mButton07,
            mButton08, mButton09, mButton10, mButton11, mButton12, mButton13, mButton14, mButton15, mButton16,
            mButton17, mButton18, mButton19, mButton20, mButton21, mButton22, mButton23, mButton24, mButton25, mButton26, mButton27, mButton28, mButton29, mButton30, mButton31, mButton32, mButton33, mButton34, mButton35, mButton36, mButton37, mButton38, mButton39, mButton40, mButton41, mButton42, mButton43, mButton44, mButton45, mButton46, mButton47, mButton48, mButton49, mButton50, mButton51, mButton52, mButton53, mButton54, mButton55, mButton56, mButton57, mButton58, mButton59, mButton60, mButton61, mButton62, mButton63, mButton64, mButton65, mButton66, mButton67, mButton68, mButton69, mButton70, mButton71, mButton72, mButton73, mButton74, mButton75, mButton76, mButton77, mButton78, mButton79, mButton80, mButton81, mButton82, mButton83, mButton84, mButton85, mButton86, mButton87, mButton88, mButton89, mButton90, mButton91, mButton92, mButton93, mButton94, mButton95, mButton96, mButton97, mButton98, mButton99, mButton100, mButton101, mButton102, mButton103, mButton104, mButton105, mButton106, mButton107, mButton108, mButton109, mButton110, mButton111, mButton112, mButton113, mButton114, mButton115, mButton116, mButton117, mButton118, mButton119, mButton120, mButton121, mButton122, mButton123, mButton124, mButton125, mButton126, mButton127, mButton128, mButton129, mButton130, mButton131, mButton132, mButton133, mButton134, mButton135, mButton136, mButton137, mButton138, mButton139, mButton140, mButton141, mButton142, mButton143, mButton144, mButton145, mButton146, mButton147, mButton148, mButton149, mButton150, mButton151, mButton152, mButton153, mButton154, mButton155, mButton156, set00, set01, set02, custom1set, custom2set, custom3set, lampset, lampok, marco1, marco2, marco3, marco4, marco5, marco6, marcoset1, marcoset2, marcoset3, marcoset4, marcoset5, marcoset6, curtainset, marcosetok, marcoset1ok, marcoset2ok, marcoset3ok, marcoset4ok, marcoset5ok, marcoset6ok, curok, s1, s2, s3, s4, s5, opcs, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, s18, s19, s20, s21, s22, s23, s24, s25, s26, s27, s28, s29, s30, home, home0, home1, home2, home3, home4, home5, home6, home7, home8, home9, home10, row1, row2, row3, row4, row5, row6, row7, f1, f2, f3, f4, f5, f6, firstset, firstok, airset, airok, main1, main2, main3, main4, main5, main6, safe1, safe2, safe3, safe4,
            window1;

    public EditText meditText1, meditText2, meditText3, meditText4, meditText5, meditText6, meditText7, meditText8, meditText9, meditText10, meditText11, meditText12, meditText13, meditText14, meditText15, meditText16, meditText17, meditText18, meditText19, meditText20, meditText21, meditText22, meditText23, meditText24, meditText25, meditText26, meditText27, meditText28, meditText29, meditText30, meditText31, meditText32, meditText33, meditText34, meditText35, meditText36, meditText37, meditText38, meditText39, meditText40, meditText41, meditText42, meditText43, meditText44, meditText45, meditText46, meditText47, meditText48, meditText49, meditText50, meditText51, meditText52, meditText53, meditText54, meditText55, meditText56, meditText57, meditText58, meditText59, meditText60, meditText61, meditText62, meditText63, lampeditText1, lampeditText2, lampeditText3, cur1edit, cur2edit, cur3edit, cur4edit, cur5edit, cur6edit, cur7edit, cur8edit, cur9edit, cur10edit, cur11edit, cur12edit, custom1edit, custom2edit, custom3edit, lampeditpic1, lampeditpic2, lampeditpic3, lampeditpic4, lampeditpic5, lampeditpic6, lampeditpic7, lampeditpic8, lampeditpic9, lampeditpic10, lampeditpic11, lampeditpic12, lampeditText4, lampeditText5, lampeditText6, lampeditText7, lampeditText8, lampeditText9, lampeditText10, lampeditText11, lampeditText12, marcosetname, marcoset1name, marcoset2name, marcoset3name, marcoset4name, marcoset5name, marcoset6name, marco1EditText, marcosetbtn, marco1setbtn, marco2setbtn, marco3setbtn, marco4setbtn, marco5setbtn, marco6setbtn, f1edit, f2edit, f3edit, f4edit, f5edit, f6edit, ip, port, ip2, port2, opencam, ip_gcm, port_gcm;//YILINEDIT

    public TextView cus1name, cus2name, cus3name, cur1name, cur2name, cur3name, cur4name, cur5name, cur6name, cur7name, cur8name, cur9name, cur10name, cur11name, cur12name;

    public ToggleButton toggleButton;

    public ImageView imagebtn1, imagebtn2, imagebtn3, imagebtn4, imagebtn5, imagebtn6, imagebtn7, imagebtn8, imagebtn9, imagebtn10, imagebtn11, imagebtn12;

    public TabWidget tabs;

    public ScrollView scrollView1;


    int count1, count2, count3, count4, count5, count6, count7, count8, count9, count10, count11, count12, count13, count14, count15, count16, count17, count18_, count19, count20, count21, count22, count23, count24, count25, count26, count27, count28, count29, count30, count31, count32, count33, count34, count35, count36, count37, count38, count39, count40, count41, count42, count43, count44, count45, count46, count47, count48, count49, count50, count51, count52, count53, count54, count55, count56, count57, count58, count59, count60, count61, count62, count63, count64, count65, count66, count67, count68, count69, count70, count71, count72, count73, count74, count75, count76, count77, count78, count79, count80, count81, count82, count83, count84, count85, count86, count87, count88, count89, count90, count91, count92, count93, count94, count95, count96, count97, count98, count99, count100, count101, count102, count103, count104, count105, count106, count107, count108, count109, count110, count111, count112, count113, count114, count115, count116, count117, count118, count119, count120, count121, count122, count123, count124, count125, count126, count127, count128, count129, count130, count131, count132, count133, count134, count135, count136, count137, count138, count139, count140, count141, count142, count143, count144, count145, count146, count147, count148, count149, count150, count151, count152 = 0;


    // Member object for the chat services
    //private BluetoothChatService mChatService = null;

    private View view0, view1, view2, view3, view4, view5, view6, view7, view8, view9, view10, view11;//需要滑动的页卡
    //YILINEDIT-頁卡的編號
    private int page_index = 0, page_macro = 1, page_lamp = 2, page_tv = 3, page_air = 4, page_window = 5, page_custom1 = 5, page_custom2 = 6, page_custom3 = 7, page_ip_config = 8, page_cam_config = 9, page_safe_alert = 10, page_about = 11;

    public static ViewPager viewPager;//viewpager
    private List<View> viewList;//把需要滑动的页卡添加到这个list中
    private List<String> titleList;//viewpager的标题
    private String[] lunch = {"連線設定預設-第一組", "連線設定預設-第二組", "連線設定-第一組", "連線設定-第二組", "連線設定-第三組", "連線設定-第四組"};
    private ArrayAdapter<String> lunchList;
    private static final String last_select_ip = "last_select_ip";
    private SharedPreferences lamp;
    private static final String lampdata = "DATA";
    private static final String btn01name = "btn01name";
    private static final String btn02name = "btn02name";
    private static final String btn03name = "btn03name";
    private static final String btn04name = "btn04name";
    private static final String btn05name = "btn05name";
    private static final String btn06name = "btn06name";
    private static final String btn07name = "btn07name";
    private static final String btn08name = "btn08name";
    private static final String btn09name = "btn09name";
    private static final String btn10name = "btn10name";
    private static final String btn11name = "btn11name";
    private static final String btn12name = "btn12name";
    private static final String btn1pic = "btn1pic";
    private static final String btn2pic = "btn2pic";
    private static final String btn3pic = "btn3pic";
    private static final String btn4pic = "btn4pic";
    private static final String btn5pic = "btn5pic";
    private static final String btn6pic = "btn6pic";
    private static final String btn7pic = "btn7pic";
    private static final String btn8pic = "btn8pic";
    private static final String btn9pic = "btn9pic";
    private static final String btn10pic = "btn10pic";
    private static final String btn11pic = "btn11pic";
    private static final String btn12pic = "btn12pic";

    private SharedPreferences curtain;
    private static final String curtaindata = "DATA10";
    private static final String curtain1 = "curtain1";
    private static final String curtain2 = "curtain2";
    private static final String curtain3 = "curtain3";
    private static final String curtain4 = "curtain4";
    private static final String curtain5 = "curtain5";
    private static final String curtain6 = "curtain6";
    private static final String curtain7 = "curtain7";
    private static final String curtain8 = "curtain8";
    private static final String curtain9 = "curtain9";
    private static final String curtain10 = "curtain10";
    private static final String curtain11 = "curtain11";
    private static final String curtain12 = "curtain12";

    private SharedPreferences setting;
    private static final String custom1data = "DATA1";
    private static final String custom1name = "custom1name";
    private static final String btn63name = "btn63name";
    private static final String btn64name = "btn64name";
    private static final String btn65name = "btn65name";
    private static final String btn66name = "btn66name";
    private static final String btn67name = "btn67name";
    private static final String btn68name = "btn68name";

    private SharedPreferences settingb;
    private static final String custom2data = "DATA2";
    private static final String custom2name = "custom2name";
    private static final String btn78name = "btn78name";
    private static final String btn79name = "btn79name";
    private static final String btn80name = "btn80name";
    private static final String btn81name = "btn81name";
    private static final String btn82name = "btn82name";
    private static final String btn83name = "btn83name";


    private SharedPreferences setting2;
    private static final String custom3data = "DATA3";
    private static final String custom3name = "custom3name";
    private static final String btn93name = "btn93name";
    private static final String btn94name = "btn94name";
    private static final String btn95name = "btn95name";
    private static final String btn96name = "btn96name";
    private static final String btn97name = "btn97name";
    private static final String btn98name = "btn98name";

    private SharedPreferences marcoset;
    private static final String marcosetdata = "DATA4";
    private static final String marconame = "marconame";
    private static final String marcobtn = "marcobtn";

    private SharedPreferences marco1set;
    private static final String marcoset1data = "DATA5";
    private static final String marco1name = "marco1name";
    private static final String marco1btn = "marco1btn";

    private SharedPreferences marco2set;
    private static final String marcoset2data = "DATA6";
    private static final String marco2name = "marco2name";
    private static final String marco2btn = "marco2btn";

    private SharedPreferences marco3set;
    private static final String marcoset3data = "DATA7";
    private static final String marco3name = "marco3name";
    private static final String marco3btn = "marco3btn";

    private SharedPreferences marco4set;
    private static final String marcoset4data = "DATA8";
    private static final String marco4name = "marco4name";
    private static final String marco4btn = "marco4btn";

    private SharedPreferences marco5set;
    private static final String marcoset5data = "DATA9";
    private static final String marco5name = "marco5name";
    private static final String marco5btn = "marco5btn";

    private SharedPreferences airseting;
    private static final String airdata = "DATA10";
    private static final String btn17name = "btn17name";
    private static final String btn18name = "btn18name";
    private static final String btn19name = "btn19name";
    private static final String btn20name = "btn20name";
    private static final String btn21name = "btn21name";
    private static final String btn22name = "btn22name";

    private SharedPreferences IPname;
    private static final String Ipdata = "IPDATA";
    private static final String IP = "IP";
    private static final String PORT = "PORT";
    private static final String NAME = "NAME";

    //YILINEDIT-voiceRecg
    private boolean isVoiceRecMode = false;
    private boolean isRecTypeLearning = false;
    private String recAction = "";
    private String[] recResult = new String[5];

    private int portSwitch;
    private final Handler mHandler = new myHandler(this);
    //!@#
    FloatService mService;
    ArrayList<String> arr_temp = new ArrayList<>();
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (D) Log.d("AppFloater", "Connected to service");
            mService = ((FloatService.FloatBinder) service).getService();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            if (D) Log.d("AppFloat", "Disconnected from service");

        }
    };

    private AssetDataBaseHelper assetDataBaseHelper;
    private int alertButton = -1;
    private PagerAdapter apdter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (D) Log.e(TAG, "+++ ON CREATE +++");

        // Set up the window layout
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);

        checkSelfPermission();


        //語音資料庫
        assetDataBaseHelper = new AssetDataBaseHelper(this);

        //YILINEDIT-->可能會閃退
        //Intent intent = new Intent(BluetoothChat.this, FloatService.class);
        //bindService(intent, mConnection, Context.BIND_AUTO_CREATE);


        getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.custom_title);

        if (D) Log.e(TAG, "+++ savedInstanceState +++");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            if (extras.getString("msg") != null)
                if (extras.getString("msg").equals("DIALOG_MENU"))
                    showDialog(DIALOG_MENU);
                else if (extras.getString("msg").contains("w+0{")) {
                    alertButton = Integer.parseInt(extras.getString("msg").substring(4, 6));
                }
            /*if (extras.getString("switch") != null) {
                portSwitch = extras.getString("switch");

            }
                }*/
        }


        //紀錄最後選擇的組態--> 開啟APP則套用//
        portSwitch = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).getString(QuickstartPreferences.SPINNER_WHICH, "0"));

        //portSwitch = String.valueOf(spinner.getSelectedItemPosition());
        //*********************************//

        // Set up the custom title
        //mTitle = (TextView) findViewById(R.id.title_left_text);
        //mTitle.setText(R.string.app_name);
        mTitle = findViewById(R.id.title_right_text);
        pagerInit();
        init();


    }

    private void checkSelfPermission() {
        //判斷是否為第一次使用程式
        String status = PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).getString(QuickstartPreferences.USER_USE_STATUS, "first");
        if (status.equals("first")) {
            //第一次使用
            new AlertDialog.Builder(this)
                    .setTitle(R.string.privace_title)
                    .setMessage(R.string.privace_content)
                    .setPositiveButton(R.string.privace_positive, null)
                    .show();
            PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).edit().putString(QuickstartPreferences.USER_USE_STATUS, "nofirst").apply();
        }

        new com.android.util.Permission(this);
    }

    private void pagerInit() {
        page_macro = 0;
        page_lamp = 1;
        page_tv = 2;
        page_air = 3;
        page_custom1 = 4;
        page_custom2 = 5;
        page_custom3 = 6;
        page_ip_config = 7;
        //page_safe_alert=8;
        page_about = 8;
        viewPager = this.findViewById(R.id.viewPager);
        // getLayoutInflater();
        LayoutInflater lf = LayoutInflater.from(this);
        view1 = lf.inflate(R.layout.first, null);
        view2 = lf.inflate(R.layout.lamp, null);
        view3 = lf.inflate(R.layout.tv1, null);
        view4 = lf.inflate(R.layout.air, null);
        view5 = lf.inflate(R.layout.custom1, null);
        view6 = lf.inflate(R.layout.custom2, null);
        view7 = lf.inflate(R.layout.custom3, null);
        view8 = lf.inflate(R.layout.activity_main, null);
        //view9 = lf.inflate(R.layout.safe_alert, null);
        view10 = lf.inflate(R.layout.about, null);
        viewPager.setOffscreenPageLimit(9);

        viewList = new ArrayList<View>();// 将要分页显示的View装入数组中
        viewList.add(view1);
        viewList.add(view2);
        viewList.add(view3);
        viewList.add(view4);
        viewList.add(view5);
        viewList.add(view6);
        viewList.add(view7);
        viewList.add(view8);
//        viewList.add(view9);
        viewList.add(view10);
        //viewList.add(view11);

         apdter = new PagerAdapter() {

            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                // TODO Auto-generated method stub
                return arg0 == arg1;
            }

            public int getItemPosition(Object object) {
                return POSITION_NONE;
            }

            @Override
            public int getCount() {
                // TODO Auto-generated method stub
                return viewList.size();
            }

            @Override
            public void destroyItem(ViewGroup container, int position,
                                    Object object) {
                // TODO Auto-generated method stub
                container.removeView(viewList.get(position));
            }

            @Override
            public CharSequence getPageTitle(int position) {
                //TODO Auto-generated method stub

                return titleList.get(position);//這裡需回傳Title的名稱,position就是每個Page的index
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                switch (position) {

                    case 1:
                        findViewById(R.id.firstset).setOnClickListener(new ButtonListener());

                        findViewById(R.id.f1).setOnClickListener(new ButtonListener());
                        findViewById(R.id.f2).setOnClickListener(new ButtonListener());
                        findViewById(R.id.f3).setOnClickListener(new ButtonListener());
                        findViewById(R.id.f4).setOnClickListener(new ButtonListener());
                        findViewById(R.id.f5).setOnClickListener(new ButtonListener());
                        findViewById(R.id.f6).setOnClickListener(new ButtonListener());
                        findViewById(R.id.home0).setOnClickListener(new ButtonListener());

                        //YILINEDIT-情境設定
                        findViewById(R.id.f1).setOnLongClickListener(new ButtonLongListner());
                        findViewById(R.id.f2).setOnLongClickListener(new ButtonLongListner());
                        findViewById(R.id.f3).setOnLongClickListener(new ButtonLongListner());
                        findViewById(R.id.f4).setOnLongClickListener(new ButtonLongListner());
                        findViewById(R.id.f5).setOnLongClickListener(new ButtonLongListner());
                        findViewById(R.id.f6).setOnLongClickListener(new ButtonLongListner());

                        marcoset = getSharedPreferences(portSwitch + marcosetdata, Context.MODE_PRIVATE);
                        marco1set = getSharedPreferences(portSwitch + marcoset1data, Context.MODE_PRIVATE);
                        marco2set = getSharedPreferences(portSwitch + marcoset2data, Context.MODE_PRIVATE);
                        marco3set = getSharedPreferences(portSwitch + marcoset3data, Context.MODE_PRIVATE);
                        marco4set = getSharedPreferences(portSwitch + marcoset4data, Context.MODE_PRIVATE);
                        marco5set = getSharedPreferences(portSwitch + marcoset5data, Context.MODE_PRIVATE);

                        ((Button)findViewById(R.id.f1)).setText(marcoset.getString(marconame, ""));
                        ((Button)findViewById(R.id.f2)).setText(marco1set.getString(marco1name, ""));
                        ((Button)findViewById(R.id.f3)).setText(marco2set.getString(marco2name, ""));
                        ((Button)findViewById(R.id.f4)).setText(marco3set.getString(marco3name, ""));
                        ((Button)findViewById(R.id.f5)).setText(marco4set.getString(marco4name, ""));
                        ((Button)findViewById(R.id.f6)).setText(marco5set.getString(marco5name, ""));
                        break;

                    case 2:
                        lamp = getSharedPreferences(portSwitch + lampdata, 0);

                        imagebtn1 = findViewById(R.id.imagebtn1);
                        imagebtn2 = findViewById(R.id.imagebtn2);
                        imagebtn3 = findViewById(R.id.imagebtn3);
                        imagebtn4 = findViewById(R.id.imagebtn4);
                        imagebtn5 = findViewById(R.id.imagebtn5);
                        imagebtn6 = findViewById(R.id.imagebtn6);
                        imagebtn7 = findViewById(R.id.imagebtn7);
                        imagebtn8 = findViewById(R.id.imagebtn8);
                        imagebtn9 = findViewById(R.id.imagebtn9);
                        imagebtn10 = findViewById(R.id.imagebtn10);
                        imagebtn11 = findViewById(R.id.imagebtn11);
                        imagebtn12 = findViewById(R.id.imagebtn12);

                        getLampBtnPic();

                        mButton01 = findViewById(R.id.btn01);
                        mButton02 = findViewById(R.id.btn02);
                        mButton03 = findViewById(R.id.btn03);
                        mButton04 = findViewById(R.id.btn04);
                        mButton05 = findViewById(R.id.btn05);
                        mButton06 = findViewById(R.id.btn06);
                        mButton07 = findViewById(R.id.btn07);
                        mButton08 = findViewById(R.id.btn08);
                        mButton09 = findViewById(R.id.btn09);
                        mButton10 = findViewById(R.id.btn10);
                        mButton11 = findViewById(R.id.btn11);
                        mButton12 = findViewById(R.id.btn12);
                        home1 = findViewById(R.id.home1);


                        findViewById(R.id.lampseting).setOnClickListener(new ButtonListener());
                        mButton01.setOnClickListener(new ButtonListener());
                        mButton02.setOnClickListener(new ButtonListener());
                        mButton03.setOnClickListener(new ButtonListener());
                        mButton04.setOnClickListener(new ButtonListener());
                        mButton05.setOnClickListener(new ButtonListener());
                        mButton06.setOnClickListener(new ButtonListener());
                        mButton07.setOnClickListener(new ButtonListener());
                        mButton08.setOnClickListener(new ButtonListener());
                        mButton09.setOnClickListener(new ButtonListener());
                        mButton10.setOnClickListener(new ButtonListener());
                        mButton11.setOnClickListener(new ButtonListener());
                        mButton12.setOnClickListener(new ButtonListener());
                        home1.setOnClickListener(new ButtonListener());


                        //lamp = getSharedPreferences(lampdata,0);
                        mButton01.setText(lamp.getString(btn01name, ""));
                        mButton02.setText(lamp.getString(btn02name, ""));
                        mButton03.setText(lamp.getString(btn03name, ""));
                        mButton04.setText(lamp.getString(btn04name, ""));
                        mButton05.setText(lamp.getString(btn05name, ""));
                        mButton06.setText(lamp.getString(btn06name, ""));
                        mButton07.setText(lamp.getString(btn07name, ""));
                        mButton08.setText(lamp.getString(btn08name, ""));
                        mButton09.setText(lamp.getString(btn09name, ""));
                        mButton10.setText(lamp.getString(btn10name, ""));
                        mButton11.setText(lamp.getString(btn11name, ""));
                        mButton12.setText(lamp.getString(btn12name, ""));

                        Animation mAnimation = new AlphaAnimation(1, 0);
                        mAnimation.setDuration(200);
                        mAnimation.setInterpolator(new LinearInterpolator());
                        mAnimation.setRepeatCount(Animation.INFINITE);
                        mAnimation.setRepeatMode(Animation.REVERSE);
                        if (alertButton != -1) viewPager.setCurrentItem(page_lamp);
                        switch (alertButton) {
                            case 0:
                                mButton01.setAnimation(mAnimation);
                                break;
                            case 1:
                                mButton02.setAnimation(mAnimation);
                                break;
                            case 2:
                                mButton03.setAnimation(mAnimation);
                                break;
                            case 3:
                                mButton04.setAnimation(mAnimation);
                                break;
                            case 4:
                                mButton05.setAnimation(mAnimation);
                                break;
                            case 5:
                                mButton06.setAnimation(mAnimation);
                                break;
                            case 6:
                                mButton07.setAnimation(mAnimation);
                                break;
                            case 7:
                                mButton08.setAnimation(mAnimation);
                                break;
                            case 8:
                                mButton09.setAnimation(mAnimation);
                                break;
                            case 9:
                                mButton10.setAnimation(mAnimation);
                                break;
                            case 10:
                                mButton11.setAnimation(mAnimation);
                                break;
                            case 11:
                                mButton12.setAnimation(mAnimation);
                                break;
                        }
                        break;

                    case 3:
                        mButton33 = findViewById(R.id.btn33);
                        mButton35 = findViewById(R.id.btn35);
                        mButton42 = findViewById(R.id.btn42);
                        mButton43 = findViewById(R.id.btn43);
                        mButton44 = findViewById(R.id.btn44);
                        mButton45 = findViewById(R.id.btn45);
                        mButton46 = findViewById(R.id.btn46);
                        mButton47 = findViewById(R.id.btn47);
                        mButton48 = findViewById(R.id.btn48);
                        mButton49 = findViewById(R.id.btn49);
                        mButton50 = findViewById(R.id.btn50);
                        mButton51 = findViewById(R.id.btn51);
                        mButton52 = findViewById(R.id.btn52);
                        mButton53 = findViewById(R.id.btn53);
                        mButton153 = findViewById(R.id.btn153);
                        mButton154 = findViewById(R.id.btn154);
                        mButton155 = findViewById(R.id.btn155);
                        mButton156 = findViewById(R.id.btn156);
                        home3 = findViewById(R.id.home3);

                        mButton33.setOnClickListener(new ButtonListener());
                        mButton35.setOnClickListener(new ButtonListener());
                        mButton42.setOnClickListener(new ButtonListener());
                        mButton43.setOnClickListener(new ButtonListener());
                        mButton44.setOnClickListener(new ButtonListener());
                        mButton45.setOnClickListener(new ButtonListener());
                        mButton46.setOnClickListener(new ButtonListener());
                        mButton47.setOnClickListener(new ButtonListener());
                        mButton48.setOnClickListener(new ButtonListener());
                        mButton49.setOnClickListener(new ButtonListener());
                        mButton50.setOnClickListener(new ButtonListener());
                        mButton51.setOnClickListener(new ButtonListener());
                        mButton52.setOnClickListener(new ButtonListener());
                        mButton53.setOnClickListener(new ButtonListener());
                        mButton153.setOnClickListener(new ButtonListener());
                        mButton154.setOnClickListener(new ButtonListener());
                        mButton155.setOnClickListener(new ButtonListener());
                        mButton156.setOnClickListener(new ButtonListener());
                        findViewById(R.id.tv_btn_func).setOnClickListener(new ButtonListener());
                        home3.setOnClickListener(new ButtonListener());

                        break;
                    case 4:
                        mButton13 = findViewById(R.id.btn13);
                        mButton14 = findViewById(R.id.btn14);
                        mButton15 = findViewById(R.id.btn15);
                        mButton16 = findViewById(R.id.btn16);
                        mButton17 = findViewById(R.id.btn17);
                        mButton18 = findViewById(R.id.btn18);
                        mButton19 = findViewById(R.id.btn19);
                        mButton20 = findViewById(R.id.btn20);
                        mButton21 = findViewById(R.id.btn21);
                        mButton22 = findViewById(R.id.btn22);
                        mButton23 = findViewById(R.id.btn23);
                        mButton24 = findViewById(R.id.btn24);

                        home2 = findViewById(R.id.home2);
                        airset = findViewById(R.id.airset);

                        mButton13.setOnClickListener(new ButtonListener());
                        mButton14.setOnClickListener(new ButtonListener());
                        mButton15.setOnClickListener(new ButtonListener());
                        mButton16.setOnClickListener(new ButtonListener());
                        mButton17.setOnClickListener(new ButtonListener());
                        mButton18.setOnClickListener(new ButtonListener());
                        mButton19.setOnClickListener(new ButtonListener());
                        mButton20.setOnClickListener(new ButtonListener());
                        mButton21.setOnClickListener(new ButtonListener());
                        mButton22.setOnClickListener(new ButtonListener());
                        mButton23.setOnClickListener(new ButtonListener());
                        mButton24.setOnClickListener(new ButtonListener());

                        home2.setOnClickListener(new ButtonListener());
                        airset.setOnClickListener(new ButtonListener());

                        airseting = getSharedPreferences(portSwitch + airdata, 0);
                        mButton17.setText(airseting.getString(btn17name, ""));
                        mButton18.setText(airseting.getString(btn18name, ""));
                        mButton19.setText(airseting.getString(btn19name, ""));
                        mButton20.setText(airseting.getString(btn20name, ""));
                        mButton21.setText(airseting.getString(btn21name, ""));
                        mButton22.setText(airseting.getString(btn22name, ""));


                        break;


                    case 5:
                        mButton63 = findViewById(R.id.btn63);
                        mButton64 = findViewById(R.id.btn64);
                        mButton65 = findViewById(R.id.btn65);
                        mButton66 = findViewById(R.id.btn66);
                        mButton67 = findViewById(R.id.btn67);
                        mButton68 = findViewById(R.id.btn68);

                        custom1set = findViewById(R.id.custom1set);
                        home5 = findViewById(R.id.home5);

                        mButton63.setOnClickListener(new ButtonListener());
                        mButton64.setOnClickListener(new ButtonListener());
                        mButton65.setOnClickListener(new ButtonListener());
                        mButton66.setOnClickListener(new ButtonListener());
                        mButton67.setOnClickListener(new ButtonListener());
                        mButton68.setOnClickListener(new ButtonListener());

                        custom1set.setOnClickListener(new ButtonListener());
                        home5.setOnClickListener(new ButtonListener());


                        setting = getSharedPreferences(portSwitch + custom1data, 0);
                        cus1name = findViewById(R.id.cus1name);
                        cus1name.setText(setting.getString(custom1name, ""));

                        mButton63.setText(setting.getString(btn63name, ""));
                        mButton64.setText(setting.getString(btn64name, ""));
                        mButton65.setText(setting.getString(btn65name, ""));
                        mButton66.setText(setting.getString(btn66name, ""));
                        mButton67.setText(setting.getString(btn67name, ""));
                        mButton68.setText(setting.getString(btn68name, ""));

                        break;

                    case 6:

                        mButton78 = findViewById(R.id.btn78);
                        mButton79 = findViewById(R.id.btn79);
                        mButton80 = findViewById(R.id.btn80);
                        mButton81 = findViewById(R.id.btn81);
                        mButton82 = findViewById(R.id.btn82);
                        mButton83 = findViewById(R.id.btn83);

                        custom2set = findViewById(R.id.custom2set);
                        home6 = findViewById(R.id.home6);


                        mButton78.setOnClickListener(new ButtonListener());
                        mButton79.setOnClickListener(new ButtonListener());
                        mButton80.setOnClickListener(new ButtonListener());
                        mButton81.setOnClickListener(new ButtonListener());
                        mButton82.setOnClickListener(new ButtonListener());
                        mButton83.setOnClickListener(new ButtonListener());

                        custom2set.setOnClickListener(new ButtonListener());
                        home6.setOnClickListener(new ButtonListener());

                        settingb = getSharedPreferences(portSwitch + custom2data, 0);
                        cus2name = findViewById(R.id.cus2name);
                        cus2name.setText(settingb.getString(custom2name, ""));

                        mButton78.setText(settingb.getString(btn78name, ""));
                        mButton79.setText(settingb.getString(btn79name, ""));
                        mButton80.setText(settingb.getString(btn80name, ""));
                        mButton81.setText(settingb.getString(btn81name, ""));
                        mButton82.setText(settingb.getString(btn82name, ""));
                        mButton83.setText(settingb.getString(btn83name, ""));

                        break;

                    case 7:
                        mButton94 = findViewById(R.id.btn94);
                        mButton95 = findViewById(R.id.btn95);
                        mButton96 = findViewById(R.id.btn96);
                        mButton97 = findViewById(R.id.btn97);
                        mButton98 = findViewById(R.id.btn98);
                        custom3set = findViewById(R.id.custom3set);
                        home7 = findViewById(R.id.home7);

                        findViewById(R.id.btn93).setOnClickListener(new ButtonListener());
                        mButton94.setOnClickListener(new ButtonListener());
                        mButton95.setOnClickListener(new ButtonListener());
                        mButton96.setOnClickListener(new ButtonListener());
                        mButton97.setOnClickListener(new ButtonListener());
                        mButton98.setOnClickListener(new ButtonListener());
                        custom3set.setOnClickListener(new ButtonListener());
                        home7.setOnClickListener(new ButtonListener());

                        setting2 = getSharedPreferences(portSwitch + custom3data, 0);
                        cus3name = findViewById(R.id.cus3name);
                        cus3name.setText(setting2.getString(custom3name, ""));
                        ((Button)findViewById(R.id.btn93)).setText(setting2.getString(btn93name, ""));
                        mButton94.setText(setting2.getString(btn94name, ""));
                        mButton95.setText(setting2.getString(btn95name, ""));
                        mButton96.setText(setting2.getString(btn96name, ""));
                        mButton97.setText(setting2.getString(btn97name, ""));
                        mButton98.setText(setting2.getString(btn98name, ""));

                        break;


                    case 8:
                        //IP設定頁面
                        findViewById(R.id.save).setOnClickListener(new ButtonListener());
                        findViewById(R.id.open).setOnClickListener(new ButtonListener());
                        findViewById(R.id.btn_IR_Switch).setOnClickListener(new ButtonListener());
                        findViewById(R.id.btn_ap).setOnClickListener(new ButtonListener());
                        spinner = findViewById(R.id.mySpinner);
                        lunchList = new ArrayAdapter<String>(BluetoothChat.this, R.layout.myspinner, lunch);
                        lunchList.setDropDownViewResource(R.layout.myspinner);

                        spinner.setAdapter(lunchList);
                        /*if (portSwitch.length() == 0) {
                            spinner.setSelection(0);
                        } else {
                            spinner.setSelection(Integer.parseInt(portSwitch) - 1);
                        }*/
                        spinner.setSelection(Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).getString(QuickstartPreferences.SPINNER_WHICH, "0")));

                        if (Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).getString(QuickstartPreferences.SPINNER_WHICH, "0")) == 0 | Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).getString(QuickstartPreferences.SPINNER_WHICH, "0")) == 1) {
                            findViewById(R.id.ip).setEnabled(false);
                            findViewById(R.id.port).setEnabled(false);
                        } else {
                            findViewById(R.id.ip).setEnabled(true);
                            findViewById(R.id.port).setEnabled(true);
                        }

                        spinner.setOnItemSelectedListener(null);
                        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                                // TODO Auto-generated method stub
//
                                if (avoidSpinnerListenerCheck) {
                                    //restart(spinner.getSelectedItemPosition());

                                    if (position == 0 | position == 1) {
                                        findViewById(R.id.ip).setEnabled(false);
                                        findViewById(R.id.port).setEnabled(false);
                                    } else {
                                        findViewById(R.id.ip).setEnabled(true);
                                        findViewById(R.id.port).setEnabled(true);
                                    }


                                    PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).edit().putString(QuickstartPreferences.SPINNER_WHICH, String.valueOf(spinner.getSelectedItemPosition())).apply();
                                    readData(13);
                                    init();
                                } else avoidSpinnerListenerCheck = true;
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> arg0) {
                                // TODO Auto-generated method stub

                            }
                        });

                        ip = findViewById(R.id.ip);
                        port = findViewById(R.id.port);

                        //sendContent=(EditText) findViewById(R.id.sendContent);
                        //recContent=(EditText) findViewById(R.id.recContent);

                        IPname = getSharedPreferences(Ipdata, 0);

                        //紅外線功能設定
                        String str_IR_STATUS = PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).getString(QuickstartPreferences.IR_STATUS, "關閉");

                        if (str_IR_STATUS.equals("開啟")) {
                            ((TextView) findViewById(R.id.tv_IR_status)).setText("開啟");
                            ((TextView) findViewById(R.id.tv_IR_status)).setTextColor(Color.GREEN);
                            ((Button) findViewById(R.id.btn_IR_Switch)).setText("關閉");
                        } else if (str_IR_STATUS.equals("關閉")) {
                            ((TextView) findViewById(R.id.tv_IR_status)).setText("關閉");
                            ((TextView) findViewById(R.id.tv_IR_status)).setTextColor(Color.RED);
                            ((Button) findViewById(R.id.btn_IR_Switch)).setText("開啟");
                        }


                        readData(13);
                        //init();

                        break;


                }

                container.addView(viewList.get(position));
                return viewList.get(position);
            }


            private ContentResolver getContentResolver() {
                // TODO Auto-generated method stub
                return null;
            }

             @Override
             public void notifyDataSetChanged() {
                 super.notifyDataSetChanged();
             }

             @Override
             public Parcelable saveState() {
                 return super.saveState();
             }
         };


        viewPager.setAdapter(apdter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {//逶｣閨ｽ逡ｶViewPager陲ｫ謾ｹ隶咳age譎�
            @Override
            public void onPageSelected(int arg0) {
                //YILINEDIT-voiceRecg
                if (!isVoiceRecMode && viewPager.getCurrentItem() == page_lamp)
                    sendmessage("W+j?u");
                if (isVoiceRecMode && (viewPager.getCurrentItem() != page_macro && viewPager.getCurrentItem() != page_lamp)) {
                    viewPager.setCurrentItem(page_lamp);
                    Toast.makeText(BluetoothChat.this, "請先解除語音學習模式", Toast.LENGTH_SHORT).show();
                    //Log.d(TAG,"move");
                }
            }


            @Override
            public void onPageScrolled(int index, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int index) {
            }
        });
    }

    private void getLampBtnPic() {

        //燈控圖片顯示
        lamp = getSharedPreferences(portSwitch + lampdata, 0);
        if (lamp.getString(btn1pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn1)).setImageURI(Uri.parse(lamp.getString(btn1pic, "")));
        }
        if (lamp.getString(btn2pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn2)).setImageURI(Uri.parse(lamp.getString(btn2pic, "")));
        }
        if (lamp.getString(btn3pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn3)).setImageURI(Uri.parse(lamp.getString(btn3pic, "")));
        }
        if (lamp.getString(btn4pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn4)).setImageURI(Uri.parse(lamp.getString(btn4pic, "")));
        }
        if (lamp.getString(btn5pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn5)).setImageURI(Uri.parse(lamp.getString(btn5pic, "")));
        }
        if (lamp.getString(btn6pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn6)).setImageURI(Uri.parse(lamp.getString(btn6pic, "")));
        }
        if (lamp.getString(btn7pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn7)).setImageURI(Uri.parse(lamp.getString(btn7pic, "")));
        }
        if (lamp.getString(btn8pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn8)).setImageURI(Uri.parse(lamp.getString(btn8pic, "")));
        }
        if (lamp.getString(btn9pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn9)).setImageURI(Uri.parse(lamp.getString(btn9pic, "")));
        }
        if (lamp.getString(btn10pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn10)).setImageURI(Uri.parse(lamp.getString(btn10pic, "")));
        }
        if (lamp.getString(btn11pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn11)).setImageURI(Uri.parse(lamp.getString(btn11pic, "")));
        }
        if (lamp.getString(btn12pic, "").length() != 0) {
            ((ImageView) findViewById(R.id.imagebtn12)).setImageURI(Uri.parse(lamp.getString(btn12pic, "")));
        }
    }

    //YILINEDIT-voiceRecg
    private AlertDialog getVoiceAlertDialog(final String title, final String message, boolean islamp) {
        //產生一個Builder物件
        Builder builder = new AlertDialog.Builder(this);
        //設定Dialog的標題
        builder.setTitle("請選擇" + title + "的語音動作");
        //設定Dialog的內容
        // builder.setMessage("MS");

        if (islamp) {
            builder.setItems(new String[]{"開", "關"}, (dialog, which) -> {
                //當使用者點選對話框時，顯示使用者所點選的項目
                if (D) Log.d(TAG, "which" + which);
                switch (which) {
                    case 0:
                        recAction = message + "4";
                        break;
                    case 1:
                        recAction = message + "5";
                        break;
                }
                if (D) Log.d(TAG, "recAction " + recAction);
                voiceRec("請說出 " + title + (which == 0 ? "開" : "關") + "的動作(確認網路良好)");
            });
            //設定Negative按鈕資料
            builder.setNegativeButton("Cancel", (dialog, which) -> {
                //按下按鈕時顯示快顯
                //Toast.makeText(this, "您按下Cancel按鈕", Toast.LENGTH_SHORT).show();
            });
            //利用Builder物件建立AlertDialog
            return builder.create();
        } else {
//            builder.setItems(new String[] { "開始錄音" },new DialogInterface.OnClickListener(){
//                @Override
//                public void onClick(DialogInterface dialog,int which){
//                    //當使用者點選對話框時，顯示使用者所點選的項目
//                    Log.d(TAG,"which"+which);
//                    voiceRec(title);
//                }
//            });
            recAction = message;
            if (D) Log.d(TAG, "recAction " + recAction);
            voiceRec("請說出 " + title + " 的動作(確認網路良好)");
        }
        return null;


    }

    private void floatApp(int x, int y, int id) {
        mService.floatApp(x, y, id);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.about_btn_privacy:
                new AlertDialog.Builder(this)
                        .setTitle(R.string.privace_title)
                        .setMessage(R.string.privace_content)
                        .setPositiveButton(R.string.privace_positive, null)
                        .show();
                break;
        }
    }

    //YILINEDIT-longclick
    public class ButtonLongListner implements View.OnLongClickListener {

        @Override
        public boolean onLongClick(View v) {
            // TODO Auto-generated method stub
            if (D) Log.e(TAG, "" + v.getId());
            switch (v.getId()) {
                case R.id.f1:
                    // TODO Auto-generated catch block
                    setContentView(R.layout.marcoset);

                    marcosetname = findViewById(R.id.marcosetname);
                    marcosetbtn = findViewById(R.id.marcosetbtn);
                    mButton01 = findViewById(R.id.btn01);
                    mButton02 = findViewById(R.id.btn02);
                    mButton03 = findViewById(R.id.btn03);
                    mButton04 = findViewById(R.id.btn04);
                    mButton05 = findViewById(R.id.btn05);
                    mButton06 = findViewById(R.id.btn06);
                    mButton07 = findViewById(R.id.btn07);
                    mButton08 = findViewById(R.id.btn08);
                    mButton09 = findViewById(R.id.btn09);
                    mButton10 = findViewById(R.id.btn10);
                    mButton11 = findViewById(R.id.btn11);
                    mButton12 = findViewById(R.id.btn12);
                    mButton13 = findViewById(R.id.btn13);
                    mButton14 = findViewById(R.id.btn14);
                    mButton15 = findViewById(R.id.btn15);
                    mButton16 = findViewById(R.id.btn16);
                    mButton17 = findViewById(R.id.btn17);
                    mButton18 = findViewById(R.id.btn18);
                    mButton19 = findViewById(R.id.btn19);
                    mButton20 = findViewById(R.id.btn20);
                    mButton21 = findViewById(R.id.btn21);
                    mButton22 = findViewById(R.id.btn22);
                    mButton23 = findViewById(R.id.btn23);
                    mButton24 = findViewById(R.id.btn24);
//	    			mButton25 = (Button) findViewById(R.id.btn25);
//	    			mButton26 = (Button) findViewById(R.id.btn26);
//	    			mButton27 = (Button) findViewById(R.id.btn27);
//	    			mButton28 = (Button) findViewById(R.id.btn28);
//	    			mButton29 = (Button) findViewById(R.id.btn29);
//	    			mButton30 = (Button) findViewById(R.id.btn30);
//	    			mButton31 = (Button) findViewById(R.id.btn31);
//	    			mButton32 = (Button) findViewById(R.id.btn32);
                    mButton33 = findViewById(R.id.btn33);
//	    			mButton34 = (Button) findViewById(R.id.btn34);
                    mButton35 = findViewById(R.id.btn35);
//	    			mButton36 = (Button) findViewById(R.id.btn36);
//	    			mButton37 = (Button) findViewById(R.id.btn37);
//	    			mButton38 = (Button) findViewById(R.id.btn38);
//	    			mButton39 = (Button) findViewById(R.id.btn39);
//	    			mButton40 = (Button) findViewById(R.id.btn40);
//	    			mButton41 = (Button) findViewById(R.id.btn41);
                    mButton42 = findViewById(R.id.btn42);
                    mButton43 = findViewById(R.id.btn43);
                    mButton44 = findViewById(R.id.btn44);
                    mButton45 = findViewById(R.id.btn45);
                    mButton46 = findViewById(R.id.btn46);
                    mButton47 = findViewById(R.id.btn47);
                    mButton48 = findViewById(R.id.btn48);
                    mButton49 = findViewById(R.id.btn49);
                    mButton50 = findViewById(R.id.btn50);
                    mButton51 = findViewById(R.id.btn51);
                    mButton52 = findViewById(R.id.btn52);
                    mButton53 = findViewById(R.id.btn53);
//	    			mButton54 = (Button) findViewById(R.id.btn54);
//	    			mButton55 = (Button) findViewById(R.id.btn55);
//	    			mButton56 = (Button) findViewById(R.id.btn56);
//	    			mButton57 = (Button) findViewById(R.id.btn57);
//	    			mButton58 = (Button) findViewById(R.id.btn58);
//	    			mButton59 = (Button) findViewById(R.id.btn59);
//	    			mButton60 = (Button) findViewById(R.id.btn60);
//	    			mButton61 = (Button) findViewById(R.id.btn61);
//	    			mButton62 = (Button) findViewById(R.id.btn62);
                    mButton63 = findViewById(R.id.btn63);
                    mButton64 = findViewById(R.id.btn64);
                    mButton65 = findViewById(R.id.btn65);
                    mButton66 = findViewById(R.id.btn66);
                    mButton67 = findViewById(R.id.btn67);
                    mButton68 = findViewById(R.id.btn68);
//	    			mButton69 = (Button) findViewById(R.id.btn69);
//	    			mButton70 = (Button) findViewById(R.id.btn70);
//	    			mButton71 = (Button) findViewById(R.id.btn71);
//	    			mButton72 = (Button) findViewById(R.id.btn72);
//	    			mButton73 = (Button) findViewById(R.id.btn73);
//	    			mButton74 = (Button) findViewById(R.id.btn74);
//	    			mButton75 = (Button) findViewById(R.id.btn75);
//	    			mButton76 = (Button) findViewById(R.id.btn76);
//	    			mButton77 = (Button) findViewById(R.id.btn77);
                    mButton78 = findViewById(R.id.btn78);
                    mButton79 = findViewById(R.id.btn79);
                    mButton80 = findViewById(R.id.btn80);
                    mButton81 = findViewById(R.id.btn81);
                    mButton82 = findViewById(R.id.btn82);
                    mButton83 = findViewById(R.id.btn83);
                    //mButton84 = (Button) findViewById(R.id.btn84);
                    //mButton85 = (Button) findViewById(R.id.btn85);
                    //mButton86 = (Button) findViewById(R.id.btn86);
                    //mButton87 = (Button) findViewById(R.id.btn87);
                    //mButton88 = (Button) findViewById(R.id.btn88);
                    //mButton89 = (Button) findViewById(R.id.btn89);
                    //mButton90 = (Button) findViewById(R.id.btn90);
                    //mButton91 = (Button) findViewById(R.id.btn91);
                    //mButton92 = (Button) findViewById(R.id.btn92);
                    mButton93 = findViewById(R.id.btn93);
                    mButton94 = findViewById(R.id.btn94);
                    mButton95 = findViewById(R.id.btn95);
                    mButton96 = findViewById(R.id.btn96);
                    mButton97 = findViewById(R.id.btn97);
                    mButton98 = findViewById(R.id.btn98);
//	    			mButton99 = (Button) findViewById(R.id.btn99);
//	    			mButton100 = (Button) findViewById(R.id.btn100);
//	    			mButton101 = (Button) findViewById(R.id.btn101);
//	    			mButton102 = (Button) findViewById(R.id.btn102);
//	    			mButton103 = (Button) findViewById(R.id.btn103);
//	    			mButton104 = (Button) findViewById(R.id.btn104);
//	    			mButton105 = (Button) findViewById(R.id.btn105);
//	    			mButton106 = (Button) findViewById(R.id.btn106);
//	    			mButton107 = (Button) findViewById(R.id.btn107);
//	    			mButton108 = (Button) findViewById(R.id.btn108);
//	    			mButton109 = (Button) findViewById(R.id.btn109);
//	    			mButton110 = (Button) findViewById(R.id.btn110);
//	    			mButton111 = (Button) findViewById(R.id.btn111);
//	    			mButton112 = (Button) findViewById(R.id.btn112);
                    mButton153 = findViewById(R.id.btn153);
                    mButton154 = findViewById(R.id.btn154);
                    mButton155 = findViewById(R.id.btn155);
                    mButton156 = findViewById(R.id.btn156);


                    marcosetok = findViewById(R.id.marcosetok);
                    s1 = findViewById(R.id.s1);
                    s2 = findViewById(R.id.s2);
                    s3 = findViewById(R.id.s3);
                    s4 = findViewById(R.id.s4);
                    s5 = findViewById(R.id.s5);

                    //row1 = (Button) findViewById(R.id.row1);
                    //row2 = (Button) findViewById(R.id.row2);
                    //row3 = (Button) findViewById(R.id.row3);
                    //row4 = (Button) findViewById(R.id.row4);
                    //row5 = (Button) findViewById(R.id.row5);
                    //row6 = (Button) findViewById(R.id.row6);
                    //row7 = (Button) findViewById(R.id.row7);

//                    s1.setOnClickListener(new ButtonListener());
//                    s2.setOnClickListener(new ButtonListener());
//                    s3.setOnClickListener(new ButtonListener());
//                    s4.setOnClickListener(new ButtonListener());
//                    s5.setOnClickListener(new ButtonListener());

                    //row1.setOnClickListener(new ButtonListener());
                    //row2.setOnClickListener(new ButtonListener());
                    //row3.setOnClickListener(new ButtonListener());
                    //row4.setOnClickListener(new ButtonListener());
                    //row5.setOnClickListener(new ButtonListener());
                    //row6.setOnClickListener(new ButtonListener());
                    //row7.setOnClickListener(new ButtonListener());


//                    marcosetok.setOnClickListener(new ButtonListener());

                    lamp = getSharedPreferences(portSwitch + lampdata, 0);
                    mButton01.setText(lamp.getString(btn01name, ""));
                    mButton02.setText(lamp.getString(btn02name, ""));
                    mButton03.setText(lamp.getString(btn03name, ""));
                    mButton04.setText(lamp.getString(btn04name, ""));
                    mButton05.setText(lamp.getString(btn05name, ""));
                    mButton06.setText(lamp.getString(btn06name, ""));
                    mButton07.setText(lamp.getString(btn07name, ""));
                    mButton08.setText(lamp.getString(btn08name, ""));
                    mButton09.setText(lamp.getString(btn09name, ""));
                    mButton10.setText(lamp.getString(btn10name, ""));
                    mButton11.setText(lamp.getString(btn11name, ""));
                    mButton12.setText(lamp.getString(btn12name, ""));

                    setting = getSharedPreferences(portSwitch + custom1data, 0);

                    cus1name = findViewById(R.id.cus1name);
                    cus1name.setText(setting.getString(custom1name, ""));
//	    			mButton54.setText(setting.getString(btn54name, ""));
//	    			mButton55.setText(setting.getString(btn55name, ""));
//	    			mButton56.setText(setting.getString(btn56name, ""));
//	    			mButton57.setText(setting.getString(btn57name, ""));
//	    			mButton58.setText(setting.getString(btn58name, ""));
//	    			mButton59.setText(setting.getString(btn59name, ""));
//	    			mButton60.setText(setting.getString(btn60name, ""));
//	    			mButton61.setText(setting.getString(btn61name, ""));
//	    			mButton62.setText(setting.getString(btn62name, ""));
                    mButton63.setText(setting.getString(btn63name, ""));
                    mButton64.setText(setting.getString(btn64name, ""));
                    mButton65.setText(setting.getString(btn65name, ""));
                    mButton66.setText(setting.getString(btn66name, ""));
                    mButton67.setText(setting.getString(btn67name, ""));
                    mButton68.setText(setting.getString(btn68name, ""));


                    settingb = getSharedPreferences(portSwitch + custom2data, 0);

                    cus2name = findViewById(R.id.cus2name);
                    cus2name.setText(settingb.getString(custom2name, ""));
//	    			 mButton69.setText(settingb.getString(btn69name, ""));
//	    			 mButton70.setText(settingb.getString(btn70name, ""));
//	    			 mButton71.setText(settingb.getString(btn71name, ""));
//	    			 mButton72.setText(settingb.getString(btn72name, ""));
//	    			 mButton73.setText(settingb.getString(btn73name, ""));
//	    			 mButton74.setText(settingb.getString(btn74name, ""));
//	    			 mButton75.setText(settingb.getString(btn75name, ""));
//	    			 mButton76.setText(settingb.getString(btn76name, ""));
//	    			 mButton77.setText(settingb.getString(btn77name, ""));
                    mButton78.setText(settingb.getString(btn78name, ""));
                    mButton79.setText(settingb.getString(btn79name, ""));
                    mButton80.setText(settingb.getString(btn80name, ""));
                    mButton81.setText(settingb.getString(btn81name, ""));
                    mButton82.setText(settingb.getString(btn82name, ""));
                    mButton83.setText(settingb.getString(btn83name, ""));
//	    			 mButton105.setText(settingb.getString(btn105name, ""));
//	    			 mButton106.setText(settingb.getString(btn106name, ""));
//	    			 mButton107.setText(settingb.getString(btn107name, ""));
//	    			 mButton108.setText(settingb.getString(btn108name, ""));
//	    			 mButton109.setText(settingb.getString(btn109name, ""));
//	    			 mButton110.setText(settingb.getString(btn110name, ""));

                    setting2 = getSharedPreferences(portSwitch + custom3data, 0);

                    cus3name = findViewById(R.id.cus3name);
                    cus3name.setText(setting2.getString(custom3name, ""));
                    // mButton84.setText(setting2.getString(btn84name, ""));
                    //mButton85.setText(setting2.getString(btn85name, ""));
                    //mButton86.setText(setting2.getString(btn86name, ""));
                    //mButton87.setText(setting2.getString(btn87name, ""));
                    //mButton88.setText(setting2.getString(btn88name, ""));
                    //mButton89.setText(setting2.getString(btn89name, ""));
                    //mButton90.setText(setting2.getString(btn90name, ""));
                    //mButton91.setText(setting2.getString(btn91name, ""));
                    //mButton92.setText(setting2.getString(btn92name, ""));
                    mButton93.setText(setting2.getString(btn93name, ""));
                    mButton94.setText(setting2.getString(btn94name, ""));
                    mButton95.setText(setting2.getString(btn95name, ""));
                    mButton96.setText(setting2.getString(btn96name, ""));
                    mButton97.setText(setting2.getString(btn97name, ""));
                    mButton98.setText(setting2.getString(btn98name, ""));
                    //mButton111.setText(setting2.getString(btn111name, ""));
                    //mButton112.setText(setting2.getString(btn112name, ""));
                    //mButton113.setText(setting2.getString(btn113name, ""));
                    //mButton114.setText(setting2.getString(btn114name, ""));
                    //mButton115.setText(setting2.getString(btn115name, ""));
                    //mButton116.setText(setting2.getString(btn116name, ""));

                    airseting = getSharedPreferences(portSwitch + airdata, 0);
                    mButton17.setText(airseting.getString(btn17name, ""));
                    mButton18.setText(airseting.getString(btn18name, ""));
                    mButton19.setText(airseting.getString(btn19name, ""));
                    mButton20.setText(airseting.getString(btn20name, ""));
                    mButton21.setText(airseting.getString(btn21name, ""));
                    mButton22.setText(airseting.getString(btn22name, ""));


                    getLampBtnPic();
                    readData(5);

                    break;

                //marco2
                case R.id.f2:
                    // TODO Auto-generated catch block
                    setContentView(R.layout.marcoset1);

                    marcoset1name = findViewById(R.id.marcoset1name);
                    marco1setbtn = findViewById(R.id.marco1setbtn);
                    mButton01 = findViewById(R.id.btn01);
                    mButton02 = findViewById(R.id.btn02);
                    mButton03 = findViewById(R.id.btn03);
                    mButton04 = findViewById(R.id.btn04);
                    mButton05 = findViewById(R.id.btn05);
                    mButton06 = findViewById(R.id.btn06);
                    mButton07 = findViewById(R.id.btn07);
                    mButton08 = findViewById(R.id.btn08);
                    mButton09 = findViewById(R.id.btn09);
                    mButton10 = findViewById(R.id.btn10);
                    mButton11 = findViewById(R.id.btn11);
                    mButton12 = findViewById(R.id.btn12);
                    mButton13 = findViewById(R.id.btn13);
                    mButton14 = findViewById(R.id.btn14);
                    mButton15 = findViewById(R.id.btn15);
                    mButton16 = findViewById(R.id.btn16);
                    mButton17 = findViewById(R.id.btn17);
                    mButton18 = findViewById(R.id.btn18);
                    mButton19 = findViewById(R.id.btn19);
                    mButton20 = findViewById(R.id.btn20);
                    mButton21 = findViewById(R.id.btn21);
                    mButton22 = findViewById(R.id.btn22);
                    mButton23 = findViewById(R.id.btn23);
                    mButton24 = findViewById(R.id.btn24);
//				mButton25 = (Button) findViewById(R.id.btn25);
//				mButton26 = (Button) findViewById(R.id.btn26);
//				mButton27 = (Button) findViewById(R.id.btn27);
//				mButton28 = (Button) findViewById(R.id.btn28);
//				mButton29 = (Button) findViewById(R.id.btn29);
//				mButton30 = (Button) findViewById(R.id.btn30);
//				mButton31 = (Button) findViewById(R.id.btn31);
//				mButton32 = (Button) findViewById(R.id.btn32);
                    mButton33 = findViewById(R.id.btn33);
//				mButton34 = (Button) findViewById(R.id.btn34);
                    mButton35 = findViewById(R.id.btn35);
//				mButton36 = (Button) findViewById(R.id.btn36);
//				mButton37 = (Button) findViewById(R.id.btn37);
//				mButton38 = (Button) findViewById(R.id.btn38);
//				mButton39 = (Button) findViewById(R.id.btn39);
//				mButton40 = (Button) findViewById(R.id.btn40);
//				mButton41 = (Button) findViewById(R.id.btn41);
                    mButton42 = findViewById(R.id.btn42);
                    mButton43 = findViewById(R.id.btn43);
                    mButton44 = findViewById(R.id.btn44);
                    mButton45 = findViewById(R.id.btn45);
                    mButton46 = findViewById(R.id.btn46);
                    mButton47 = findViewById(R.id.btn47);
                    mButton48 = findViewById(R.id.btn48);
                    mButton49 = findViewById(R.id.btn49);
                    mButton50 = findViewById(R.id.btn50);
                    mButton51 = findViewById(R.id.btn51);
                    mButton52 = findViewById(R.id.btn52);
                    mButton53 = findViewById(R.id.btn53);
//				mButton54 = (Button) findViewById(R.id.btn54);
//				mButton55 = (Button) findViewById(R.id.btn55);
//				mButton56 = (Button) findViewById(R.id.btn56);
//				mButton57 = (Button) findViewById(R.id.btn57);
//				mButton58 = (Button) findViewById(R.id.btn58);
//				mButton59 = (Button) findViewById(R.id.btn59);
//				mButton60 = (Button) findViewById(R.id.btn60);
//				mButton61 = (Button) findViewById(R.id.btn61);
//				mButton62 = (Button) findViewById(R.id.btn62);
                    mButton63 = findViewById(R.id.btn63);
                    mButton64 = findViewById(R.id.btn64);
                    mButton65 = findViewById(R.id.btn65);
                    mButton66 = findViewById(R.id.btn66);
                    mButton67 = findViewById(R.id.btn67);
                    mButton68 = findViewById(R.id.btn68);
//				mButton69 = (Button) findViewById(R.id.btn69);
//				mButton70 = (Button) findViewById(R.id.btn70);
//				mButton71 = (Button) findViewById(R.id.btn71);
//				mButton72 = (Button) findViewById(R.id.btn72);
//				mButton73 = (Button) findViewById(R.id.btn73);
//				mButton74 = (Button) findViewById(R.id.btn74);
//				mButton75 = (Button) findViewById(R.id.btn75);
//				mButton76 = (Button) findViewById(R.id.btn76);
//				mButton77 = (Button) findViewById(R.id.btn77);
                    mButton78 = findViewById(R.id.btn78);
                    mButton79 = findViewById(R.id.btn79);
                    mButton80 = findViewById(R.id.btn80);
                    mButton81 = findViewById(R.id.btn81);
                    mButton82 = findViewById(R.id.btn82);
                    mButton83 = findViewById(R.id.btn83);
                    //mButton84 = (Button) findViewById(R.id.btn84);
                    //mButton85 = (Button) findViewById(R.id.btn85);
                    //mButton86 = (Button) findViewById(R.id.btn86);
                    //mButton87 = (Button) findViewById(R.id.btn87);
                    //mButton88 = (Button) findViewById(R.id.btn88);
                    //mButton89 = (Button) findViewById(R.id.btn89);
                    //mButton90 = (Button) findViewById(R.id.btn90);
                    //mButton91 = (Button) findViewById(R.id.btn91);
                    //mButton92 = (Button) findViewById(R.id.btn92);
                    mButton93 = findViewById(R.id.btn93);
                    mButton94 = findViewById(R.id.btn94);
                    mButton95 = findViewById(R.id.btn95);
                    mButton96 = findViewById(R.id.btn96);
                    mButton97 = findViewById(R.id.btn97);
                    mButton98 = findViewById(R.id.btn98);
//				mButton99 = (Button) findViewById(R.id.btn99);
//				mButton100 = (Button) findViewById(R.id.btn100);
//				mButton101 = (Button) findViewById(R.id.btn101);
//				mButton102 = (Button) findViewById(R.id.btn102);
//				mButton103 = (Button) findViewById(R.id.btn103);
//				mButton104 = (Button) findViewById(R.id.btn104);
//				mButton105 = (Button) findViewById(R.id.btn105);
//				mButton106 = (Button) findViewById(R.id.btn106);
//				mButton107 = (Button) findViewById(R.id.btn107);
//				mButton108 = (Button) findViewById(R.id.btn108);
//				mButton109 = (Button) findViewById(R.id.btn109);
//				mButton110 = (Button) findViewById(R.id.btn110);
//				mButton111 = (Button) findViewById(R.id.btn111);
//				mButton112 = (Button) findViewById(R.id.btn112);
                    mButton153 = findViewById(R.id.btn153);
                    mButton154 = findViewById(R.id.btn154);
                    mButton155 = findViewById(R.id.btn155);
                    mButton156 = findViewById(R.id.btn156);


                    s6 = findViewById(R.id.s6);
                    s7 = findViewById(R.id.s7);
                    s8 = findViewById(R.id.s8);
                    s9 = findViewById(R.id.s9);
                    s10 = findViewById(R.id.s10);

                    s6.setOnClickListener(new ButtonListener());
                    s7.setOnClickListener(new ButtonListener());
                    s8.setOnClickListener(new ButtonListener());
                    s9.setOnClickListener(new ButtonListener());
                    s10.setOnClickListener(new ButtonListener());


                    findViewById(R.id.marcoset1ok).setOnClickListener(new ButtonListener());

                    lamp = getSharedPreferences(portSwitch + lampdata, 0);
                    mButton01.setText(lamp.getString(btn01name, ""));
                    mButton02.setText(lamp.getString(btn02name, ""));
                    mButton03.setText(lamp.getString(btn03name, ""));
                    mButton04.setText(lamp.getString(btn04name, ""));
                    mButton05.setText(lamp.getString(btn05name, ""));
                    mButton06.setText(lamp.getString(btn06name, ""));
                    mButton07.setText(lamp.getString(btn07name, ""));
                    mButton08.setText(lamp.getString(btn08name, ""));
                    mButton09.setText(lamp.getString(btn09name, ""));
                    mButton10.setText(lamp.getString(btn10name, ""));
                    mButton11.setText(lamp.getString(btn11name, ""));
                    mButton12.setText(lamp.getString(btn12name, ""));

                    setting = getSharedPreferences(portSwitch + custom1data, 0);

                    cus1name = findViewById(R.id.cus1name);
                    cus1name.setText(setting.getString(custom1name, ""));
//				mButton54.setText(setting.getString(btn54name, ""));
//				mButton55.setText(setting.getString(btn55name, ""));
//				mButton56.setText(setting.getString(btn56name, ""));
//				mButton57.setText(setting.getString(btn57name, ""));
//				mButton58.setText(setting.getString(btn58name, ""));
//				mButton59.setText(setting.getString(btn59name, ""));
//				mButton60.setText(setting.getString(btn60name, ""));
//				mButton61.setText(setting.getString(btn61name, ""));
//				mButton62.setText(setting.getString(btn62name, ""));
                    mButton63.setText(setting.getString(btn63name, ""));
                    mButton64.setText(setting.getString(btn64name, ""));
                    mButton65.setText(setting.getString(btn65name, ""));
                    mButton66.setText(setting.getString(btn66name, ""));
                    mButton67.setText(setting.getString(btn67name, ""));
                    mButton68.setText(setting.getString(btn68name, ""));
//				mButton99.setText(setting.getString(btn99name, ""));
//				mButton100.setText(setting.getString(btn100name, ""));
//				mButton101.setText(setting.getString(btn101name, ""));
//				mButton102.setText(setting.getString(btn102name, ""));
//				mButton103.setText(setting.getString(btn103name, ""));
//				mButton104.setText(setting.getString(btn104name, ""));

                    settingb = getSharedPreferences(portSwitch + custom2data, 0);

                    cus2name = findViewById(R.id.cus2name);
                    cus2name.setText(settingb.getString(custom2name, ""));
//				 mButton69.setText(settingb.getString(btn69name, ""));
//				 mButton70.setText(settingb.getString(btn70name, ""));
//				 mButton71.setText(settingb.getString(btn71name, ""));
//				 mButton72.setText(settingb.getString(btn72name, ""));
//				 mButton73.setText(settingb.getString(btn73name, ""));
//				 mButton74.setText(settingb.getString(btn74name, ""));
//				 mButton75.setText(settingb.getString(btn75name, ""));
//				 mButton76.setText(settingb.getString(btn76name, ""));
//				 mButton77.setText(settingb.getString(btn77name, ""));
                    mButton78.setText(settingb.getString(btn78name, ""));
                    mButton79.setText(settingb.getString(btn79name, ""));
                    mButton80.setText(settingb.getString(btn80name, ""));
                    mButton81.setText(settingb.getString(btn81name, ""));
                    mButton82.setText(settingb.getString(btn82name, ""));
                    mButton83.setText(settingb.getString(btn83name, ""));
//				 mButton105.setText(settingb.getString(btn105name, ""));
//				 mButton106.setText(settingb.getString(btn106name, ""));
//				 mButton107.setText(settingb.getString(btn107name, ""));
//				 mButton108.setText(settingb.getString(btn108name, ""));
//				 mButton109.setText(settingb.getString(btn109name, ""));
//				 mButton110.setText(settingb.getString(btn110name, ""));

                    setting2 = getSharedPreferences(portSwitch + custom3data, 0);

                    cus3name = findViewById(R.id.cus3name);
                    cus3name.setText(setting2.getString(custom3name, ""));
                    // mButton84.setText(setting2.getString(btn84name, ""));
                    //mButton85.setText(setting2.getString(btn85name, ""));
                    //mButton86.setText(setting2.getString(btn86name, ""));
                    //mButton87.setText(setting2.getString(btn87name, ""));
                    //mButton88.setText(setting2.getString(btn88name, ""));
                    //mButton89.setText(setting2.getString(btn89name, ""));
                    //mButton90.setText(setting2.getString(btn90name, ""));
                    //mButton91.setText(setting2.getString(btn91name, ""));
                    //mButton92.setText(setting2.getString(btn92name, ""));
                    mButton93.setText(setting2.getString(btn93name, ""));
                    mButton94.setText(setting2.getString(btn94name, ""));
                    mButton95.setText(setting2.getString(btn95name, ""));
                    mButton96.setText(setting2.getString(btn96name, ""));
                    mButton97.setText(setting2.getString(btn97name, ""));
                    mButton98.setText(setting2.getString(btn98name, ""));
                    //mButton111.setText(setting2.getString(btn111name, ""));
                    //mButton112.setText(setting2.getString(btn112name, ""));
                    //mButton113.setText(setting2.getString(btn113name, ""));
                    //mButton114.setText(setting2.getString(btn114name, ""));
                    //mButton115.setText(setting2.getString(btn115name, ""));
                    //mButton116.setText(setting2.getString(btn116name, ""));

                    airseting = getSharedPreferences(portSwitch + airdata, 0);
                    mButton17.setText(airseting.getString(btn17name, ""));
                    mButton18.setText(airseting.getString(btn18name, ""));
                    mButton19.setText(airseting.getString(btn19name, ""));
                    mButton20.setText(airseting.getString(btn20name, ""));
                    mButton21.setText(airseting.getString(btn21name, ""));
                    mButton22.setText(airseting.getString(btn22name, ""));

                    getLampBtnPic();

                    readData(6);
                    break;

                //marco3
                case R.id.f3:
                    // TODO Auto-generated catch block
                    setContentView(R.layout.marcoset2);

                    marcoset2name = findViewById(R.id.marcoset2name);
                    marco2setbtn = findViewById(R.id.marco2setbtn);
                    mButton01 = findViewById(R.id.btn01);
                    mButton02 = findViewById(R.id.btn02);
                    mButton03 = findViewById(R.id.btn03);
                    mButton04 = findViewById(R.id.btn04);
                    mButton05 = findViewById(R.id.btn05);
                    mButton06 = findViewById(R.id.btn06);
                    mButton07 = findViewById(R.id.btn07);
                    mButton08 = findViewById(R.id.btn08);
                    mButton09 = findViewById(R.id.btn09);
                    mButton10 = findViewById(R.id.btn10);
                    mButton11 = findViewById(R.id.btn11);
                    mButton12 = findViewById(R.id.btn12);
                    mButton13 = findViewById(R.id.btn13);
                    mButton14 = findViewById(R.id.btn14);
                    mButton15 = findViewById(R.id.btn15);
                    mButton16 = findViewById(R.id.btn16);
                    mButton17 = findViewById(R.id.btn17);
                    mButton18 = findViewById(R.id.btn18);
                    mButton19 = findViewById(R.id.btn19);
                    mButton20 = findViewById(R.id.btn20);
                    mButton21 = findViewById(R.id.btn21);
                    mButton22 = findViewById(R.id.btn22);
                    mButton23 = findViewById(R.id.btn23);
                    mButton24 = findViewById(R.id.btn24);
//				mButton25 = (Button) findViewById(R.id.btn25);
//				mButton26 = (Button) findViewById(R.id.btn26);
//				mButton27 = (Button) findViewById(R.id.btn27);
//				mButton28 = (Button) findViewById(R.id.btn28);
//				mButton29 = (Button) findViewById(R.id.btn29);
//				mButton30 = (Button) findViewById(R.id.btn30);
//				mButton31 = (Button) findViewById(R.id.btn31);
//				mButton32 = (Button) findViewById(R.id.btn32);
                    mButton33 = findViewById(R.id.btn33);
//				mButton34 = (Button) findViewById(R.id.btn34);
                    mButton35 = findViewById(R.id.btn35);
//				mButton36 = (Button) findViewById(R.id.btn36);
//				mButton37 = (Button) findViewById(R.id.btn37);
//				mButton38 = (Button) findViewById(R.id.btn38);
//				mButton39 = (Button) findViewById(R.id.btn39);
//				mButton40 = (Button) findViewById(R.id.btn40);
//				mButton41 = (Button) findViewById(R.id.btn41);
                    mButton42 = findViewById(R.id.btn42);
                    mButton43 = findViewById(R.id.btn43);
                    mButton44 = findViewById(R.id.btn44);
                    mButton45 = findViewById(R.id.btn45);
                    mButton46 = findViewById(R.id.btn46);
                    mButton47 = findViewById(R.id.btn47);
                    mButton48 = findViewById(R.id.btn48);
                    mButton49 = findViewById(R.id.btn49);
                    mButton50 = findViewById(R.id.btn50);
                    mButton51 = findViewById(R.id.btn51);
                    mButton52 = findViewById(R.id.btn52);
                    mButton53 = findViewById(R.id.btn53);
//				mButton54 = (Button) findViewById(R.id.btn54);
//				mButton55 = (Button) findViewById(R.id.btn55);
//				mButton56 = (Button) findViewById(R.id.btn56);
//				mButton57 = (Button) findViewById(R.id.btn57);
//				mButton58 = (Button) findViewById(R.id.btn58);
//				mButton59 = (Button) findViewById(R.id.btn59);
//				mButton60 = (Button) findViewById(R.id.btn60);
//				mButton61 = (Button) findViewById(R.id.btn61);
//				mButton62 = (Button) findViewById(R.id.btn62);
                    mButton63 = findViewById(R.id.btn63);
                    mButton64 = findViewById(R.id.btn64);
                    mButton65 = findViewById(R.id.btn65);
                    mButton66 = findViewById(R.id.btn66);
                    mButton67 = findViewById(R.id.btn67);
                    mButton68 = findViewById(R.id.btn68);
//				mButton69 = (Button) findViewById(R.id.btn69);
//				mButton70 = (Button) findViewById(R.id.btn70);
//				mButton71 = (Button) findViewById(R.id.btn71);
//				mButton72 = (Button) findViewById(R.id.btn72);
//				mButton73 = (Button) findViewById(R.id.btn73);
//				mButton74 = (Button) findViewById(R.id.btn74);
//				mButton75 = (Button) findViewById(R.id.btn75);
//				mButton76 = (Button) findViewById(R.id.btn76);
//				mButton77 = (Button) findViewById(R.id.btn77);
                    mButton78 = findViewById(R.id.btn78);
                    mButton79 = findViewById(R.id.btn79);
                    mButton80 = findViewById(R.id.btn80);
                    mButton81 = findViewById(R.id.btn81);
                    mButton82 = findViewById(R.id.btn82);
                    mButton83 = findViewById(R.id.btn83);
                    //mButton84 = (Button) findViewById(R.id.btn84);
                    //mButton85 = (Button) findViewById(R.id.btn85);
                    //mButton86 = (Button) findViewById(R.id.btn86);
                    //mButton87 = (Button) findViewById(R.id.btn87);
                    //mButton88 = (Button) findViewById(R.id.btn88);
                    //mButton89 = (Button) findViewById(R.id.btn89);
                    //mButton90 = (Button) findViewById(R.id.btn90);
                    //mButton91 = (Button) findViewById(R.id.btn91);
                    //mButton92 = (Button) findViewById(R.id.btn92);
                    mButton93 = findViewById(R.id.btn93);
                    mButton94 = findViewById(R.id.btn94);
                    mButton95 = findViewById(R.id.btn95);
                    mButton96 = findViewById(R.id.btn96);
                    mButton97 = findViewById(R.id.btn97);
                    mButton98 = findViewById(R.id.btn98);
//				mButton99 = (Button) findViewById(R.id.btn99);
//				mButton100 = (Button) findViewById(R.id.btn100);
//				mButton101 = (Button) findViewById(R.id.btn101);
//				mButton102 = (Button) findViewById(R.id.btn102);
//				mButton103 = (Button) findViewById(R.id.btn103);
//				mButton104 = (Button) findViewById(R.id.btn104);
//				mButton105 = (Button) findViewById(R.id.btn105);
//				mButton106 = (Button) findViewById(R.id.btn106);
//				mButton107 = (Button) findViewById(R.id.btn107);
//				mButton108 = (Button) findViewById(R.id.btn108);
//				mButton109 = (Button) findViewById(R.id.btn109);
//				mButton110 = (Button) findViewById(R.id.btn110);
//				mButton111 = (Button) findViewById(R.id.btn111);
//				mButton112 = (Button) findViewById(R.id.btn112);
                    mButton153 = findViewById(R.id.btn153);
                    mButton154 = findViewById(R.id.btn154);
                    mButton155 = findViewById(R.id.btn155);
                    mButton156 = findViewById(R.id.btn156);


                    findViewById(R.id.marcoset2ok).setOnClickListener(new ButtonListener());

                    findViewById(R.id.s11).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s12).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s13).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s14).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s15).setOnClickListener(new ButtonListener());


                    lamp = getSharedPreferences(portSwitch + lampdata, 0);
                    mButton01.setText(lamp.getString(btn01name, ""));
                    mButton02.setText(lamp.getString(btn02name, ""));
                    mButton03.setText(lamp.getString(btn03name, ""));
                    mButton04.setText(lamp.getString(btn04name, ""));
                    mButton05.setText(lamp.getString(btn05name, ""));
                    mButton06.setText(lamp.getString(btn06name, ""));
                    mButton07.setText(lamp.getString(btn07name, ""));
                    mButton08.setText(lamp.getString(btn08name, ""));
                    mButton09.setText(lamp.getString(btn09name, ""));
                    mButton10.setText(lamp.getString(btn10name, ""));
                    mButton11.setText(lamp.getString(btn11name, ""));
                    mButton12.setText(lamp.getString(btn12name, ""));

                    setting = getSharedPreferences(portSwitch + custom1data, 0);

                    cus1name = findViewById(R.id.cus1name);
                    cus1name.setText(setting.getString(custom1name, ""));
//				mButton54.setText(setting.getString(btn54name, ""));
//				mButton55.setText(setting.getString(btn55name, ""));
//				mButton56.setText(setting.getString(btn56name, ""));
//				mButton57.setText(setting.getString(btn57name, ""));
//				mButton58.setText(setting.getString(btn58name, ""));
//				mButton59.setText(setting.getString(btn59name, ""));
//				mButton60.setText(setting.getString(btn60name, ""));
//				mButton61.setText(setting.getString(btn61name, ""));
//				mButton62.setText(setting.getString(btn62name, ""));
                    mButton63.setText(setting.getString(btn63name, ""));
                    mButton64.setText(setting.getString(btn64name, ""));
                    mButton65.setText(setting.getString(btn65name, ""));
                    mButton66.setText(setting.getString(btn66name, ""));
                    mButton67.setText(setting.getString(btn67name, ""));
                    mButton68.setText(setting.getString(btn68name, ""));
//				mButton99.setText(setting.getString(btn99name, ""));
//				mButton100.setText(setting.getString(btn100name, ""));
//				mButton101.setText(setting.getString(btn101name, ""));
//				mButton102.setText(setting.getString(btn102name, ""));
//				mButton103.setText(setting.getString(btn103name, ""));
//				mButton104.setText(setting.getString(btn104name, ""));

                    settingb = getSharedPreferences(portSwitch + custom2data, 0);

                    cus2name = findViewById(R.id.cus2name);
                    cus2name.setText(settingb.getString(custom2name, ""));
//				 mButton69.setText(settingb.getString(btn69name, ""));
//				 mButton70.setText(settingb.getString(btn70name, ""));
//				 mButton71.setText(settingb.getString(btn71name, ""));
//				 mButton72.setText(settingb.getString(btn72name, ""));
//				 mButton73.setText(settingb.getString(btn73name, ""));
//				 mButton74.setText(settingb.getString(btn74name, ""));
//				 mButton75.setText(settingb.getString(btn75name, ""));
//				 mButton76.setText(settingb.getString(btn76name, ""));
//				 mButton77.setText(settingb.getString(btn77name, ""));
                    mButton78.setText(settingb.getString(btn78name, ""));
                    mButton79.setText(settingb.getString(btn79name, ""));
                    mButton80.setText(settingb.getString(btn80name, ""));
                    mButton81.setText(settingb.getString(btn81name, ""));
                    mButton82.setText(settingb.getString(btn82name, ""));
                    mButton83.setText(settingb.getString(btn83name, ""));
//				 mButton105.setText(settingb.getString(btn105name, ""));
//				 mButton106.setText(settingb.getString(btn106name, ""));
//				 mButton107.setText(settingb.getString(btn107name, ""));
//				 mButton108.setText(settingb.getString(btn108name, ""));
//				 mButton109.setText(settingb.getString(btn109name, ""));
//				 mButton110.setText(settingb.getString(btn110name, ""));

                    setting2 = getSharedPreferences(portSwitch + custom3data, 0);

                    cus3name = findViewById(R.id.cus3name);
                    cus3name.setText(setting2.getString(custom3name, ""));
                    // mButton84.setText(setting2.getString(btn84name, ""));
                    //mButton85.setText(setting2.getString(btn85name, ""));
                    //mButton86.setText(setting2.getString(btn86name, ""));
                    //mButton87.setText(setting2.getString(btn87name, ""));
                    //mButton88.setText(setting2.getString(btn88name, ""));
                    //mButton89.setText(setting2.getString(btn89name, ""));
                    //mButton90.setText(setting2.getString(btn90name, ""));
                    //mButton91.setText(setting2.getString(btn91name, ""));
                    //mButton92.setText(setting2.getString(btn92name, ""));
                    mButton93.setText(setting2.getString(btn93name, ""));
                    mButton94.setText(setting2.getString(btn94name, ""));
                    mButton95.setText(setting2.getString(btn95name, ""));
                    mButton96.setText(setting2.getString(btn96name, ""));
                    mButton97.setText(setting2.getString(btn97name, ""));
                    mButton98.setText(setting2.getString(btn98name, ""));
                    //mButton111.setText(setting2.getString(btn111name, ""));
                    //mButton112.setText(setting2.getString(btn112name, ""));
                    //mButton113.setText(setting2.getString(btn113name, ""));
                    //mButton114.setText(setting2.getString(btn114name, ""));
                    //mButton115.setText(setting2.getString(btn115name, ""));
                    //mButton116.setText(setting2.getString(btn116name, ""));

                    airseting = getSharedPreferences(portSwitch + airdata, 0);
                    mButton17.setText(airseting.getString(btn17name, ""));
                    mButton18.setText(airseting.getString(btn18name, ""));
                    mButton19.setText(airseting.getString(btn19name, ""));
                    mButton20.setText(airseting.getString(btn20name, ""));
                    mButton21.setText(airseting.getString(btn21name, ""));
                    mButton22.setText(airseting.getString(btn22name, ""));


                    lamp = getSharedPreferences(portSwitch + lampdata, 0);
                    imagebtn1 = findViewById(R.id.imagebtn1);
                    imagebtn2 = findViewById(R.id.imagebtn2);
                    imagebtn3 = findViewById(R.id.imagebtn3);
                    imagebtn4 = findViewById(R.id.imagebtn4);
                    imagebtn5 = findViewById(R.id.imagebtn5);
                    imagebtn6 = findViewById(R.id.imagebtn6);
                    imagebtn7 = findViewById(R.id.imagebtn7);
                    imagebtn8 = findViewById(R.id.imagebtn8);
                    imagebtn9 = findViewById(R.id.imagebtn9);
                    imagebtn10 = findViewById(R.id.imagebtn10);
                    imagebtn11 = findViewById(R.id.imagebtn11);
                    imagebtn12 = findViewById(R.id.imagebtn12);


                    getLampBtnPic();
                    readData(7);
                    break;


                //marco4
                case R.id.f4:
                    // TODO Auto-generated catch block
                    setContentView(R.layout.marcoset3);

                    marcoset3name = findViewById(R.id.marcoset3name);
                    marco3setbtn = findViewById(R.id.marco3setbtn);
                    mButton01 = findViewById(R.id.btn01);
                    mButton02 = findViewById(R.id.btn02);
                    mButton03 = findViewById(R.id.btn03);
                    mButton04 = findViewById(R.id.btn04);
                    mButton05 = findViewById(R.id.btn05);
                    mButton06 = findViewById(R.id.btn06);
                    mButton07 = findViewById(R.id.btn07);
                    mButton08 = findViewById(R.id.btn08);
                    mButton09 = findViewById(R.id.btn09);
                    mButton10 = findViewById(R.id.btn10);
                    mButton11 = findViewById(R.id.btn11);
                    mButton12 = findViewById(R.id.btn12);
                    mButton13 = findViewById(R.id.btn13);
                    mButton14 = findViewById(R.id.btn14);
                    mButton15 = findViewById(R.id.btn15);
                    mButton16 = findViewById(R.id.btn16);
                    mButton17 = findViewById(R.id.btn17);
                    mButton18 = findViewById(R.id.btn18);
                    mButton19 = findViewById(R.id.btn19);
                    mButton20 = findViewById(R.id.btn20);
                    mButton21 = findViewById(R.id.btn21);
                    mButton22 = findViewById(R.id.btn22);
                    mButton23 = findViewById(R.id.btn23);
                    mButton24 = findViewById(R.id.btn24);
//				mButton25 = (Button) findViewById(R.id.btn25);
//				mButton26 = (Button) findViewById(R.id.btn26);
//				mButton27 = (Button) findViewById(R.id.btn27);
//				mButton28 = (Button) findViewById(R.id.btn28);
//				mButton29 = (Button) findViewById(R.id.btn29);
//				mButton30 = (Button) findViewById(R.id.btn30);
//				mButton31 = (Button) findViewById(R.id.btn31);
//				mButton32 = (Button) findViewById(R.id.btn32);
                    mButton33 = findViewById(R.id.btn33);
//				mButton34 = (Button) findViewById(R.id.btn34);
                    mButton35 = findViewById(R.id.btn35);
//				mButton36 = (Button) findViewById(R.id.btn36);
//				mButton37 = (Button) findViewById(R.id.btn37);
//				mButton38 = (Button) findViewById(R.id.btn38);
//				mButton39 = (Button) findViewById(R.id.btn39);
//				mButton40 = (Button) findViewById(R.id.btn40);
//				mButton41 = (Button) findViewById(R.id.btn41);
                    mButton42 = findViewById(R.id.btn42);
                    mButton43 = findViewById(R.id.btn43);
                    mButton44 = findViewById(R.id.btn44);
                    mButton45 = findViewById(R.id.btn45);
                    mButton46 = findViewById(R.id.btn46);
                    mButton47 = findViewById(R.id.btn47);
                    mButton48 = findViewById(R.id.btn48);
                    mButton49 = findViewById(R.id.btn49);
                    mButton50 = findViewById(R.id.btn50);
                    mButton51 = findViewById(R.id.btn51);
                    mButton52 = findViewById(R.id.btn52);
                    mButton53 = findViewById(R.id.btn53);
//				mButton54 = (Button) findViewById(R.id.btn54);
//				mButton55 = (Button) findViewById(R.id.btn55);
//				mButton56 = (Button) findViewById(R.id.btn56);
//				mButton57 = (Button) findViewById(R.id.btn57);
//				mButton58 = (Button) findViewById(R.id.btn58);
//				mButton59 = (Button) findViewById(R.id.btn59);
//				mButton60 = (Button) findViewById(R.id.btn60);
//				mButton61 = (Button) findViewById(R.id.btn61);
//				mButton62 = (Button) findViewById(R.id.btn62);
                    mButton63 = findViewById(R.id.btn63);
                    mButton64 = findViewById(R.id.btn64);
                    mButton65 = findViewById(R.id.btn65);
                    mButton66 = findViewById(R.id.btn66);
                    mButton67 = findViewById(R.id.btn67);
                    mButton68 = findViewById(R.id.btn68);
//				mButton69 = (Button) findViewById(R.id.btn69);
//				mButton70 = (Button) findViewById(R.id.btn70);
//				mButton71 = (Button) findViewById(R.id.btn71);
//				mButton72 = (Button) findViewById(R.id.btn72);
//				mButton73 = (Button) findViewById(R.id.btn73);
//				mButton74 = (Button) findViewById(R.id.btn74);
//				mButton75 = (Button) findViewById(R.id.btn75);
//				mButton76 = (Button) findViewById(R.id.btn76);
//				mButton77 = (Button) findViewById(R.id.btn77);
                    mButton78 = findViewById(R.id.btn78);
                    mButton79 = findViewById(R.id.btn79);
                    mButton80 = findViewById(R.id.btn80);
                    mButton81 = findViewById(R.id.btn81);
                    mButton82 = findViewById(R.id.btn82);
                    mButton83 = findViewById(R.id.btn83);
                    //mButton84 = (Button) findViewById(R.id.btn84);
                    //mButton85 = (Button) findViewById(R.id.btn85);
                    //mButton86 = (Button) findViewById(R.id.btn86);
                    //mButton87 = (Button) findViewById(R.id.btn87);
                    //mButton88 = (Button) findViewById(R.id.btn88);
                    //mButton89 = (Button) findViewById(R.id.btn89);
                    //mButton90 = (Button) findViewById(R.id.btn90);
                    //mButton91 = (Button) findViewById(R.id.btn91);
                    //mButton92 = (Button) findViewById(R.id.btn92);
                    mButton93 = findViewById(R.id.btn93);
                    mButton94 = findViewById(R.id.btn94);
                    mButton95 = findViewById(R.id.btn95);
                    mButton96 = findViewById(R.id.btn96);
                    mButton97 = findViewById(R.id.btn97);
                    mButton98 = findViewById(R.id.btn98);
//				mButton99 = (Button) findViewById(R.id.btn99);
//				mButton100 = (Button) findViewById(R.id.btn100);
//				mButton101 = (Button) findViewById(R.id.btn101);
//				mButton102 = (Button) findViewById(R.id.btn102);
//				mButton103 = (Button) findViewById(R.id.btn103);
//				mButton104 = (Button) findViewById(R.id.btn104);
//				mButton105 = (Button) findViewById(R.id.btn105);
//				mButton106 = (Button) findViewById(R.id.btn106);
//				mButton107 = (Button) findViewById(R.id.btn107);
//				mButton108 = (Button) findViewById(R.id.btn108);
//				mButton109 = (Button) findViewById(R.id.btn109);
//				mButton110 = (Button) findViewById(R.id.btn110);
//				mButton111 = (Button) findViewById(R.id.btn111);
//				mButton112 = (Button) findViewById(R.id.btn112);
                    mButton153 = findViewById(R.id.btn153);
                    mButton154 = findViewById(R.id.btn154);
                    mButton155 = findViewById(R.id.btn155);
                    mButton156 = findViewById(R.id.btn156);


                    findViewById(R.id.marcoset3ok).setOnClickListener(new ButtonListener());

                    findViewById(R.id.s16).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s17).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s18).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s19).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s20).setOnClickListener(new ButtonListener());


                    lamp = getSharedPreferences(portSwitch + lampdata, 0);
                    mButton01.setText(lamp.getString(btn01name, ""));
                    mButton02.setText(lamp.getString(btn02name, ""));
                    mButton03.setText(lamp.getString(btn03name, ""));
                    mButton04.setText(lamp.getString(btn04name, ""));
                    mButton05.setText(lamp.getString(btn05name, ""));
                    mButton06.setText(lamp.getString(btn06name, ""));
                    mButton07.setText(lamp.getString(btn07name, ""));
                    mButton08.setText(lamp.getString(btn08name, ""));
                    mButton09.setText(lamp.getString(btn09name, ""));
                    mButton10.setText(lamp.getString(btn10name, ""));
                    mButton11.setText(lamp.getString(btn11name, ""));
                    mButton12.setText(lamp.getString(btn12name, ""));

                    setting = getSharedPreferences(portSwitch + custom1data, 0);

                    cus1name = findViewById(R.id.cus1name);
                    cus1name.setText(setting.getString(custom1name, ""));
//				mButton54.setText(setting.getString(btn54name, ""));
//				mButton55.setText(setting.getString(btn55name, ""));
//				mButton56.setText(setting.getString(btn56name, ""));
//				mButton57.setText(setting.getString(btn57name, ""));
//				mButton58.setText(setting.getString(btn58name, ""));
//				mButton59.setText(setting.getString(btn59name, ""));
//				mButton60.setText(setting.getString(btn60name, ""));
//				mButton61.setText(setting.getString(btn61name, ""));
//				mButton62.setText(setting.getString(btn62name, ""));
                    mButton63.setText(setting.getString(btn63name, ""));
                    mButton64.setText(setting.getString(btn64name, ""));
                    mButton65.setText(setting.getString(btn65name, ""));
                    mButton66.setText(setting.getString(btn66name, ""));
                    mButton67.setText(setting.getString(btn67name, ""));
                    mButton68.setText(setting.getString(btn68name, ""));
//				mButton99.setText(setting.getString(btn99name, ""));
//				mButton100.setText(setting.getString(btn100name, ""));
//				mButton101.setText(setting.getString(btn101name, ""));
//				mButton102.setText(setting.getString(btn102name, ""));
//				mButton103.setText(setting.getString(btn103name, ""));
//				mButton104.setText(setting.getString(btn104name, ""));

                    settingb = getSharedPreferences(portSwitch + custom2data, 0);

                    cus2name = findViewById(R.id.cus2name);
                    cus2name.setText(settingb.getString(custom2name, ""));
//				 mButton69.setText(settingb.getString(btn69name, ""));
//				 mButton70.setText(settingb.getString(btn70name, ""));
//				 mButton71.setText(settingb.getString(btn71name, ""));
//				 mButton72.setText(settingb.getString(btn72name, ""));
//				 mButton73.setText(settingb.getString(btn73name, ""));
//				 mButton74.setText(settingb.getString(btn74name, ""));
//				 mButton75.setText(settingb.getString(btn75name, ""));
//				 mButton76.setText(settingb.getString(btn76name, ""));
//				 mButton77.setText(settingb.getString(btn77name, ""));
                    mButton78.setText(settingb.getString(btn78name, ""));
                    mButton79.setText(settingb.getString(btn79name, ""));
                    mButton80.setText(settingb.getString(btn80name, ""));
                    mButton81.setText(settingb.getString(btn81name, ""));
                    mButton82.setText(settingb.getString(btn82name, ""));
                    mButton83.setText(settingb.getString(btn83name, ""));
//				 mButton105.setText(settingb.getString(btn105name, ""));
//				 mButton106.setText(settingb.getString(btn106name, ""));
//				 mButton107.setText(settingb.getString(btn107name, ""));
//				 mButton108.setText(settingb.getString(btn108name, ""));
//				 mButton109.setText(settingb.getString(btn109name, ""));
//				 mButton110.setText(settingb.getString(btn110name, ""));

                    setting2 = getSharedPreferences(portSwitch + custom3data, 0);

                    cus3name = findViewById(R.id.cus3name);
                    cus3name.setText(setting2.getString(custom3name, ""));
                    // mButton84.setText(setting2.getString(btn84name, ""));
                    //mButton85.setText(setting2.getString(btn85name, ""));
                    //mButton86.setText(setting2.getString(btn86name, ""));
                    //mButton87.setText(setting2.getString(btn87name, ""));
                    //mButton88.setText(setting2.getString(btn88name, ""));
                    //mButton89.setText(setting2.getString(btn89name, ""));
                    //mButton90.setText(setting2.getString(btn90name, ""));
                    //mButton91.setText(setting2.getString(btn91name, ""));
                    //mButton92.setText(setting2.getString(btn92name, ""));
                    mButton93.setText(setting2.getString(btn93name, ""));
                    mButton94.setText(setting2.getString(btn94name, ""));
                    mButton95.setText(setting2.getString(btn95name, ""));
                    mButton96.setText(setting2.getString(btn96name, ""));
                    mButton97.setText(setting2.getString(btn97name, ""));
                    mButton98.setText(setting2.getString(btn98name, ""));
                    //mButton111.setText(setting2.getString(btn111name, ""));
                    //mButton112.setText(setting2.getString(btn112name, ""));
                    //mButton113.setText(setting2.getString(btn113name, ""));
                    //mButton114.setText(setting2.getString(btn114name, ""));
                    //mButton115.setText(setting2.getString(btn115name, ""));
                    //mButton116.setText(setting2.getString(btn116name, ""));

                    airseting = getSharedPreferences(portSwitch + airdata, 0);
                    mButton17.setText(airseting.getString(btn17name, ""));
                    mButton18.setText(airseting.getString(btn18name, ""));
                    mButton19.setText(airseting.getString(btn19name, ""));
                    mButton20.setText(airseting.getString(btn20name, ""));
                    mButton21.setText(airseting.getString(btn21name, ""));
                    mButton22.setText(airseting.getString(btn22name, ""));


                    lamp = getSharedPreferences(portSwitch + lampdata, 0);
                    imagebtn1 = findViewById(R.id.imagebtn1);
                    imagebtn2 = findViewById(R.id.imagebtn2);
                    imagebtn3 = findViewById(R.id.imagebtn3);
                    imagebtn4 = findViewById(R.id.imagebtn4);
                    imagebtn5 = findViewById(R.id.imagebtn5);
                    imagebtn6 = findViewById(R.id.imagebtn6);
                    imagebtn7 = findViewById(R.id.imagebtn7);
                    imagebtn8 = findViewById(R.id.imagebtn8);
                    imagebtn9 = findViewById(R.id.imagebtn9);
                    imagebtn10 = findViewById(R.id.imagebtn10);
                    imagebtn11 = findViewById(R.id.imagebtn11);
                    imagebtn12 = findViewById(R.id.imagebtn12);


                    getLampBtnPic();
                    readData(8);
                    break;


                //marco5
                case R.id.f5:
                    // TODO Auto-generated catch block
                    setContentView(R.layout.marcoset4);

                    marcoset4name = findViewById(R.id.marcoset4name);
                    marco4setbtn = findViewById(R.id.marco4setbtn);
                    mButton01 = findViewById(R.id.btn01);
                    mButton02 = findViewById(R.id.btn02);
                    mButton03 = findViewById(R.id.btn03);
                    mButton04 = findViewById(R.id.btn04);
                    mButton05 = findViewById(R.id.btn05);
                    mButton06 = findViewById(R.id.btn06);
                    mButton07 = findViewById(R.id.btn07);
                    mButton08 = findViewById(R.id.btn08);
                    mButton09 = findViewById(R.id.btn09);
                    mButton10 = findViewById(R.id.btn10);
                    mButton11 = findViewById(R.id.btn11);
                    mButton12 = findViewById(R.id.btn12);
                    mButton13 = findViewById(R.id.btn13);
                    mButton14 = findViewById(R.id.btn14);
                    mButton15 = findViewById(R.id.btn15);
                    mButton16 = findViewById(R.id.btn16);
                    mButton17 = findViewById(R.id.btn17);
                    mButton18 = findViewById(R.id.btn18);
                    mButton19 = findViewById(R.id.btn19);
                    mButton20 = findViewById(R.id.btn20);
                    mButton21 = findViewById(R.id.btn21);
                    mButton22 = findViewById(R.id.btn22);
                    mButton23 = findViewById(R.id.btn23);
                    mButton24 = findViewById(R.id.btn24);
//				mButton25 = (Button) findViewById(R.id.btn25);
//				mButton26 = (Button) findViewById(R.id.btn26);
//				mButton27 = (Button) findViewById(R.id.btn27);
//				mButton28 = (Button) findViewById(R.id.btn28);
//				mButton29 = (Button) findViewById(R.id.btn29);
//				mButton30 = (Button) findViewById(R.id.btn30);
//				mButton31 = (Button) findViewById(R.id.btn31);
//				mButton32 = (Button) findViewById(R.id.btn32);
                    mButton33 = findViewById(R.id.btn33);
//				mButton34 = (Button) findViewById(R.id.btn34);
                    mButton35 = findViewById(R.id.btn35);
//				mButton36 = (Button) findViewById(R.id.btn36);
//				mButton37 = (Button) findViewById(R.id.btn37);
//				mButton38 = (Button) findViewById(R.id.btn38);
//				mButton39 = (Button) findViewById(R.id.btn39);
//				mButton40 = (Button) findViewById(R.id.btn40);
//				mButton41 = (Button) findViewById(R.id.btn41);
                    mButton42 = findViewById(R.id.btn42);
                    mButton43 = findViewById(R.id.btn43);
                    mButton44 = findViewById(R.id.btn44);
                    mButton45 = findViewById(R.id.btn45);
                    mButton46 = findViewById(R.id.btn46);
                    mButton47 = findViewById(R.id.btn47);
                    mButton48 = findViewById(R.id.btn48);
                    mButton49 = findViewById(R.id.btn49);
                    mButton50 = findViewById(R.id.btn50);
                    mButton51 = findViewById(R.id.btn51);
                    mButton52 = findViewById(R.id.btn52);
                    mButton53 = findViewById(R.id.btn53);
//				mButton54 = (Button) findViewById(R.id.btn54);
//				mButton55 = (Button) findViewById(R.id.btn55);
//				mButton56 = (Button) findViewById(R.id.btn56);
//				mButton57 = (Button) findViewById(R.id.btn57);
//				mButton58 = (Button) findViewById(R.id.btn58);
//				mButton59 = (Button) findViewById(R.id.btn59);
//				mButton60 = (Button) findViewById(R.id.btn60);
//				mButton61 = (Button) findViewById(R.id.btn61);
//				mButton62 = (Button) findViewById(R.id.btn62);
                    mButton63 = findViewById(R.id.btn63);
                    mButton64 = findViewById(R.id.btn64);
                    mButton65 = findViewById(R.id.btn65);
                    mButton66 = findViewById(R.id.btn66);
                    mButton67 = findViewById(R.id.btn67);
                    mButton68 = findViewById(R.id.btn68);
//				mButton69 = (Button) findViewById(R.id.btn69);
//				mButton70 = (Button) findViewById(R.id.btn70);
//				mButton71 = (Button) findViewById(R.id.btn71);
//				mButton72 = (Button) findViewById(R.id.btn72);
//				mButton73 = (Button) findViewById(R.id.btn73);
//				mButton74 = (Button) findViewById(R.id.btn74);
//				mButton75 = (Button) findViewById(R.id.btn75);
//				mButton76 = (Button) findViewById(R.id.btn76);
//				mButton77 = (Button) findViewById(R.id.btn77);
                    mButton78 = findViewById(R.id.btn78);
                    mButton79 = findViewById(R.id.btn79);
                    mButton80 = findViewById(R.id.btn80);
                    mButton81 = findViewById(R.id.btn81);
                    mButton82 = findViewById(R.id.btn82);
                    mButton83 = findViewById(R.id.btn83);
                    //mButton84 = (Button) findViewById(R.id.btn84);
                    //mButton85 = (Button) findViewById(R.id.btn85);
                    //mButton86 = (Button) findViewById(R.id.btn86);
                    //mButton87 = (Button) findViewById(R.id.btn87);
                    //mButton88 = (Button) findViewById(R.id.btn88);
                    //mButton89 = (Button) findViewById(R.id.btn89);
                    //mButton90 = (Button) findViewById(R.id.btn90);
                    //mButton91 = (Button) findViewById(R.id.btn91);
                    //mButton92 = (Button) findViewById(R.id.btn92);
                    mButton93 = findViewById(R.id.btn93);
                    mButton94 = findViewById(R.id.btn94);
                    mButton95 = findViewById(R.id.btn95);
                    mButton96 = findViewById(R.id.btn96);
                    mButton97 = findViewById(R.id.btn97);
                    mButton98 = findViewById(R.id.btn98);
//				mButton99 = (Button) findViewById(R.id.btn99);
//				mButton100 = (Button) findViewById(R.id.btn100);
//				mButton101 = (Button) findViewById(R.id.btn101);
//				mButton102 = (Button) findViewById(R.id.btn102);
//				mButton103 = (Button) findViewById(R.id.btn103);
//				mButton104 = (Button) findViewById(R.id.btn104);
//				mButton105 = (Button) findViewById(R.id.btn105);
//				mButton106 = (Button) findViewById(R.id.btn106);
//				mButton107 = (Button) findViewById(R.id.btn107);
//				mButton108 = (Button) findViewById(R.id.btn108);
//				mButton109 = (Button) findViewById(R.id.btn109);
//				mButton110 = (Button) findViewById(R.id.btn110);
//				mButton111 = (Button) findViewById(R.id.btn111);
//				mButton112 = (Button) findViewById(R.id.btn112);
                    mButton153 = findViewById(R.id.btn153);
                    mButton154 = findViewById(R.id.btn154);
                    mButton155 = findViewById(R.id.btn155);
                    mButton156 = findViewById(R.id.btn156);


                    findViewById(R.id.marcoset4ok).setOnClickListener(new ButtonListener());

                    findViewById(R.id.s21).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s22).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s23).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s24).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s25).setOnClickListener(new ButtonListener());


                    lamp = getSharedPreferences(portSwitch + lampdata, 0);
                    mButton01.setText(lamp.getString(btn01name, ""));
                    mButton02.setText(lamp.getString(btn02name, ""));
                    mButton03.setText(lamp.getString(btn03name, ""));
                    mButton04.setText(lamp.getString(btn04name, ""));
                    mButton05.setText(lamp.getString(btn05name, ""));
                    mButton06.setText(lamp.getString(btn06name, ""));
                    mButton07.setText(lamp.getString(btn07name, ""));
                    mButton08.setText(lamp.getString(btn08name, ""));
                    mButton09.setText(lamp.getString(btn09name, ""));
                    mButton10.setText(lamp.getString(btn10name, ""));
                    mButton11.setText(lamp.getString(btn11name, ""));
                    mButton12.setText(lamp.getString(btn12name, ""));

                    setting = getSharedPreferences(portSwitch + custom1data, 0);

                    cus1name = findViewById(R.id.cus1name);
                    cus1name.setText(setting.getString(custom1name, ""));
//				mButton54.setText(setting.getString(btn54name, ""));
//				mButton55.setText(setting.getString(btn55name, ""));
//				mButton56.setText(setting.getString(btn56name, ""));
//				mButton57.setText(setting.getString(btn57name, ""));
//				mButton58.setText(setting.getString(btn58name, ""));
//				mButton59.setText(setting.getString(btn59name, ""));
//				mButton60.setText(setting.getString(btn60name, ""));
//				mButton61.setText(setting.getString(btn61name, ""));
//				mButton62.setText(setting.getString(btn62name, ""));
                    mButton63.setText(setting.getString(btn63name, ""));
                    mButton64.setText(setting.getString(btn64name, ""));
                    mButton65.setText(setting.getString(btn65name, ""));
                    mButton66.setText(setting.getString(btn66name, ""));
                    mButton67.setText(setting.getString(btn67name, ""));
                    mButton68.setText(setting.getString(btn68name, ""));
//				mButton99.setText(setting.getString(btn99name, ""));
//				mButton100.setText(setting.getString(btn100name, ""));
//				mButton101.setText(setting.getString(btn101name, ""));
//				mButton102.setText(setting.getString(btn102name, ""));
//				mButton103.setText(setting.getString(btn103name, ""));
//				mButton104.setText(setting.getString(btn104name, ""));

                    settingb = getSharedPreferences(portSwitch + custom2data, 0);

                    cus2name = findViewById(R.id.cus2name);
                    cus2name.setText(settingb.getString(custom2name, ""));
//				 mButton69.setText(settingb.getString(btn69name, ""));
//				 mButton70.setText(settingb.getString(btn70name, ""));
//				 mButton71.setText(settingb.getString(btn71name, ""));
//				 mButton72.setText(settingb.getString(btn72name, ""));
//				 mButton73.setText(settingb.getString(btn73name, ""));
//				 mButton74.setText(settingb.getString(btn74name, ""));
//				 mButton75.setText(settingb.getString(btn75name, ""));
//				 mButton76.setText(settingb.getString(btn76name, ""));
//				 mButton77.setText(settingb.getString(btn77name, ""));
                    mButton78.setText(settingb.getString(btn78name, ""));
                    mButton79.setText(settingb.getString(btn79name, ""));
                    mButton80.setText(settingb.getString(btn80name, ""));
                    mButton81.setText(settingb.getString(btn81name, ""));
                    mButton82.setText(settingb.getString(btn82name, ""));
                    mButton83.setText(settingb.getString(btn83name, ""));
//				 mButton105.setText(settingb.getString(btn105name, ""));
//				 mButton106.setText(settingb.getString(btn106name, ""));
//				 mButton107.setText(settingb.getString(btn107name, ""));
//				 mButton108.setText(settingb.getString(btn108name, ""));
//				 mButton109.setText(settingb.getString(btn109name, ""));
//				 mButton110.setText(settingb.getString(btn110name, ""));

                    setting2 = getSharedPreferences(portSwitch + custom3data, 0);

                    cus3name = findViewById(R.id.cus3name);
                    cus3name.setText(setting2.getString(custom3name, ""));
                    // mButton84.setText(setting2.getString(btn84name, ""));
                    //mButton85.setText(setting2.getString(btn85name, ""));
                    //mButton86.setText(setting2.getString(btn86name, ""));
                    //mButton87.setText(setting2.getString(btn87name, ""));
                    //mButton88.setText(setting2.getString(btn88name, ""));
                    //mButton89.setText(setting2.getString(btn89name, ""));
                    //mButton90.setText(setting2.getString(btn90name, ""));
                    //mButton91.setText(setting2.getString(btn91name, ""));
                    //mButton92.setText(setting2.getString(btn92name, ""));
                    mButton93.setText(setting2.getString(btn93name, ""));
                    mButton94.setText(setting2.getString(btn94name, ""));
                    mButton95.setText(setting2.getString(btn95name, ""));
                    mButton96.setText(setting2.getString(btn96name, ""));
                    mButton97.setText(setting2.getString(btn97name, ""));
                    mButton98.setText(setting2.getString(btn98name, ""));
                    //mButton111.setText(setting2.getString(btn111name, ""));
                    //mButton112.setText(setting2.getString(btn112name, ""));
                    //mButton113.setText(setting2.getString(btn113name, ""));
                    //mButton114.setText(setting2.getString(btn114name, ""));
                    //mButton115.setText(setting2.getString(btn115name, ""));
                    //mButton116.setText(setting2.getString(btn116name, ""));

                    airseting = getSharedPreferences(portSwitch + airdata, 0);
                    mButton17.setText(airseting.getString(btn17name, ""));
                    mButton18.setText(airseting.getString(btn18name, ""));
                    mButton19.setText(airseting.getString(btn19name, ""));
                    mButton20.setText(airseting.getString(btn20name, ""));
                    mButton21.setText(airseting.getString(btn21name, ""));
                    mButton22.setText(airseting.getString(btn22name, ""));


                    lamp = getSharedPreferences(portSwitch + lampdata, 0);
                    imagebtn1 = findViewById(R.id.imagebtn1);
                    imagebtn2 = findViewById(R.id.imagebtn2);
                    imagebtn3 = findViewById(R.id.imagebtn3);
                    imagebtn4 = findViewById(R.id.imagebtn4);
                    imagebtn5 = findViewById(R.id.imagebtn5);
                    imagebtn6 = findViewById(R.id.imagebtn6);
                    imagebtn7 = findViewById(R.id.imagebtn7);
                    imagebtn8 = findViewById(R.id.imagebtn8);
                    imagebtn9 = findViewById(R.id.imagebtn9);
                    imagebtn10 = findViewById(R.id.imagebtn10);
                    imagebtn11 = findViewById(R.id.imagebtn11);
                    imagebtn12 = findViewById(R.id.imagebtn12);


                    getLampBtnPic();
                    readData(9);
                    break;


                //marco6
                case R.id.f6:
                    // TODO Auto-generated catch block
                    setContentView(R.layout.marcoset5);

                    marcoset5name = findViewById(R.id.marcoset5name);
                    marco5setbtn = findViewById(R.id.marco5setbtn);
                    mButton01 = findViewById(R.id.btn01);
                    mButton02 = findViewById(R.id.btn02);
                    mButton03 = findViewById(R.id.btn03);
                    mButton04 = findViewById(R.id.btn04);
                    mButton05 = findViewById(R.id.btn05);
                    mButton06 = findViewById(R.id.btn06);
                    mButton07 = findViewById(R.id.btn07);
                    mButton08 = findViewById(R.id.btn08);
                    mButton09 = findViewById(R.id.btn09);
                    mButton10 = findViewById(R.id.btn10);
                    mButton11 = findViewById(R.id.btn11);
                    mButton12 = findViewById(R.id.btn12);
                    mButton13 = findViewById(R.id.btn13);
                    mButton14 = findViewById(R.id.btn14);
                    mButton15 = findViewById(R.id.btn15);
                    mButton16 = findViewById(R.id.btn16);
                    mButton17 = findViewById(R.id.btn17);
                    mButton18 = findViewById(R.id.btn18);
                    mButton19 = findViewById(R.id.btn19);
                    mButton20 = findViewById(R.id.btn20);
                    mButton21 = findViewById(R.id.btn21);
                    mButton22 = findViewById(R.id.btn22);
                    mButton23 = findViewById(R.id.btn23);
                    mButton24 = findViewById(R.id.btn24);
//				mButton25 = (Button) findViewById(R.id.btn25);
//				mButton26 = (Button) findViewById(R.id.btn26);
//				mButton27 = (Button) findViewById(R.id.btn27);
//				mButton28 = (Button) findViewById(R.id.btn28);
//				mButton29 = (Button) findViewById(R.id.btn29);
//				mButton30 = (Button) findViewById(R.id.btn30);
//				mButton31 = (Button) findViewById(R.id.btn31);
//				mButton32 = (Button) findViewById(R.id.btn32);
                    mButton33 = findViewById(R.id.btn33);
//				mButton34 = (Button) findViewById(R.id.btn34);
                    mButton35 = findViewById(R.id.btn35);
//				mButton36 = (Button) findViewById(R.id.btn36);
//				mButton37 = (Button) findViewById(R.id.btn37);
//				mButton38 = (Button) findViewById(R.id.btn38);
//				mButton39 = (Button) findViewById(R.id.btn39);
//				mButton40 = (Button) findViewById(R.id.btn40);
//				mButton41 = (Button) findViewById(R.id.btn41);
                    mButton42 = findViewById(R.id.btn42);
                    mButton43 = findViewById(R.id.btn43);
                    mButton44 = findViewById(R.id.btn44);
                    mButton45 = findViewById(R.id.btn45);
                    mButton46 = findViewById(R.id.btn46);
                    mButton47 = findViewById(R.id.btn47);
                    mButton48 = findViewById(R.id.btn48);
                    mButton49 = findViewById(R.id.btn49);
                    mButton50 = findViewById(R.id.btn50);
                    mButton51 = findViewById(R.id.btn51);
                    mButton52 = findViewById(R.id.btn52);
                    mButton53 = findViewById(R.id.btn53);
//				mButton54 = (Button) findViewById(R.id.btn54);
//				mButton55 = (Button) findViewById(R.id.btn55);
//				mButton56 = (Button) findViewById(R.id.btn56);
//				mButton57 = (Button) findViewById(R.id.btn57);
//				mButton58 = (Button) findViewById(R.id.btn58);
//				mButton59 = (Button) findViewById(R.id.btn59);
//				mButton60 = (Button) findViewById(R.id.btn60);
//				mButton61 = (Button) findViewById(R.id.btn61);
//				mButton62 = (Button) findViewById(R.id.btn62);
                    mButton63 = findViewById(R.id.btn63);
                    mButton64 = findViewById(R.id.btn64);
                    mButton65 = findViewById(R.id.btn65);
                    mButton66 = findViewById(R.id.btn66);
                    mButton67 = findViewById(R.id.btn67);
                    mButton68 = findViewById(R.id.btn68);
//				mButton69 = (Button) findViewById(R.id.btn69);
//				mButton70 = (Button) findViewById(R.id.btn70);
//				mButton71 = (Button) findViewById(R.id.btn71);
//				mButton72 = (Button) findViewById(R.id.btn72);
//				mButton73 = (Button) findViewById(R.id.btn73);
//				mButton74 = (Button) findViewById(R.id.btn74);
//				mButton75 = (Button) findViewById(R.id.btn75);
//				mButton76 = (Button) findViewById(R.id.btn76);
//				mButton77 = (Button) findViewById(R.id.btn77);
                    mButton78 = findViewById(R.id.btn78);
                    mButton79 = findViewById(R.id.btn79);
                    mButton80 = findViewById(R.id.btn80);
                    mButton81 = findViewById(R.id.btn81);
                    mButton82 = findViewById(R.id.btn82);
                    mButton83 = findViewById(R.id.btn83);
                    //mButton84 = (Button) findViewById(R.id.btn84);
                    //mButton85 = (Button) findViewById(R.id.btn85);
                    //mButton86 = (Button) findViewById(R.id.btn86);
                    //mButton87 = (Button) findViewById(R.id.btn87);
                    //mButton88 = (Button) findViewById(R.id.btn88);
                    //mButton89 = (Button) findViewById(R.id.btn89);
                    //mButton90 = (Button) findViewById(R.id.btn90);
                    //mButton91 = (Button) findViewById(R.id.btn91);
                    //mButton92 = (Button) findViewById(R.id.btn92);
                    mButton93 = findViewById(R.id.btn93);
                    mButton94 = findViewById(R.id.btn94);
                    mButton95 = findViewById(R.id.btn95);
                    mButton96 = findViewById(R.id.btn96);
                    mButton97 = findViewById(R.id.btn97);
                    mButton98 = findViewById(R.id.btn98);
//				mButton99 = (Button) findViewById(R.id.btn99);
//				mButton100 = (Button) findViewById(R.id.btn100);
//				mButton101 = (Button) findViewById(R.id.btn101);
//				mButton102 = (Button) findViewById(R.id.btn102);
//				mButton103 = (Button) findViewById(R.id.btn103);
//				mButton104 = (Button) findViewById(R.id.btn104);
//				mButton105 = (Button) findViewById(R.id.btn105);
//				mButton106 = (Button) findViewById(R.id.btn106);
//				mButton107 = (Button) findViewById(R.id.btn107);
//				mButton108 = (Button) findViewById(R.id.btn108);
//				mButton109 = (Button) findViewById(R.id.btn109);
//				mButton110 = (Button) findViewById(R.id.btn110);
//				mButton111 = (Button) findViewById(R.id.btn111);
//				mButton112 = (Button) findViewById(R.id.btn112);
                    mButton153 = findViewById(R.id.btn153);
                    mButton154 = findViewById(R.id.btn154);
                    mButton155 = findViewById(R.id.btn155);
                    mButton156 = findViewById(R.id.btn156);


                    findViewById(R.id.marcoset5ok).setOnClickListener(new ButtonListener());

                    findViewById(R.id.s26).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s27).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s28).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s29).setOnClickListener(new ButtonListener());
                    findViewById(R.id.s30).setOnClickListener(new ButtonListener());


                    lamp = getSharedPreferences(portSwitch + lampdata, 0);
                    mButton01.setText(lamp.getString(btn01name, ""));
                    mButton02.setText(lamp.getString(btn02name, ""));
                    mButton03.setText(lamp.getString(btn03name, ""));
                    mButton04.setText(lamp.getString(btn04name, ""));
                    mButton05.setText(lamp.getString(btn05name, ""));
                    mButton06.setText(lamp.getString(btn06name, ""));
                    mButton07.setText(lamp.getString(btn07name, ""));
                    mButton08.setText(lamp.getString(btn08name, ""));
                    mButton09.setText(lamp.getString(btn09name, ""));
                    mButton10.setText(lamp.getString(btn10name, ""));
                    mButton11.setText(lamp.getString(btn11name, ""));
                    mButton12.setText(lamp.getString(btn12name, ""));

                    setting = getSharedPreferences(portSwitch + custom1data, 0);

                    cus1name = findViewById(R.id.cus1name);
                    cus1name.setText(setting.getString(custom1name, ""));
//				mButton54.setText(setting.getString(btn54name, ""));
//				mButton55.setText(setting.getString(btn55name, ""));
//				mButton56.setText(setting.getString(btn56name, ""));
//				mButton57.setText(setting.getString(btn57name, ""));
//				mButton58.setText(setting.getString(btn58name, ""));
//				mButton59.setText(setting.getString(btn59name, ""));
//				mButton60.setText(setting.getString(btn60name, ""));
//				mButton61.setText(setting.getString(btn61name, ""));
//				mButton62.setText(setting.getString(btn62name, ""));
                    mButton63.setText(setting.getString(btn63name, ""));
                    mButton64.setText(setting.getString(btn64name, ""));
                    mButton65.setText(setting.getString(btn65name, ""));
                    mButton66.setText(setting.getString(btn66name, ""));
                    mButton67.setText(setting.getString(btn67name, ""));
                    mButton68.setText(setting.getString(btn68name, ""));
//				mButton99.setText(setting.getString(btn99name, ""));
//				mButton100.setText(setting.getString(btn100name, ""));
//				mButton101.setText(setting.getString(btn101name, ""));
//				mButton102.setText(setting.getString(btn102name, ""));
//				mButton103.setText(setting.getString(btn103name, ""));
//				mButton104.setText(setting.getString(btn104name, ""));

                    settingb = getSharedPreferences(portSwitch + custom2data, 0);

                    cus2name = findViewById(R.id.cus2name);
                    cus2name.setText(settingb.getString(custom2name, ""));
//				 mButton69.setText(settingb.getString(btn69name, ""));
//				 mButton70.setText(settingb.getString(btn70name, ""));
//				 mButton71.setText(settingb.getString(btn71name, ""));
//				 mButton72.setText(settingb.getString(btn72name, ""));
//				 mButton73.setText(settingb.getString(btn73name, ""));
//				 mButton74.setText(settingb.getString(btn74name, ""));
//				 mButton75.setText(settingb.getString(btn75name, ""));
//				 mButton76.setText(settingb.getString(btn76name, ""));
//				 mButton77.setText(settingb.getString(btn77name, ""));
                    mButton78.setText(settingb.getString(btn78name, ""));
                    mButton79.setText(settingb.getString(btn79name, ""));
                    mButton80.setText(settingb.getString(btn80name, ""));
                    mButton81.setText(settingb.getString(btn81name, ""));
                    mButton82.setText(settingb.getString(btn82name, ""));
                    mButton83.setText(settingb.getString(btn83name, ""));
//				 mButton105.setText(settingb.getString(btn105name, ""));
//				 mButton106.setText(settingb.getString(btn106name, ""));
//				 mButton107.setText(settingb.getString(btn107name, ""));
//				 mButton108.setText(settingb.getString(btn108name, ""));
//				 mButton109.setText(settingb.getString(btn109name, ""));
//				 mButton110.setText(settingb.getString(btn110name, ""));

                    setting2 = getSharedPreferences(portSwitch + custom3data, 0);

                    cus3name = findViewById(R.id.cus3name);
                    cus3name.setText(setting2.getString(custom3name, ""));
                    // mButton84.setText(setting2.getString(btn84name, ""));
                    //mButton85.setText(setting2.getString(btn85name, ""));
                    //mButton86.setText(setting2.getString(btn86name, ""));
                    //mButton87.setText(setting2.getString(btn87name, ""));
                    //mButton88.setText(setting2.getString(btn88name, ""));
                    //mButton89.setText(setting2.getString(btn89name, ""));
                    //mButton90.setText(setting2.getString(btn90name, ""));
                    //mButton91.setText(setting2.getString(btn91name, ""));
                    //mButton92.setText(setting2.getString(btn92name, ""));
                    mButton93.setText(setting2.getString(btn93name, ""));
                    mButton94.setText(setting2.getString(btn94name, ""));
                    mButton95.setText(setting2.getString(btn95name, ""));
                    mButton96.setText(setting2.getString(btn96name, ""));
                    mButton97.setText(setting2.getString(btn97name, ""));
                    mButton98.setText(setting2.getString(btn98name, ""));
                    //mButton111.setText(setting2.getString(btn111name, ""));
                    //mButton112.setText(setting2.getString(btn112name, ""));
                    //mButton113.setText(setting2.getString(btn113name, ""));
                    //mButton114.setText(setting2.getString(btn114name, ""));
                    //mButton115.setText(setting2.getString(btn115name, ""));
                    //mButton116.setText(setting2.getString(btn116name, ""));

                    airseting = getSharedPreferences(portSwitch + airdata, 0);
                    mButton17.setText(airseting.getString(btn17name, ""));
                    mButton18.setText(airseting.getString(btn18name, ""));
                    mButton19.setText(airseting.getString(btn19name, ""));
                    mButton20.setText(airseting.getString(btn20name, ""));
                    mButton21.setText(airseting.getString(btn21name, ""));
                    mButton22.setText(airseting.getString(btn22name, ""));


                    lamp = getSharedPreferences(portSwitch + lampdata, 0);
                    imagebtn1 = findViewById(R.id.imagebtn1);
                    imagebtn2 = findViewById(R.id.imagebtn2);
                    imagebtn3 = findViewById(R.id.imagebtn3);
                    imagebtn4 = findViewById(R.id.imagebtn4);
                    imagebtn5 = findViewById(R.id.imagebtn5);
                    imagebtn6 = findViewById(R.id.imagebtn6);
                    imagebtn7 = findViewById(R.id.imagebtn7);
                    imagebtn8 = findViewById(R.id.imagebtn8);
                    imagebtn9 = findViewById(R.id.imagebtn9);
                    imagebtn10 = findViewById(R.id.imagebtn10);
                    imagebtn11 = findViewById(R.id.imagebtn11);
                    imagebtn12 = findViewById(R.id.imagebtn12);


                    lamp = getSharedPreferences(portSwitch + lampdata, 0);

                    imagebtn1 = findViewById(R.id.imagebtn1);
                    imagebtn2 = findViewById(R.id.imagebtn2);
                    imagebtn3 = findViewById(R.id.imagebtn3);
                    imagebtn4 = findViewById(R.id.imagebtn4);
                    imagebtn5 = findViewById(R.id.imagebtn5);
                    imagebtn6 = findViewById(R.id.imagebtn6);
                    imagebtn7 = findViewById(R.id.imagebtn7);
                    imagebtn8 = findViewById(R.id.imagebtn8);
                    imagebtn9 = findViewById(R.id.imagebtn9);
                    imagebtn10 = findViewById(R.id.imagebtn10);
                    imagebtn11 = findViewById(R.id.imagebtn11);
                    imagebtn12 = findViewById(R.id.imagebtn12);

                    getLampBtnPic();
                    readData(10);

                    break;
            }
            return true;
        }

    }

    public class ButtonListener implements OnClickListener {


        @SuppressWarnings("deprecation")
        public void onClick(View v) {

            //YILINEDIT-voiceRecg
            if (isVoiceRecMode) {
                lamp = getSharedPreferences(portSwitch + lampdata, 0);
                marcoset = getSharedPreferences(portSwitch + marcosetdata, 0);
                marco1set = getSharedPreferences(portSwitch + marcoset1data, 0);
                marco2set = getSharedPreferences(portSwitch + marcoset2data, 0);
                marco3set = getSharedPreferences(portSwitch + marcoset3data, 0);
                marco4set = getSharedPreferences(portSwitch + marcoset4data, 0);
                marco5set = getSharedPreferences(portSwitch + marcoset5data, 0);
                AlertDialog alertDialog;
                switch (v.getId()) {
                    case R.id.btn01:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn01name, "").isEmpty() ? "此燈" : lamp.getString(btn01name, ""), "a", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn02:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn02name, "").isEmpty() ? "此燈" : lamp.getString(btn02name, ""), "b", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn03:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn03name, "").isEmpty() ? "此燈" : lamp.getString(btn03name, ""), "c", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn04:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn04name, "").isEmpty() ? "此燈" : lamp.getString(btn04name, ""), "d", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn05:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn05name, "").isEmpty() ? "此燈" : lamp.getString(btn05name, ""), "e", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn06:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn06name, "").isEmpty() ? "此燈" : lamp.getString(btn06name, ""), "f", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn07:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn07name, "").isEmpty() ? "此燈" : lamp.getString(btn07name, ""), "g", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn08:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn08name, "").isEmpty() ? "此燈" : lamp.getString(btn08name, ""), "h", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn09:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn09name, "").isEmpty() ? "此燈" : lamp.getString(btn09name, ""), "i", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn10:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn10name, "").isEmpty() ? "此燈" : lamp.getString(btn10name, ""), "j", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn11:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn11name, "").isEmpty() ? "此燈" : lamp.getString(btn11name, ""), "k", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.btn12:
                        alertDialog = getVoiceAlertDialog(lamp.getString(btn12name, "").isEmpty() ? "此燈" : lamp.getString(btn12name, ""), "l", true);
                        if (alertDialog != null) {
                            alertDialog.show();
                        }
                        break;
                    case R.id.home1:
                        Toast.makeText(BluetoothChat.this, "請先解除語音學習模式", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.lampseting:
                        Toast.makeText(BluetoothChat.this, "請先解除語音學習模式", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.home0:
                        Toast.makeText(BluetoothChat.this, "請先解除語音學習模式", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.f1:
                        alertDialog = getVoiceAlertDialog(marcoset.getString(marconame, "").isEmpty() ? "情境1" : marcoset.getString(marconame, ""), "macro0", false);
                        break;
                    case R.id.f2:
                        alertDialog = getVoiceAlertDialog(marco1set.getString(marco1name, "").isEmpty() ? "情境2" : marco1set.getString(marco1name, ""), "macro1", false);
                        break;
                    case R.id.f3:
                        alertDialog = getVoiceAlertDialog(marco2set.getString(marco2name, "").isEmpty() ? "情境3" : marco2set.getString(marco2name, ""), "macro2", false);
                        break;
                    case R.id.f4:
                        alertDialog = getVoiceAlertDialog(marco3set.getString(marco3name, "").isEmpty() ? "情境4" : marco3set.getString(marco3name, ""), "macro3", false);
                        break;
                    case R.id.f5:
                        alertDialog = getVoiceAlertDialog(marco4set.getString(marco4name, "").isEmpty() ? "情境5" : marco4set.getString(marco4name, ""), "macro4", false);
                        break;
                    case R.id.f6:
                        alertDialog = getVoiceAlertDialog(marco5set.getString(marco5name, "").isEmpty() ? "情境6" : marco5set.getString(marco5name, ""), "macro5", false);
                        break;
                }
            } else
                switch (v.getId()) {
                    case R.id.main1:
                        restart(1);
                        break;
                    case R.id.main2:
                        restart(2);
                        break;
                    case R.id.main3:
                        restart(3);
                        break;
                    case R.id.main4:
                        restart(4);
                        break;
                    case R.id.main5:
                        restart(5);
                        break;
                    case R.id.main6:
                        restart(6);
                        break;
                    case R.id.save:
                        if (isValidIPAddr(((EditText) findViewById(R.id.ip)).getText().toString()) && !"".equals(((EditText) findViewById(R.id.ip)).getText().toString())) {
                            saveData(14);
                            UtilMessageDialog.showToast(BluetoothChat.this, "連線參數儲存");
                        } else {
                            UtilMessageDialog.showToast(BluetoothChat.this, "連線參數設定格式不符");
                        }

                        break;

                    case R.id.open:
                        saveData(14);
                        restart(0);
                        new MySocketThread(mHandler, BluetoothChat.this, 0, "");

                        break;

                    case R.id.btn_IR_Switch:
                        String status = PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).getString(QuickstartPreferences.IR_STATUS, "關閉");
                        String str_IR_status = "";
                        int color;
                        if (status.equals("開啟")) {
                            ((Button) findViewById(R.id.btn_IR_Switch)).setText("開啟");
                            str_IR_status = "關閉";
                            color = Color.RED;
                        } else {
                            ((Button) findViewById(R.id.btn_IR_Switch)).setText("關閉");
                            str_IR_status = "開啟";
                            color = Color.GREEN;
                        }

                        PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).edit().putString(
                                QuickstartPreferences.IR_STATUS, str_IR_status).apply();

                        ((TextView) findViewById(R.id.tv_IR_status)).setText(str_IR_status);
                        ((TextView) findViewById(R.id.tv_IR_status)).setTextColor(color);

                        break;

                    case R.id.btn_ap:
                        APSettingDialog.getInstance().show(getSupportFragmentManager(), "AP設定");
                        break;


                    case R.id.s1:
                        String delay1 = ("'");
                        marcosetbtn.append(delay1);
                        break;

                    case R.id.s2:
                        String delay2 = ("(");
                        marcosetbtn.append(delay2);
                        break;

                    case R.id.s3:
                        String delay3 = (")");
                        marcosetbtn.append(delay3);
                        break;

                    case R.id.s4:
                        String delay4 = ("*");
                        marcosetbtn.append(delay4);
                        break;

                    case R.id.s5:
                        String delay5 = ("+");
                        marcosetbtn.append(delay5);
                        break;

                    case R.id.s6:
                        String delay6 = ("'");
                        marco1setbtn.append(delay6);
                        break;

                    case R.id.s7:
                        String delay7 = ("(");
                        marco1setbtn.append(delay7);
                        break;

                    case R.id.s8:
                        String delay8 = (")");
                        marco1setbtn.append(delay8);
                        break;

                    case R.id.s9:
                        String delay9 = ("*");
                        marco1setbtn.append(delay9);
                        break;

                    case R.id.s10:
                        String delay10 = ("+");
                        marco1setbtn.append(delay10);
                        break;

                    case R.id.s11:
                        String delay11 = ("'");
                        marco2setbtn.append(delay11);
                        break;

                    case R.id.s12:
                        String delay12 = ("(");
                        marco2setbtn.append(delay12);
                        break;

                    case R.id.s13:
                        String delay13 = (")");
                        marco2setbtn.append(delay13);
                        break;

                    case R.id.s14:
                        String delay14 = ("*");
                        marco2setbtn.append(delay14);
                        break;

                    case R.id.s15:
                        String delay15 = ("+");
                        marco2setbtn.append(delay15);
                        break;

                    case R.id.s16:
                        String delay16 = ("'");
                        marco3setbtn.append(delay16);
                        break;

                    case R.id.s17:
                        String delay17 = ("(");
                        marco3setbtn.append(delay17);
                        break;

                    case R.id.s18:
                        String delay18 = (")");
                        marco3setbtn.append(delay18);
                        break;

                    case R.id.s19:
                        String delay19 = ("*");
                        marco3setbtn.append(delay19);
                        break;

                    case R.id.s20:
                        String delay20 = ("+");
                        marco3setbtn.append(delay20);
                        break;

                    case R.id.s21:
                        String delay21 = ("'");
                        marco4setbtn.append(delay21);
                        break;

                    case R.id.s22:
                        String delay22 = ("(");
                        marco4setbtn.append(delay22);
                        break;

                    case R.id.s23:
                        String delay23 = (")");
                        marco4setbtn.append(delay23);
                        break;

                    case R.id.s24:
                        String delay24 = ("*");
                        marco4setbtn.append(delay24);
                        break;

                    case R.id.s25:
                        String delay25 = ("+");
                        marco4setbtn.append(delay25);
                        break;

                    case R.id.s26:
                        String delay26 = ("'");
                        marco5setbtn.append(delay26);
                        break;

                    case R.id.s27:
                        String delay27 = ("(");
                        marco5setbtn.append(delay27);
                        break;

                    case R.id.s28:
                        String delay28 = (")");
                        marco5setbtn.append(delay28);
                        break;

                    case R.id.s29:
                        String delay29 = ("*");
                        marco5setbtn.append(delay29);
                        break;

                    case R.id.s30:
                        String delay30 = ("+");
                        marco5setbtn.append(delay30);
                        break;

                    case R.id.home0:
                        MenuDialog.getInstance().show(getSupportFragmentManager(), "menu");
                        break;

                    case R.id.home1:
                        MenuDialog.getInstance().show(getSupportFragmentManager(), "menu");
                        break;

                    case R.id.home2:
                        MenuDialog.getInstance().show(getSupportFragmentManager(), "menu");
                        break;

                    case R.id.home3:
                        MenuDialog.getInstance().show(getSupportFragmentManager(), "menu");
                        break;

                    case R.id.home10:
                        MenuDialog.getInstance().show(getSupportFragmentManager(), "menu");
                        break;

                    case R.id.home5:
                        MenuDialog.getInstance().show(getSupportFragmentManager(), "menu");
                        break;

                    case R.id.home6:
                        MenuDialog.getInstance().show(getSupportFragmentManager(), "menu");
                        break;

                    case R.id.home7:
                        MenuDialog.getInstance().show(getSupportFragmentManager(), "menu");
                        break;

                    case R.id.home8:
                        MenuDialog.getInstance().show(getSupportFragmentManager(), "menu");
                        break;

                    case R.id.home9:
                        MenuDialog.getInstance().show(getSupportFragmentManager(), "menu");
                        break;

                    case R.id.btn01:
                        String view = ("a3");
                        sendmessage(view);
                        sendMessageAfter1s("W+j?j");
                        break;

                    case R.id.btn02:
                        String view2 = ("b3");
                        sendmessage(view2);
                        sendMessageAfter1s("W+k?k");
                        break;

                    case R.id.btn03:
                        String view3 = ("c3");
                        sendmessage(view3);
                        sendMessageAfter1s("W+l?l");
                        break;

                    case R.id.btn04:
                        String view4 = ("d3");
                        sendmessage(view4);
                        sendMessageAfter1s("W+m?m");
                        break;

                    case R.id.btn05:
                        String view5 = ("e3");
                        sendmessage(view5);
                        sendMessageAfter1s("W+n?n");
                        break;

                    case R.id.btn06:
                        String view6 = ("f3");
                        sendmessage(view6);
                        sendMessageAfter1s("W+o?o");
                        break;

                    case R.id.btn07:
                        String view7 = ("g3");
                        sendmessage(view7);
                        sendMessageAfter1s("W+p?p");
                        break;

                    case R.id.btn08:
                        String view8 = ("h3");
                        sendmessage(view8);
                        sendMessageAfter1s("W+q?q");
                        break;

                    case R.id.btn09:
                        String view9 = ("i3");
                        sendmessage(view9);
                        sendMessageAfter1s("W+r?r");
                        break;

                    case R.id.btn10:
                        String view10 = ("j3");
                        sendmessage(view10);
                        sendMessageAfter1s("W+s?s");
                        break;

                    case R.id.btn11:
                        String view11 = ("k3");
                        sendmessage(view11);
                        sendMessageAfter1s("W+t?t");
                        break;

                    case R.id.btn12:
                        String view12 = ("l3");
                        sendmessage(view12);
                        sendMessageAfter1s("W+u?u");
                        break;


                    case R.id.btn13:
                        sendmessage("Aa");
                        break;

                    case R.id.btn14:
                        sendmessage("Ad");
                        break;

                    case R.id.btn15:
                        sendmessage("Ac");
                        break;

                    case R.id.btn16:
                        sendmessage("Af");
                        break;

                    case R.id.btn17:

                        sendmessage("Ag");
                        break;

                    case R.id.btn18:
                        sendmessage("Ah");
                        break;

                    case R.id.btn19:
                        sendmessage("Ai");
                        break;

                    case R.id.btn20:
                        sendmessage("Aj");
                        break;

                    case R.id.btn21:
                        sendmessage("Ak");
                        break;

                    case R.id.btn22:
                        sendmessage("Al");
                        break;

                    case R.id.btn23:
                        sendmessage("Ab");
                        break;

                    case R.id.btn24:
                        sendmessage("Ae");
                        break;

//    		case R.id.btn25:
//    			String view25 = ("Am");
//                String message25 = view25.toString();
//                sendMessage(message25);
//    			break;
//
//    		case R.id.btn26:
//    			String view26 = ("An");
//                String message26 = view26.toString();
//                sendMessage(message26);
//    			break;
//
//    		case R.id.btn27:
//    			String view27 = ("Ao");
//                String message27 = view27.toString();
//                sendMessage(message27);
//    			break;
//
//    		case R.id.btn28:
//    			String view28 = ("Ap");
//                String message28 = view28.toString();
//                sendMessage(message28);
//    			break;
//
//    		case R.id.btn29:
//    			String view29 = ("Aq");
//                String message29 = view29.toString();
//                sendMessage(message29);
//    			break;
//
//    		case R.id.btn30:
//    			String view30 = ("Ar");
//                String message30 = view30.toString();
//                sendMessage(message30);
//    			break;
//
//    		case R.id.btn31:
//    			String view31 = ("As");
//                String message31 = view31.toString();
//                sendMessage(message31);
//    			break;

                    //case R.id.btn32:
                    //String view32 = ("At");
                    //String message32 = view32.toString();
                    //sendMessage(message32);
                    //break;

                    case R.id.btn33:
                        sendmessage("Tb");
                        break;

//    		case R.id.btn34:
//    			String view34 = ("Tb");
//                String message34 = view34.toString();
//                sendMessage(message34);
//    			break;

                    case R.id.btn35:
                        sendmessage("Te");
                        break;

//    		case R.id.btn36:
//    			String view36 = ("Td");
//                String message36 = view36.toString();
//                sendMessage(message36);
//    			break;
//
//    		case R.id.btn37:
//    			String view37 = ("Te");
//                String message37 = view37.toString();
//                sendMessage(message37);
//    			break;
//
//    		case R.id.btn38:
//    			String view38 = ("Tf");
//                String message38 = view38.toString();
//                sendMessage(message38);
//    			break;
//
//    		case R.id.btn40:
//    			String view40 = ("Th");
//                String message40 = view40.toString();
//                sendMessage(message40);
//    			break;
//
//    			//tv home
//    		case R.id.button25:
//    			String view39 = ("Tg");
//    			String message39 = view39.toString();
//                sendMessage(message39);
//    			break;
//    			//tv 靜音
//    		case R.id.button26:
//    			String view41 = ("Ti");
//                String message41 = view41.toString();
//                sendMessage(message41);
//    			break;

                    case R.id.btn42:
                        sendmessage("Tg");
                        break;

                    case R.id.btn43:
                        sendmessage("Th");
                        break;

                    case R.id.btn44:
                        sendmessage("Ti");
                        break;

                    case R.id.btn45:
                        sendmessage("Tj");
                        break;

                    case R.id.btn46:
                        sendmessage("Tk");
                        break;

                    case R.id.btn47:
                        sendmessage("Tl");
                        break;

                    case R.id.btn48:
                        sendmessage("Tm");
                        break;

                    case R.id.btn49:
                        sendmessage("Tn");
                        break;

                    case R.id.btn50:
                        sendmessage("To");
                        break;

                    case R.id.btn51:
                        sendmessage("Tp");
                        break;

                    case R.id.btn52:
                        sendmessage("Tq");
                        break;

                    case R.id.btn53:
                        sendmessage("Tr");
                        break;

//    		case R.id.btn54:
//    			String view54 = ("Xa");
//                String message54 = view54.toString();
//                sendMessage(message54);
//    				break;
//
//    		case R.id.btn55:
//    			String view55 = ("Xb");
//                String message55 = view55.toString();
//                sendMessage(message55);
//    				break;
//
//    		case R.id.btn56:
//    			String view56 = ("Xc");
//                String message56 = view56.toString();
//                sendMessage(message56);
//    				break;
//
//    		case R.id.btn57:
//    			String view57 = ("Xd");
//                String message57 = view57.toString();
//                sendMessage(message57);
//    				break;
//
//    		case R.id.btn58:
//    			String view58 = ("Xe");
//                String message58 = view58.toString();
//                sendMessage(message58);
//    				break;
//
//    		case R.id.btn59:
//    			String view59 = ("Xf");
//                String message59 = view59.toString();
//                sendMessage(message59);
//    				break;
//
//    		case R.id.btn60:
//    			String view60 = ("Xg");
//                String message60 = view60.toString();
//                sendMessage(message60);
//    				break;
//
//    		case R.id.btn61:
//    			String view61 = ("Xh");
//                String message61 = view61.toString();
//                sendMessage(message61);
//    				break;
//
//    		case R.id.btn62:
//    			String view62 = ("Xi");
//                String message62 = view62.toString();
//                sendMessage(message62);
//    				break;

                    case R.id.btn63:
                        sendmessage("Xa");
                        break;

                    case R.id.btn64:
                        sendmessage("Xb");
                        break;

                    case R.id.btn65:
                        sendmessage("Xc");
                        break;

                    case R.id.btn66:
                        sendmessage("Xd");
                        break;

                    case R.id.btn67:
                        sendmessage("Xe");
                        break;

                    case R.id.btn68:
                        sendmessage("Xf");
                        break;

//    		case R.id.btn69:
//    			String view69 = ("Ya");
//                String message69 = view69.toString();
//                sendMessage(message69);
//    				break;
//
//    		case R.id.btn70:
//    			String view70 = ("Yb");
//                String message70 = view70.toString();
//                sendMessage(message70);
//    				break;
//
//    		case R.id.btn71:
//    			String view71 = ("Yc");
//                String message71 = view71.toString();
//                sendMessage(message71);
//    				break;
//
//    		case R.id.btn72:
//    			String view72 = ("Yd");
//                String message72 = view72.toString();
//                sendMessage(message72);
//    				break;
//
//    		case R.id.btn73:
//    			String view73 = ("Ye");
//                String message73 = view73.toString();
//                sendMessage(message73);
//    				break;
//
//    		case R.id.btn74:
//    			String view74 = ("Yf");
//                String message74 = view74.toString();
//                sendMessage(message74);
//    				break;
//
//    		case R.id.btn75:
//    			String view75 = ("Yg");
//                String message75 = view75.toString();
//                sendMessage(message75);
//    				break;
//
//    		case R.id.btn76:
//    			String view76 = ("Yh");
//                String message76 = view76.toString();
//                sendMessage(message76);
//    				break;
//
//    		case R.id.btn77:
//    			String view77 = ("Yi");
//                String message77 = view77.toString();
//                sendMessage(message77);
//    				break;

                    case R.id.btn78:
                        sendmessage("Ya");
                        break;

                    case R.id.btn79:
                        sendmessage("Yb");
                        break;

                    case R.id.btn80:
                        sendmessage("Yc");
                        break;

                    case R.id.btn81:
                        sendmessage("Yd");
                        break;

                    case R.id.btn82:
                        sendmessage("Ye");
                        break;

                    case R.id.btn83:
                        sendmessage("Yf");
                        break;

//    		case R.id.btn84:
//    			String view84 = ("Za");
//                String message84 = view84.toString();
//                sendMessage(message84);
//    				break;
//
//    		case R.id.btn85:
//    			String view85 = ("Zb");
//                String message85 = view85.toString();
//                sendMessage(message85);
//    				break;
//
//    		case R.id.btn86:
//    			String view86 = ("Zc");
//                String message86 = view86.toString();
//                sendMessage(message86);
//    				break;
//
//    		case R.id.btn87:
//    			String view87 = ("Zd");
//                String message87 = view87.toString();
//                sendMessage(message87);
//    				break;
//
//    		case R.id.btn88:
//    			String view88 = ("Ze");
//                String message88 = view88.toString();
//                sendMessage(message88);
//    				break;
//
//    		case R.id.btn89:
//    			String view89 = ("Zf");
//                String message89 = view89.toString();
//                sendMessage(message89);
//    				break;
//
//    		case R.id.btn90:
//    			String view90 = ("Zg");
//                String message90 = view90.toString();
//                sendMessage(message90);
//    				break;
//
//    		case R.id.btn91:
//    			String view91 = ("Zh");
//                String message91 = view91.toString();
//                sendMessage(message91);
//    				break;
//
//    		case R.id.btn92:
//    			String view92 = ("Zi");
//                String message92 = view92.toString();
//                sendMessage(message92);
//    				break;

                    case R.id.btn93:
                        sendmessage("Za");
                        break;

                    case R.id.btn94:
                        sendmessage("Zb");
                        break;

                    case R.id.btn95:
                        sendmessage("Zc");
                        break;

                    case R.id.btn96:
                        sendmessage("Zd");
                        break;

                    case R.id.btn97:
                        sendmessage("Ze");
                        break;

                    case R.id.btn98:
                        sendmessage("Zf");
                        break;

//    		case R.id.btn99:
//    			String view99 = ("Xp");
//                String message99 = view99.toString();
//                sendMessage(message99);
//    				break;
//
//    		case R.id.btn100:
//    			String view100 = ("Xq");
//                String message100 = view100.toString();
//                sendMessage(message100);
//    				break;
//
//    		case R.id.btn101:
//    			String view101 = ("Xr");
//                String message101 = view101.toString();
//                sendMessage(message101);
//    				break;
//
//    		case R.id.btn102:
//    			String view102 = ("Xs");
//                String message102 = view102.toString();
//                sendMessage(message102);
//    				break;
//
//    		case R.id.btn103:
//    			String view103 = ("Xt");
//                String message103 = view103.toString();
//                sendMessage(message103);
//    				break;
//
//    		case R.id.btn104:
//    			String view104 = ("Xu");
//                String message104 = view104.toString();
//                sendMessage(message104);
//    				break;
//
//    		case R.id.btn105:
//    			String view105 = ("Yp");
//                String message105 = view105.toString();
//                sendMessage(message105);
//    				break;
//
//    		case R.id.btn106:
//    			String view106 = ("Yq");
//                String message106 = view106.toString();
//                sendMessage(message106);
//    				break;
//
//    		case R.id.btn107:
//    			String view107 = ("Yr");
//                String message107 = view107.toString();
//                sendMessage(message107);
//    				break;
//
//    		case R.id.btn108:
//    			String view108 = ("Ys");
//                String message108 = view108.toString();
//                sendMessage(message108);
//    				break;
//
//    		case R.id.btn109:
//    			String view109 = ("Yt");
//                String message109 = view109.toString();
//                sendMessage(message109);
//    				break;
//
//    		case R.id.btn110:
//    			String view110 = ("Yu");
//                String message110 = view110.toString();
//                sendMessage(message110);
//    				break;
//
//    		case R.id.btn111:
//    			String view111 = ("Zp");
//                String message111 = view111.toString();
//                sendMessage(message111);
//    				break;
//
//    		case R.id.btn112:
//    			String view112 = ("Zq");
//                String message112 = view112.toString();
//                sendMessage(message112);
//    				break;
//
//    		case R.id.btn113:
//    			String view113 = ("Zr");
//                String message113 = view113.toString();
//                sendMessage(message113);
//    				break;
//
//    		case R.id.btn114:
//    			String view114 = ("Zs");
//                String message114 = view114.toString();
//                sendMessage(message114);
//    				break;
//
//    		case R.id.btn115:
//    			String view115 = ("Zt");
//                String message115 = view115.toString();
//                sendMessage(message115);
//    				break;
//
//    		case R.id.btn116:
//    			String view116 = ("Zu");
//                String message116 = view116.toString();
//                sendMessage(message116);
//    				break;

                    //TV 音量跟頻道
                    case R.id.tv_btn_func:
                        arr_temp.clear();
                        arr_temp.add("開啟學習");
                        arr_temp.add("關閉學習");
                        arr_temp.add("重新連線");
                        funMenuSelection(arr_temp, "tv");
                        break;


                    case R.id.btn153:
                        sendmessage("Tc");
                        break;

                    case R.id.btn154:
                        sendmessage("Ta");
                        break;

                    case R.id.btn155:
                        sendmessage("Tf");
                        break;

                    case R.id.btn156:
                        sendmessage("Td");
                        break;

                    case R.id.lampseting:

                        arr_temp.clear();
                        arr_temp.add("自訂按鍵");
                        arr_temp.add("重新連線");
                        funMenuSelection(arr_temp, "lamp");

                        break;


                    case R.id.custom1set:
                        arr_temp.clear();
                        arr_temp.add("開啟學習");
                        arr_temp.add("關閉學習");
                        arr_temp.add("自訂按鍵");
                        arr_temp.add("重新連線");
                        funMenuSelection(arr_temp, "custom1");
                        break;

                    case R.id.custom2set:
                        arr_temp.clear();
                        arr_temp.add("開啟學習");
                        arr_temp.add("關閉學習");
                        arr_temp.add("自訂按鍵");
                        arr_temp.add("重新連線");
                        funMenuSelection(arr_temp, "custom2");
                        break;

                    case R.id.custom3set:

                        arr_temp.clear();
                        arr_temp.add("開啟學習");
                        arr_temp.add("關閉學習");
                        arr_temp.add("自訂按鍵");
                        arr_temp.add("重新連線");
                        funMenuSelection(arr_temp, "custom3");

                        break;

                    case R.id.airset:

                        arr_temp.clear();
                        arr_temp.add("開啟學習");
                        arr_temp.add("關閉學習");
                        arr_temp.add("自訂按鍵");
                        arr_temp.add("重新連線");
                        funMenuSelection(arr_temp, "air");

                        break;


                    case R.id.marco1:

                        marcoset = getSharedPreferences(portSwitch + marcosetdata, 0);
                        String a = marcoset.getString(marcobtn, "");
                        sendmessage("@!" + a + "@");

                        break;

                    case R.id.marco2:

                        marco1set = getSharedPreferences(portSwitch + marcoset1data, 0);
                        String b = marco1set.getString(marco1btn, "");
                        sendmessage("@" + "\"" + b + "@");

                        break;

                    case R.id.marco3:

                        marco2set = getSharedPreferences(portSwitch + marcoset2data, 0);
                        String c = marco2set.getString(marco2btn, "");
                        sendmessage("@#" + c + "@");

                        break;

                    case R.id.marco4:

                        marco3set = getSharedPreferences(portSwitch + marcoset3data, 0);
                        String d = marco3set.getString(marco3btn, "");
                        sendmessage("@$" + d + "@");

                        break;

                    case R.id.marco5:

                        marco4set = getSharedPreferences(portSwitch + marcoset4data, 0);
                        String e = marco4set.getString(marco4btn, "");
                        sendmessage("@%" + e + "@");

                        break;

                    case R.id.marco6:

                        marco5set = getSharedPreferences(portSwitch + marcoset5data, 0);
                        String f = marco5set.getString(marco5btn, "");
                        sendmessage("@&" + f + "@");

                        break;

                    case R.id.f1:

                        marcoset = getSharedPreferences(portSwitch + marcosetdata, 0);
                        //YILINEDIT

                        sendMacro(marcoset.getString(marcobtn, ""), handler_sendMacro);
//                        sendMessage("Ma");

                        break;

                    case R.id.f2:

                        marco1set = getSharedPreferences(portSwitch + marcoset1data, 0);
                        //String b = marco1set.getString(marco1btn,"");
                        sendMacro(marco1set.getString(marco1btn, ""), handler_sendMacro);

                        break;

                    case R.id.f3:

                        marco2set = getSharedPreferences(portSwitch + marcoset2data, 0);
                        //String c = marco2set.getString(marco2btn,"");
                        sendMacro(marco2set.getString(marco2btn, ""), handler_sendMacro);

                        break;

                    case R.id.f4:

                        marco3set = getSharedPreferences(portSwitch + marcoset3data, 0);
                        //String d = marco3set.getString(marco3btn,"");
                        sendMacro(marco3set.getString(marco3btn, ""), handler_sendMacro);

                        break;

                    case R.id.f5:

                        marco4set = getSharedPreferences(portSwitch + marcoset4data, 0);
                        //String e = marco4set.getString(marco4btn,"");
                        sendMacro(marco4set.getString(marco4btn, ""), handler_sendMacro);

                        break;

                    case R.id.f6:

                        marco5set = getSharedPreferences(portSwitch + marcoset5data, 0);
                        //String f = marco5set.getString(marco5btn,"");
                        sendMacro(marco5set.getString(marco5btn, ""), handler_sendMacro);

                        break;


                    case R.id.lampok:

                        saveData(1);
                        Intent intentlamp = new Intent();
                        intentlamp.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                        startActivity(intentlamp);
                        finish();

                        break;

                    case R.id.curok:

                        saveData(11);
                        Intent intentcur = new Intent();
                        intentcur.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);

                        startActivity(intentcur);
                        finish();

                        break;


                    case R.id.set00:

                        saveData(2);
                        Intent intent = new Intent();
                        intent.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);

                        startActivity(intent);
                        finish();

                        break;

                    case R.id.set01:

                        saveData(3);
                        Intent intent1 = new Intent();
                        intent1.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                        finish();
                        startActivity(intent1);

                        break;

                    case R.id.set02:

                        saveData(4);
                        Intent intent2 = new Intent();
                        intent2.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);

                        startActivity(intent2);
                        finish();
                        break;

                    case R.id.marcosetok:

                        saveData(5);

                        Intent intent3 = new Intent();
                        intent3.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                        finish();
                        startActivity(intent3);

                        break;

                    case R.id.marcoset1ok:

                        saveData(6);
                        Intent intent4 = new Intent();
                        intent4.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                        finish();
                        startActivity(intent4);

                        break;

                    case R.id.marcoset2ok:

                        saveData(7);
                        Intent intent5 = new Intent();
                        intent5.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                        finish();
                        startActivity(intent5);

                        break;

                    case R.id.marcoset3ok:

                        saveData(8);
                        Intent intent6 = new Intent();
                        intent6.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                        finish();
                        startActivity(intent6);

                        break;

                    case R.id.marcoset4ok:

                        saveData(9);
                        Intent intent7 = new Intent();
                        intent7.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                        finish();
                        startActivity(intent7);

                        break;

                    case R.id.marcoset5ok:

                        saveData(10);
                        Intent intent8 = new Intent();
                        intent8.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                        finish();
                        startActivity(intent8);

                        break;

                    case R.id.airok:
                        saveData(13);
                        Intent intent9 = new Intent();
                        intent9.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                        finish();
                        startActivity(intent9);
                        break;


                    case R.id.door1:
                        String door1 = ("W+sa1");
                        sendmessage(door1);
                        break;

                    case R.id.door2:
                        String door2 = ("W+sb1");
                        sendmessage(door2);
                        break;

                    case R.id.door3:
                        String door3 = ("W+sc1");
                        sendmessage(door3);
                        break;

                    case R.id.lampic1:

                        ((EditText) findViewById(R.id.lampeditpic1)).setText("");
                        Intent lampic1 = new Intent().putExtra("switch", portSwitch);
                        //開啟Pictures畫面Type設定為image
                        lampic1.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic1.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic1, LAMP_PIC1);
                        setResult(RESULT_OK, lampic1);

                        break;

                    case R.id.lampic2:
                        ((EditText) findViewById(R.id.lampeditpic2)).setText("");
                        Intent lampic2 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic2.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic2.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic2, LAMP_PIC2);
                        setResult(RESULT_OK, lampic2);
                        break;

                    case R.id.lampic3:
                        ((EditText) findViewById(R.id.lampeditpic3)).setText("");
                        Intent lampic3 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic3.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic3.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic3, LAMP_PIC3);
                        setResult(RESULT_OK, lampic3);
                        break;

                    case R.id.lampic4:
                        ((EditText) findViewById(R.id.lampeditpic4)).setText("");
                        Intent lampic4 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic4.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic4.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic4, LAMP_PIC4);
                        setResult(RESULT_OK, lampic4);
                        break;

                    case R.id.lampic5:
                        ((EditText) findViewById(R.id.lampeditpic5)).setText("");
                        Intent lampic5 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic5.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic5.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic5, LAMP_PIC5);
                        setResult(RESULT_OK, lampic5);
                        break;

                    case R.id.lampic6:
                        ((EditText) findViewById(R.id.lampeditpic6)).setText("");
                        Intent lampic6 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic6.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic6.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic6, LAMP_PIC6);
                        setResult(RESULT_OK, lampic6);
                        break;

                    case R.id.lampic7:
                        ((EditText) findViewById(R.id.lampeditpic7)).setText("");
                        Intent lampic7 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic7.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic7.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic7, LAMP_PIC7);
                        setResult(RESULT_OK, lampic7);
                        break;

                    case R.id.lampic8:
                        ((EditText) findViewById(R.id.lampeditpic8)).setText("");
                        Intent lampic8 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic8.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic8.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic8, LAMP_PIC8);
                        setResult(RESULT_OK, lampic8);
                        break;

                    case R.id.lampic9:
                        ((EditText) findViewById(R.id.lampeditpic9)).setText("");
                        Intent lampic9 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic9.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic9.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic9, LAMP_PIC9);
                        setResult(RESULT_OK, lampic9);
                        break;

                    case R.id.lampic10:
                        ((EditText) findViewById(R.id.lampeditpic10)).setText("");
                        Intent lampic10 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic10.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic10.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic10, LAMP_PIC10);
                        setResult(RESULT_OK, lampic10);
                        break;

                    case R.id.lampic11:
                        ((EditText) findViewById(R.id.lampeditpic11)).setText("");
                        Intent lampic11 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic11.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic11.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic11, LAMP_PIC11);
                        setResult(RESULT_OK, lampic11);
                        break;

                    case R.id.lampic12:
                        ((EditText) findViewById(R.id.lampeditpic12)).setText("");
                        Intent lampic12 = new Intent();
                        //開啟Pictures畫面Type設定為image
                        lampic12.setType("image/*");
                        //使用Intent.ACTION_GET_CONTENT這個Action
                        //會開啟選取圖檔視窗讓您選取手機內圖檔
                        lampic12.setAction(Intent.ACTION_GET_CONTENT);
                        //取得相片後返回本畫面
                        startActivityForResult(lampic12, LAMP_PIC12);
                        setResult(RESULT_OK, lampic12);
                        break;

                    case R.id.firstset:
                        /*setContentView(R.layout.firstset);

                        f1edit = (EditText) findViewById(R.id.f1edit);
                        f2edit = (EditText) findViewById(R.id.f2edit);
                        f3edit = (EditText) findViewById(R.id.f3edit);
                        f4edit = (EditText) findViewById(R.id.f4edit);
                        f5edit = (EditText) findViewById(R.id.f5edit);
                        f6edit = (EditText) findViewById(R.id.f6edit);
                        findViewById(R.id.firstok).setOnClickListener(new ButtonListener());

                        marcoset = getSharedPreferences(portSwitch + marcosetdata, 0);
                        marco1set = getSharedPreferences(portSwitch + marcoset1data, 0);
                        marco2set = getSharedPreferences(portSwitch + marcoset2data, 0);
                        marco3set = getSharedPreferences(portSwitch + marcoset3data, 0);
                        marco4set = getSharedPreferences(portSwitch + marcoset4data, 0);
                        marco5set = getSharedPreferences(portSwitch + marcoset5data, 0);

                        f1edit.setText(marcoset.getString(marconame, ""));
                        f2edit.setText(marco1set.getString(marco1name, ""));
                        f3edit.setText(marco2set.getString(marco2name, ""));
                        f4edit.setText(marco3set.getString(marco3name, ""));
                        f5edit.setText(marco4set.getString(marco4name, ""));
                        f6edit.setText(marco5set.getString(marco5name, ""));
                        */

                        new MySocketThread(mHandler, BluetoothChat.this, 0, "0");

                        break;

                    case R.id.firstok:
                        saveData(12);
                        Intent intent10 = new Intent();
                        intent10.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                        finish();
                        startActivity(intent10);
                        break;

                    case R.id.saveOpenCam:
                        if (D) Log.e(TAG, "R.id.saveOpenCam");
                        saveData(15);
                        Toast.makeText(BluetoothChat.this, "saved", Toast.LENGTH_LONG).show();
                        viewPager.setCurrentItem(0);
                        break;

                    case R.id.toggleButton:
                        if (toggleButton.isChecked()) {
                            sendmessage("w+!1");
                        } else {
                            sendmessage("w+!0");
                        }
                        break;
                    case R.id.window1up:
                        sendmessage("a8");
                        break;
                    case R.id.window1stop:
                        sendmessage("a7");
                        break;
                    case R.id.window1down:
                        sendmessage("a6");
                        break;
                    case R.id.window2up:
                        sendmessage("b8");
                        break;
                    case R.id.window2stop:
                        sendmessage("b7");
                        break;
                    case R.id.window2down:
                        sendmessage("b6");
                        break;
                    case R.id.window3up:
                        sendmessage("c8");
                        break;
                    case R.id.window3stop:
                        sendmessage("c7");
                        break;
                    case R.id.window3down:
                        sendmessage("c6");
                        break;
                }
        }
    }

    private void restart(int i) {
        //unbindService(mConnection);
        Intent intent = new Intent(this, BluetoothChat.class);
        intent.putExtra("switch", i == 1 ? "" : Integer.toString(i));
//        intent.putExtra("goto",9);
        startActivity(intent);
        finish();
    }

    //marco1
    public void ClickHandler(View v) {
        // TODO Auto-generated catch block

        switch (v.getId()) {
            case R.id.marcosetClear:
                marcosetbtn.setText("");
                count1 = 0;
                count2 = 0;
                count3 = 0;
                count4 = 0;
                count5 = 0;
                count6 = 0;
                break;
            case R.id.marcosetok:
                saveData(5);
                Intent intent3 = new Intent();
                intent3.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                finish();

                startActivity(intent3);
                break;


            case R.id.s1:
                String delay1 = ("'");
                marcosetbtn.append(delay1);
                break;

            case R.id.s2:
                String delay2 = ("(");
                marcosetbtn.append(delay2);
                break;

            case R.id.s3:
                String delay3 = (")");
                marcosetbtn.append(delay3);
                break;

            case R.id.s4:
                String delay4 = ("*");
                marcosetbtn.append(delay4);
                break;

            case R.id.s5:
                String delay5 = ("+");
                marcosetbtn.append(delay5);
                break;

            case R.id.btn01:

                if (count1 == 0) {
                    String view = ("a4");
                    mButton01.setTextColor(Color.parseColor("#FF0000"));
                    mButton01.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append(view);
                    count1 = 1;
                } else if (count1 == 1) {
                    mButton01.setTextColor(Color.parseColor("#ffffff"));
                    mButton01.setBackgroundResource(R.drawable.set_befor_9);
                    marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("a4", "a5"));
                    count1 = 2;
                } else if (count1 == 2) {
                    String btn01 = marcosetbtn.getText().toString();

                    if (btn01.contains("a5'")) {
                        marcosetbtn.setText(btn01.replaceFirst("a5'", ""));
                        count1 = 0;
                    } else if (btn01.contains("a5(")) {
                        marcosetbtn.setText(btn01.replaceFirst("a5\\(", ""));
                        String test = "";
                        count1 = 0;
                    } else if (btn01.contains("a5)")) {
                        marcosetbtn.setText(btn01.replaceFirst("a5\\)", ""));
                        count1 = 0;
                    } else if (btn01.contains("a5*")) {
                        marcosetbtn.setText(btn01.replaceFirst("a5*", ""));
                        count1 = 0;
                    } else if (btn01.contains("a5+")) {
                        marcosetbtn.setText(btn01.replaceFirst("a5+", ""));
                        count1 = 0;
                    } else if (btn01.contains("a5")) {
                        marcosetbtn.setText(btn01.replaceFirst("a5", ""));
                        count1 = 0;
                    }
                }

                break;

            case R.id.btn02:

                if (count2 == 0) {
                    mButton02.setTextColor(Color.parseColor("#FF0000"));
                    mButton02.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append("b4");
                    count2 = 1;
                } else if (count2 == 1) {
                    mButton02.setTextColor(Color.parseColor("#ffffff"));
                    mButton02.setBackgroundResource(R.drawable.set_befor_9);
                    marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("b4", "b5"));
                    count2 = 2;
                } else if (count2 == 2) {
                    String btn02 = marcosetbtn.getText().toString();

                    if (btn02.contains("b54")) {
                        marcosetbtn.setText(btn02.replaceFirst("b54", ""));
                        count2 = 0;
                    } else if (btn02.contains("b55")) {
                        marcosetbtn.setText(btn02.replaceFirst("b55", ""));
                        count2 = 0;
                    } else if (btn02.contains("b56")) {
                        marcosetbtn.setText(btn02.replaceFirst("b56", ""));
                        count2 = 0;
                    } else if (btn02.contains("b57")) {
                        marcosetbtn.setText(btn02.replaceFirst("b57", ""));
                        count2 = 0;
                    } else if (btn02.contains("b58")) {
                        marcosetbtn.setText(btn02.replaceFirst("b58", ""));
                        count2 = 0;
                    } else if (btn02.contains("b5")) {
                        marcosetbtn.setText(btn02.replaceFirst("b5", ""));
                        count2 = 0;
                    }
                }
                break;

            case R.id.btn03:
                if (count3 == 0) {
                    mButton03.setTextColor(Color.parseColor("#FF0000"));
                    mButton03.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append("c4");
                    count3 = 1;
                } else if (count3 == 1) {
                    mButton03.setTextColor(Color.parseColor("#ffffff"));
                    mButton03.setBackgroundResource(R.drawable.set_befor_9);
                    marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c4", "c5"));
                    count3 = 2;
                } else if (count3 == 2) {

                    if (marcosetbtn.getText().toString().contains("c54")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c54", ""));
                        count3 = 0;
                    } else if (marcosetbtn.getText().toString().contains("c55")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c55", ""));
                        count3 = 0;
                    } else if (marcosetbtn.getText().toString().contains("c56")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c56", ""));
                        count3 = 0;
                    } else if (marcosetbtn.getText().toString().contains("c57")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c57", ""));
                        count3 = 0;
                    } else if (marcosetbtn.getText().toString().contains("c58")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c58", ""));
                        count3 = 0;
                    } else if (marcosetbtn.getText().toString().contains("c5")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("c5", ""));
                        count3 = 0;
                    }
                }
                break;

            case R.id.btn04:
                if (count4 == 0) {
                    mButton04.setTextColor(Color.parseColor("#FF0000"));
                    mButton04.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append("d4");
                    count4 = 1;
                } else if (count4 == 1) {
                    mButton04.setTextColor(Color.parseColor("#ffffff"));
                    mButton04.setBackgroundResource(R.drawable.set_befor_9);
                    marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d4", "d5"));
                    count4 = 2;
                } else if (count4 == 2) {

                    if (marcosetbtn.getText().toString().contains("d54")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d54", ""));
                        count4 = 0;
                    } else if (marcosetbtn.getText().toString().contains("d55")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d55", ""));
                        count4 = 0;
                    } else if (marcosetbtn.getText().toString().contains("d56")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d56", ""));
                        count4 = 0;
                    } else if (marcosetbtn.getText().toString().contains("d57")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d57", ""));
                        count4 = 0;
                    } else if (marcosetbtn.getText().toString().contains("d58")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d58", ""));
                        count4 = 0;
                    } else if (marcosetbtn.getText().toString().contains("d5")) {
                        marcosetbtn.setText(marcosetbtn.getText().toString().replaceFirst("d5", ""));
                        count4 = 0;
                    }
                }
                break;

            case R.id.btn05:
                if (count5 == 0) {
                    String view5 = ("e4");
                    mButton05.setTextColor(Color.parseColor("#FF0000"));
                    mButton05.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append(view5);
                    count5 = 1;
                } else if (count5 == 1) {
                    mButton05.setTextColor(Color.parseColor("#ffffff"));
                    mButton05.setBackgroundResource(R.drawable.set_befor_9);
                    String btn05 = marcosetbtn.getText().toString();
                    String newa = btn05.replaceFirst("e4", "e5");
                    marcosetbtn.setText(newa);
                    count5 = 2;
                } else if (count5 == 2) {
                    String btn05 = marcosetbtn.getText().toString();
                    String newa = btn05.replaceFirst("e5", "");
                    marcosetbtn.setText(newa);
                    count5 = 0;
                }
                break;

            case R.id.btn06:
                if (count6 == 0) {
                    String view6 = ("f4");
                    mButton06.setTextColor(Color.parseColor("#FF0000"));
                    mButton06.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append(view6);
                    count6 = 1;
                } else if (count6 == 1) {
                    mButton06.setTextColor(Color.parseColor("#ffffff"));
                    mButton06.setBackgroundResource(R.drawable.set_befor_9);
                    String btn06 = marcosetbtn.getText().toString();
                    String newa = btn06.replaceFirst("f4", "f5");
                    marcosetbtn.setText(newa);
                    count6 = 2;
                } else if (count6 == 2) {
                    String btn06 = marcosetbtn.getText().toString();
                    String newa = btn06.replaceFirst("f5", "");
                    marcosetbtn.setText(newa);
                    count6 = 0;
                }
                break;

            case R.id.btn07:
                if (count7 == 0) {
                    String view7 = ("g4");
                    mButton07.setTextColor(Color.parseColor("#FF0000"));
                    mButton07.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append(view7);
                    count7 = 1;
                } else if (count7 == 1) {
                    mButton07.setTextColor(Color.parseColor("#ffffff"));
                    mButton07.setBackgroundResource(R.drawable.set_befor_9);
                    String btn07 = marcosetbtn.getText().toString();
                    String newa = btn07.replaceFirst("g4", "g5");
                    marcosetbtn.setText(newa);
                    count7 = 2;
                } else if (count7 == 2) {
                    String btn07 = marcosetbtn.getText().toString();
                    String newa = btn07.replaceFirst("g5", "");
                    marcosetbtn.setText(newa);
                    count7 = 0;
                }
                break;

            case R.id.btn08:
                if (count8 == 0) {
                    String view8 = ("h4");
                    mButton08.setTextColor(Color.parseColor("#FF0000"));
                    mButton08.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append(view8);
                    count8 = 1;
                } else if (count8 == 1) {
                    mButton08.setTextColor(Color.parseColor("#ffffff"));
                    mButton08.setBackgroundResource(R.drawable.set_befor_9);
                    String btn08 = marcosetbtn.getText().toString();
                    String newa = btn08.replaceFirst("h4", "h5");
                    marcosetbtn.setText(newa);
                    count8 = 2;
                } else if (count8 == 2) {
                    String btn08 = marcosetbtn.getText().toString();
                    String newa = btn08.replaceFirst("h5", "");
                    marcosetbtn.setText(newa);
                    count8 = 0;
                }
                break;

            case R.id.btn09:
                if (count9 == 0) {
                    String view9 = ("i4");
                    mButton09.setTextColor(Color.parseColor("#FF0000"));
                    mButton09.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append(view9);
                    count9 = 1;
                } else if (count9 == 1) {
                    mButton09.setTextColor(Color.parseColor("#ffffff"));
                    mButton09.setBackgroundResource(R.drawable.set_befor_9);
                    String btn09 = marcosetbtn.getText().toString();
                    String newa = btn09.replaceFirst("i4", "i5");
                    marcosetbtn.setText(newa);
                    count9 = 2;
                } else if (count9 == 2) {
                    String btn09 = marcosetbtn.getText().toString();
                    String newa = btn09.replaceFirst("i5", "");
                    marcosetbtn.setText(newa);
                    count9 = 0;
                }
                break;

            case R.id.btn10:
                if (count10 == 0) {
                    String view10 = ("j4");
                    mButton10.setTextColor(Color.parseColor("#FF0000"));
                    mButton10.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append(view10);
                    count10 = 1;
                } else if (count10 == 1) {
                    mButton10.setTextColor(Color.parseColor("#ffffff"));
                    mButton10.setBackgroundResource(R.drawable.set_befor_9);
                    String btn10 = marcosetbtn.getText().toString();
                    String newa = btn10.replaceFirst("j4", "j5");
                    marcosetbtn.setText(newa);
                    count10 = 2;
                } else if (count10 == 2) {
                    String btn10 = marcosetbtn.getText().toString();
                    String newa = btn10.replaceFirst("j5", "");
                    marcosetbtn.setText(newa);
                    count10 = 0;
                }
                break;

            case R.id.btn11:
                if (count11 == 0) {
                    String view11 = ("k4");
                    mButton11.setTextColor(Color.parseColor("#FF0000"));
                    mButton11.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append(view11);
                    count11 = 1;
                } else if (count11 == 1) {
                    mButton11.setTextColor(Color.parseColor("#ffffff"));
                    mButton11.setBackgroundResource(R.drawable.set_befor_9);
                    String btn11 = marcosetbtn.getText().toString();
                    String newa = btn11.replaceFirst("k4", "k5");
                    marcosetbtn.setText(newa);
                    count11 = 2;
                } else if (count11 == 2) {
                    String btn11 = marcosetbtn.getText().toString();
                    String newa = btn11.replaceFirst("k5", "");
                    marcosetbtn.setText(newa);
                    count11 = 0;
                }
                break;

            case R.id.btn12:
                if (count12 == 0) {
                    String view12 = ("l4");
                    mButton12.setTextColor(Color.parseColor("#FF0000"));
                    mButton12.setBackgroundResource(R.drawable.set_after_9);
                    marcosetbtn.append(view12);
                    count12 = 1;
                } else if (count12 == 1) {
                    mButton12.setTextColor(Color.parseColor("#ffffff"));
                    mButton12.setBackgroundResource(R.drawable.set_befor_9);
                    String btn12 = marcosetbtn.getText().toString();
                    String newa = btn12.replaceFirst("l4", "l5");
                    marcosetbtn.setText(newa);
                    count12 = 2;
                } else if (count12 == 2) {
                    String btn12 = marcosetbtn.getText().toString();
                    String newa = btn12.replaceFirst("l5", "");
                    marcosetbtn.setText(newa);
                    count12 = 0;
                }
                break;

            case R.id.btn13:
                String view13 = ("Aa");
                mButton13.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view13);

                break;

            case R.id.btn14:
                String view14 = ("Ad");
                mButton14.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view14);

                break;

            case R.id.btn15:
                String view15 = ("Ac");
                mButton15.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view15);
                break;

            case R.id.btn16:
                String view16 = ("Af");
                mButton16.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view16);
                break;

            case R.id.btn17:
                String view17 = ("Ag");
                mButton17.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view17);
                break;

            case R.id.btn18:
                String view18 = ("Ah");
                mButton18.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view18);
                break;

            case R.id.btn19:
                String view19 = ("Ai");
                mButton19.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view19);
                break;

            case R.id.btn20:
                String view20 = ("Aj");
                mButton20.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view20);
                break;

            case R.id.btn21:
                String view21 = ("Ak");
                mButton21.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view21);
                break;

            case R.id.btn22:
                String view22 = ("Al");
                mButton22.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view22);
                break;

            case R.id.btn23:
                String view23 = ("Ab");
                mButton23.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view23);
                break;

            case R.id.btn24:
                String view24 = ("Ae");
                mButton24.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view24);
                break;

//	case R.id.btn25:
//		String view25 = ("Am");
//		mButton25.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view25);
//		break;
//
//	case R.id.btn26:
//		String view26 = ("An");
//		mButton26.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view26);
//		break;
//
//	case R.id.btn27:
//		String view27 = ("Ao");
//		mButton27.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view27);
//		break;
//
//	case R.id.btn28:
//		String view28 = ("Ap");
//		mButton28.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view28);
//		break;
//
//	case R.id.btn29:
//		String view29 = ("Aq");
//		mButton29.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view29);
//		break;
//
//	case R.id.btn30:
//		String view30 = ("Ar");
//		mButton30.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view30);
//		break;
//
//	case R.id.btn31:
//		String view31 = ("As");
//		mButton31.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view31);
//		break;
//
            //case R.id.btn32:
            //String view32 = ("At");
            //mButton32.setTextColor(Color.parseColor("#FF0000"));
            //marcosetbtn.append(view32);
            //break;

            case R.id.btn33:
                String view33 = ("Tb");
                mButton33.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view33);
                break;

//	case R.id.btn34:
//		String view34 = ("Tb");
//		mButton34.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view34);
//		break;

            case R.id.btn35:
                String view35 = ("Te");
                mButton35.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view35);
                break;

//	case R.id.btn36:
//		String view36 = ("Td");
//		mButton36.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view36);
//		break;
//
//	case R.id.btn37:
//		String view37 = ("Te");
//		mButton37.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view37);
//		break;
//
//	case R.id.btn38:
//		String view38 = ("Tf");
//		mButton38.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view38);
//		break;
//
//	case R.id.btn39:
//		String view39 = ("Tg");
//		mButton39.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view39);
//		break;
//
//	case R.id.btn40:
//		String view40 = ("Th");
//		mButton40.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view40);
//		break;
//
//	case R.id.btn41:
//		String view41 = ("Ti");
//		mButton41.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view41);
//		break;

            case R.id.btn42:
                String view42 = ("Tg");
                mButton42.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view42);
                break;

            case R.id.btn43:
                String view43 = ("Th");
                mButton43.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view43);
                break;

            case R.id.btn44:
                String view44 = ("Ti");
                mButton44.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view44);
                break;

            case R.id.btn45:
                String view45 = ("Tj");
                mButton45.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view45);
                break;

            case R.id.btn46:
                String view46 = ("Tk");
                mButton46.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view46);
                break;

            case R.id.btn47:
                String view47 = ("Tl");
                mButton47.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view47);
                break;

            case R.id.btn48:
                String view48 = ("Tm");
                mButton48.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view48);
                break;

            case R.id.btn49:
                String view49 = ("Tn");
                mButton49.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view49);
                break;

            case R.id.btn50:
                String view50 = ("To");
                mButton50.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view50);
                break;

            case R.id.btn51:
                String view51 = ("Tp");
                mButton51.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view51);
                break;

            case R.id.btn52:
                String view52 = ("Tq");
                mButton52.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view52);
                break;

            case R.id.btn53:
                String view53 = ("Tr");
                mButton53.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view53);
                break;

//	case R.id.btn54:
//		String view54 = ("Xa");
//		mButton54.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view54);
//			break;
//
//	case R.id.btn55:
//		String view55 = ("Xb");
//		mButton55.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view55);
//			break;
//
//	case R.id.btn56:
//		String view56 = ("Xc");
//		mButton56.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view56);
//			break;
//
//	case R.id.btn57:
//		String view57 = ("Xd");
//		mButton57.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view57);
//			break;
//
//	case R.id.btn58:
//		String view58 = ("Xe");
//		mButton58.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view58);
//			break;
//
//	case R.id.btn59:
//		String view59 = ("Xf");
//		mButton59.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view59);
//			break;
//
//	case R.id.btn60:
//		String view60 = ("Xg");
//		mButton60.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view60);
//			break;
//
//	case R.id.btn61:
//		String view61 = ("Xh");
//		mButton61.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view61);
//			break;
//
//	case R.id.btn62:
//		String view62 = ("Xi");
//		mButton62.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view62);
//			break;

            case R.id.btn63:
                String view63 = ("Xa");
                mButton63.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view63);
                break;

            case R.id.btn64:
                String view64 = ("Xb");
                mButton64.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view64);
                break;

            case R.id.btn65:
                String view65 = ("Xc");
                mButton65.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view65);
                break;

            case R.id.btn66:
                String view66 = ("Xd");
                mButton66.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view66);
                break;

            case R.id.btn67:
                String view67 = ("Xe");
                mButton67.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view67);
                break;

            case R.id.btn68:
                String view68 = ("Xf");
                mButton68.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view68);
                break;

//	case R.id.btn69:
//		String view69 = ("Ya");
//		mButton69.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view69);
//			break;
//
//	case R.id.btn70:
//		String view70 = ("Yb");
//		mButton70.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view70);
//			break;
//
//	case R.id.btn71:
//		String view71 = ("Yc");
//		mButton71.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view71);
//			break;
//
//	case R.id.btn72:
//		String view72 = ("Yd");
//		mButton72.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view72);
//			break;
//
//	case R.id.btn73:
//		String view73 = ("Ye");
//		mButton73.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view73);
//			break;
//
//	case R.id.btn74:
//		String view74 = ("Yf");
//		mButton74.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view74);
//			break;
//
//	case R.id.btn75:
//		String view75 = ("Yg");
//		mButton75.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view75);
//			break;
//
//	case R.id.btn76:
//		String view76 = ("Yh");
//		mButton76.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view76);
//			break;
//
//	case R.id.btn77:
//		String view77 = ("Yi");
//		mButton77.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view77);
//			break;

            case R.id.btn78:
                String view78 = ("Ya");
                mButton78.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view78);
                break;

            case R.id.btn79:
                String view79 = ("Yb");
                mButton79.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view79);
                break;

            case R.id.btn80:
                String view80 = ("Yc");
                mButton80.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view80);
                break;

            case R.id.btn81:
                String view81 = ("Yd");
                mButton81.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view81);
                break;

            case R.id.btn82:
                String view82 = ("Ye");
                mButton82.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view82);
                break;

            case R.id.btn83:
                String view83 = ("Yf");
                mButton83.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view83);
                break;

//	case R.id.btn84:
//		String view84 = ("Za");
//		mButton84.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view84);
//			break;
//
//	case R.id.btn85:
//		String view85 = ("Zb");
//		mButton85.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view85);
//			break;
//
//	case R.id.btn86:
//		String view86 = ("Zc");
//		mButton86.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view86);
//			break;
//
//	case R.id.btn87:
//		String view87 = ("Zd");
//		mButton87.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view87);
//			break;
//
//	case R.id.btn88:
//		String view88 = ("Ze");
//		mButton88.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view88);
//			break;
//
//	case R.id.btn89:
//		String view89 = ("Zf");
//		mButton89.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view89);
//			break;
//
//	case R.id.btn90:
//		String view90 = ("Zg");
//		mButton90.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view90);
//			break;
//
//	case R.id.btn91:
//		String view91 = ("Zh");
//		mButton91.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view91);
//			break;
//
//	case R.id.btn92:
//		String view92 = ("Zi");
//		mButton92.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view92);
//			break;

            case R.id.btn93:
                String view93 = ("Za");
                mButton93.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view93);
                break;

            case R.id.btn94:
                String view94 = ("Zb");
                mButton94.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view94);
                break;

            case R.id.btn95:
                String view95 = ("Zc");
                mButton95.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view95);
                break;

            case R.id.btn96:
                String view96 = ("Zd");
                mButton96.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view96);
                break;

            case R.id.btn97:
                String view97 = ("Ze");
                mButton97.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view97);
                break;

            case R.id.btn98:
                String view98 = ("Zf");
                mButton98.setTextColor(Color.parseColor("#FF0000"));
                marcosetbtn.append(view98);
                break;

//	case R.id.btn99:
//		String view99 = ("Xp");
//		mButton99.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view99);
//			break;
//
//	case R.id.btn100:
//		String view100 = ("Xq");
//		mButton100.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view100);
//			break;
//
//	case R.id.btn101:
//		String view101 = ("Xr");
//		mButton101.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view101);
//			break;
//
//	case R.id.btn102:
//		String view102 = ("Xs");
//		mButton102.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view102);
//			break;
//
//	case R.id.btn103:
//		String view103 = ("Xt");
//		mButton103.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view103);
//			break;
//
//	case R.id.btn104:
//		String view104 = ("Xu");
//		mButton104.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view104);
//			break;
//
//	case R.id.btn105:
//		String view105 = ("Yp");
//		mButton105.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view105);
//			break;
//
//	case R.id.btn106:
//		String view106 = ("Yq");
//		mButton106.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view106);
//			break;
//
//	case R.id.btn107:
//		String view107 = ("Yr");
//		mButton107.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view107);
//			break;
//
//	case R.id.btn108:
//		String view108 = ("Ys");
//		mButton10.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view108);
//			break;
//
//	case R.id.btn109:
//		String view109 = ("Yt");
//		mButton109.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view109);
//			break;
//
//	case R.id.btn110:
//		String view110 = ("Yu");
//		mButton110.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view110);
//			break;
//
//	case R.id.btn111:
//		String view111 = ("Zp");
//		mButton111.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view111);
//			break;
//
//	case R.id.btn112:
//		String view112 = ("Zq");
//		mButton112.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view112);
//			break;
//
//	case R.id.btn113:
//		String view113 = ("Zr");
//		mButton113.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view113);
//			break;
//
//	case R.id.btn114:
//		String view114 = ("Zs");
//		mButton114.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view114);
//			break;
//
//	case R.id.btn115:
//		String view115 = ("Zt");
//		mButton115.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view115);
//			break;
//
//	case R.id.btn116:
//		String view116 = ("Zt");
//		mButton116.setTextColor(Color.parseColor("#FF0000"));
//		marcosetbtn.append(view116);
//			break;

            case R.id.btn153:
                String view153 = ("Tc");
                marcosetbtn.append(view153);
                break;

            case R.id.btn154:
                String view154 = ("Ta");
                marcosetbtn.append(view154);
                break;

            case R.id.btn155:
                String view155 = ("Tf");
                marcosetbtn.append(view155);
                break;

            case R.id.btn156:
                String view156 = ("Td");
                marcosetbtn.append(view156);
                break;


            case R.id.door1:
                String door1 = ("W+sa1");
                marcosetbtn.append(door1);
                break;

            case R.id.door2:
                String door2 = ("W+sb1");
                marcosetbtn.append(door2);
                break;

            case R.id.door3:
                String door3 = ("W+sc1");
                marcosetbtn.append(door3);
                break;
            case R.id.window1up:
                marcosetbtn.append("a8");
                break;
            case R.id.window1stop:
                marcosetbtn.append("a7");
                break;
            case R.id.window1down:
                marcosetbtn.append("a6");
                break;
            case R.id.window2up:
                marcosetbtn.append("b8");
                break;
            case R.id.window2stop:
                marcosetbtn.append("b7");
                break;
            case R.id.window2down:
                marcosetbtn.append("b6");
                break;
            case R.id.window3up:
                marcosetbtn.append("c8");
                break;
            case R.id.window3stop:
                marcosetbtn.append("c7");
                break;
            case R.id.window3down:
                marcosetbtn.append("c6");
                break;
            //tv home
//	case R.id.button25:
//		String view39 = ("Tg");
//		String message39 = view39.toString();
//		marcosetbtn.append(message39);
//		break;
//		//tv 靜音
//	case R.id.button26:
//		String view41 = ("Ti");
//        String message41 = view41.toString();
//        marcosetbtn.append(message41);
//		break;

        }
    }

    //marco2 click
    public void Click1Handler(View v) {
        // TODO Auto-generated catch block

        switch (v.getId()) {
            case R.id.marcoset1Clear:
                marco1setbtn.setText("");
                count1 = 0;
                count2 = 0;
                count3 = 0;
                count4 = 0;
                count5 = 0;
                count6 = 0;
                break;
            case R.id.marcoset1ok:

                saveData(6);
                Intent intent4 = new Intent();
                intent4.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                finish();
                startActivity(intent4);

                break;

            case R.id.s6:
                String delay6 = ("'");
                marco1setbtn.append(delay6);
                break;

            case R.id.s7:
                String delay7 = ("(");
                marco1setbtn.append(delay7);
                break;

            case R.id.s8:
                String delay8 = (")");
                marco1setbtn.append(delay8);
                break;

            case R.id.s9:
                String delay9 = ("*");
                marco1setbtn.append(delay9);
                break;

            case R.id.s10:
                String delay10 = ("+");
                marco1setbtn.append(delay10);
                break;

            case R.id.btn01:

                if (count1 == 0) {
                    String view = ("a4");
                    mButton01.setTextColor(Color.parseColor("#FF0000"));
                    mButton01.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view);
                    count1 = 1;
                } else if (count1 == 1) {
                    mButton01.setTextColor(Color.parseColor("#ffffff"));
                    mButton01.setBackgroundResource(R.drawable.set_befor_9);
                    String btn01 = marco1setbtn.getText().toString();
                    String newa = btn01.replaceFirst("a4", "a5");
                    marco1setbtn.setText(newa);
                    count1 = 2;
                } else if (count1 == 2) {
                    String btn01 = marco1setbtn.getText().toString();
                    String newa = btn01.replaceFirst("a5", "");
                    marco1setbtn.setText(newa);
                    count1 = 0;
                }

                break;

            case R.id.btn02:

                if (count2 == 0) {
                    String view2 = ("b4");
                    mButton02.setTextColor(Color.parseColor("#FF0000"));
                    mButton02.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view2);
                    count2 = 1;
                } else if (count2 == 1) {
                    mButton02.setTextColor(Color.parseColor("#ffffff"));
                    mButton02.setBackgroundResource(R.drawable.set_befor_9);
                    String btn02 = marco1setbtn.getText().toString();
                    String newa = btn02.replaceFirst("b4", "b5");
                    marco1setbtn.setText(newa);
                    count2 = 2;
                } else if (count2 == 2) {
                    String btn02 = marco1setbtn.getText().toString();
                    String newa = btn02.replaceFirst("b5", "");
                    marco1setbtn.setText(newa);
                    count2 = 0;
                }
                break;

            case R.id.btn03:
                if (count3 == 0) {
                    String view3 = ("c4");
                    mButton03.setTextColor(Color.parseColor("#FF0000"));
                    mButton03.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view3);
                    count3 = 1;
                } else if (count3 == 1) {
                    mButton03.setTextColor(Color.parseColor("#ffffff"));
                    mButton03.setBackgroundResource(R.drawable.set_befor_9);
                    String btn03 = marco1setbtn.getText().toString();
                    String newa = btn03.replaceFirst("c4", "c5");
                    marco1setbtn.setText(newa);
                    count3 = 2;
                } else if (count3 == 2) {
                    String btn03 = marco1setbtn.getText().toString();
                    String newa = btn03.replaceFirst("c5", "");
                    marco1setbtn.setText(newa);
                    count3 = 0;
                }
                break;

            case R.id.btn04:
                if (count4 == 0) {
                    String view4 = ("d4");
                    mButton04.setTextColor(Color.parseColor("#FF0000"));
                    mButton04.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view4);
                    count4 = 1;
                } else if (count4 == 1) {
                    mButton04.setTextColor(Color.parseColor("#ffffff"));
                    mButton04.setBackgroundResource(R.drawable.set_befor_9);
                    String btn04 = marco1setbtn.getText().toString();
                    String newa = btn04.replaceFirst("d4", "d5");
                    marco1setbtn.setText(newa);
                    count4 = 2;
                } else if (count4 == 2) {
                    String btn04 = marco1setbtn.getText().toString();
                    String newa = btn04.replaceFirst("d5", "");
                    marco1setbtn.setText(newa);
                    count4 = 0;
                }
                break;

            case R.id.btn05:
                if (count5 == 0) {
                    String view5 = ("e4");
                    mButton05.setTextColor(Color.parseColor("#FF0000"));
                    mButton05.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view5);
                    count5 = 1;
                } else if (count5 == 1) {
                    mButton05.setTextColor(Color.parseColor("#ffffff"));
                    mButton05.setBackgroundResource(R.drawable.set_befor_9);
                    String btn05 = marco1setbtn.getText().toString();
                    String newa = btn05.replaceFirst("e4", "e5");
                    marco1setbtn.setText(newa);
                    count5 = 2;
                } else if (count5 == 2) {
                    String btn05 = marco1setbtn.getText().toString();
                    String newa = btn05.replaceFirst("e5", "");
                    marco1setbtn.setText(newa);
                    count5 = 0;
                }
                break;

            case R.id.btn06:
                if (count6 == 0) {
                    String view6 = ("f4");
                    mButton06.setTextColor(Color.parseColor("#FF0000"));
                    mButton06.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view6);
                    count6 = 1;
                } else if (count6 == 1) {
                    mButton06.setTextColor(Color.parseColor("#ffffff"));
                    mButton06.setBackgroundResource(R.drawable.set_befor_9);
                    String btn06 = marco1setbtn.getText().toString();
                    String newa = btn06.replaceFirst("f4", "f5");
                    marco1setbtn.setText(newa);
                    count6 = 2;
                } else if (count6 == 2) {
                    String btn06 = marco1setbtn.getText().toString();
                    String newa = btn06.replaceFirst("f5", "");
                    marco1setbtn.setText(newa);
                    count6 = 0;
                }
                break;

            case R.id.btn07:
                if (count7 == 0) {
                    String view7 = ("g4");
                    mButton07.setTextColor(Color.parseColor("#FF0000"));
                    mButton07.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view7);
                    count7 = 1;
                } else if (count7 == 1) {
                    mButton07.setTextColor(Color.parseColor("#ffffff"));
                    mButton07.setBackgroundResource(R.drawable.set_befor_9);
                    String btn07 = marco1setbtn.getText().toString();
                    String newa = btn07.replaceFirst("g4", "g5");
                    marco1setbtn.setText(newa);
                    count7 = 2;
                } else if (count7 == 2) {
                    String btn07 = marco1setbtn.getText().toString();
                    String newa = btn07.replaceFirst("g5", "");
                    marco1setbtn.setText(newa);
                    count7 = 0;
                }
                break;

            case R.id.btn08:
                if (count8 == 0) {
                    String view8 = ("h4");
                    mButton08.setTextColor(Color.parseColor("#FF0000"));
                    mButton08.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view8);
                    count8 = 1;
                } else if (count8 == 1) {
                    mButton08.setTextColor(Color.parseColor("#ffffff"));
                    mButton08.setBackgroundResource(R.drawable.set_befor_9);
                    String btn08 = marco1setbtn.getText().toString();
                    String newa = btn08.replaceFirst("h4", "h5");
                    marco1setbtn.setText(newa);
                    count8 = 2;
                } else if (count8 == 2) {
                    String btn08 = marco1setbtn.getText().toString();
                    String newa = btn08.replaceFirst("h5", "");
                    marco1setbtn.setText(newa);
                    count8 = 0;
                }
                break;

            case R.id.btn09:
                if (count9 == 0) {
                    String view9 = ("i4");
                    mButton09.setTextColor(Color.parseColor("#FF0000"));
                    mButton09.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view9);
                    count9 = 1;
                } else if (count9 == 1) {
                    mButton09.setTextColor(Color.parseColor("#ffffff"));
                    mButton09.setBackgroundResource(R.drawable.set_befor_9);
                    String btn09 = marco1setbtn.getText().toString();
                    String newa = btn09.replaceFirst("i4", "i5");
                    marco1setbtn.setText(newa);
                    count9 = 2;
                } else if (count9 == 2) {
                    String btn09 = marco1setbtn.getText().toString();
                    String newa = btn09.replaceFirst("i5", "");
                    marco1setbtn.setText(newa);
                    count9 = 0;
                }
                break;

            case R.id.btn10:
                if (count10 == 0) {
                    String view10 = ("j4");
                    mButton10.setTextColor(Color.parseColor("#FF0000"));
                    mButton10.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view10);
                    count10 = 1;
                } else if (count10 == 1) {
                    mButton10.setTextColor(Color.parseColor("#ffffff"));
                    mButton10.setBackgroundResource(R.drawable.set_befor_9);
                    String btn10 = marco1setbtn.getText().toString();
                    String newa = btn10.replaceFirst("j4", "j5");
                    marco1setbtn.setText(newa);
                    count10 = 2;
                } else if (count10 == 2) {
                    String btn10 = marco1setbtn.getText().toString();
                    String newa = btn10.replaceFirst("j5", "");
                    marco1setbtn.setText(newa);
                    count10 = 0;
                }
                break;

            case R.id.btn11:
                if (count11 == 0) {
                    String view11 = ("k4");
                    mButton11.setTextColor(Color.parseColor("#FF0000"));
                    mButton11.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view11);
                    count11 = 1;
                } else if (count11 == 1) {
                    mButton11.setTextColor(Color.parseColor("#ffffff"));
                    mButton11.setBackgroundResource(R.drawable.set_befor_9);
                    String btn11 = marco1setbtn.getText().toString();
                    String newa = btn11.replaceFirst("k4", "k5");
                    marco1setbtn.setText(newa);
                    count11 = 2;
                } else if (count11 == 2) {
                    String btn11 = marco1setbtn.getText().toString();
                    String newa = btn11.replaceFirst("k5", "");
                    marco1setbtn.setText(newa);
                    count11 = 0;
                }
                break;

            case R.id.btn12:
                if (count12 == 0) {
                    String view12 = ("l4");
                    mButton12.setTextColor(Color.parseColor("#FF0000"));
                    mButton12.setBackgroundResource(R.drawable.set_after_9);
                    marco1setbtn.append(view12);
                    count12 = 1;
                } else if (count12 == 1) {
                    mButton12.setTextColor(Color.parseColor("#ffffff"));
                    mButton12.setBackgroundResource(R.drawable.set_befor_9);
                    String btn12 = marco1setbtn.getText().toString();
                    String newa = btn12.replaceFirst("l4", "l5");
                    marco1setbtn.setText(newa);
                    count12 = 2;
                } else if (count12 == 2) {
                    String btn12 = marco1setbtn.getText().toString();
                    String newa = btn12.replaceFirst("l5", "");
                    marco1setbtn.setText(newa);
                    count12 = 0;
                }
                break;

            case R.id.btn13:
                String view13 = ("Aa");
                mButton13.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view13);

                break;

            case R.id.btn14:
                String view14 = ("Ad");
                mButton14.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view14);
                break;

            case R.id.btn15:
                String view15 = ("Ac");
                mButton15.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view15);
                break;

            case R.id.btn16:
                String view16 = ("Af");
                mButton16.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view16);
                break;

            case R.id.btn17:
                String view17 = ("Ag");
                mButton17.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view17);
                break;

            case R.id.btn18:
                String view18 = ("Ah");
                mButton18.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view18);
                break;

            case R.id.btn19:
                String view19 = ("Ai");
                mButton19.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view19);
                break;

            case R.id.btn20:
                String view20 = ("Aj");
                mButton20.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view20);
                break;

            case R.id.btn21:
                String view21 = ("Ak");
                mButton21.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view21);
                break;

            case R.id.btn22:
                String view22 = ("Al");
                mButton22.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view22);
                break;

            case R.id.btn23:
                String view23 = ("Ab");
                mButton23.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view23);
                break;

            case R.id.btn24:
                String view24 = ("Ae");
                mButton24.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view24);
                break;

//case R.id.btn25:
//	String view25 = ("Am");
//	mButton25.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view25);
//	break;
//
//case R.id.btn26:
//	String view26 = ("An");
//	mButton26.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view26);
//	break;
//
//case R.id.btn27:
//	String view27 = ("Ao");
//	mButton27.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view27);
//	break;
//
//case R.id.btn28:
//	String view28 = ("Ap");
//	mButton28.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view28);
//	break;
//
//case R.id.btn29:
//	String view29 = ("Aq");
//	mButton29.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view29);
//	break;
//
//case R.id.btn30:
//	String view30 = ("Ar");
//	mButton30.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view30);
//	break;
//
//case R.id.btn31:
//	String view31 = ("As");
//	mButton31.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view31);
//	break;
//
//case R.id.btn32:
            //String view32 = ("At");
            //mButton32.setTextColor(Color.parseColor("#FF0000"));
            //marco1setbtn.append(view32);
            //break;

            case R.id.btn33:
                String view33 = ("Tb");
                mButton33.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view33);
                break;

//case R.id.btn34:
//	String view34 = ("Tb");
//	mButton34.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view34);
//	break;
//
            case R.id.btn35:
                String view35 = ("Te");
                mButton35.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view35);
                break;
//
//case R.id.btn36:
//	String view36 = ("Td");
//	mButton36.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view36);
//	break;
//
//case R.id.btn37:
//	String view37 = ("Te");
//	mButton37.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view37);
//	break;
//
//case R.id.btn38:
//	String view38 = ("Tf");
//	mButton38.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view38);
//	break;
//
//case R.id.btn39:
//	String view39 = ("Tg");
//	mButton39.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view39);
//	break;
//
//case R.id.btn40:
//	String view40 = ("Th");
//	mButton40.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view40);
//	break;
//
//case R.id.btn41:
            //String view41 = ("Ti");
            //mButton41.setTextColor(Color.parseColor("#FF0000"));
            //marco1setbtn.append(view41);
            //break;

            case R.id.btn42:
                String view42 = ("Tg");
                mButton42.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view42);
                break;

            case R.id.btn43:
                String view43 = ("Th");
                mButton43.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view43);
                break;

            case R.id.btn44:
                String view44 = ("Ti");
                mButton44.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view44);
                break;

            case R.id.btn45:
                String view45 = ("Tj");
                mButton45.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view45);
                break;

            case R.id.btn46:
                String view46 = ("Tk");
                mButton46.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view46);
                break;

            case R.id.btn47:
                String view47 = ("Tl");
                mButton47.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view47);
                break;

            case R.id.btn48:
                String view48 = ("Tm");
                mButton48.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view48);
                break;

            case R.id.btn49:
                String view49 = ("Tn");
                mButton49.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view49);
                break;

            case R.id.btn50:
                String view50 = ("To");
                mButton50.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view50);
                break;

            case R.id.btn51:
                String view51 = ("Tp");
                mButton51.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view51);
                break;

            case R.id.btn52:
                String view52 = ("Tq");
                mButton52.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view52);
                break;

            case R.id.btn53:
                String view53 = ("Tr");
                mButton53.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view53);
                break;

//case R.id.btn54:
//	String view54 = ("Xa");
//	mButton54.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view54);
//		break;
//
//case R.id.btn55:
//	String view55 = ("Xb");
//	mButton55.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view55);
//		break;
//
//case R.id.btn56:
//	String view56 = ("Xc");
//	mButton56.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view56);
//		break;
//
//case R.id.btn57:
//	String view57 = ("Xd");
//	mButton57.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view57);
//		break;
//
//case R.id.btn58:
//	String view58 = ("Xe");
//	mButton58.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view58);
//		break;
//
//case R.id.btn59:
//	String view59 = ("Xf");
//	mButton59.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view59);
//		break;
//
//case R.id.btn60:
//	String view60 = ("Xg");
//	mButton60.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view60);
//		break;
//
//case R.id.btn61:
//	String view61 = ("Xh");
//	mButton61.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view61);
//		break;
//
//case R.id.btn62:
//	String view62 = ("Xi");
//	mButton62.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view62);
//		break;
//
            case R.id.btn63:
                String view63 = ("Xa");
                mButton63.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view63);
                break;

            case R.id.btn64:
                String view64 = ("Xb");
                mButton64.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view64);
                break;

            case R.id.btn65:
                String view65 = ("Xc");
                mButton65.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view65);
                break;

            case R.id.btn66:
                String view66 = ("Xd");
                mButton66.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view66);
                break;

            case R.id.btn67:
                String view67 = ("Xe");
                mButton67.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view67);
                break;

            case R.id.btn68:
                String view68 = ("Xf");
                mButton68.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view68);
                break;

//case R.id.btn69:
//	String view69 = ("Ya");
//	mButton69.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view69);
//		break;
//
//case R.id.btn70:
//	String view70 = ("Yb");
//	mButton70.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view70);
//		break;
//
//case R.id.btn71:
//	String view71 = ("Yc");
//	mButton71.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view71);
//		break;
//
//case R.id.btn72:
//	String view72 = ("Yd");
//	mButton72.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view72);
//		break;
//
//case R.id.btn73:
//	String view73 = ("Ye");
//	mButton73.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view73);
//		break;
//
//case R.id.btn74:
//	String view74 = ("Yf");
//	mButton74.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view74);
//		break;
//
//case R.id.btn75:
//	String view75 = ("Yg");
//	mButton75.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view75);
//		break;
//
//case R.id.btn76:
//	String view76 = ("Yh");
//	mButton76.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view76);
//		break;
//
//case R.id.btn77:
//	String view77 = ("Yi");
//	mButton77.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view77);
//		break;

            case R.id.btn78:
                String view78 = ("Ya");
                mButton78.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view78);
                break;

            case R.id.btn79:
                String view79 = ("Yb");
                mButton79.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view79);
                break;

            case R.id.btn80:
                String view80 = ("Yc");
                mButton80.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view80);
                break;

            case R.id.btn81:
                String view81 = ("Yd");
                mButton81.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view81);
                break;

            case R.id.btn82:
                String view82 = ("Ye");
                mButton82.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view82);
                break;

            case R.id.btn83:
                String view83 = ("Yf");
                mButton83.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view83);
                break;

//case R.id.btn84:
//	String view84 = ("Za");
//	mButton84.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view84);
//		break;
//
//case R.id.btn85:
//	String view85 = ("Zb");
//	mButton85.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view85);
//		break;
//
//case R.id.btn86:
//	String view86 = ("Zc");
//	mButton86.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view86);
//		break;
//
//case R.id.btn87:
//	String view87 = ("Zd");
//	mButton87.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view87);
//		break;
//
//case R.id.btn88:
//	String view88 = ("Ze");
//	mButton88.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view88);
//		break;
//
//case R.id.btn89:
//	String view89 = ("Zf");
//	mButton89.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view89);
//		break;
//
//case R.id.btn90:
//	String view90 = ("Zg");
//	mButton90.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view90);
//		break;
//
//case R.id.btn91:
//	String view91 = ("Zh");
//	mButton91.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view91);
//		break;
//
//case R.id.btn92:
//	String view92 = ("Zi");
//	mButton92.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view92);
//		break;

            case R.id.btn93:
                String view93 = ("Za");
                mButton93.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view93);
                break;

            case R.id.btn94:
                String view94 = ("Zb");
                mButton94.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view94);
                break;

            case R.id.btn95:
                String view95 = ("Zc");
                mButton95.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view95);
                break;

            case R.id.btn96:
                String view96 = ("Zd");
                mButton96.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view96);
                break;

            case R.id.btn97:
                String view97 = ("Ze");
                mButton97.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view97);
                break;

            case R.id.btn98:
                String view98 = ("Zf");
                mButton98.setTextColor(Color.parseColor("#FF0000"));
                marco1setbtn.append(view98);
                break;

//case R.id.btn99:
//	String view99 = ("Xp");
//	mButton99.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view99);
//		break;
//
//case R.id.btn100:
//	String view100 = ("Xq");
//	mButton100.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view100);
//		break;
//
//case R.id.btn101:
//	String view101 = ("Xr");
//	mButton101.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view101);
//		break;
//
//case R.id.btn102:
//	String view102 = ("Xs");
//	mButton102.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view102);
//		break;
//
//case R.id.btn103:
//	String view103 = ("Xt");
//	mButton103.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view103);
//		break;
//
//case R.id.btn104:
//	String view104 = ("Xu");
//	mButton104.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view104);
//		break;
//
//case R.id.btn105:
//	String view105 = ("Yp");
//	mButton105.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view105);
//		break;
//
//case R.id.btn106:
//	String view106 = ("Yq");
//	mButton106.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view106);
//		break;
//
//case R.id.btn107:
//	String view107 = ("Yr");
//	mButton107.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view107);
//		break;
//
//case R.id.btn108:
//	String view108 = ("Ys");
//	mButton10.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view108);
//		break;
//
//case R.id.btn109:
//	String view109 = ("Yt");
//	mButton109.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view109);
//		break;
//
//case R.id.btn110:
//	String view110 = ("Yu");
//	mButton110.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view110);
//		break;
//
//case R.id.btn111:
//	String view111 = ("Zp");
//	mButton111.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view111);
//		break;
//
//case R.id.btn112:
//	String view112 = ("Zq");
//	mButton112.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view112);
//		break;
//
//case R.id.btn113:
//	String view113 = ("Zr");
//	mButton113.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view113);
//		break;
//
//case R.id.btn114:
//	String view114 = ("Zs");
//	mButton114.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view114);
//		break;
//
//case R.id.btn115:
//	String view115 = ("Zt");
//	mButton115.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view115);
//		break;
//
//case R.id.btn116:
//	String view116 = ("Zt");
//	mButton116.setTextColor(Color.parseColor("#FF0000"));
//	marco1setbtn.append(view116);
//		break;

            case R.id.btn153:
                String view153 = ("Tc");
                marco1setbtn.append(view153);
                break;

            case R.id.btn154:
                String view154 = ("Ta");
                marco1setbtn.append(view154);
                break;

            case R.id.btn155:
                String view155 = ("Tf");
                marco1setbtn.append(view155);
                break;

            case R.id.btn156:
                String view156 = ("Td");
                marco1setbtn.append(view156);
                break;


            case R.id.door1:
                String door1 = ("W+sa1");
                marco1setbtn.append(door1);
                break;

            case R.id.door2:
                String door2 = ("W+sb1");
                marco1setbtn.append(door2);
                break;

            case R.id.door3:
                String door3 = ("W+sc1");
                marco1setbtn.append(door3);
                break;
            case R.id.window1up:
                marco1setbtn.append("a8");
                break;
            case R.id.window1stop:
                marco1setbtn.append("a7");
                break;
            case R.id.window1down:
                marco1setbtn.append("a6");
                break;
            case R.id.window2up:
                marco1setbtn.append("b8");
                break;
            case R.id.window2stop:
                marco1setbtn.append("b7");
                break;
            case R.id.window2down:
                marco1setbtn.append("b6");
                break;
            case R.id.window3up:
                marco1setbtn.append("c8");
                break;
            case R.id.window3stop:
                marco1setbtn.append("c7");
                break;
            case R.id.window3down:
                marco1setbtn.append("c6");
                break;
            //tv home
//case R.id.button25:
//	String view39 = ("Tg");
//	String message39 = view39.toString();
//	marco1setbtn.append(message39);
//	break;
//	//tv 靜音
//case R.id.button26:
//	String view41 = ("Ti");
//    String message41 = view41.toString();
//    marco1setbtn.append(message41);
//	break;

        }
    }

    //marco3
    public void Click2Handler(View v) {
        // TODO Auto-generated catch block

        switch (v.getId()) {
            case R.id.marcoset2Clear:
                marco2setbtn.setText("");
                count1 = 0;
                count2 = 0;
                count3 = 0;
                count4 = 0;
                count5 = 0;
                count6 = 0;
                break;
            case R.id.marcoset2ok:

                saveData(7);
                Intent intent5 = new Intent();
                intent5.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                startActivity(intent5);
                finish();

                break;

            case R.id.s11:
                String delay11 = ("'");
                marco2setbtn.append(delay11);
                break;

            case R.id.s12:
                String delay12 = ("(");
                marco2setbtn.append(delay12);
                break;

            case R.id.s13:
                String delay13 = (")");
                marco2setbtn.append(delay13);
                break;

            case R.id.s14:
                String delay14 = ("*");
                marco2setbtn.append(delay14);
                break;

            case R.id.s15:
                String delay15 = ("+");
                marco2setbtn.append(delay15);
                break;

            case R.id.btn01:

                if (count1 == 0) {
                    String view = ("a4");
                    mButton01.setTextColor(Color.parseColor("#FF0000"));
                    mButton01.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view);
                    count1 = 1;
                } else if (count1 == 1) {
                    mButton01.setTextColor(Color.parseColor("#ffffff"));
                    mButton01.setBackgroundResource(R.drawable.set_befor_9);
                    String btn01 = marco2setbtn.getText().toString();
                    String newa = btn01.replaceFirst("a4", "a5");
                    marco2setbtn.setText(newa);
                    count1 = 2;
                } else if (count1 == 2) {
                    String btn01 = marco2setbtn.getText().toString();
                    String newa = btn01.replaceFirst("a5", "");
                    marco2setbtn.setText(newa);
                    count1 = 0;
                }

                break;

            case R.id.btn02:

                if (count2 == 0) {
                    String view2 = ("b4");
                    mButton02.setTextColor(Color.parseColor("#FF0000"));
                    mButton02.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view2);
                    count2 = 1;
                } else if (count2 == 1) {
                    mButton02.setTextColor(Color.parseColor("#ffffff"));
                    mButton02.setBackgroundResource(R.drawable.set_befor_9);
                    String btn02 = marco2setbtn.getText().toString();
                    String newa = btn02.replaceFirst("b4", "b5");
                    marco2setbtn.setText(newa);
                    count2 = 2;
                } else if (count2 == 2) {
                    String btn02 = marco2setbtn.getText().toString();
                    String newa = btn02.replaceFirst("b5", "");
                    marco2setbtn.setText(newa);
                    count2 = 0;
                }
                break;

            case R.id.btn03:
                if (count3 == 0) {
                    String view3 = ("c4");
                    mButton03.setTextColor(Color.parseColor("#FF0000"));
                    mButton03.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view3);
                    count3 = 1;
                } else if (count3 == 1) {
                    mButton03.setTextColor(Color.parseColor("#ffffff"));
                    mButton03.setBackgroundResource(R.drawable.set_befor_9);
                    String btn03 = marco2setbtn.getText().toString();
                    String newa = btn03.replaceFirst("c4", "c5");
                    marco2setbtn.setText(newa);
                    count3 = 2;
                } else if (count3 == 2) {
                    String btn03 = marco2setbtn.getText().toString();
                    String newa = btn03.replaceFirst("c5", "");
                    marco2setbtn.setText(newa);
                    count3 = 0;
                }
                break;

            case R.id.btn04:
                if (count4 == 0) {
                    String view4 = ("d4");
                    mButton04.setTextColor(Color.parseColor("#FF0000"));
                    mButton04.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view4);
                    count4 = 1;
                } else if (count4 == 1) {
                    mButton04.setTextColor(Color.parseColor("#ffffff"));
                    mButton04.setBackgroundResource(R.drawable.set_befor_9);
                    String btn04 = marco2setbtn.getText().toString();
                    String newa = btn04.replaceFirst("d4", "d5");
                    marco2setbtn.setText(newa);
                    count4 = 2;
                } else if (count4 == 2) {
                    String btn04 = marco2setbtn.getText().toString();
                    String newa = btn04.replaceFirst("d5", "");
                    marco2setbtn.setText(newa);
                    count4 = 0;
                }
                break;

            case R.id.btn05:
                if (count5 == 0) {
                    String view5 = ("e4");
                    mButton05.setTextColor(Color.parseColor("#FF0000"));
                    mButton05.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view5);
                    count5 = 1;
                } else if (count5 == 1) {
                    mButton05.setTextColor(Color.parseColor("#ffffff"));
                    mButton05.setBackgroundResource(R.drawable.set_befor_9);
                    String btn05 = marco2setbtn.getText().toString();
                    String newa = btn05.replaceFirst("e4", "e5");
                    marco2setbtn.setText(newa);
                    count5 = 2;
                } else if (count5 == 2) {
                    String btn05 = marco2setbtn.getText().toString();
                    String newa = btn05.replaceFirst("e5", "");
                    marco2setbtn.setText(newa);
                    count5 = 0;
                }
                break;

            case R.id.btn06:
                if (count6 == 0) {
                    String view6 = ("f4");
                    mButton06.setTextColor(Color.parseColor("#FF0000"));
                    mButton06.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view6);
                    count6 = 1;
                } else if (count6 == 1) {
                    mButton06.setTextColor(Color.parseColor("#ffffff"));
                    mButton06.setBackgroundResource(R.drawable.set_befor_9);
                    String btn06 = marco2setbtn.getText().toString();
                    String newa = btn06.replaceFirst("f4", "f5");
                    marco2setbtn.setText(newa);
                    count6 = 2;
                } else if (count6 == 2) {
                    String btn06 = marco2setbtn.getText().toString();
                    String newa = btn06.replaceFirst("f5", "");
                    marco2setbtn.setText(newa);
                    count6 = 0;
                }
                break;

            case R.id.btn07:
                if (count7 == 0) {
                    String view7 = ("g4");
                    mButton07.setTextColor(Color.parseColor("#FF0000"));
                    mButton07.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view7);
                    count7 = 1;
                } else if (count7 == 1) {
                    mButton07.setTextColor(Color.parseColor("#ffffff"));
                    mButton07.setBackgroundResource(R.drawable.set_befor_9);
                    String btn07 = marco2setbtn.getText().toString();
                    String newa = btn07.replaceFirst("g4", "g5");
                    marco2setbtn.setText(newa);
                    count7 = 2;
                } else if (count7 == 2) {
                    String btn07 = marco2setbtn.getText().toString();
                    String newa = btn07.replaceFirst("g5", "");
                    marco2setbtn.setText(newa);
                    count7 = 0;
                }
                break;

            case R.id.btn08:
                if (count8 == 0) {
                    String view8 = ("h4");
                    mButton08.setTextColor(Color.parseColor("#FF0000"));
                    mButton08.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view8);
                    count8 = 1;
                } else if (count8 == 1) {
                    mButton08.setTextColor(Color.parseColor("#ffffff"));
                    mButton08.setBackgroundResource(R.drawable.set_befor_9);
                    String btn08 = marco2setbtn.getText().toString();
                    String newa = btn08.replaceFirst("h4", "h5");
                    marco2setbtn.setText(newa);
                    count8 = 2;
                } else if (count8 == 2) {
                    String btn08 = marco2setbtn.getText().toString();
                    String newa = btn08.replaceFirst("h5", "");
                    marco2setbtn.setText(newa);
                    count8 = 0;
                }
                break;

            case R.id.btn09:
                if (count9 == 0) {
                    String view9 = ("i4");
                    mButton09.setTextColor(Color.parseColor("#FF0000"));
                    mButton09.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view9);
                    count9 = 1;
                } else if (count9 == 1) {
                    mButton09.setTextColor(Color.parseColor("#ffffff"));
                    mButton09.setBackgroundResource(R.drawable.set_befor_9);
                    String btn09 = marco2setbtn.getText().toString();
                    String newa = btn09.replaceFirst("i4", "i5");
                    marco2setbtn.setText(newa);
                    count9 = 2;
                } else if (count9 == 2) {
                    String btn09 = marco2setbtn.getText().toString();
                    String newa = btn09.replaceFirst("i5", "");
                    marco2setbtn.setText(newa);
                    count9 = 0;
                }
                break;

            case R.id.btn10:
                if (count10 == 0) {
                    String view10 = ("j4");
                    mButton10.setTextColor(Color.parseColor("#FF0000"));
                    mButton10.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view10);
                    count10 = 1;
                } else if (count10 == 1) {
                    mButton10.setTextColor(Color.parseColor("#ffffff"));
                    mButton10.setBackgroundResource(R.drawable.set_befor_9);
                    String btn10 = marco2setbtn.getText().toString();
                    String newa = btn10.replaceFirst("j4", "j5");
                    marco2setbtn.setText(newa);
                    count10 = 2;
                } else if (count10 == 2) {
                    String btn10 = marco2setbtn.getText().toString();
                    String newa = btn10.replaceFirst("j5", "");
                    marco2setbtn.setText(newa);
                    count10 = 0;
                }
                break;

            case R.id.btn11:
                if (count11 == 0) {
                    String view11 = ("k4");
                    mButton11.setTextColor(Color.parseColor("#FF0000"));
                    mButton11.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view11);
                    count11 = 1;
                } else if (count11 == 1) {
                    mButton11.setTextColor(Color.parseColor("#ffffff"));
                    mButton11.setBackgroundResource(R.drawable.set_befor_9);
                    String btn11 = marco2setbtn.getText().toString();
                    String newa = btn11.replaceFirst("k4", "k5");
                    marco2setbtn.setText(newa);
                    count11 = 2;
                } else if (count11 == 2) {
                    String btn11 = marco2setbtn.getText().toString();
                    String newa = btn11.replaceFirst("k5", "");
                    marco2setbtn.setText(newa);
                    count11 = 0;
                }
                break;

            case R.id.btn12:
                if (count12 == 0) {
                    String view12 = ("l4");
                    mButton12.setTextColor(Color.parseColor("#FF0000"));
                    mButton12.setBackgroundResource(R.drawable.set_after_9);
                    marco2setbtn.append(view12);
                    count12 = 1;
                } else if (count12 == 1) {
                    mButton12.setTextColor(Color.parseColor("#ffffff"));
                    mButton12.setBackgroundResource(R.drawable.set_befor_9);
                    String btn12 = marco2setbtn.getText().toString();
                    String newa = btn12.replaceFirst("l4", "l5");
                    marco2setbtn.setText(newa);
                    count12 = 2;
                } else if (count12 == 2) {
                    String btn12 = marco2setbtn.getText().toString();
                    String newa = btn12.replaceFirst("l5", "");
                    marco2setbtn.setText(newa);
                    count12 = 0;
                }
                break;

            case R.id.btn13:
                String view13 = ("Aa");
                mButton13.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view13);

                break;

            case R.id.btn14:
                String view14 = ("Ad");
                mButton14.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view14);

                break;

            case R.id.btn15:
                String view15 = ("Ac");
                mButton15.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view15);
                break;

            case R.id.btn16:
                String view16 = ("Af");
                mButton16.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view16);
                break;

            case R.id.btn17:
                String view17 = ("Ag");
                mButton17.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view17);
                break;

            case R.id.btn18:
                String view18 = ("Ah");
                mButton18.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view18);
                break;

            case R.id.btn19:
                String view19 = ("Ai");
                mButton19.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view19);
                break;

            case R.id.btn20:
                String view20 = ("Aj");
                mButton20.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view20);
                break;

            case R.id.btn21:
                String view21 = ("Ak");
                mButton21.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view21);
                break;

            case R.id.btn22:
                String view22 = ("Al");
                mButton22.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view22);
                break;

            case R.id.btn23:
                String view23 = ("Ab");
                mButton23.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view23);
                break;

            case R.id.btn24:
                String view24 = ("Ae");
                mButton24.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view24);
                break;

//case R.id.btn25:
//String view25 = ("Am");
//mButton25.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view25);
//break;
//
//case R.id.btn26:
//String view26 = ("An");
//mButton26.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view26);
//break;
//
//case R.id.btn27:
//String view27 = ("Ao");
//mButton27.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view27);
//break;
//
//case R.id.btn28:
//String view28 = ("Ap");
//mButton28.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view28);
//break;
//
//case R.id.btn29:
//String view29 = ("Aq");
//mButton29.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view29);
//break;
//
//case R.id.btn30:
//String view30 = ("Ar");
//mButton30.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view30);
//break;
//
//case R.id.btn31:
//String view31 = ("As");
//mButton31.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view31);
//break;
//
//case R.id.btn32:
//String view32 = ("At");
//mButton32.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view32);
//break;

            case R.id.btn33:
                String view33 = ("Tb");
                mButton33.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view33);
                break;

//case R.id.btn34:
//String view34 = ("Tb");
//mButton34.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view34);
//break;

            case R.id.btn35:
                String view35 = ("Te");
                mButton35.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view35);
                break;

//case R.id.btn36:
//String view36 = ("Td");
//mButton36.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view36);
//break;
//
//case R.id.btn37:
//String view37 = ("Te");
//mButton37.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view37);
//break;
//
//case R.id.btn38:
//String view38 = ("Tf");
//mButton38.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view38);
//break;
//
//case R.id.btn39:
//String view39 = ("Tg");
//mButton39.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view39);
//break;
//
//case R.id.btn40:
//String view40 = ("Th");
//mButton40.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view40);
//break;
//
//case R.id.btn41:
//String view41 = ("Ti");
//mButton41.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view41);
//break;

            case R.id.btn42:
                String view42 = ("Tg");
                mButton42.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view42);
                break;

            case R.id.btn43:
                String view43 = ("Th");
                mButton43.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view43);
                break;

            case R.id.btn44:
                String view44 = ("Ti");
                mButton44.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view44);
                break;

            case R.id.btn45:
                String view45 = ("Tj");
                mButton45.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view45);
                break;

            case R.id.btn46:
                String view46 = ("Tk");
                mButton46.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view46);
                break;

            case R.id.btn47:
                String view47 = ("Tl");
                mButton47.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view47);
                break;

            case R.id.btn48:
                String view48 = ("Tm");
                mButton48.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view48);
                break;

            case R.id.btn49:
                String view49 = ("Tn");
                mButton49.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view49);
                break;

            case R.id.btn50:
                String view50 = ("To");
                mButton50.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view50);
                break;

            case R.id.btn51:
                String view51 = ("Tp");
                mButton51.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view51);
                break;

            case R.id.btn52:
                String view52 = ("Tq");
                mButton52.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view52);
                break;

            case R.id.btn53:
                String view53 = ("Tr");
                mButton53.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view53);
                break;

//case R.id.btn54:
//String view54 = ("Xa");
//mButton54.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view54);
//	break;
//
//case R.id.btn55:
//String view55 = ("Xb");
//mButton55.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view55);
//	break;
//
//case R.id.btn56:
//String view56 = ("Xc");
//mButton56.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view56);
//	break;
//
//case R.id.btn57:
//String view57 = ("Xd");
//mButton57.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view57);
//	break;
//
//case R.id.btn58:
//String view58 = ("Xe");
//mButton58.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view58);
//	break;
//
//case R.id.btn59:
//String view59 = ("Xf");
//mButton59.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view59);
//	break;
//
//case R.id.btn60:
//String view60 = ("Xg");
//mButton60.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view60);
//	break;
//
//case R.id.btn61:
//String view61 = ("Xh");
//mButton61.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view61);
//	break;
//
//case R.id.btn62:
//String view62 = ("Xi");
//mButton62.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view62);
//	break;

            case R.id.btn63:
                String view63 = ("Xa");
                mButton63.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view63);
                break;

            case R.id.btn64:
                String view64 = ("Xb");
                mButton64.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view64);
                break;

            case R.id.btn65:
                String view65 = ("Xc");
                mButton65.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view65);
                break;

            case R.id.btn66:
                String view66 = ("Xd");
                mButton66.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view66);
                break;

            case R.id.btn67:
                String view67 = ("Xe");
                mButton67.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view67);
                break;

            case R.id.btn68:
                String view68 = ("Xf");
                mButton68.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view68);
                break;

//case R.id.btn69:
//String view69 = ("Ya");
//mButton69.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view69);
//	break;
//
//case R.id.btn70:
//String view70 = ("Yb");
//mButton70.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view70);
//	break;
//
//case R.id.btn71:
//String view71 = ("Yc");
//mButton71.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view71);
//	break;
//
//case R.id.btn72:
//String view72 = ("Yd");
//mButton72.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view72);
//	break;
//
//case R.id.btn73:
//String view73 = ("Ye");
//mButton73.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view73);
//	break;
//
//case R.id.btn74:
//String view74 = ("Yf");
//mButton74.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view74);
//	break;
//
//case R.id.btn75:
//String view75 = ("Yg");
//mButton75.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view75);
//	break;
//
//case R.id.btn76:
//String view76 = ("Yh");
//mButton76.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view76);
//	break;
//
//case R.id.btn77:
//String view77 = ("Yi");
//mButton77.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view77);
//	break;

            case R.id.btn78:
                String view78 = ("Ya");
                mButton78.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view78);
                break;

            case R.id.btn79:
                String view79 = ("Yb");
                mButton79.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view79);
                break;

            case R.id.btn80:
                String view80 = ("Yc");
                mButton80.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view80);
                break;

            case R.id.btn81:
                String view81 = ("Yd");
                mButton81.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view81);
                break;

            case R.id.btn82:
                String view82 = ("Ye");
                mButton82.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view82);
                break;

            case R.id.btn83:
                String view83 = ("Yf");
                mButton83.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view83);
                break;

//case R.id.btn84:
//String view84 = ("Za");
//mButton84.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view84);
//	break;
//
//case R.id.btn85:
//String view85 = ("Zb");
//mButton85.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view85);
//	break;
//
//case R.id.btn86:
//String view86 = ("Zc");
//mButton86.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view86);
//	break;
//
//case R.id.btn87:
//String view87 = ("Zd");
//mButton87.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view87);
//	break;
//
//case R.id.btn88:
//String view88 = ("Ze");
//mButton88.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view88);
//	break;
//
//case R.id.btn89:
//String view89 = ("Zf");
//mButton89.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view89);
//	break;
//
//case R.id.btn90:
//String view90 = ("Zg");
//mButton90.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view90);
//	break;
//
//case R.id.btn91:
//String view91 = ("Zh");
//mButton91.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view91);
//	break;
//
//case R.id.btn92:
//String view92 = ("Zi");
//mButton92.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view92);
//	break;

            case R.id.btn93:
                String view93 = ("Za");
                mButton93.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view93);
                break;

            case R.id.btn94:
                String view94 = ("Zb");
                mButton94.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view94);
                break;

            case R.id.btn95:
                String view95 = ("Zc");
                mButton95.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view95);
                break;

            case R.id.btn96:
                String view96 = ("Zd");
                mButton96.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view96);
                break;

            case R.id.btn97:
                String view97 = ("Ze");
                mButton97.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view97);
                break;

            case R.id.btn98:
                String view98 = ("Zf");
                mButton98.setTextColor(Color.parseColor("#FF0000"));
                marco2setbtn.append(view98);
                break;

//case R.id.btn99:
//String view99 = ("Xp");
//mButton99.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view99);
//	break;
//
//case R.id.btn100:
//String view100 = ("Xq");
//mButton100.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view100);
//	break;
//
//case R.id.btn101:
//String view101 = ("Xr");
//mButton101.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view101);
//	break;
//
//case R.id.btn102:
//String view102 = ("Xs");
//mButton102.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view102);
//	break;
//
//case R.id.btn103:
//String view103 = ("Xt");
//mButton103.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view103);
//	break;
//
//case R.id.btn104:
//String view104 = ("Xu");
//mButton104.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view104);
//	break;
//
//case R.id.btn105:
//String view105 = ("Yp");
//mButton105.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view105);
//	break;
//
//case R.id.btn106:
//String view106 = ("Yq");
//mButton106.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view106);
//	break;
//
//case R.id.btn107:
//String view107 = ("Yr");
//mButton107.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view107);
//	break;
//
//case R.id.btn108:
//String view108 = ("Ys");
//mButton10.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view108);
//	break;
//
//case R.id.btn109:
//String view109 = ("Yt");
//mButton109.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view109);
//	break;
//
//case R.id.btn110:
//String view110 = ("Yu");
//mButton110.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view110);
//	break;
//
//case R.id.btn111:
//String view111 = ("Zp");
//mButton111.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view111);
//	break;
//
//case R.id.btn112:
//String view112 = ("Zq");
//mButton112.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view112);
//	break;
//
//case R.id.btn113:
//String view113 = ("Zr");
//mButton113.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view113);
//	break;
//
//case R.id.btn114:
//String view114 = ("Zs");
//mButton114.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view114);
//	break;
//
//case R.id.btn115:
//String view115 = ("Zt");
//mButton115.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view115);
//	break;
//
//case R.id.btn116:
//String view116 = ("Zt");
//mButton116.setTextColor(Color.parseColor("#FF0000"));
//marco2setbtn.append(view116);
//	break;

            case R.id.btn153:
                String view153 = ("Tc");
                marco2setbtn.append(view153);
                break;

            case R.id.btn154:
                String view154 = ("Ta");
                marco2setbtn.append(view154);
                break;

            case R.id.btn155:
                String view155 = ("Tf");
                marco2setbtn.append(view155);
                break;

            case R.id.btn156:
                String view156 = ("Td");
                marco2setbtn.append(view156);
                break;


            case R.id.door1:
                String door1 = ("W+sa1");
                marco2setbtn.append(door1);
                break;

            case R.id.door2:
                String door2 = ("W+sb1");
                marco2setbtn.append(door2);
                break;

            case R.id.door3:
                String door3 = ("W+sc1");
                marco2setbtn.append(door3);
                break;
            case R.id.window1up:
                marco2setbtn.append("a8");
                break;
            case R.id.window1stop:
                marco2setbtn.append("a7");
                break;
            case R.id.window1down:
                marco2setbtn.append("a6");
                break;
            case R.id.window2up:
                marco2setbtn.append("b8");
                break;
            case R.id.window2stop:
                marco2setbtn.append("b7");
                break;
            case R.id.window2down:
                marco2setbtn.append("b6");
                break;
            case R.id.window3up:
                marco2setbtn.append("c8");
                break;
            case R.id.window3stop:
                marco2setbtn.append("c7");
                break;
            case R.id.window3down:
                marco2setbtn.append("c6");
                break;
            //tv home
//case R.id.button25:
//	String view39 = ("Tg");
//	String message39 = view39.toString();
//	marco2setbtn.append(message39);
//	break;
//	//tv 靜音
//case R.id.button26:
//	String view41 = ("Ti");
//    String message41 = view41.toString();
//    marco2setbtn.append(message41);
//	break;

        }
    }

    //marco4
    public void Click3Handler(View v) {
        // TODO Auto-generated catch block
        switch (v.getId()) {
            case R.id.marcoset3Clear:
                marco3setbtn.setText("");
                count1 = 0;
                count2 = 0;
                count3 = 0;
                count4 = 0;
                count5 = 0;
                count6 = 0;
                break;
            case R.id.marcoset3ok:

                saveData(8);
                Intent intent6 = new Intent();
                intent6.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                startActivity(intent6);
                finish();

                break;

            case R.id.s16:
                String delay16 = ("'");
                marco3setbtn.append(delay16);
                break;

            case R.id.s17:
                String delay17 = ("(");
                marco3setbtn.append(delay17);
                break;

            case R.id.s18:
                String delay18 = (")");
                marco3setbtn.append(delay18);
                break;

            case R.id.s19:
                String delay19 = ("*");
                marco3setbtn.append(delay19);
                break;

            case R.id.s20:
                String delay20 = ("+");
                marco3setbtn.append(delay20);
                break;

            case R.id.btn01:

                if (count1 == 0) {
                    String marco4btn1 = ("a4");
                    mButton01.setTextColor(Color.parseColor("#FF0000"));
                    mButton01.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(marco4btn1);
                    count1 = 1;
                } else if (count1 == 1) {
                    mButton01.setTextColor(Color.parseColor("#ffffff"));
                    mButton01.setBackgroundResource(R.drawable.set_befor_9);
                    String btn01 = marco3setbtn.getText().toString();
                    String newa = btn01.replaceFirst("a4", "a5");
                    marco3setbtn.setText(newa);
                    count1 = 2;
                } else if (count1 == 2) {
                    String btn01 = marco3setbtn.getText().toString();
                    String newa = btn01.replaceFirst("a5", "");
                    marco3setbtn.setText(newa);
                    count1 = 0;
                }

                break;

            case R.id.btn02:

                if (count2 == 0) {
                    String marco4btn2 = ("b4");
                    mButton02.setTextColor(Color.parseColor("#FF0000"));
                    mButton02.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(marco4btn2);
                    count2 = 1;
                } else if (count2 == 1) {
                    mButton02.setTextColor(Color.parseColor("#ffffff"));
                    mButton02.setBackgroundResource(R.drawable.set_befor_9);
                    String btn02 = marco3setbtn.getText().toString();
                    String newa = btn02.replaceFirst("b4", "b5");
                    marco3setbtn.setText(newa);
                    count2 = 2;
                } else if (count2 == 2) {
                    String btn02 = marco3setbtn.getText().toString();
                    String newa = btn02.replaceFirst("b5", "");
                    marco3setbtn.setText(newa);
                    count2 = 0;
                }
                break;

            case R.id.btn03:
                if (count3 == 0) {
                    String marco4btn3 = ("c4");
                    mButton03.setTextColor(Color.parseColor("#FF0000"));
                    mButton03.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(marco4btn3);
                    count3 = 1;
                } else if (count3 == 1) {
                    mButton03.setTextColor(Color.parseColor("#ffffff"));
                    mButton03.setBackgroundResource(R.drawable.set_befor_9);
                    String btn03 = marco3setbtn.getText().toString();
                    String newa = btn03.replaceFirst("c4", "c5");
                    marco3setbtn.setText(newa);
                    count3 = 2;
                } else if (count3 == 2) {
                    String btn03 = marco3setbtn.getText().toString();
                    String newa = btn03.replaceFirst("c5", "");
                    marco3setbtn.setText(newa);
                    count3 = 0;
                }
                break;

            case R.id.btn04:
                if (count4 == 0) {
                    String view4 = ("d4");
                    mButton04.setTextColor(Color.parseColor("#FF0000"));
                    mButton04.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(view4);
                    count4 = 1;
                } else if (count4 == 1) {
                    mButton04.setTextColor(Color.parseColor("#ffffff"));
                    mButton04.setBackgroundResource(R.drawable.set_befor_9);
                    String btn04 = marco3setbtn.getText().toString();
                    String newa = btn04.replaceFirst("d4", "d5");
                    marco3setbtn.setText(newa);
                    count4 = 2;
                } else if (count4 == 2) {
                    String btn04 = marco3setbtn.getText().toString();
                    String newa = btn04.replaceFirst("d5", "");
                    marco3setbtn.setText(newa);
                    count4 = 0;
                }
                break;

            case R.id.btn05:
                if (count5 == 0) {
                    String view5 = ("e4");
                    mButton05.setTextColor(Color.parseColor("#FF0000"));
                    mButton05.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(view5);
                    count5 = 1;
                } else if (count5 == 1) {
                    mButton05.setTextColor(Color.parseColor("#ffffff"));
                    mButton05.setBackgroundResource(R.drawable.set_befor_9);
                    String btn05 = marco3setbtn.getText().toString();
                    String newa = btn05.replaceFirst("e4", "e5");
                    marco3setbtn.setText(newa);
                    count5 = 2;
                } else if (count5 == 2) {
                    String btn05 = marco3setbtn.getText().toString();
                    String newa = btn05.replaceFirst("e5", "");
                    marco3setbtn.setText(newa);
                    count5 = 0;
                }
                break;

            case R.id.btn06:
                if (count6 == 0) {
                    String view6 = ("f4");
                    mButton06.setTextColor(Color.parseColor("#FF0000"));
                    mButton06.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(view6);
                    count6 = 1;
                } else if (count6 == 1) {
                    mButton06.setTextColor(Color.parseColor("#ffffff"));
                    mButton06.setBackgroundResource(R.drawable.set_befor_9);
                    String btn06 = marco3setbtn.getText().toString();
                    String newa = btn06.replaceFirst("f4", "f5");
                    marco3setbtn.setText(newa);
                    count6 = 2;
                } else if (count6 == 2) {
                    String btn06 = marco3setbtn.getText().toString();
                    String newa = btn06.replaceFirst("f5", "");
                    marco3setbtn.setText(newa);
                    count6 = 0;
                }
                break;

            case R.id.btn07:
                if (count7 == 0) {
                    String view7 = ("g4");
                    mButton07.setTextColor(Color.parseColor("#FF0000"));
                    mButton07.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(view7);
                    count7 = 1;
                } else if (count7 == 1) {
                    mButton07.setTextColor(Color.parseColor("#ffffff"));
                    mButton07.setBackgroundResource(R.drawable.set_befor_9);
                    String btn07 = marco3setbtn.getText().toString();
                    String newa = btn07.replaceFirst("g4", "g5");
                    marco3setbtn.setText(newa);
                    count7 = 2;
                } else if (count7 == 2) {
                    String btn07 = marco3setbtn.getText().toString();
                    String newa = btn07.replaceFirst("g5", "");
                    marco3setbtn.setText(newa);
                    count7 = 0;
                }
                break;

            case R.id.btn08:
                if (count8 == 0) {
                    String view8 = ("h4");
                    mButton08.setTextColor(Color.parseColor("#FF0000"));
                    mButton08.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(view8);
                    count8 = 1;
                } else if (count8 == 1) {
                    mButton08.setTextColor(Color.parseColor("#ffffff"));
                    mButton08.setBackgroundResource(R.drawable.set_befor_9);
                    String btn08 = marco3setbtn.getText().toString();
                    String newa = btn08.replaceFirst("h4", "h5");
                    marco3setbtn.setText(newa);
                    count8 = 2;
                } else if (count8 == 2) {
                    String btn08 = marco3setbtn.getText().toString();
                    String newa = btn08.replaceFirst("h5", "");
                    marco3setbtn.setText(newa);
                    count8 = 0;
                }
                break;

            case R.id.btn09:
                if (count9 == 0) {
                    String view9 = ("i4");
                    mButton09.setTextColor(Color.parseColor("#FF0000"));
                    mButton09.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(view9);
                    count9 = 1;
                } else if (count9 == 1) {
                    mButton09.setTextColor(Color.parseColor("#ffffff"));
                    mButton09.setBackgroundResource(R.drawable.set_befor_9);
                    String btn09 = marco3setbtn.getText().toString();
                    String newa = btn09.replaceFirst("i4", "i5");
                    marco3setbtn.setText(newa);
                    count9 = 2;
                } else if (count9 == 2) {
                    String btn09 = marco3setbtn.getText().toString();
                    String newa = btn09.replaceFirst("i5", "");
                    marco3setbtn.setText(newa);
                    count9 = 0;
                }
                break;

            case R.id.btn10:
                if (count10 == 0) {
                    String view10 = ("j4");
                    mButton10.setTextColor(Color.parseColor("#FF0000"));
                    mButton10.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(view10);
                    count10 = 1;
                } else if (count10 == 1) {
                    mButton10.setTextColor(Color.parseColor("#ffffff"));
                    mButton10.setBackgroundResource(R.drawable.set_befor_9);
                    String btn10 = marco3setbtn.getText().toString();
                    String newa = btn10.replaceFirst("j4", "j5");
                    marco3setbtn.setText(newa);
                    count10 = 2;
                } else if (count10 == 2) {
                    String btn10 = marco3setbtn.getText().toString();
                    String newa = btn10.replaceFirst("j5", "");
                    marco3setbtn.setText(newa);
                    count10 = 0;
                }
                break;

            case R.id.btn11:
                if (count11 == 0) {
                    String view11 = ("k4");
                    mButton11.setTextColor(Color.parseColor("#FF0000"));
                    mButton11.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(view11);
                    count11 = 1;
                } else if (count11 == 1) {
                    mButton11.setTextColor(Color.parseColor("#ffffff"));
                    mButton11.setBackgroundResource(R.drawable.set_befor_9);
                    String btn11 = marco3setbtn.getText().toString();
                    String newa = btn11.replaceFirst("k4", "k5");
                    marco3setbtn.setText(newa);
                    count11 = 2;
                } else if (count11 == 2) {
                    String btn11 = marco3setbtn.getText().toString();
                    String newa = btn11.replaceFirst("k5", "");
                    marco3setbtn.setText(newa);
                    count11 = 0;
                }
                break;

            case R.id.btn12:
                if (count12 == 0) {
                    String view12 = ("l4");
                    mButton12.setTextColor(Color.parseColor("#FF0000"));
                    mButton12.setBackgroundResource(R.drawable.set_after_9);
                    marco3setbtn.append(view12);
                    count12 = 1;
                } else if (count12 == 1) {
                    mButton12.setTextColor(Color.parseColor("#ffffff"));
                    mButton12.setBackgroundResource(R.drawable.set_befor_9);
                    String btn12 = marco3setbtn.getText().toString();
                    String newa = btn12.replaceFirst("l4", "l5");
                    marco3setbtn.setText(newa);
                    count12 = 2;
                } else if (count12 == 2) {
                    String btn12 = marco3setbtn.getText().toString();
                    String newa = btn12.replaceFirst("l5", "");
                    marco3setbtn.setText(newa);
                    count12 = 0;
                }
                break;

            case R.id.btn13:
                String view13 = ("Aa");
                mButton13.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view13);

                break;

            case R.id.btn14:
                String view14 = ("Ad");
                mButton14.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view14);

                break;

            case R.id.btn15:
                String view15 = ("Ac");
                mButton15.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view15);
                break;

            case R.id.btn16:
                String view16 = ("Af");
                mButton16.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view16);
                break;

            case R.id.btn17:
                String view17 = ("Ag");
                mButton17.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view17);
                break;

            case R.id.btn18:
                String view18 = ("Ah");
                mButton18.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view18);
                break;

            case R.id.btn19:
                String view19 = ("Ai");
                mButton19.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view19);
                break;

            case R.id.btn20:
                String view20 = ("Aj");
                mButton20.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view20);
                break;

            case R.id.btn21:
                String view21 = ("Ak");
                mButton21.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view21);
                break;

            case R.id.btn22:
                String view22 = ("Al");
                mButton22.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view22);
                break;

            case R.id.btn23:
                String view23 = ("Ab");
                mButton23.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view23);
                break;

            case R.id.btn24:
                String view24 = ("Ae");
                mButton24.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view24);
                break;

//case R.id.btn25:
//String view25 = ("Am");
//mButton25.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view25);
//break;
//
//case R.id.btn26:
//String view26 = ("An");
//mButton26.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view26);
//break;
//
//case R.id.btn27:
//String view27 = ("Ao");
//mButton27.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view27);
//break;
//
//case R.id.btn28:
//String view28 = ("Ap");
//mButton28.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view28);
//break;

//case R.id.btn29:
//String view29 = ("Aq");
//mButton29.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view29);
//break;
//
//case R.id.btn30:
//String view30 = ("Ar");
//mButton30.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view30);
//break;
//
//case R.id.btn31:
//String view31 = ("As");
//mButton31.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view31);
//break;

//case R.id.btn32:
//String view32 = ("At");
//mButton32.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view32);
//break;

            case R.id.btn33:
                String view33 = ("Tb");
                mButton33.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view33);
                break;

//case R.id.btn34:
//String view34 = ("Tb");
//mButton34.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view34);
//break;

            case R.id.btn35:
                String view35 = ("Te");
                mButton35.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view35);
                break;

//case R.id.btn36:
//String view36 = ("Td");
//mButton36.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view36);
//break;
//
//case R.id.btn37:
//String view37 = ("Te");
//mButton37.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view37);
//break;
//
//case R.id.btn38:
//String view38 = ("Tf");
//mButton38.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view38);
//break;
//
//case R.id.btn39:
//String view39 = ("Tg");
//mButton39.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view39);
//break;
//
//case R.id.btn40:
//String view40 = ("Th");
//mButton40.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view40);
//break;
//
//case R.id.btn41:
//String view41 = ("Ti");
//mButton41.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view41);
//break;

            case R.id.btn42:
                String view42 = ("Tg");
                mButton42.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view42);
                break;

            case R.id.btn43:
                String view43 = ("Th");
                mButton43.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view43);
                break;

            case R.id.btn44:
                String view44 = ("Ti");
                mButton44.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view44);
                break;

            case R.id.btn45:
                String view45 = ("Tj");
                mButton45.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view45);
                break;

            case R.id.btn46:
                String view46 = ("Tk");
                mButton46.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view46);
                break;

            case R.id.btn47:
                String view47 = ("Tl");
                mButton47.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view47);
                break;

            case R.id.btn48:
                String view48 = ("Tm");
                mButton48.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view48);
                break;

            case R.id.btn49:
                String view49 = ("Tn");
                mButton49.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view49);
                break;

            case R.id.btn50:
                String view50 = ("To");
                mButton50.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view50);
                break;

            case R.id.btn51:
                String view51 = ("Tp");
                mButton51.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view51);
                break;

            case R.id.btn52:
                String view52 = ("Tq");
                mButton52.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view52);
                break;

            case R.id.btn53:
                String view53 = ("Tr");
                mButton53.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view53);
                break;

//case R.id.btn54:
//String view54 = ("Xa");
//mButton54.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view54);
//	break;
//
//case R.id.btn55:
//String view55 = ("Xb");
//mButton55.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view55);
//	break;
//
//case R.id.btn56:
//String view56 = ("Xc");
//mButton56.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view56);
//	break;
//
//case R.id.btn57:
//String view57 = ("Xd");
//mButton57.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view57);
//	break;
//
//case R.id.btn58:
//String view58 = ("Xe");
//mButton58.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view58);
//	break;
//
//case R.id.btn59:
//String view59 = ("Xf");
//mButton59.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view59);
//	break;
//
//case R.id.btn60:
//String view60 = ("Xg");
//mButton60.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view60);
//	break;
//
//case R.id.btn61:
//String view61 = ("Xh");
//mButton61.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view61);
//	break;
//
//case R.id.btn62:
//String view62 = ("Xi");
//mButton62.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view62);
//	break;

            case R.id.btn63:
                String view63 = ("Xa");
                mButton63.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view63);
                break;

            case R.id.btn64:
                String view64 = ("Xb");
                mButton64.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view64);
                break;

            case R.id.btn65:
                String view65 = ("Xc");
                mButton65.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view65);
                break;

            case R.id.btn66:
                String view66 = ("Xd");
                mButton66.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view66);
                break;

            case R.id.btn67:
                String view67 = ("Xe");
                mButton67.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view67);
                break;

            case R.id.btn68:
                String view68 = ("Xf");
                mButton68.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view68);
                break;

//case R.id.btn69:
//String view69 = ("Ya");
//mButton69.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view69);
//	break;
//
//case R.id.btn70:
//String view70 = ("Yb");
//mButton70.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view70);
//	break;
//
//case R.id.btn71:
//String view71 = ("Yc");
//mButton71.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view71);
//	break;
//
//case R.id.btn72:
//String view72 = ("Yd");
//mButton72.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view72);
//	break;
//
//case R.id.btn73:
//String view73 = ("Ye");
//mButton73.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view73);
//	break;
//
//case R.id.btn74:
//String view74 = ("Yf");
//mButton74.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view74);
//	break;
//
//case R.id.btn75:
//String view75 = ("Yg");
//mButton75.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view75);
//	break;
//
//case R.id.btn76:
//String view76 = ("Yh");
//mButton76.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view76);
//	break;
//
//case R.id.btn77:
//String view77 = ("Yi");
//mButton77.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view77);
//	break;

            case R.id.btn78:
                String view78 = ("Ya");
                mButton78.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view78);
                break;

            case R.id.btn79:
                String view79 = ("Yb");
                mButton79.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view79);
                break;

            case R.id.btn80:
                String view80 = ("Yc");
                mButton80.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view80);
                break;

            case R.id.btn81:
                String view81 = ("Yd");
                mButton81.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view81);
                break;

            case R.id.btn82:
                String view82 = ("Ye");
                mButton82.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view82);
                break;

            case R.id.btn83:
                String view83 = ("Yf");
                mButton83.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view83);
                break;

//case R.id.btn84:
//String view84 = ("Za");
//mButton84.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view84);
//	break;
//
//case R.id.btn85:
//String view85 = ("Zb");
//mButton85.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view85);
//	break;
//
//case R.id.btn86:
//String view86 = ("Zc");
//mButton86.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view86);
//	break;
//
//case R.id.btn87:
//String view87 = ("Zd");
//mButton87.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view87);
//	break;
//
//case R.id.btn88:
//String view88 = ("Ze");
//mButton88.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view88);
//	break;
//
//case R.id.btn89:
//String view89 = ("Zf");
//mButton89.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view89);
//	break;
//
//case R.id.btn90:
//String view90 = ("Zg");
//mButton90.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view90);
//	break;
//
//case R.id.btn91:
//String view91 = ("Zh");
//mButton91.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view91);
//	break;
//
//case R.id.btn92:
//String view92 = ("Zi");
//mButton92.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view92);
//	break;

            case R.id.btn93:
                String view93 = ("Za");
                mButton93.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view93);
                break;

            case R.id.btn94:
                String view94 = ("Zb");
                mButton94.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view94);
                break;

            case R.id.btn95:
                String view95 = ("Zc");
                mButton95.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view95);
                break;

            case R.id.btn96:
                String view96 = ("Zd");
                mButton96.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view96);
                break;

            case R.id.btn97:
                String view97 = ("Ze");
                mButton97.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view97);
                break;

            case R.id.btn98:
                String view98 = ("Zf");
                mButton98.setTextColor(Color.parseColor("#FF0000"));
                marco3setbtn.append(view98);
                break;

//case R.id.btn99:
//String view99 = ("Xp");
//mButton99.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view99);
//	break;
//
//case R.id.btn100:
//String view100 = ("Xq");
//mButton100.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view100);
//	break;

//case R.id.btn101:
//String view101 = ("Xr");
//mButton101.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view101);
//	break;
//
//case R.id.btn102:
//String view102 = ("Xs");
//mButton102.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view102);
//	break;
//
//case R.id.btn103:
//String view103 = ("Xt");
//mButton103.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view103);
//	break;
//
//case R.id.btn104:
//String view104 = ("Xu");
//mButton104.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view104);
//	break;
//
//case R.id.btn105:
//String view105 = ("Yp");
//mButton105.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view105);
//	break;
//
//case R.id.btn106:
//String view106 = ("Yq");
//mButton106.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view106);
//	break;
//
//case R.id.btn107:
//String view107 = ("Yr");
//mButton107.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view107);
//	break;
//
//case R.id.btn108:
//String view108 = ("Ys");
//mButton10.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view108);
//	break;
//
//case R.id.btn109:
//String view109 = ("Yt");
//mButton109.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view109);
//	break;
//
//case R.id.btn110:
//String view110 = ("Yu");
//mButton110.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view110);
//	break;

//case R.id.btn111:
//String view111 = ("Zp");
//mButton111.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view111);
//	break;
//
//case R.id.btn112:
//String view112 = ("Zq");
//mButton112.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view112);
//	break;
//
//case R.id.btn113:
//String view113 = ("Zr");
//mButton113.setTextColor(Color.parseColor("#FF0000"));
//marco3setbtn.append(view113);
//	break;
//
//case R.id.btn114:
//	String view114 = ("Zs");
//	mButton114.setTextColor(Color.parseColor("#FF0000"));
//	marco3setbtn.append(view114);
//	break;
//
//case R.id.btn115:
//	String view115 = ("Zt");
//	mButton115.setTextColor(Color.parseColor("#FF0000"));
//	marco3setbtn.append(view115);
//	break;
//
//case R.id.btn116:
//	String view116 = ("Zt");
//	mButton116.setTextColor(Color.parseColor("#FF0000"));
//	marco3setbtn.append(view116);
//	break;

            case R.id.btn153:
                String view153 = ("Tc");
                marco3setbtn.append(view153);
                break;

            case R.id.btn154:
                String view154 = ("Ta");
                marco3setbtn.append(view154);
                break;

            case R.id.btn155:
                String view155 = ("Tf");
                marco3setbtn.append(view155);
                break;

            case R.id.btn156:
                String view156 = ("Td");
                marco3setbtn.append(view156);
                break;


            case R.id.door1:
                String door1 = ("W+sa1");
                marco3setbtn.append(door1);
                break;

            case R.id.door2:
                String door2 = ("W+sb1");
                marco3setbtn.append(door2);
                break;

            case R.id.door3:
                String door3 = ("W+sc1");
                marco3setbtn.append(door3);
                break;
            case R.id.window1up:
                marco3setbtn.append("a8");
                break;
            case R.id.window1stop:
                marco3setbtn.append("a7");
                break;
            case R.id.window1down:
                marco3setbtn.append("a6");
                break;
            case R.id.window2up:
                marco3setbtn.append("b8");
                break;
            case R.id.window2stop:
                marco3setbtn.append("b7");
                break;
            case R.id.window2down:
                marco3setbtn.append("b6");
                break;
            case R.id.window3up:
                marco3setbtn.append("c8");
                break;
            case R.id.window3stop:
                marco3setbtn.append("c7");
                break;
            case R.id.window3down:
                marco3setbtn.append("c6");
                break;
            //tv home
//case R.id.button25:
//	String view39 = ("Tg");
//	String message39 = view39.toString();
//	marco3setbtn.append(message39);
//	break;
//	//tv 靜音
//case R.id.button26:
//	String view41 = ("Ti");
//    String message41 = view41.toString();
//    marco3setbtn.append(message41);
//	break;

        }
    }

    //marco5
    public void Click4Handler(View v) {
        // TODO Auto-generated catch block


        switch (v.getId()) {
            case R.id.marcoset4Clear:
                marco4setbtn.setText("");
                count1 = 0;
                count2 = 0;
                count3 = 0;
                count4 = 0;
                count5 = 0;
                count6 = 0;
                break;
            case R.id.marcoset4ok:

                saveData(9);
                Intent intent7 = new Intent();
                intent7.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                startActivity(intent7);
                finish();

                break;

            case R.id.s21:
                String delay21 = ("'");
                marco4setbtn.append(delay21);
                break;

            case R.id.s22:
                String delay22 = ("(");
                marco4setbtn.append(delay22);
                break;

            case R.id.s23:
                String delay23 = (")");
                marco4setbtn.append(delay23);
                break;

            case R.id.s24:
                String delay24 = ("*");
                marco4setbtn.append(delay24);
                break;

            case R.id.s25:
                String delay25 = ("+");
                marco4setbtn.append(delay25);
                break;

            case R.id.btn01:

                if (count1 == 0) {
                    String view = ("a4");
                    mButton01.setTextColor(Color.parseColor("#FF0000"));
                    mButton01.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view);
                    count1 = 1;
                } else if (count1 == 1) {
                    mButton01.setTextColor(Color.parseColor("#ffffff"));
                    mButton01.setBackgroundResource(R.drawable.set_befor_9);
                    String btn01 = marco4setbtn.getText().toString();
                    String newa = btn01.replaceFirst("a4", "a5");
                    marco4setbtn.setText(newa);
                    count1 = 2;
                } else if (count1 == 2) {
                    String btn01 = marco4setbtn.getText().toString();
                    String newa = btn01.replaceFirst("a5", "");
                    marco4setbtn.setText(newa);
                    count1 = 0;
                }

                break;

            case R.id.btn02:

                if (count2 == 0) {
                    String view2 = ("b4");
                    mButton02.setTextColor(Color.parseColor("#FF0000"));
                    mButton02.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view2);
                    count2 = 1;
                } else if (count2 == 1) {
                    mButton02.setTextColor(Color.parseColor("#ffffff"));
                    mButton02.setBackgroundResource(R.drawable.set_befor_9);
                    String btn02 = marco4setbtn.getText().toString();
                    String newa = btn02.replaceFirst("b4", "b5");
                    marco4setbtn.setText(newa);
                    count2 = 2;
                } else if (count2 == 2) {
                    String btn02 = marco4setbtn.getText().toString();
                    String newa = btn02.replaceFirst("b5", "");
                    marco4setbtn.setText(newa);
                    count2 = 0;
                }
                break;

            case R.id.btn03:
                if (count3 == 0) {
                    String view3 = ("c4");
                    mButton03.setTextColor(Color.parseColor("#FF0000"));
                    mButton03.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view3);
                    count3 = 1;
                } else if (count3 == 1) {
                    mButton03.setTextColor(Color.parseColor("#ffffff"));
                    mButton03.setBackgroundResource(R.drawable.set_befor_9);
                    String btn03 = marco4setbtn.getText().toString();
                    String newa = btn03.replaceFirst("c4", "c5");
                    marco4setbtn.setText(newa);
                    count3 = 2;
                } else if (count3 == 2) {
                    String btn03 = marco4setbtn.getText().toString();
                    String newa = btn03.replaceFirst("c5", "");
                    marco4setbtn.setText(newa);
                    count3 = 0;
                }
                break;

            case R.id.btn04:
                if (count4 == 0) {
                    String view4 = ("d4");
                    mButton04.setTextColor(Color.parseColor("#FF0000"));
                    mButton04.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view4);
                    count4 = 1;
                } else if (count4 == 1) {
                    mButton04.setTextColor(Color.parseColor("#ffffff"));
                    mButton04.setBackgroundResource(R.drawable.set_befor_9);
                    String btn04 = marco4setbtn.getText().toString();
                    String newa = btn04.replaceFirst("d4", "d5");
                    marco4setbtn.setText(newa);
                    count4 = 2;
                } else if (count4 == 2) {
                    String btn04 = marco4setbtn.getText().toString();
                    String newa = btn04.replaceFirst("d5", "");
                    marco4setbtn.setText(newa);
                    count4 = 0;
                }
                break;

            case R.id.btn05:
                if (count5 == 0) {
                    String view5 = ("e4");
                    mButton05.setTextColor(Color.parseColor("#FF0000"));
                    mButton05.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view5);
                    count5 = 1;
                } else if (count5 == 1) {
                    mButton05.setTextColor(Color.parseColor("#ffffff"));
                    mButton05.setBackgroundResource(R.drawable.set_befor_9);
                    String btn05 = marco4setbtn.getText().toString();
                    String newa = btn05.replaceFirst("e4", "e5");
                    marco4setbtn.setText(newa);
                    count5 = 2;
                } else if (count5 == 2) {
                    String btn05 = marco4setbtn.getText().toString();
                    String newa = btn05.replaceFirst("e5", "");
                    marco4setbtn.setText(newa);
                    count5 = 0;
                }
                break;

            case R.id.btn06:
                if (count6 == 0) {
                    String view6 = ("f4");
                    mButton06.setTextColor(Color.parseColor("#FF0000"));
                    mButton06.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view6);
                    count6 = 1;
                } else if (count6 == 1) {
                    mButton06.setTextColor(Color.parseColor("#ffffff"));
                    mButton06.setBackgroundResource(R.drawable.set_befor_9);
                    String btn06 = marco4setbtn.getText().toString();
                    String newa = btn06.replaceFirst("f4", "f5");
                    marco4setbtn.setText(newa);
                    count6 = 2;
                } else if (count6 == 2) {
                    String btn06 = marco4setbtn.getText().toString();
                    String newa = btn06.replaceFirst("f5", "");
                    marco4setbtn.setText(newa);
                    count6 = 0;
                }
                break;

            case R.id.btn07:
                if (count7 == 0) {
                    String view7 = ("g4");
                    mButton07.setTextColor(Color.parseColor("#FF0000"));
                    mButton07.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view7);
                    count7 = 1;
                } else if (count7 == 1) {
                    mButton07.setTextColor(Color.parseColor("#ffffff"));
                    mButton07.setBackgroundResource(R.drawable.set_befor_9);
                    String btn07 = marco4setbtn.getText().toString();
                    String newa = btn07.replaceFirst("g4", "g5");
                    marco4setbtn.setText(newa);
                    count7 = 2;
                } else if (count7 == 2) {
                    String btn07 = marco4setbtn.getText().toString();
                    String newa = btn07.replaceFirst("g5", "");
                    marco4setbtn.setText(newa);
                    count7 = 0;
                }
                break;

            case R.id.btn08:
                if (count8 == 0) {
                    String view8 = ("h4");
                    mButton08.setTextColor(Color.parseColor("#FF0000"));
                    mButton08.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view8);
                    count8 = 1;
                } else if (count8 == 1) {
                    mButton08.setTextColor(Color.parseColor("#ffffff"));
                    mButton08.setBackgroundResource(R.drawable.set_befor_9);
                    String btn08 = marco4setbtn.getText().toString();
                    String newa = btn08.replaceFirst("h4", "h5");
                    marco4setbtn.setText(newa);
                    count8 = 2;
                } else if (count8 == 2) {
                    String btn08 = marco4setbtn.getText().toString();
                    String newa = btn08.replaceFirst("h5", "");
                    marco4setbtn.setText(newa);
                    count8 = 0;
                }
                break;

            case R.id.btn09:
                if (count9 == 0) {
                    String view9 = ("i4");
                    mButton09.setTextColor(Color.parseColor("#FF0000"));
                    mButton09.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view9);
                    count9 = 1;
                } else if (count9 == 1) {
                    mButton09.setTextColor(Color.parseColor("#ffffff"));
                    mButton09.setBackgroundResource(R.drawable.set_befor_9);
                    String btn09 = marco4setbtn.getText().toString();
                    String newa = btn09.replaceFirst("i4", "i5");
                    marco4setbtn.setText(newa);
                    count9 = 2;
                } else if (count9 == 2) {
                    String btn09 = marco4setbtn.getText().toString();
                    String newa = btn09.replaceFirst("i5", "");
                    marco4setbtn.setText(newa);
                    count9 = 0;
                }
                break;

            case R.id.btn10:
                if (count10 == 0) {
                    String view10 = ("j4");
                    mButton10.setTextColor(Color.parseColor("#FF0000"));
                    mButton10.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view10);
                    count10 = 1;
                } else if (count10 == 1) {
                    mButton10.setTextColor(Color.parseColor("#ffffff"));
                    mButton10.setBackgroundResource(R.drawable.set_befor_9);
                    String btn10 = marcosetbtn.getText().toString();
                    String newa = btn10.replaceFirst("j4", "j5");
                    marco4setbtn.setText(newa);
                    count10 = 2;
                } else if (count10 == 2) {
                    String btn10 = marco4setbtn.getText().toString();
                    String newa = btn10.replaceFirst("j5", "");
                    marco4setbtn.setText(newa);
                    count10 = 0;
                }
                break;

            case R.id.btn11:
                if (count11 == 0) {
                    String view11 = ("k4");
                    mButton11.setTextColor(Color.parseColor("#FF0000"));
                    mButton11.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view11);
                    count11 = 1;
                } else if (count11 == 1) {
                    mButton11.setTextColor(Color.parseColor("#ffffff"));
                    mButton11.setBackgroundResource(R.drawable.set_befor_9);
                    String btn11 = marco4setbtn.getText().toString();
                    String newa = btn11.replaceFirst("k4", "k5");
                    marco4setbtn.setText(newa);
                    count11 = 2;
                } else if (count11 == 2) {
                    String btn11 = marco4setbtn.getText().toString();
                    String newa = btn11.replaceFirst("k5", "");
                    marco4setbtn.setText(newa);
                    count11 = 0;
                }
                break;

            case R.id.btn12:
                if (count12 == 0) {
                    String view12 = ("l4");
                    mButton12.setTextColor(Color.parseColor("#FF0000"));
                    mButton12.setBackgroundResource(R.drawable.set_after_9);
                    marco4setbtn.append(view12);
                    count12 = 1;
                } else if (count12 == 1) {
                    mButton12.setTextColor(Color.parseColor("#ffffff"));
                    mButton12.setBackgroundResource(R.drawable.set_befor_9);
                    String btn12 = marco4setbtn.getText().toString();
                    String newa = btn12.replaceFirst("l4", "l5");
                    marco4setbtn.setText(newa);
                    count12 = 2;
                } else if (count12 == 2) {
                    String btn12 = marco4setbtn.getText().toString();
                    String newa = btn12.replaceFirst("l5", "");
                    marco4setbtn.setText(newa);
                    count12 = 0;
                }
                break;

            case R.id.btn13:
                String view13 = ("Aa");
                mButton13.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view13);

                break;

            case R.id.btn14:
                String view14 = ("Ad");
                mButton14.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view14);

                break;

            case R.id.btn15:
                String view15 = ("Ac");
                mButton15.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view15);
                break;

            case R.id.btn16:
                String view16 = ("Af");
                mButton16.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view16);
                break;

            case R.id.btn17:
                String view17 = ("Ag");
                mButton17.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view17);
                break;

            case R.id.btn18:
                String view18 = ("Ah");
                mButton18.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view18);
                break;

            case R.id.btn19:
                String view19 = ("Ai");
                mButton19.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view19);
                break;

            case R.id.btn20:
                String view20 = ("Aj");
                mButton20.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view20);
                break;

            case R.id.btn21:
                String view21 = ("Ak");
                mButton21.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view21);
                break;

            case R.id.btn22:
                String view22 = ("Al");
                mButton22.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view22);
                break;

            case R.id.btn23:
                String view23 = ("Ab");
                mButton23.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view23);
                break;

            case R.id.btn24:
                String view24 = ("Ae");
                mButton24.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view24);
                break;

//case R.id.btn25:
//String view25 = ("Am");
//mButton25.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view25);
//break;
//
//case R.id.btn26:
//String view26 = ("An");
//mButton26.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view26);
//break;
//
//case R.id.btn27:
//String view27 = ("Ao");
//mButton27.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view27);
//break;
//
//case R.id.btn28:
//String view28 = ("Ap");
//mButton28.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view28);
//break;
//
//case R.id.btn29:
//String view29 = ("Aq");
//mButton29.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view29);
//break;
//
//case R.id.btn30:
//String view30 = ("Ar");
//mButton30.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view30);
//break;
//
//case R.id.btn31:
//String view31 = ("As");
//mButton31.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view31);
//break;

//case R.id.btn32:
//String view32 = ("At");
//mButton32.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view32);
//break;

            case R.id.btn33:
                String view33 = ("Tb");
                mButton33.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view33);
                break;

//case R.id.btn34:
//String view34 = ("Tb");
//mButton34.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view34);
//break;

            case R.id.btn35:
                String view35 = ("Te");
                mButton35.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view35);
                break;

//case R.id.btn36:
//String view36 = ("Td");
//mButton36.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view36);
//break;
//
//case R.id.btn37:
//String view37 = ("Te");
//mButton37.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view37);
//break;
//
//case R.id.btn38:
//String view38 = ("Tf");
//mButton38.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view38);
//break;

//case R.id.btn39:
//String view39 = ("Tg");
//mButton39.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view39);
//break;

//case R.id.btn40:
//String view40 = ("Th");
//mButton40.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view40);
//break;

//case R.id.btn41:
//String view41 = ("Ti");
//mButton41.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view41);
//break;

            case R.id.btn42:
                String view42 = ("Tg");
                mButton42.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view42);
                break;

            case R.id.btn43:
                String view43 = ("Th");
                mButton43.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view43);
                break;

            case R.id.btn44:
                String view44 = ("Ti");
                mButton44.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view44);
                break;

            case R.id.btn45:
                String view45 = ("Tj");
                mButton45.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view45);
                break;

            case R.id.btn46:
                String view46 = ("Tk");
                mButton46.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view46);
                break;

            case R.id.btn47:
                String view47 = ("Tl");
                mButton47.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view47);
                break;

            case R.id.btn48:
                String view48 = ("Tm");
                mButton48.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view48);
                break;

            case R.id.btn49:
                String view49 = ("Tn");
                mButton49.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view49);
                break;

            case R.id.btn50:
                String view50 = ("To");
                mButton50.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view50);
                break;

            case R.id.btn51:
                String view51 = ("Tp");
                mButton51.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view51);
                break;

            case R.id.btn52:
                String view52 = ("Tq");
                mButton52.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view52);
                break;

            case R.id.btn53:
                String view53 = ("Tr");
                mButton53.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view53);
                break;

//case R.id.btn54:
//String view54 = ("Xa");
//mButton54.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view54);
//	break;
//
//case R.id.btn55:
//String view55 = ("Xb");
//mButton55.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view55);
//	break;
//
//case R.id.btn56:
//String view56 = ("Xc");
//mButton56.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view56);
//	break;
//
//case R.id.btn57:
//String view57 = ("Xd");
//mButton57.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view57);
//	break;
//
//case R.id.btn58:
//String view58 = ("Xe");
//mButton58.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view58);
//	break;
//
//case R.id.btn59:
//String view59 = ("Xf");
//mButton59.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view59);
//	break;
//
//case R.id.btn60:
//String view60 = ("Xg");
//mButton60.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view60);
//	break;
//
//case R.id.btn61:
//String view61 = ("Xh");
//mButton61.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view61);
//	break;
//
//case R.id.btn62:
//String view62 = ("Xi");
//mButton62.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view62);
//	break;

            case R.id.btn63:
                String view63 = ("Xa");
                mButton63.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view63);
                break;

            case R.id.btn64:
                String view64 = ("Xb");
                mButton64.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view64);
                break;

            case R.id.btn65:
                String view65 = ("Xc");
                mButton65.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view65);
                break;

            case R.id.btn66:
                String view66 = ("Xd");
                mButton66.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view66);
                break;

            case R.id.btn67:
                String view67 = ("Xe");
                mButton67.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view67);
                break;

            case R.id.btn68:
                String view68 = ("Xf");
                mButton68.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view68);
                break;

//case R.id.btn69:
//String view69 = ("Ya");
//mButton69.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view69);
//	break;
//
//case R.id.btn70:
//String view70 = ("Yb");
//mButton70.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view70);
//	break;
//
//case R.id.btn71:
//String view71 = ("Yc");
//mButton71.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view71);
//	break;
//
//case R.id.btn72:
//String view72 = ("Yd");
//mButton72.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view72);
//	break;
//
//case R.id.btn73:
//String view73 = ("Ye");
//mButton73.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view73);
//	break;
//
//case R.id.btn74:
//String view74 = ("Yf");
//mButton74.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view74);
//	break;
//
//case R.id.btn75:
//String view75 = ("Yg");
//mButton75.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view75);
//	break;
//
//case R.id.btn76:
//String view76 = ("Yh");
//mButton76.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view76);
//	break;
//
//case R.id.btn77:
//String view77 = ("Yi");
//mButton77.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view77);
//	break;

            case R.id.btn78:
                String view78 = ("Ya");
                mButton78.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view78);
                break;

            case R.id.btn79:
                String view79 = ("Yb");
                mButton79.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view79);
                break;

            case R.id.btn80:
                String view80 = ("Yc");
                mButton80.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view80);
                break;

            case R.id.btn81:
                String view81 = ("Yd");
                mButton81.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view81);
                break;

            case R.id.btn82:
                String view82 = ("Ye");
                mButton82.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view82);
                break;

            case R.id.btn83:
                String view83 = ("Yf");
                mButton83.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view83);
                break;

//case R.id.btn84:
//String view84 = ("Za");
//mButton84.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view84);
//	break;
//
//case R.id.btn85:
//String view85 = ("Zb");
//mButton85.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view85);
//	break;
//
//case R.id.btn86:
//String view86 = ("Zc");
//mButton86.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view86);
//	break;
//
//case R.id.btn87:
//String view87 = ("Zd");
//mButton87.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view87);
//	break;
//
//case R.id.btn88:
//String view88 = ("Ze");
//mButton88.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view88);
//	break;
//
//case R.id.btn89:
//String view89 = ("Zf");
//mButton89.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view89);
//	break;
//
//case R.id.btn90:
//String view90 = ("Zg");
//mButton90.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view90);
//	break;
//
//case R.id.btn91:
//String view91 = ("Zh");
//mButton91.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view91);
//	break;
//
//case R.id.btn92:
//String view92 = ("Zi");
//mButton92.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view92);
//	break;

            case R.id.btn93:
                String view93 = ("Za");
                mButton93.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view93);
                break;

            case R.id.btn94:
                String view94 = ("Zb");
                mButton94.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view94);
                break;

            case R.id.btn95:
                String view95 = ("Zc");
                mButton95.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view95);
                break;

            case R.id.btn96:
                String view96 = ("Zd");
                mButton96.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view96);
                break;

            case R.id.btn97:
                String view97 = ("Ze");
                mButton97.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view97);
                break;

            case R.id.btn98:
                String view98 = ("Zf");
                mButton98.setTextColor(Color.parseColor("#FF0000"));
                marco4setbtn.append(view98);
                break;

//case R.id.btn99:
//String view99 = ("Xp");
//mButton99.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view99);
//	break;
//
//case R.id.btn100:
//String view100 = ("Xq");
//mButton100.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view100);
//	break;
//
//case R.id.btn101:
//String view101 = ("Xr");
//mButton101.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view101);
//	break;
//
//case R.id.btn102:
//String view102 = ("Xs");
//mButton102.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view102);
//	break;
//
//case R.id.btn103:
//String view103 = ("Xt");
//mButton103.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view103);
//	break;
//
//case R.id.btn104:
//String view104 = ("Xu");
//mButton104.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view104);
//	break;
//
//case R.id.btn105:
//String view105 = ("Yp");
//mButton105.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view105);
//	break;
//
//case R.id.btn106:
//String view106 = ("Yq");
//mButton106.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view106);
//	break;
//
//case R.id.btn107:
//String view107 = ("Yr");
//mButton107.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view107);
//	break;
//
//case R.id.btn108:
//String view108 = ("Ys");
//mButton10.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view108);
//	break;
//
//case R.id.btn109:
//String view109 = ("Yt");
//mButton109.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view109);
//	break;
//
//case R.id.btn110:
//String view110 = ("Yu");
//mButton110.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view110);
//	break;

//case R.id.btn111:
//String view111 = ("Zp");
//mButton111.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view111);
//	break;
//
//case R.id.btn112:
//String view112 = ("Zq");
//mButton112.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view112);
//	break;
//
//case R.id.btn113:
//String view113 = ("Zr");
//mButton113.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view113);
//	break;
//
//case R.id.btn114:
//String view114 = ("Zs");
//mButton114.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view114);
//	break;
//
//case R.id.btn115:
//String view115 = ("Zt");
//mButton115.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view115);
//	break;
//
//case R.id.btn116:
//String view116 = ("Zt");
//mButton116.setTextColor(Color.parseColor("#FF0000"));
//marco4setbtn.append(view116);
//	break;

            case R.id.btn153:
                String view153 = ("Tc");
                marco4setbtn.append(view153);
                break;

            case R.id.btn154:
                String view154 = ("Ta");
                marco4setbtn.append(view154);
                break;

            case R.id.btn155:
                String view155 = ("Tf");
                marco4setbtn.append(view155);
                break;

            case R.id.btn156:
                String view156 = ("Td");
                marco4setbtn.append(view156);
                break;


            case R.id.door1:
                String door1 = ("W+sa1");
                marco4setbtn.append(door1);
                break;

            case R.id.door2:
                String door2 = ("W+sb1");
                marco4setbtn.append(door2);
                break;

            case R.id.door3:
                String door3 = ("W+sc1");
                marco4setbtn.append(door3);
                break;
            case R.id.window1up:
                marco4setbtn.append("a8");
                break;
            case R.id.window1stop:
                marco4setbtn.append("a7");
                break;
            case R.id.window1down:
                marco4setbtn.append("a6");
                break;
            case R.id.window2up:
                marco4setbtn.append("b8");
                break;
            case R.id.window2stop:
                marco4setbtn.append("b7");
                break;
            case R.id.window2down:
                marco4setbtn.append("b6");
                break;
            case R.id.window3up:
                marco4setbtn.append("c8");
                break;
            case R.id.window3stop:
                marco4setbtn.append("c7");
                break;
            case R.id.window3down:
                marco4setbtn.append("c6");
                break;
            //tv home
//case R.id.button25:
//	String view39 = ("Tg");
//	String message39 = view39.toString();
//	marco4setbtn.append(message39);
//	break;
//	//tv 靜音
//case R.id.button26:
//	String view41 = ("Ti");
//    String message41 = view41.toString();
//    marco4setbtn.append(message41);
//	break;

        }
    }

    //marco6
    public void Click5Handler(View v) {
        // TODO Auto-generated catch block


        switch (v.getId()) {
            case R.id.marcoset5Clear:
                marco5setbtn.setText("");
                count1 = 0;
                count2 = 0;
                count3 = 0;
                count4 = 0;
                count5 = 0;
                count6 = 0;
                break;
            case R.id.marcoset5ok:

                saveData(10);
                Intent intent8 = new Intent();
                intent8.setClass(BluetoothChat.this, BluetoothChat.class).putExtra("switch", portSwitch);
                finish();
                startActivity(intent8);

                break;

            case R.id.s26:
                String delay21 = ("'");
                marco5setbtn.append(delay21);
                break;

            case R.id.s27:
                String delay22 = ("(");
                marco5setbtn.append(delay22);
                break;

            case R.id.s28:
                String delay23 = (")");
                marco5setbtn.append(delay23);
                break;

            case R.id.s29:
                String delay24 = ("*");
                marco5setbtn.append(delay24);
                break;

            case R.id.s30:
                String delay25 = ("+");
                marco5setbtn.append(delay25);
                break;

            case R.id.btn01:

                if (count1 == 0) {
                    String view = ("a4");
                    mButton01.setTextColor(Color.parseColor("#FF0000"));
                    mButton01.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view);
                    count1 = 1;
                } else if (count1 == 1) {
                    mButton01.setTextColor(Color.parseColor("#ffffff"));
                    mButton01.setBackgroundResource(R.drawable.set_befor_9);
                    String btn01 = marco5setbtn.getText().toString();
                    String newa = btn01.replaceFirst("a4", "a5");
                    marco5setbtn.setText(newa);
                    count1 = 2;
                } else if (count1 == 2) {
                    String btn01 = marco5setbtn.getText().toString();
                    String newa = btn01.replaceFirst("a5", "");
                    marco5setbtn.setText(newa);
                    count1 = 0;
                }

                break;

            case R.id.btn02:

                if (count2 == 0) {
                    String view2 = ("b4");
                    mButton02.setTextColor(Color.parseColor("#FF0000"));
                    mButton02.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view2);
                    count2 = 1;
                } else if (count2 == 1) {
                    mButton02.setTextColor(Color.parseColor("#ffffff"));
                    mButton02.setBackgroundResource(R.drawable.set_befor_9);
                    String btn02 = marco5setbtn.getText().toString();
                    String newa = btn02.replaceFirst("b4", "b5");
                    marco5setbtn.setText(newa);
                    count2 = 2;
                } else if (count2 == 2) {
                    String btn02 = marco5setbtn.getText().toString();
                    String newa = btn02.replaceFirst("b5", "");
                    marco5setbtn.setText(newa);
                    count2 = 0;
                }
                break;

            case R.id.btn03:
                if (count3 == 0) {
                    String view3 = ("c4");
                    mButton03.setTextColor(Color.parseColor("#FF0000"));
                    mButton03.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view3);
                    count3 = 1;
                } else if (count3 == 1) {
                    mButton03.setTextColor(Color.parseColor("#ffffff"));
                    mButton03.setBackgroundResource(R.drawable.set_befor_9);
                    String btn03 = marco5setbtn.getText().toString();
                    String newa = btn03.replaceFirst("c4", "c5");
                    marco5setbtn.setText(newa);
                    count3 = 2;
                } else if (count3 == 2) {
                    String btn03 = marco5setbtn.getText().toString();
                    String newa = btn03.replaceFirst("c5", "");
                    marco5setbtn.setText(newa);
                    count3 = 0;
                }
                break;

            case R.id.btn04:
                if (count4 == 0) {
                    String view4 = ("d4");
                    mButton04.setTextColor(Color.parseColor("#FF0000"));
                    mButton04.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view4);
                    count4 = 1;
                } else if (count4 == 1) {
                    mButton04.setTextColor(Color.parseColor("#ffffff"));
                    mButton04.setBackgroundResource(R.drawable.set_befor_9);
                    String btn04 = marco5setbtn.getText().toString();
                    String newa = btn04.replaceFirst("d4", "d5");
                    marco5setbtn.setText(newa);
                    count4 = 2;
                } else if (count4 == 2) {
                    String btn04 = marco5setbtn.getText().toString();
                    String newa = btn04.replaceFirst("d5", "");
                    marco5setbtn.setText(newa);
                    count4 = 0;
                }
                break;

            case R.id.btn05:
                if (count5 == 0) {
                    String view5 = ("e4");
                    mButton05.setTextColor(Color.parseColor("#FF0000"));
                    mButton05.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view5);
                    count5 = 1;
                } else if (count5 == 1) {
                    mButton05.setTextColor(Color.parseColor("#ffffff"));
                    mButton05.setBackgroundResource(R.drawable.set_befor_9);
                    String btn05 = marco5setbtn.getText().toString();
                    String newa = btn05.replaceFirst("e4", "e5");
                    marco5setbtn.setText(newa);
                    count5 = 2;
                } else if (count5 == 2) {
                    String btn05 = marco5setbtn.getText().toString();
                    String newa = btn05.replaceFirst("e5", "");
                    marco5setbtn.setText(newa);
                    count5 = 0;
                }
                break;

            case R.id.btn06:
                if (count6 == 0) {
                    String view6 = ("f4");
                    mButton06.setTextColor(Color.parseColor("#FF0000"));
                    mButton06.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view6);
                    count6 = 1;
                } else if (count6 == 1) {
                    mButton06.setTextColor(Color.parseColor("#ffffff"));
                    mButton06.setBackgroundResource(R.drawable.set_befor_9);
                    String btn06 = marco5setbtn.getText().toString();
                    String newa = btn06.replaceFirst("f4", "f5");
                    marco5setbtn.setText(newa);
                    count6 = 2;
                } else if (count6 == 2) {
                    String btn06 = marco5setbtn.getText().toString();
                    String newa = btn06.replaceFirst("f5", "");
                    marco5setbtn.setText(newa);
                    count6 = 0;
                }
                break;

            case R.id.btn07:
                if (count7 == 0) {
                    String view7 = ("g4");
                    mButton07.setTextColor(Color.parseColor("#FF0000"));
                    mButton07.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view7);
                    count7 = 1;
                } else if (count7 == 1) {
                    mButton07.setTextColor(Color.parseColor("#ffffff"));
                    mButton07.setBackgroundResource(R.drawable.set_befor_9);
                    String btn07 = marco5setbtn.getText().toString();
                    String newa = btn07.replaceFirst("g4", "g5");
                    marco5setbtn.setText(newa);
                    count7 = 2;
                } else if (count7 == 2) {
                    String btn07 = marco5setbtn.getText().toString();
                    String newa = btn07.replaceFirst("g5", "");
                    marco5setbtn.setText(newa);
                    count7 = 0;
                }
                break;

            case R.id.btn08:
                if (count8 == 0) {
                    String view8 = ("h4");
                    mButton08.setTextColor(Color.parseColor("#FF0000"));
                    mButton08.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view8);
                    count8 = 1;
                } else if (count8 == 1) {
                    mButton08.setTextColor(Color.parseColor("#ffffff"));
                    mButton08.setBackgroundResource(R.drawable.set_befor_9);
                    String btn08 = marco5setbtn.getText().toString();
                    String newa = btn08.replaceFirst("h4", "h5");
                    marco5setbtn.setText(newa);
                    count8 = 2;
                } else if (count8 == 2) {
                    String btn08 = marco5setbtn.getText().toString();
                    String newa = btn08.replaceFirst("h5", "");
                    marco5setbtn.setText(newa);
                    count8 = 0;
                }
                break;

            case R.id.btn09:
                if (count9 == 0) {
                    String view9 = ("i4");
                    mButton09.setTextColor(Color.parseColor("#FF0000"));
                    mButton09.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view9);
                    count9 = 1;
                } else if (count9 == 1) {
                    mButton09.setTextColor(Color.parseColor("#ffffff"));
                    mButton09.setBackgroundResource(R.drawable.set_befor_9);
                    String btn09 = marco5setbtn.getText().toString();
                    String newa = btn09.replaceFirst("i4", "i5");
                    marco5setbtn.setText(newa);
                    count9 = 2;
                } else if (count9 == 2) {
                    String btn09 = marco5setbtn.getText().toString();
                    String newa = btn09.replaceFirst("i5", "");
                    marco5setbtn.setText(newa);
                    count9 = 0;
                }
                break;

            case R.id.btn10:
                if (count10 == 0) {
                    String view10 = ("j4");
                    mButton10.setTextColor(Color.parseColor("#FF0000"));
                    mButton10.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view10);
                    count10 = 1;
                } else if (count10 == 1) {
                    mButton10.setTextColor(Color.parseColor("#ffffff"));
                    mButton10.setBackgroundResource(R.drawable.set_befor_9);
                    String btn10 = marco5setbtn.getText().toString();
                    String newa = btn10.replaceFirst("j4", "j5");
                    marco5setbtn.setText(newa);
                    count10 = 2;
                } else if (count10 == 2) {
                    String btn10 = marco5setbtn.getText().toString();
                    String newa = btn10.replaceFirst("j5", "");
                    marco5setbtn.setText(newa);
                    count10 = 0;
                }
                break;

            case R.id.btn11:
                if (count11 == 0) {
                    String view11 = ("k4");
                    mButton11.setTextColor(Color.parseColor("#FF0000"));
                    mButton11.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view11);
                    count11 = 1;
                } else if (count11 == 1) {
                    mButton11.setTextColor(Color.parseColor("#ffffff"));
                    mButton11.setBackgroundResource(R.drawable.set_befor_9);
                    String btn11 = marco5setbtn.getText().toString();
                    String newa = btn11.replaceFirst("k4", "k5");
                    marco5setbtn.setText(newa);
                    count11 = 2;
                } else if (count11 == 2) {
                    String btn11 = marco5setbtn.getText().toString();
                    String newa = btn11.replaceFirst("k5", "");
                    marco5setbtn.setText(newa);
                    count11 = 0;
                }
                break;

            case R.id.btn12:
                if (count12 == 0) {
                    String view12 = ("l4");
                    mButton12.setTextColor(Color.parseColor("#FF0000"));
                    mButton12.setBackgroundResource(R.drawable.set_after_9);
                    marco5setbtn.append(view12);
                    count12 = 1;
                } else if (count12 == 1) {
                    mButton12.setTextColor(Color.parseColor("#ffffff"));
                    mButton12.setBackgroundResource(R.drawable.set_befor_9);
                    String btn12 = marco5setbtn.getText().toString();
                    String newa = btn12.replaceFirst("l4", "l5");
                    marco5setbtn.setText(newa);
                    count12 = 2;
                } else if (count12 == 2) {
                    String btn12 = marco5setbtn.getText().toString();
                    String newa = btn12.replaceFirst("l5", "");
                    marco5setbtn.setText(newa);
                    count12 = 0;
                }
                break;

            case R.id.btn13:
                String view13 = ("Aa");
                mButton13.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view13);

                break;

            case R.id.btn14:
                String view14 = ("Ad");
                mButton14.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view14);

                break;

            case R.id.btn15:
                String view15 = ("Ac");
                mButton15.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view15);
                break;

            case R.id.btn16:
                String view16 = ("Af");
                mButton16.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view16);
                break;

            case R.id.btn17:
                String view17 = ("Ag");
                mButton17.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view17);
                break;

            case R.id.btn18:
                String view18 = ("Ah");
                mButton18.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view18);
                break;

            case R.id.btn19:
                String view19 = ("Ai");
                mButton19.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view19);
                break;

            case R.id.btn20:
                String view20 = ("Aj");
                mButton20.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view20);
                break;

            case R.id.btn21:
                String view21 = ("Ak");
                mButton21.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view21);
                break;

            case R.id.btn22:
                String view22 = ("Al");
                mButton22.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view22);
                break;

            case R.id.btn23:
                String view23 = ("Ab");
                mButton23.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view23);
                break;

            case R.id.btn24:
                String view24 = ("Ae");
                mButton24.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view24);
                break;

//case R.id.btn25:
//String view25 = ("Am");
//mButton25.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view25);
//break;

//case R.id.btn26:
//String view26 = ("An");
//mButton26.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view26);
//break;
//
//case R.id.btn27:
//String view27 = ("Ao");
//mButton27.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view27);
//break;

//case R.id.btn28:
//String view28 = ("Ap");
//mButton28.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view28);
//break;
//
//case R.id.btn29:
//String view29 = ("Aq");
//mButton29.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view29);
//break;
//
//case R.id.btn30:
//String view30 = ("Ar");
//mButton30.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view30);
//break;
//
//case R.id.btn31:
//String view31 = ("As");
//mButton31.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view31);
//break;

//case R.id.btn32:
//String view32 = ("At");
//mButton32.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view32);
//break;

            case R.id.btn33:
                String view33 = ("Tb");
                mButton33.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view33);
                break;

//case R.id.btn34:
//String view34 = ("Tb");
//mButton34.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view34);
//break;

            case R.id.btn35:
                String view35 = ("Te");
                mButton35.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view35);
                break;

//case R.id.btn36:
//String view36 = ("Td");
//mButton36.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view36);
//break;
//
//case R.id.btn37:
//String view37 = ("Te");
//mButton37.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view37);
//break;
//
//case R.id.btn38:
//String view38 = ("Tf");
//mButton38.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view38);
//break;
//
//case R.id.btn39:
//String view39 = ("Tg");
//mButton39.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view39);
//break;
//
//case R.id.btn40:
//String view40 = ("Th");
//mButton40.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view40);
//break;
//
//case R.id.btn41:
//String view41 = ("Ti");
//mButton41.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view41);
//break;

            case R.id.btn42:
                String view42 = ("Tg");
                mButton42.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view42);
                break;

            case R.id.btn43:
                String view43 = ("Th");
                mButton43.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view43);
                break;

            case R.id.btn44:
                String view44 = ("Ti");
                mButton44.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view44);
                break;

            case R.id.btn45:
                String view45 = ("Tj");
                mButton45.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view45);
                break;

            case R.id.btn46:
                String view46 = ("Tk");
                mButton46.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view46);
                break;

            case R.id.btn47:
                String view47 = ("Tl");
                mButton47.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view47);
                break;

            case R.id.btn48:
                String view48 = ("Tm");
                mButton48.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view48);
                break;

            case R.id.btn49:
                String view49 = ("Tn");
                mButton49.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view49);
                break;

            case R.id.btn50:
                String view50 = ("To");
                mButton50.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view50);
                break;

            case R.id.btn51:
                String view51 = ("Tp");
                mButton51.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view51);
                break;

            case R.id.btn52:
                String view52 = ("Tq");
                mButton52.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view52);
                break;

            case R.id.btn53:
                String view53 = ("Tr");
                mButton53.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view53);
                break;

//case R.id.btn54:
//String view54 = ("Xa");
//mButton54.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view54);
//	break;
//
//case R.id.btn55:
//String view55 = ("Xb");
//mButton55.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view55);
//	break;
//
//case R.id.btn56:
//String view56 = ("Xc");
//mButton56.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view56);
//	break;
//
//case R.id.btn57:
//String view57 = ("Xd");
//mButton57.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view57);
//	break;
//
//case R.id.btn58:
//String view58 = ("Xe");
//mButton58.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view58);
//	break;
//
//case R.id.btn59:
//String view59 = ("Xf");
//mButton59.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view59);
//	break;
//
//case R.id.btn60:
//String view60 = ("Xg");
//mButton60.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view60);
//	break;
//
//case R.id.btn61:
//String view61 = ("Xh");
//mButton61.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view61);
//	break;
//
//case R.id.btn62:
//String view62 = ("Xi");
//mButton62.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view62);
//	break;

            case R.id.btn63:
                String view63 = ("Xa");
                mButton63.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view63);
                break;

            case R.id.btn64:
                String view64 = ("Xb");
                mButton64.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view64);
                break;

            case R.id.btn65:
                String view65 = ("Xc");
                mButton65.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view65);
                break;

            case R.id.btn66:
                String view66 = ("Xd");
                mButton66.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view66);
                break;

            case R.id.btn67:
                String view67 = ("Xe");
                mButton67.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view67);
                break;

            case R.id.btn68:
                String view68 = ("Xf");
                mButton68.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view68);
                break;

//case R.id.btn69:
//String view69 = ("Ya");
//mButton69.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view69);
//	break;
//
//case R.id.btn70:
//String view70 = ("Yb");
//mButton70.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view70);
//	break;
//
//case R.id.btn71:
//String view71 = ("Yc");
//mButton71.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view71);
//	break;
//
//case R.id.btn72:
//String view72 = ("Yd");
//mButton72.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view72);
//	break;
//
//case R.id.btn73:
//String view73 = ("Ye");
//mButton73.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view73);
//	break;
//
//case R.id.btn74:
//String view74 = ("Yf");
//mButton74.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view74);
//	break;
//
//case R.id.btn75:
//String view75 = ("Yg");
//mButton75.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view75);
//	break;
//
//case R.id.btn76:
//String view76 = ("Yh");
//mButton76.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view76);
//	break;
//
//case R.id.btn77:
//String view77 = ("Yi");
//mButton77.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view77);
//	break;

            case R.id.btn78:
                String view78 = ("Ya");
                mButton78.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view78);
                break;

            case R.id.btn79:
                String view79 = ("Yb");
                mButton79.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view79);
                break;

            case R.id.btn80:
                String view80 = ("Yc");
                mButton80.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view80);
                break;

            case R.id.btn81:
                String view81 = ("Yd");
                mButton81.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view81);
                break;

            case R.id.btn82:
                String view82 = ("Ye");
                mButton82.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view82);
                break;

            case R.id.btn83:
                String view83 = ("Yf");
                mButton83.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view83);
                break;

//case R.id.btn84:
//String view84 = ("Xa");
//mButton84.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view84);
//	break;
//
//case R.id.btn85:
//String view85 = ("Xb");
//mButton85.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view85);
//	break;
//
//case R.id.btn86:
//String view86 = ("Xc");
//mButton86.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view86);
//	break;
//
//case R.id.btn87:
//String view87 = ("Xd");
//mButton87.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view87);
//	break;
//
//case R.id.btn88:
//String view88 = ("Xe");
//mButton88.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view88);
//	break;
//
//case R.id.btn89:
//String view89 = ("Xf");
//mButton89.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view89);
//	break;
//
//case R.id.btn90:
//String view90 = ("Xg");
//mButton90.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view90);
//	break;
//
//case R.id.btn91:
//String view91 = ("Xh");
//mButton91.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view91);
//	break;
//
//case R.id.btn92:
//String view92 = ("Xi");
//mButton92.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view92);
//	break;

            case R.id.btn93:
                String view93 = ("Za");
                mButton93.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view93);
                break;

            case R.id.btn94:
                String view94 = ("Zb");
                mButton94.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view94);
                break;

            case R.id.btn95:
                String view95 = ("Zc");
                mButton95.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view95);
                break;

            case R.id.btn96:
                String view96 = ("Zd");
                mButton96.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view96);
                break;

            case R.id.btn97:
                String view97 = ("Ze");
                mButton97.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view97);
                break;

            case R.id.btn98:
                String view98 = ("Zf");
                mButton98.setTextColor(Color.parseColor("#FF0000"));
                marco5setbtn.append(view98);
                break;

//case R.id.btn99:
//String view99 = ("Xp");
//mButton99.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view99);
//	break;
//
//case R.id.btn100:
//String view100 = ("Xq");
//mButton100.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view100);
//	break;
//
//case R.id.btn101:
//String view101 = ("Xr");
//mButton101.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view101);
//	break;
//
//case R.id.btn102:
//String view102 = ("Xs");
//mButton102.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view102);
//	break;
//
//case R.id.btn103:
//String view103 = ("Xt");
//mButton103.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view103);
//	break;
//
//case R.id.btn104:
//String view104 = ("Xu");
//mButton104.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view104);
//	break;
//
//case R.id.btn105:
//String view105 = ("Yp");
//mButton105.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view105);
//	break;
//
//case R.id.btn106:
//String view106 = ("Yq");
//mButton106.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view106);
//	break;
//
//case R.id.btn107:
//String view107 = ("Yr");
//mButton107.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view107);
//	break;
//
//case R.id.btn108:
//String view108 = ("Ys");
//mButton10.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view108);
//	break;
//
//case R.id.btn109:
//String view109 = ("Yt");
//mButton109.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view109);
//	break;
//
//case R.id.btn110:
//String view110 = ("Yu");
//mButton110.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view110);
//	break;

//case R.id.btn111:
//String view111 = ("Zp");
//mButton111.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view111);
//	break;
//
//case R.id.btn112:
//String view112 = ("Zq");
//mButton112.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view112);
//	break;
//
//case R.id.btn113:
//String view113 = ("Zr");
//mButton113.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view113);
//	break;
//
//case R.id.btn114:
//String view114 = ("Zs");
//mButton114.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view114);
//	break;
//
//case R.id.btn115:
//String view115 = ("Zt");
//mButton115.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view115);
//	break;
//
//case R.id.btn116:
//String view116 = ("Zt");
//mButton116.setTextColor(Color.parseColor("#FF0000"));
//marco5setbtn.append(view116);
//	break;

            case R.id.btn153:
                String view153 = ("Tc");
                marco5setbtn.append(view153);
                break;

            case R.id.btn154:
                String view154 = ("Ta");
                marco5setbtn.append(view154);
                break;

            case R.id.btn155:
                String view155 = ("Tf");
                marco5setbtn.append(view155);
                break;

            case R.id.btn156:
                String view156 = ("Td");
                marco5setbtn.append(view156);
                break;

            case R.id.door1:
                String door1 = ("W+sa1");
                marco5setbtn.append(door1);
                break;

            case R.id.door2:
                String door2 = ("W+sb1");
                marco5setbtn.append(door2);
                break;

            case R.id.door3:
                String door3 = ("W+sc1");
                marco5setbtn.append(door3);
                break;
            case R.id.window1up:
                marco5setbtn.append("a8");
                break;
            case R.id.window1stop:
                marco5setbtn.append("a7");
                break;
            case R.id.window1down:
                marco5setbtn.append("a6");
                break;
            case R.id.window2up:
                marco5setbtn.append("b8");
                break;
            case R.id.window2stop:
                marco5setbtn.append("b7");
                break;
            case R.id.window2down:
                marco5setbtn.append("b6");
                break;
            case R.id.window3up:
                marco5setbtn.append("c8");
                break;
            case R.id.window3stop:
                marco5setbtn.append("c7");
                break;
            case R.id.window3down:
                marco5setbtn.append("c6");
                break;
            //tv home
//case R.id.button25:
//	String view39 = ("Tg");
//	String message39 = view39.toString();
//	marco5setbtn.append(message39);
//	break;
//	//tv 靜音
//case R.id.button26:
//	String view41 = ("Ti");
//    String message41 = view41.toString();
//    marco5setbtn.append(message41);
//	break;

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (D) Log.e(TAG, "++ ON START ++");
        //new MySocketThread(mHandler, BluetoothChat.this, 0, "00");
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if (D) Log.e(TAG, "+ ON RESUME +");
        new MySocketThread(mHandler, this, 0, "00");
    }


    @Override
    public synchronized void onPause() {
        super.onPause();
        if (D) Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        StopSocket();
        if (D) Log.e(TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        //if (mChatService != null) mChatService.stop();
        if (D) Log.e(TAG, "--- ON DESTROY ---");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (D) Log.e(TAG, "--- ON onRestart ---");

    }


    public void init() {
        //裝置連線
        new MySocketThread(mHandler, this, 0, "");
    }


    public void sendmessage(String message) {

        //判斷紅外線功能是否開啟

        String IR_Status = PreferenceManager.getDefaultSharedPreferences(BluetoothChat.this).getString(QuickstartPreferences.IR_STATUS, "關閉");

        if (IR_Status.equals("關閉")) {
            //紅外線功能判斷

            String[] strLampCode = new String[]{"a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3","i3", "j3", "k3", "l3",
                    "W+j?j", "W+k?k", "W+l?l", "W+m?m", "W+n?n", "W+o?o", "W+p?p", "W+q?q", "W+r?r", "W+s?s", "W+t?t",
                    "W+u?u", "W+j?u"};

            if (Arrays.toString(strLampCode).contains(message)) {
                //燈控類功能正常使用
                new MySocketThread(mHandler, this, 0, message);
            } else {
                //紅外線類功能阻擋使用
                UtilMessageDialog.messageDialog(this, "警告!", "請確認您的主機確實有支援情境功能，否則發送紅外線相關指令有可能會影響其他裝置的使用!");
            }


        } else if (IR_Status.equals("開啟")) {
            //全功能正常使用
            new MySocketThread(mHandler, this, 0, message);
        }


    }


    // The Handler that gets information back from the BluetoothChatService
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (D) Log.d(TAG, "onActivityResult " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {


            case LAMP_PIC1:
                if (resultCode == Activity.RESULT_OK) {

                    try {
                        String Imagelamp1 = getBitmap(data.getData(), "1");
                        if (D) Log.d(TAG, Imagelamp1);
                        ((EditText) findViewById(R.id.lampeditpic1)).setText(Imagelamp1);
                        ((ImageView) findViewById(R.id.imagebtn1)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn1)).setImageURI(Uri.parse(Imagelamp1));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC2:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp2 = getBitmap(data.getData(), "2");
                        ((EditText) findViewById(R.id.lampeditpic2)).setText(Imagelamp2);
                        ((ImageView) findViewById(R.id.imagebtn2)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn2)).setImageURI(Uri.parse(Imagelamp2));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC3:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp3 = getBitmap(data.getData(), "3");
                        ((EditText) findViewById(R.id.lampeditpic3)).setText(Imagelamp3);
                        ((ImageView) findViewById(R.id.imagebtn3)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn3)).setImageURI(Uri.parse(Imagelamp3));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC4:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp4 = getBitmap(data.getData(), "4");
                        ((EditText) findViewById(R.id.lampeditpic4)).setText(Imagelamp4);
                        ((ImageView) findViewById(R.id.imagebtn4)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn4)).setImageURI(Uri.parse(Imagelamp4));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC5:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp5 = getBitmap(data.getData(), "5");
                        ((EditText) findViewById(R.id.lampeditpic5)).setText(Imagelamp5);
                        ((ImageView) findViewById(R.id.imagebtn5)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn5)).setImageURI(Uri.parse(Imagelamp5));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC6:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp6 = getBitmap(data.getData(), "6");
                        ((EditText) findViewById(R.id.lampeditpic6)).setText(Imagelamp6);
                        ((ImageView) findViewById(R.id.imagebtn6)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn6)).setImageURI(Uri.parse(Imagelamp6));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC7:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp7 = getBitmap(data.getData(), "7");
                        ((EditText) findViewById(R.id.lampeditpic7)).setText(Imagelamp7);
                        ((ImageView) findViewById(R.id.imagebtn7)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn7)).setImageURI(Uri.parse(Imagelamp7));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC8:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp8 = getBitmap(data.getData(), "8");
                        ((EditText) findViewById(R.id.lampeditpic8)).setText(Imagelamp8);
                        ((ImageView) findViewById(R.id.imagebtn8)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn8)).setImageURI(Uri.parse(Imagelamp8));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC9:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp9 = getBitmap(data.getData(), "9");
                        ((EditText) findViewById(R.id.lampeditpic9)).setText(Imagelamp9);
                        ((ImageView) findViewById(R.id.imagebtn9)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn9)).setImageURI(Uri.parse(Imagelamp9));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC10:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp10 = getBitmap(data.getData(), "10");
                        ((EditText) findViewById(R.id.lampeditpic10)).setText(Imagelamp10);
                        ((ImageView) findViewById(R.id.imagebtn10)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn10)).setImageURI(Uri.parse(Imagelamp10));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC11:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp11 = getBitmap(data.getData(), "11");
                        ((EditText) findViewById(R.id.lampeditpic11)).setText(Imagelamp11);
                        ((ImageView) findViewById(R.id.imagebtn11)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn11)).setImageURI(Uri.parse(Imagelamp11));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case LAMP_PIC12:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        String Imagelamp12 = getBitmap(data.getData(), "12");
                        ((EditText) findViewById(R.id.lampeditpic12)).setText(Imagelamp12);
                        ((ImageView) findViewById(R.id.imagebtn12)).setImageURI(null);
                        ((ImageView) findViewById(R.id.imagebtn12)).setImageURI(Uri.parse(Imagelamp12));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case RETRUN_MAIN:
                init();
                break;
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case st:
                changeMenu = "learning";
                sendmessage("Li");
                return true;
            case close:
                changeMenu = "none";
                sendmessage("Cc");
                return true;

            case scan:
                //sendmessage("W+o?o");
                viewPager.setCurrentItem(page_ip_config);
                return true;

            case privace_text:
                new AlertDialog.Builder(this)
                        .setTitle(R.string.privace_title)
                        .setMessage(R.string.privace_content)
                        .setPositiveButton(R.string.privace_positive, null)
                        .show();
                break;
        }
        return false;
    }

    //YILINEDIT-voiceRecg
    public void voiceRec(String title) {
        try {
            Intent intent = new Intent(
                    RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(
                    RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(
                    RecognizerIntent.EXTRA_PROMPT,
                    title);

            startActivityForResult(intent, RECORD);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this,
                    "ActivityNotFoundException", Toast.LENGTH_LONG).show();
        }
    }


    public void readData(int i) {
        switch (i) {

            case 1:

                lamp = getSharedPreferences(portSwitch + lampdata, 0);
                ((EditText) findViewById(R.id.lampeditText1)).setText(lamp.getString(btn01name, ""));
                ((EditText) findViewById(R.id.lampeditText2)).setText(lamp.getString(btn02name, ""));
                ((EditText) findViewById(R.id.lampeditText3)).setText(lamp.getString(btn03name, ""));
                ((EditText) findViewById(R.id.lampeditText4)).setText(lamp.getString(btn04name, ""));
                ((EditText) findViewById(R.id.lampeditText5)).setText(lamp.getString(btn05name, ""));
                ((EditText) findViewById(R.id.lampeditText6)).setText(lamp.getString(btn06name, ""));
                ((EditText) findViewById(R.id.lampeditText7)).setText(lamp.getString(btn07name, ""));
                ((EditText) findViewById(R.id.lampeditText8)).setText(lamp.getString(btn08name, ""));
                ((EditText) findViewById(R.id.lampeditText9)).setText(lamp.getString(btn09name, ""));
                ((EditText) findViewById(R.id.lampeditText10)).setText(lamp.getString(btn10name, ""));
                ((EditText) findViewById(R.id.lampeditText11)).setText(lamp.getString(btn11name, ""));
                ((EditText) findViewById(R.id.lampeditText12)).setText(lamp.getString(btn12name, ""));

                ((EditText) findViewById(R.id.lampeditpic1)).setText(lamp.getString(btn1pic, ""));
                ((EditText) findViewById(R.id.lampeditpic2)).setText(lamp.getString(btn2pic, ""));
                ((EditText) findViewById(R.id.lampeditpic3)).setText(lamp.getString(btn3pic, ""));
                ((EditText) findViewById(R.id.lampeditpic4)).setText(lamp.getString(btn4pic, ""));
                ((EditText) findViewById(R.id.lampeditpic5)).setText(lamp.getString(btn5pic, ""));
                ((EditText) findViewById(R.id.lampeditpic6)).setText(lamp.getString(btn6pic, ""));
                ((EditText) findViewById(R.id.lampeditpic7)).setText(lamp.getString(btn7pic, ""));
                ((EditText) findViewById(R.id.lampeditpic8)).setText(lamp.getString(btn8pic, ""));
                ((EditText) findViewById(R.id.lampeditpic9)).setText(lamp.getString(btn9pic, ""));
                ((EditText) findViewById(R.id.lampeditpic10)).setText(lamp.getString(btn10pic, ""));
                ((EditText) findViewById(R.id.lampeditpic11)).setText(lamp.getString(btn11pic, ""));
                ((EditText) findViewById(R.id.lampeditpic12)).setText(lamp.getString(btn12pic, ""));


                break;

            case 2:

                setting = getSharedPreferences(portSwitch + custom1data, 0);

                custom1edit.setText(setting.getString(custom1name, ""));
//        meditText1.setText(setting.getString(btn54name, ""));
//        meditText2.setText(setting.getString(btn55name, ""));
//        meditText3.setText(setting.getString(btn56name, ""));
//        meditText4.setText(setting.getString(btn57name, ""));
//        meditText5.setText(setting.getString(btn58name, ""));
//        meditText6.setText(setting.getString(btn59name, ""));
//        meditText7.setText(setting.getString(btn60name, ""));
//        meditText8.setText(setting.getString(btn61name, ""));
//        meditText9.setText(setting.getString(btn62name, ""));
                meditText1.setText(setting.getString(btn63name, ""));
                meditText2.setText(setting.getString(btn64name, ""));
                meditText3.setText(setting.getString(btn65name, ""));
                meditText4.setText(setting.getString(btn66name, ""));
                meditText5.setText(setting.getString(btn67name, ""));
                meditText6.setText(setting.getString(btn68name, ""));
//        meditText46.setText(setting.getString(btn99name, ""));
//        meditText47.setText(setting.getString(btn100name, ""));
//        meditText48.setText(setting.getString(btn101name, ""));
//        meditText49.setText(setting.getString(btn102name, ""));
//        meditText50.setText(setting.getString(btn103name, ""));
//        meditText51.setText(setting.getString(btn104name, ""));
                break;

            case 3:

                settingb = getSharedPreferences(portSwitch + custom2data, 0);

                custom2edit.setText(settingb.getString(custom2name, ""));
//	        meditText16.setText(settingb.getString(btn69name, ""));
//	        meditText17.setText(settingb.getString(btn70name, ""));
//	        meditText18.setText(settingb.getString(btn71name, ""));
//	        meditText19.setText(settingb.getString(btn72name, ""));
//	        meditText20.setText(settingb.getString(btn73name, ""));
//	        meditText21.setText(settingb.getString(btn74name, ""));
//	        meditText22.setText(settingb.getString(btn75name, ""));
//	        meditText23.setText(settingb.getString(btn76name, ""));
//	        meditText24.setText(settingb.getString(btn77name, ""));
                meditText16.setText(settingb.getString(btn78name, ""));
                meditText17.setText(settingb.getString(btn79name, ""));
                meditText18.setText(settingb.getString(btn80name, ""));
                meditText19.setText(settingb.getString(btn81name, ""));
                meditText20.setText(settingb.getString(btn82name, ""));
                meditText21.setText(settingb.getString(btn83name, ""));
//	        meditText52.setText(settingb.getString(btn105name, ""));
//	        meditText53.setText(settingb.getString(btn106name, ""));
//	        meditText54.setText(settingb.getString(btn107name, ""));
//	        meditText55.setText(settingb.getString(btn108name, ""));
//	        meditText56.setText(settingb.getString(btn109name, ""));
//	        meditText57.setText(settingb.getString(btn110name, ""));
                break;

            case 4:

                setting2 = getSharedPreferences(portSwitch + custom3data, 0);

                custom3edit.setText(setting2.getString(custom3name, ""));
//	        meditText31.setText(setting2.getString(btn84name, ""));
//	        meditText32.setText(setting2.getString(btn85name, ""));
//	        meditText33.setText(setting2.getString(btn86name, ""));
//	        meditText34.setText(setting2.getString(btn87name, ""));
//	        meditText35.setText(setting2.getString(btn88name, ""));
//	        meditText36.setText(setting2.getString(btn89name, ""));
//	        meditText37.setText(setting2.getString(btn90name, ""));
//	        meditText38.setText(setting2.getString(btn91name, ""));
//	        meditText39.setText(setting2.getString(btn92name, ""));
                meditText31.setText(setting2.getString(btn93name, ""));
                meditText32.setText(setting2.getString(btn94name, ""));
                meditText33.setText(setting2.getString(btn95name, ""));
                meditText34.setText(setting2.getString(btn96name, ""));
                meditText35.setText(setting2.getString(btn97name, ""));
                meditText36.setText(setting2.getString(btn98name, ""));
//	        meditText58.setText(setting2.getString(btn111name, ""));
//	        meditText59.setText(setting2.getString(btn112name, ""));
//	        meditText60.setText(setting2.getString(btn113name, ""));
//	        meditText61.setText(setting2.getString(btn114name, ""));
//	        meditText62.setText(setting2.getString(btn115name, ""));
//	        meditText63.setText(setting2.getString(btn116name, ""));

                break;

            case 5:

                marcoset = getSharedPreferences(portSwitch + marcosetdata, 0);
                marcosetname.setText(marcoset.getString(marconame, ""));
                marcosetbtn.setText(marcoset.getString(marcobtn, ""));

                break;

            case 6:

                marco1set = getSharedPreferences(portSwitch + marcoset1data, 0);
                marcoset1name.setText(marco1set.getString(marco1name, ""));
                marco1setbtn.setText(marco1set.getString(marco1btn, ""));

                break;

            case 7:

                marco2set = getSharedPreferences(portSwitch + marcoset2data, 0);
                marcoset2name.setText(marco2set.getString(marco2name, ""));
                marco2setbtn.setText(marco2set.getString(marco2btn, ""));


                break;

            case 8:

                marco3set = getSharedPreferences(portSwitch + marcoset3data, 0);
                marcoset3name.setText(marco3set.getString(marco3name, ""));
                marco3setbtn.setText(marco3set.getString(marco3btn, ""));
                break;

            case 9:

                marco4set = getSharedPreferences(portSwitch + marcoset4data, 0);
                marcoset4name.setText(marco4set.getString(marco4name, ""));
                marco4setbtn.setText(marco4set.getString(marco4btn, ""));
                break;

            case 10:

                marco5set = getSharedPreferences(portSwitch + marcoset5data, 0);
                marcoset5name.setText(marco5set.getString(marco5name, ""));
                marco5setbtn.setText(marco5set.getString(marco5btn, ""));

                break;

            case 11:

                curtain = getSharedPreferences(portSwitch + curtaindata, 0);
                cur1edit.setText(curtain.getString(curtain1, ""));
                cur2edit.setText(curtain.getString(curtain2, ""));
                cur3edit.setText(curtain.getString(curtain3, ""));
                cur4edit.setText(curtain.getString(curtain4, ""));
                cur5edit.setText(curtain.getString(curtain5, ""));
                cur6edit.setText(curtain.getString(curtain6, ""));
                cur7edit.setText(curtain.getString(curtain7, ""));
                cur8edit.setText(curtain.getString(curtain8, ""));
                cur9edit.setText(curtain.getString(curtain9, ""));
                cur10edit.setText(curtain.getString(curtain10, ""));
                cur11edit.setText(curtain.getString(curtain11, ""));
                cur12edit.setText(curtain.getString(curtain12, ""));
                break;

            case 12:
                airseting = getSharedPreferences(portSwitch + airdata, 0);
                meditText52.setText(airseting.getString(btn17name, ""));
                meditText53.setText(airseting.getString(btn18name, ""));
                meditText54.setText(airseting.getString(btn19name, ""));
                meditText55.setText(airseting.getString(btn20name, ""));
                meditText56.setText(airseting.getString(btn21name, ""));
                meditText57.setText(airseting.getString(btn22name, ""));
                break;

            case 13:
                IPname = getSharedPreferences(Ipdata, 0);
                String ip_address;
                String port_gate;
                String config_name = "";

                if (spinner.getSelectedItemPosition() == 0) {
                    //預設第一組-->192.168.2.66   8899
                    ip_address = "192.168.2.66";
                    port_gate = "8899";
                } else if (spinner.getSelectedItemPosition() == 1) {
                    //預設第二組-->192.168.6.99   8899
                    ip_address = "192.168.6.99";
                    port_gate = "8899";
                } else {
                    ip_address = IPname.getString(IP + spinner.getSelectedItemPosition(), "");
                    port_gate = IPname.getString(PORT + spinner.getSelectedItemPosition(), "");
                }

                portSwitch = spinner.getSelectedItemPosition();
                if(D) Log.d("儲存IP設定 組態-->","portSwitch:" + portSwitch);
                ip.setText(ip_address);
                port.setText(port_gate);
                ((EditText) findViewById(R.id.ed_config_name)).setText(IPname.getString(NAME + spinner.getSelectedItemPosition(), ""));


                break;


        }
    }

    public void saveData(int i) {
        switch (i) {

            case 1:

                lamp = getSharedPreferences(portSwitch + lampdata, 0);
                lamp.edit()
                        .putString(btn01name, ((EditText) findViewById(R.id.lampeditText1)).getText().toString())
                        .putString(btn02name, ((EditText) findViewById(R.id.lampeditText2)).getText().toString())
                        .putString(btn03name, ((EditText) findViewById(R.id.lampeditText3)).getText().toString())
                        .putString(btn04name, ((EditText) findViewById(R.id.lampeditText4)).getText().toString())
                        .putString(btn05name, ((EditText) findViewById(R.id.lampeditText5)).getText().toString())
                        .putString(btn06name, ((EditText) findViewById(R.id.lampeditText6)).getText().toString())
                        .putString(btn07name, ((EditText) findViewById(R.id.lampeditText7)).getText().toString())
                        .putString(btn08name, ((EditText) findViewById(R.id.lampeditText8)).getText().toString())
                        .putString(btn09name, ((EditText) findViewById(R.id.lampeditText9)).getText().toString())
                        .putString(btn10name, ((EditText) findViewById(R.id.lampeditText10)).getText().toString())
                        .putString(btn11name, ((EditText) findViewById(R.id.lampeditText11)).getText().toString())
                        .putString(btn12name, ((EditText) findViewById(R.id.lampeditText12)).getText().toString())
                        .putString(btn1pic, ((EditText) findViewById(R.id.lampeditpic1)).getText().toString())
                        .putString(btn2pic, ((EditText) findViewById(R.id.lampeditpic2)).getText().toString())
                        .putString(btn3pic, ((EditText) findViewById(R.id.lampeditpic3)).getText().toString())
                        .putString(btn4pic, ((EditText) findViewById(R.id.lampeditpic4)).getText().toString())
                        .putString(btn5pic, ((EditText) findViewById(R.id.lampeditpic5)).getText().toString())
                        .putString(btn6pic, ((EditText) findViewById(R.id.lampeditpic6)).getText().toString())
                        .putString(btn7pic, ((EditText) findViewById(R.id.lampeditpic7)).getText().toString())
                        .putString(btn8pic, ((EditText) findViewById(R.id.lampeditpic8)).getText().toString())
                        .putString(btn9pic, ((EditText) findViewById(R.id.lampeditpic9)).getText().toString())
                        .putString(btn10pic, ((EditText) findViewById(R.id.lampeditpic10)).getText().toString())
                        .putString(btn11pic, ((EditText) findViewById(R.id.lampeditpic11)).getText().toString())
                        .putString(btn12pic, ((EditText) findViewById(R.id.lampeditpic12)).getText().toString())
                        .apply();


                break;

            case 2:
                setting = getSharedPreferences(portSwitch + custom1data, 0);
                setting.edit()
                        .putString(custom1name, custom1edit.getText().toString())
//            .putString(btn54name, meditText1.getText().toString())
//            .putString(btn55name, meditText2.getText().toString())
//            .putString(btn56name, meditText3.getText().toString())
//            .putString(btn57name, meditText4.getText().toString())
//            .putString(btn58name, meditText5.getText().toString())
//            .putString(btn59name, meditText6.getText().toString())
//            .putString(btn60name, meditText7.getText().toString())
//            .putString(btn61name, meditText8.getText().toString())
//            .putString(btn62name, meditText9.getText().toString())
                        .putString(btn63name, meditText1.getText().toString())
                        .putString(btn64name, meditText2.getText().toString())
                        .putString(btn65name, meditText3.getText().toString())
                        .putString(btn66name, meditText4.getText().toString())
                        .putString(btn67name, meditText5.getText().toString())
                        .putString(btn68name, meditText6.getText().toString())
//            .putString(btn99name, meditText46.getText().toString())
//            .putString(btn100name, meditText47.getText().toString())
//            .putString(btn101name, meditText48.getText().toString())
//            .putString(btn102name, meditText49.getText().toString())
//            .putString(btn103name, meditText50.getText().toString())
//            .putString(btn104name, meditText51.getText().toString())
                        .apply();
                break;

            case 3:

                settingb = getSharedPreferences(portSwitch + custom2data, 0);
                settingb.edit()
                        .putString(custom2name, custom2edit.getText().toString())
//                .putString(btn69name, meditText16.getText().toString())
//                .putString(btn70name, meditText17.getText().toString())
//                .putString(btn71name, meditText18.getText().toString())
//                .putString(btn72name, meditText19.getText().toString())
//                .putString(btn73name, meditText20.getText().toString())
//                .putString(btn74name, meditText21.getText().toString())
//                .putString(btn75name, meditText22.getText().toString())
//                .putString(btn76name, meditText23.getText().toString())
                        .putString(btn78name, meditText16.getText().toString())
                        .putString(btn79name, meditText17.getText().toString())
                        .putString(btn80name, meditText18.getText().toString())
                        .putString(btn81name, meditText19.getText().toString())
                        .putString(btn82name, meditText20.getText().toString())
                        .putString(btn83name, meditText21.getText().toString())
//                .putString(btn83name, meditText22.getText().toString())
//                .putString(btn105name, meditText52.getText().toString())
//                .putString(btn106name, meditText53.getText().toString())
//                .putString(btn107name, meditText54.getText().toString())
//                .putString(btn108name, meditText55.getText().toString())
//                .putString(btn109name, meditText56.getText().toString())
//                .putString(btn110name, meditText57.getText().toString())
                        .apply();
                break;
            case 4:
                setting2 = getSharedPreferences(portSwitch + custom3data, 0);
                setting2.edit()
                        .putString(custom3name, custom3edit.getText().toString())
//                .putString(btn84name, meditText31.getText().toString())
//                .putString(btn85name, meditText32.getText().toString())
//                .putString(btn86name, meditText33.getText().toString())
//                .putString(btn87name, meditText34.getText().toString())
//                .putString(btn88name, meditText35.getText().toString())
//                .putString(btn89name, meditText36.getText().toString())
//                .putString(btn90name, meditText37.getText().toString())
//                .putString(btn91name, meditText38.getText().toString())
//                .putString(btn92name, meditText39.getText().toString())
                        .putString(btn93name, meditText31.getText().toString())
                        .putString(btn94name, meditText32.getText().toString())
                        .putString(btn95name, meditText33.getText().toString())
                        .putString(btn96name, meditText34.getText().toString())
                        .putString(btn97name, meditText35.getText().toString())
                        .putString(btn98name, meditText36.getText().toString())
//                .putString(btn111name, meditText58.getText().toString())
//                .putString(btn112name, meditText59.getText().toString())
//                .putString(btn113name, meditText60.getText().toString())
//                .putString(btn114name, meditText61.getText().toString())
//                .putString(btn115name, meditText62.getText().toString())
//                .putString(btn116name, meditText63.getText().toString())
                        .apply();
                break;

            case 5:
                marcoset = getSharedPreferences(portSwitch + marcosetdata, 0);
                marcoset.edit()
                        .putString(marconame, marcosetname.getText().toString())
                        .putString(marcobtn, marcosetbtn.getText().toString())
                        .apply();
                break;

            case 6:
                marco1set = getSharedPreferences(portSwitch + marcoset1data, 0);
                marco1set.edit()
                        .putString(marco1name, marcoset1name.getText().toString())
                        .putString(marco1btn, marco1setbtn.getText().toString())
                        .apply();
                break;

            case 7:
                marco2set = getSharedPreferences(portSwitch + marcoset2data, 0);
                marco2set.edit()
                        .putString(marco2name, marcoset2name.getText().toString())
                        .putString(marco2btn, marco2setbtn.getText().toString())
                        .apply();
                break;

            case 8:
                marco3set = getSharedPreferences(portSwitch + marcoset3data, 0);
                marco3set.edit()
                        .putString(marco3name, marcoset3name.getText().toString())
                        .putString(marco3btn, marco3setbtn.getText().toString())
                        .apply();
                break;

            case 9:
                marco4set = getSharedPreferences(portSwitch + marcoset4data, 0);
                marco4set.edit()
                        .putString(marco4name, marcoset4name.getText().toString())
                        .putString(marco4btn, marco4setbtn.getText().toString())
                        .apply();
                break;

            case 10:
                marco5set = getSharedPreferences(portSwitch + marcoset5data, 0);
                marco5set.edit()
                        .putString(marco5name, marcoset5name.getText().toString())
                        .putString(marco5btn, marco5setbtn.getText().toString())
                        .apply();
                break;


            case 11:

                curtain = getSharedPreferences(portSwitch + curtaindata, 0);
                curtain.edit()
                        .putString(curtain1, cur1edit.getText().toString())
                        .putString(curtain2, cur2edit.getText().toString())
                        .putString(curtain3, cur3edit.getText().toString())
                        .putString(curtain4, cur4edit.getText().toString())
                        .putString(curtain5, cur5edit.getText().toString())
                        .putString(curtain6, cur6edit.getText().toString())
                        .putString(curtain7, cur7edit.getText().toString())
                        .putString(curtain8, cur8edit.getText().toString())
                        .putString(curtain9, cur9edit.getText().toString())
                        .putString(curtain10, cur10edit.getText().toString())
                        .putString(curtain11, cur11edit.getText().toString())
                        .putString(curtain12, cur12edit.getText().toString())
                        .apply();
                break;


            case 12:
                marcoset = getSharedPreferences(portSwitch + marcosetdata, 0);
                marcoset.edit()
                        .putString(marconame, f1edit.getText().toString())
                        .apply();

                marco1set = getSharedPreferences(portSwitch + marcoset1data, 0);
                marco1set.edit()
                        .putString(marco1name, f2edit.getText().toString())
                        .apply();

                marco2set = getSharedPreferences(portSwitch + marcoset2data, 0);
                marco2set.edit()
                        .putString(marco2name, f3edit.getText().toString())
                        .apply();

                marco3set = getSharedPreferences(portSwitch + marcoset3data, 0);
                marco3set.edit()
                        .putString(marco3name, f4edit.getText().toString())
                        .apply();

                marco4set = getSharedPreferences(portSwitch + marcoset4data, 0);
                marco4set.edit()
                        .putString(marco4name, f5edit.getText().toString())
                        .apply();

                marco5set = getSharedPreferences(portSwitch + marcoset5data, 0);
                marco5set.edit()
                        .putString(marco5name, f6edit.getText().toString())
                        .apply();
                break;

            case 13:
                airseting = getSharedPreferences(portSwitch + airdata, 0);
                airseting.edit()
                        .putString(btn17name, meditText52.getText().toString())
                        .putString(btn18name, meditText53.getText().toString())
                        .putString(btn19name, meditText54.getText().toString())
                        .putString(btn20name, meditText55.getText().toString())
                        .putString(btn21name, meditText56.getText().toString())
                        .putString(btn22name, meditText57.getText().toString())
                        .apply();
                break;

            case 14:
                IPname = getSharedPreferences(Ipdata, 0);
                String ip_address = ip.getText().toString();
                String port_gate = port.getText().toString();
                String config_name = ((EditText) findViewById(R.id.ed_config_name)).getText().toString();

                if (spinner.getSelectedItemPosition() == 0 | spinner.getSelectedItemPosition() == 1) {
                    //預設第一組-->192.168.2.66   8899
                    //預設第二組-->192.168.6.99   8899
                    //UtilMessageDialog.showToast(this, "該組IP設定為預設值");

                    IPname.edit()
                            .putString(NAME + spinner.getSelectedItemPosition(), config_name)
                            .apply();
                } else {
                    IPname.edit()
                            .putString(IP + spinner.getSelectedItemPosition() , ip_address)
                            .putString(PORT + spinner.getSelectedItemPosition(), port_gate)
                            .putString(NAME + spinner.getSelectedItemPosition(), config_name)
                            .apply();
                }

                if (D) Log.d("spinner: ", String.valueOf(spinner.getSelectedItemPosition()));
                break;


        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {//捕捉返回鍵
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            ConfirmExit();//按返回鍵，則執行退出確認
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void ConfirmExit() {//退出確認
        AlertDialog.Builder ad = new AlertDialog.Builder(BluetoothChat.this);
        ad.setTitle("飛鑫遙控");
        ad.setMessage("確定關閉程式?");
        ad.setPositiveButton("是", (dialog, i) -> {
            // TODO Auto-generated method stub
            BluetoothChat.this.finish();

        });
        ad.setNegativeButton("否", (dialog, i) -> {
        });
        ad.show();
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTabChanged(String tabId) {
        // TODO Auto-generated method stub

    }

    //YILINEDIT-情境
    void sendMacro(final String macroArray, final Handler handler_sendMacro) {
        //YILINEDIT
        if (count13 == 0) {
            sendmessage("@!" + macroArray + "@");
            count13 = 1;
        } else if (count13 == 1) {
            sendmessage("@\"" + macroArray + "@");
            count13 = 2;
        } else if (count13 == 2) {
            sendmessage("@#" + macroArray + "@");
            count13 = 0;
        }
        /*new Thread(new Runnable(){
            public void run(){

                int timer=0;
                try{
                    for(int i=0;i<macroArray.length();i+=3){
                        //sendMessage(macroArray.substring(i, i+2));
                        handler_sendMacro.obtainMessage(BluetoothChat.SEND_MESSAGE_MACRO, macroArray.substring(i, i+2)).sendToTarget();
                        Log.e(TAG, macroArray.substring(i, i+2));
                        Log.e(TAG, macroArray.substring(i+2, i+3));

                        if(macroArray.substring(i+2, i+3).compareTo("'")==0){
                            timer =1;
                        }else if(macroArray.substring(i+2, i+3).compareTo("(")==0){
                            timer=2;
                        }else if(macroArray.substring(i+2, i+3).compareTo(")")==0){
                        }else if(macroArray.substring(i+2, i+3).compareTo("*")==0){
                            timer=4;
                        }else if(macroArray.substring(i+2, i+3).compareTo("+")==0){
                            timer=5;
                        }
                        Calendar begin = Calendar.getInstance();
                        int iDiffSec;
                        Log.e(TAG, "等"+timer+"秒");
                        do{
                            Calendar now= Calendar.getInstance();
                            iDiffSec = 60*(now.get(Calendar.MINUTE)-begin.get(Calendar.MINUTE))+(now.get(Calendar.SECOND)-begin.get(Calendar.SECOND));

                        }while(iDiffSec <timer);

                        // Log.e(TAG, "waitForSendNext");

                    }
                }
                catch(StringIndexOutOfBoundsException e){
                    e.getStackTrace();
                }



            }

        }).start();*/
    }

    void sendMessageAfter1s(final String message) {

        ThreadPoolUtil.getInstance().execute(() -> {

            try {
                Calendar begin = Calendar.getInstance();
                int iDiffSec;
                do {
                    Calendar now = Calendar.getInstance();
                    iDiffSec = 60000 * (now.get(Calendar.MINUTE) - begin.get(Calendar.MINUTE)) + 1000 * (now.get(Calendar.SECOND) - begin.get(Calendar.SECOND)) + (now.get(Calendar.MILLISECOND) - begin.get(Calendar.MILLISECOND));

                } while (iDiffSec < 2000);
                sendmessage(message);
            } catch (StringIndexOutOfBoundsException e) {
                e.getStackTrace();
            }
        });
    }

    /**
     * 缩放Bitmap图片
     **/
    public Bitmap zoomBitmap(Bitmap bitmap, int width, int height) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix matrix = new Matrix();
        float scaleWidth = ((float) width / w);
        float scaleHeight = ((float) height / h);
        matrix.postScale(scaleWidth, scaleHeight);// 利用矩阵进行缩放不会造成内存溢出
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
    }

    public String getBitmap(Uri uri, String who) {
        try {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inSampleSize = 4;
            Bitmap photo = BitmapFactory.decodeStream(this.getContentResolver().openInputStream(uri), null, opts);

            /*判斷輸入的圖片大小，調整縮小比例
             * 目前規則:暫定如下
             * 640*320上下不處理
             * 1024*640  --> /2
             * 2048*1280 --> /4
             * 3096*2560 --> /6
             */

            int zoomSize = 2;
            int photoWidth = photo.getWidth();
            int photoHeight = photo.getHeight();

            if (photoWidth > photoHeight) {
                //Width > Height
                zoomSize = checkZoomBitmapSize(photoHeight, 1);
            } else if (photoWidth < photoHeight) {
                //Width < Height
                zoomSize = checkZoomBitmapSize(photoWidth, 2);
            } else if (photoWidth == photoHeight) {
                //Width = Height
                zoomSize = checkZoomBitmapSize(photoWidth, 3);
            }

            if (D) Log.d(TAG, "縮小比例" + zoomSize);

            Bitmap smallBitmap = zoomBitmap(photo, photo.getWidth() / zoomSize, photo.getHeight() / zoomSize);
            photo.recycle();
            FileOutputStream fos = null;
            fos = new FileOutputStream(this.getFilesDir().getPath() + portSwitch + "Lamp" + who + ".jpg");

            String Imagelamp1 = "file://" + this.getFilesDir().getPath() + portSwitch + "Lamp" + who + ".jpg";
            smallBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.close();
            return Imagelamp1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private int checkZoomBitmapSize(int OriSize, int type) {
        if (OriSize / 640 < 1)
            OriSize = 2;
        else
            OriSize = OriSize / 640;
        return OriSize;
    }

    private void funMenuSelection(ArrayList<String> menu, final String type) {

        new CircleDialog.Builder(BluetoothChat.this)
                .configDialog(new ConfigDialog() {
                    @Override
                    public void onConfig(DialogParams params) {
                        params.animStyle = R.style.dialogWindowAnim;
                    }
                })
                .setTitle("請選擇功能")
                .setTitleColor(Color.BLUE)
                .setItems(menu, (parent, view, position, id) -> {

                    switch (type) {

                        case "lamp":
                            if (position == 0) {
                                //自訂按鍵
                                setContentView(R.layout.lampset);
                                getLampBtnPic();
                                findViewById(R.id.lampok).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic1).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic2).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic3).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic4).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic5).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic6).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic7).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic8).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic9).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic10).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic11).setOnClickListener(new ButtonListener());
                                findViewById(R.id.lampic12).setOnClickListener(new ButtonListener());
                                readData(1);

                            } else
                                //重新連線
                                new MySocketThread(mHandler, BluetoothChat.this, 0, "00");
                            break;

                        case "air":

                            if (position == 0) {
                                //開啟學習
                                sendmessage("Li");
                            } else if (position == 1) {
                                sendmessage("Cc");
                            } else if (position == 2) {
                                //按鍵自訂
                                setContentView(R.layout.airseting);

                                mButton13 = findViewById(R.id.btn13);
                                mButton14 = findViewById(R.id.btn14);
                                mButton15 = findViewById(R.id.btn15);
                                mButton16 = findViewById(R.id.btn16);
                                mButton17 = findViewById(R.id.btn17);
                                mButton18 = findViewById(R.id.btn18);
                                mButton19 = findViewById(R.id.btn19);
                                mButton20 = findViewById(R.id.btn20);
                                mButton21 = findViewById(R.id.btn21);
                                mButton22 = findViewById(R.id.btn22);
                                mButton23 = findViewById(R.id.btn23);
                                mButton24 = findViewById(R.id.btn24);
                                airok = findViewById(R.id.airok);

                                meditText52 = findViewById(R.id.meditText52);
                                meditText53 = findViewById(R.id.meditText53);
                                meditText54 = findViewById(R.id.meditText54);
                                meditText55 = findViewById(R.id.meditText55);
                                meditText56 = findViewById(R.id.meditText56);
                                meditText57 = findViewById(R.id.meditText57);

                                airok.setOnClickListener(new ButtonListener());

                                readData(12);
                            } else if (position == 3) {
                                //重新連線
                                new MySocketThread(mHandler, BluetoothChat.this, 0, "00");
                            }

                            break;
                        case "tv":
                            if (position == 0) {
                                //開啟學習
                                sendmessage("Li");
                            } else if (position == 1) {
                                sendmessage("Cc");
                            } else if (position == 2) {
                                //重新連線
                                new MySocketThread(mHandler, BluetoothChat.this, 0, "00");
                            }
                            break;

                        case "custom1":

                            if (position == 0) {
                                //開啟學習
                                sendmessage("Li");
                            } else if (position == 1) {
                                //關閉學習
                                sendmessage("Cc");
                            } else if (position == 2) {
                                //自訂按鍵
                                setContentView(R.layout.setting);
                                custom1edit = findViewById(R.id.custom1edit);
                                meditText1 = findViewById(R.id.editText1);
                                meditText2 = findViewById(R.id.editText2);
                                meditText3 = findViewById(R.id.editText3);
                                meditText4 = findViewById(R.id.editText4);
                                meditText5 = findViewById(R.id.editText5);
                                meditText6 = findViewById(R.id.editText6);
                                mButton63 = findViewById(R.id.btn63);
                                mButton64 = findViewById(R.id.btn64);
                                mButton65 = findViewById(R.id.btn65);
                                mButton66 = findViewById(R.id.btn66);
                                mButton67 = findViewById(R.id.btn67);
                                mButton68 = findViewById(R.id.btn68);
                                set00 = findViewById(R.id.set00);
                                set00.setOnClickListener(new ButtonListener());
                                readData(2);
                            } else if (position == 3) {
                                //重新連線
                                new MySocketThread(mHandler, BluetoothChat.this, 0, "00");
                            }


                            break;

                        case "custom2":
                            if (position == 0) {
                                //開啟學習
                                sendmessage("Li");
                            } else if (position == 1) {
                                //關閉學習
                                sendmessage("Cc");
                            } else if (position == 2) {
                                //自訂按鍵
                                setContentView(R.layout.setting1);

                                mButton78 = findViewById(R.id.btn78);
                                mButton79 = findViewById(R.id.btn79);
                                mButton80 = findViewById(R.id.btn80);
                                mButton81 = findViewById(R.id.btn81);
                                mButton82 = findViewById(R.id.btn82);
                                mButton83 = findViewById(R.id.btn83);
                                custom2edit = findViewById(R.id.custom2edit);
                                meditText16 = findViewById(R.id.editText16);
                                meditText17 = findViewById(R.id.editText17);
                                meditText18 = findViewById(R.id.editText18);
                                meditText19 = findViewById(R.id.editText19);
                                meditText20 = findViewById(R.id.editText20);
                                meditText21 = findViewById(R.id.editText21);
                                set01 = findViewById(R.id.set01);
                                set01.setOnClickListener(new ButtonListener());
                                readData(3);
                            } else if (position == 3) {
                                //重新連線
                                new MySocketThread(mHandler, BluetoothChat.this, 0, "00");
                            }
                            break;

                        case "custom3":
                            if (position == 0) {
                                //開啟學習
                                sendmessage("Li");
                            } else if (position == 1) {
                                //關閉學習
                                sendmessage("Cc");
                            } else if (position == 2) {
                                //自訂按鍵
                                setContentView(R.layout.setting2);

                                mButton93 = findViewById(R.id.btn93);
                                mButton94 = findViewById(R.id.btn94);
                                mButton95 = findViewById(R.id.btn95);
                                mButton96 = findViewById(R.id.btn96);
                                mButton97 = findViewById(R.id.btn97);
                                mButton98 = findViewById(R.id.btn98);
                                custom3edit = findViewById(R.id.custom3edit);
                                meditText31 = findViewById(R.id.editText31);
                                meditText32 = findViewById(R.id.editText32);
                                meditText33 = findViewById(R.id.editText33);
                                meditText34 = findViewById(R.id.editText34);
                                meditText35 = findViewById(R.id.editText35);
                                meditText36 = findViewById(R.id.editText36);
                                set02 = findViewById(R.id.set02);
                                set02.setOnClickListener(new ButtonListener());
                                readData(4);
                            } else if (position == 3) {
                                //重新連線
                                new MySocketThread(mHandler, BluetoothChat.this, 0, "00");
                            }
                            break;
                    }


                }).show();
    }

    @Override
    public boolean handleMessage(Message msg) {
        // TODO Auto-generated method stub
        switch (msg.what) {
            case BluetoothChat.SEND_MESSAGE_MACRO:
                String readMessage = msg.obj.toString();
                sendmessage(readMessage);
                break;
        }

        return false;
    }

}

class myHandler extends Handler {

    private final WeakReference<BluetoothChat> mActivity;

    public myHandler(BluetoothChat activity) {
        mActivity = new WeakReference<>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);

        TextView title = mActivity.get().findViewById(R.id.title_right_text);
        String config_name = "[" +  this.mActivity.get().getSharedPreferences("IPDATA", 0).getString("NAME" + PreferenceManager.getDefaultSharedPreferences(this.mActivity.get()).getString(QuickstartPreferences.SPINNER_WHICH, "0"),"") + "]";
        String temp = "";

        try {
            if (MySocketThread.sktConnetion()) {
                 temp = config_name + "主機已連線";
                title.setText(temp);
            } else {
                 temp = config_name + "主機未連線";
                title.setText(temp);
            }
            switch (msg.what) {

                case SEND_OK:
                    temp = config_name + "主機已連線";
                    title.setText(temp);
                    title.setBackgroundColor(Color.GREEN);
                    break;

                case SEND_FAILED:
                     temp = config_name + "主機未連線";
                    title.setText(temp);
                    title.setBackgroundColor(Color.RED);
                    break;

                case SEND_RECIEVE:
                    try {
                        if (msg.obj.toString().contains("j"))
                            if (msg.obj.toString().contains("j000")) {
                                mActivity.get().findViewById(R.id.btn01).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn01).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("k"))
                            if (msg.obj.toString().contains("k000")) {
                                mActivity.get().findViewById(R.id.btn02).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn02).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("l"))
                            if (msg.obj.toString().contains("l000")) {
                                mActivity.get().findViewById(R.id.btn03).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn03).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("m"))
                            if (msg.obj.toString().contains("m000")) {
                                mActivity.get().findViewById(R.id.btn04).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn04).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("n"))
                            if (msg.obj.toString().contains("n000")) {
                                mActivity.get().findViewById(R.id.btn05).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn05).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("o"))
                            if (msg.obj.toString().contains("o000")) {
                                mActivity.get().findViewById(R.id.btn06).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn06).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("p"))
                            if (msg.obj.toString().contains("p000")) {
                                mActivity.get().findViewById(R.id.btn07).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn07).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("q"))
                            if (msg.obj.toString().contains("q000")) {
                                mActivity.get().findViewById(R.id.btn08).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn08).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("r"))
                            if (msg.obj.toString().contains("r000")) {
                                mActivity.get().findViewById(R.id.btn09).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn09).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("s"))
                            if (msg.obj.toString().contains("s000")) {
                                mActivity.get().findViewById(R.id.btn10).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn10).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("t"))
                            if (msg.obj.toString().contains("t000")) {
                                mActivity.get().findViewById(R.id.btn11).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn11).setBackgroundResource(R.drawable.set_9_invert);
                            }
                        if (msg.obj.toString().contains("u"))
                            if (msg.obj.toString().contains("u000")) {
                                mActivity.get().findViewById(R.id.btn12).setBackgroundResource(R.drawable.set_9);
                            } else {
                                mActivity.get().findViewById(R.id.btn12).setBackgroundResource(R.drawable.set_9_invert);
                            }


//                if(msg.obj.toString().contains("v")){
//                    viewPager.setCurrentItem(page_safe_alert);
//                    if (msg.obj.toString().contains("v0"))
//                    {
//                        findViewById(R.id.safe1).setBackgroundResource(R.drawable.set_9);
//                        findViewById(R.id.safe1).clearAnimation();
//                    }else {
//                        findViewById(R.id.safe1).setBackgroundResource(R.drawable.set_9_invert);
//                        Animation mAnimation = new AlphaAnimation(1, 0);
//                        mAnimation.setDuration(200);
//                        mAnimation.setInterpolator(new LinearInterpolator());
//                        mAnimation.setRepeatCount(Animation.INFINITE);
//                        mAnimation.setRepeatMode(Animation.REVERSE);
//                        findViewById(R.id.safe1).setAnimation(mAnimation);
//                    }
//                }
//
//                if(msg.obj.toString().contains("w")){
//                    viewPager.setCurrentItem(page_safe_alert);
//                    if (msg.obj.toString().contains("w0"))
//                    {
//                        findViewById(R.id.safe2).setBackgroundResource(R.drawable.set_9);
//                        findViewById(R.id.safe2).clearAnimation();
//                    }else {
//                        findViewById(R.id.safe2).setBackgroundResource(R.drawable.set_9_invert);
//                        Animation mAnimation = new AlphaAnimation(1, 0);
//                        mAnimation.setDuration(200);
//                        mAnimation.setInterpolator(new LinearInterpolator());
//                        mAnimation.setRepeatCount(Animation.INFINITE);
//                        mAnimation.setRepeatMode(Animation.REVERSE);
//                        findViewById(R.id.safe2).setAnimation(mAnimation);
//                    }
//                }
//
//                if(msg.obj.toString().contains("x")){
//                    viewPager.setCurrentItem(page_safe_alert);
//                    if (msg.obj.toString().contains("x0")) {
//                        findViewById(R.id.safe3).setBackgroundResource(R.drawable.set_9);
//                        findViewById(R.id.safe3).clearAnimation();
//                    } else {
//                        findViewById(R.id.safe3).setBackgroundResource(R.drawable.set_9_invert);
//                        Animation mAnimation = new AlphaAnimation(1, 0);
//                        mAnimation.setDuration(200);
//                        mAnimation.setInterpolator(new LinearInterpolator());
//                        mAnimation.setRepeatCount(Animation.INFINITE);
//                        mAnimation.setRepeatMode(Animation.REVERSE);
//                        findViewById(R.id.safe3).setAnimation(mAnimation);
//                    }
//                }
//
//                if(msg.obj.toString().contains("y")){
//                    viewPager.setCurrentItem(page_safe_alert);
//                    if (msg.obj.toString().contains("y0"))
//                    {
//                        findViewById(R.id.safe4).setBackgroundResource(R.drawable.set_9);
//                        findViewById(R.id.safe4).clearAnimation();
//                    }else {
//                        findViewById(R.id.safe4).setBackgroundResource(R.drawable.set_9_invert);
//                        Animation mAnimation = new AlphaAnimation(1, 0);
//                        mAnimation.setDuration(200);
//                        mAnimation.setInterpolator(new LinearInterpolator());
//                        mAnimation.setRepeatCount(Animation.INFINITE);
//                        mAnimation.setRepeatMode(Animation.REVERSE);
//                        findViewById(R.id.safe4).setAnimation(mAnimation);
//                    }
//                }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
