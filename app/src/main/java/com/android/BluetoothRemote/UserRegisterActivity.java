package com.android.BluetoothRemote;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.nfc.Tag;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.RemoteWiFi.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Pattern;

public class UserRegisterActivity extends Activity {
    private static final String TAG = "UserRegisterActivity";
    private EditText registerBirth,registerName,registerPhoneNum,registerMail,registerAddress1,registerAddress2,registerAddress3;

    private RadioButton registerMale,registerFemale;
    Socket socket_gcm = null;
    private SharedPreferences IPname,registerData;
    private static final String Ipdata = "IPDATA";
    private static final String IP_gcm = "IP_gcm";
    private static final String gcmID = "gcn_ID";

    private static final String myRegisterData = "myRegisterData";
    private static final String myRegisterName = "myRegisterName";
    private static final String myRegisterMail = "myRegisterMail";
    private static final String myRegisterBirth = "myRegisterBirth";
    private static final String myRegisterSex = "myRegisterSex";
    private static final String myregisterAddress1 = "myregisterAddress1";
    private static final String myregisterAddress2 = "myregisterAddress2";
    private static final String myregisterAddress3 = "myregisterAddress3";
    private static final String myregisterPhoneNum = "myregisterPhone";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);
        init();
        readData();
    }

    private void init(){
        registerName = (EditText)findViewById(R.id.registerName);
        registerPhoneNum = (EditText)findViewById(R.id.registerPhoneNum);
        registerMail = (EditText)findViewById(R.id.registerMail);
        registerBirth = (EditText)findViewById(R.id.registerBirth);
        registerMale =(RadioButton)findViewById(R.id.registerMale);
        registerFemale = (RadioButton)findViewById(R.id.registerFemale);
        registerAddress1 = (EditText)findViewById(R.id.registerAddress1);
        registerAddress2 = (EditText)findViewById(R.id.registerAddress2);
        registerAddress3 = (EditText)findViewById(R.id.registerAddress3);
    }

    private void readData(){
        registerData = getSharedPreferences(myRegisterData,0);
        registerName.setText(registerData.getString(myRegisterName  ,""));
        registerPhoneNum.setText(registerData.getString(myregisterPhoneNum  ,""));
        registerMail.setText(registerData.getString(myRegisterMail  ,""));
        registerBirth.setText(registerData.getString(myRegisterBirth  ,""));
        if(registerData.getString(myRegisterSex  ,"")=="F"){
            registerMale.setChecked(false);
            registerFemale.setChecked(true);
        }else {
            registerMale.setChecked(true);
            registerFemale.setChecked(false);
        }

        registerAddress1.setText(registerData.getString(myregisterAddress1  ,""));
        registerAddress2.setText(registerData.getString(myregisterAddress2  ,""));
        registerAddress3.setText(registerData.getString(myregisterAddress3  ,""));
    }

    private void saveData(){
        registerData = getSharedPreferences(myRegisterData,0);
        registerData.edit()
                .putString(myRegisterName, registerName.getText().toString())
                .putString(myregisterPhoneNum, registerPhoneNum.getText().toString())
                .putString(myRegisterMail, registerMail.getText().toString())
                .putString(myRegisterBirth, registerBirth.getText().toString())
                .putString(myRegisterSex, registerMale.isChecked() ? "M" : "F")
                .putString(myregisterAddress1, registerAddress1.getText().toString())
                .putString(myregisterAddress2, registerAddress2.getText().toString())
                .putString(myregisterAddress3, registerAddress3.getText().toString())
                .apply();
    }

    private JSONObject sendJsonData() throws JSONException {
        final String token = IPname.getString(gcmID, "");
        JSONObject j_obj = new JSONObject();
        j_obj.put("token", token)
                .put(myRegisterName, registerName.getText().toString())
                .put(myregisterPhoneNum, registerPhoneNum.getText().toString())
                .put(myRegisterMail, registerMail.getText().toString())
                .put(myRegisterBirth, registerBirth.getText().toString())
                .put(myRegisterSex, registerMale.isChecked() ? "M" : "F")
                .put(myregisterAddress1, registerAddress1.getText().toString())
                .put(myregisterAddress2, registerAddress2.getText().toString())
                .put(myregisterAddress3, registerAddress3.getText().toString())
                .put(myregisterAddress3, registerAddress3.getText().toString())
                .put("sender", "FX");
        return  j_obj;
    }
    public void getDatePickerDialog(View view){
        Calendar m_Calendar	= Calendar.getInstance();
        DatePickerDialog dialog =
                new DatePickerDialog(this,
                        datepicker,
                        m_Calendar.get(Calendar.YEAR),
                        m_Calendar.get(Calendar.MONTH),
                        m_Calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();

    }

    DatePickerDialog.OnDateSetListener datepicker = new DatePickerDialog.OnDateSetListener()
    {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
        {
            Calendar m_Calendar =Calendar.getInstance();
            m_Calendar.set(Calendar.YEAR, year);
            m_Calendar.set(Calendar.MONTH, monthOfYear);
            m_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "yyyy/MM/dd"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.TAIWAN);
            registerBirth.setText(sdf.format(m_Calendar.getTime()));
            //Log.d(TAG, "年" + year + "月" + monthOfYear + "日" + dayOfMonth);
        }
    };

    public class onfocusListner implements View.OnFocusChangeListener{

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            Toast.makeText(UserRegisterActivity.this,"",Toast.LENGTH_SHORT).show();
        }
    }

    public void setProfileData(View view){
        if(registerName.getText().toString().isEmpty() ||
                registerMail.getText().toString().isEmpty()||
                registerBirth.getText().toString().isEmpty()||
                registerAddress1.getText().toString().isEmpty()||
                registerAddress2.getText().toString().isEmpty()||
                registerAddress3.getText().toString().isEmpty()
                ){
            AlertDialog alertDialog = getAlertDialog("有些選項為空白","除了電話號碼，其餘為必填項目。");
            alertDialog.show();
        }else if(!Patterns.EMAIL_ADDRESS.matcher(registerMail.getText().toString()).matches()){
            AlertDialog alertDialog = getAlertDialog("電子郵件格式錯誤","你的電子郵件可能填錯了");
            alertDialog.show();
        }else if(!Pattern.compile("[0-9]{10}").matcher(registerPhoneNum.getText().toString()).matches() && !registerPhoneNum.getText().toString().isEmpty()){
            AlertDialog alertDialog = getAlertDialog("手機號碼格式錯誤","你的手機號碼可能填錯了");
            alertDialog.show();
        }else{
            saveData();
            gcm_init();
        }

    }

    private AlertDialog getAlertDialog(String title,String message){
        //產生一個Builder物件
        AlertDialog.Builder builder = new AlertDialog.Builder(UserRegisterActivity.this);
        //設定Dialog的標題
        builder.setTitle(title);
        //設定Dialog的內容
        builder.setMessage(message);
        builder.setPositiveButton("我瞭解了", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //按下按鈕時顯示快顯
                Toast.makeText(UserRegisterActivity.this, "您按下OK按鈕", Toast.LENGTH_SHORT).show();
            }
        });
        return builder.create();
    }

    boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    public void gcm_init() {
        //Toast.makeText(this, "INIT", Toast.LENGTH_SHORT).show();
        IPname = getSharedPreferences(Ipdata, 0);
        ThreadPoolUtil.getInstance().execute(new Runnable() {//菴ｿ逕ｨ郤ｿ遞区ｱ�			@Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    String IP = IPname.getString(IP_gcm, "");
                    //Log.d(TAG, IP);
                    int PORT = 1337;
                    socket_gcm = new Socket(IP, PORT);
                    try {
                        sendmessage_gcm(sendJsonData() + "\n");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    receievmessage_gcm();
                    socket_gcm.close();
                } catch (UnknownHostException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Message m = new Message();
                    m.what = 2;
                    handler.sendMessage(m);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Message m = new Message();
                    m.what = 2;
                    handler.sendMessage(m);
                }
            }
        });

    }



    public void sendmessage_gcm(String message) {
        //Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        try {
            // 蜷第恪蜉｡蝎ｨ遶ｯ蜿鷹�謨ｰ謐ｮ
            DataOutputStream out = new DataOutputStream(socket_gcm.getOutputStream());
            //String str = sendContent.getText().toString().trim();
            message = message +"\r\n";
            out.write(message.getBytes("UTF-8"));
            //Thread.sleep(300);
            Message m = new Message();
            m.what = 1;
            handler.sendMessage(m);
        } catch (Exception e) {
            e.printStackTrace();
            Message m = new Message();
            m.what = 2;
            handler.sendMessage(m);
        }
    }

    public void receievmessage_gcm(){
        try{
            while(true){
                DataInputStream input = new DataInputStream(socket_gcm.getInputStream());
                byte[] buffer;
                buffer = new byte[input.available()];
                if(buffer.length != 0){
                    input.read(buffer);
                    String msg = new String(buffer, "UTF-8");//豕ｨ諢剰ｽｬ遐�ｼ御ｸ咲┯荳ｭ譁�ｼ壻ｹｱ遐√�
                    Message m = new Message();
                    m.what = 4;
                    m.obj = msg;
                    handler.sendMessage(m);
                }
            }

        }catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            Message m = new Message();
            m.what = 2;
            handler.sendMessage(m);
        }
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            TextView title = (TextView)findViewById(R.id.title_right_text);
            //Log.d(TAG,msg.what+"");
            if (msg.what == 2) {
                Toast.makeText(UserRegisterActivity.this, "請確認網路連線是否正常", Toast.LENGTH_SHORT).show();
                //init();
            }
            if(msg.what == 4){
                if(msg.obj.toString().contains("okgcm")){
                    Toast.makeText(UserRegisterActivity.this, "註冊成功", Toast.LENGTH_SHORT).show();
                }else if(msg.obj.toString().contains("repeatgcm"))
                    Toast.makeText(UserRegisterActivity.this, "此裝置已註冊過", Toast.LENGTH_SHORT).show();
                else if(msg.obj.toString().contains("failgcm"))
                    Toast.makeText(UserRegisterActivity.this, "註冊失敗，請聯絡伺服器管理者", Toast.LENGTH_SHORT).show();
            }
        }
    };
}
