package com.android.BluetoothRemote;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.android.RemoteWiFi.BuildConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by lee on 2016/4/21.
 */
public class AssetDataBaseHelper extends SQLiteOpenHelper {
    private Context mycontext;
    private static String DB_NAME = "assetDB.sqlite";
    private static String DB_PATH ="/data/data/"+ BuildConfig.APPLICATION_ID+"/databases/";
    public SQLiteDatabase myDataBase;
    private String TAG ="AssetDataBaseHelper";
    public static final String VOICE_COLUMN = "voice";
    public static final String ACTION_COLUMN = "action";
    public static final String TABLE_NAME = "WIFIVoiceRecognizer";
    public AssetDataBaseHelper(Context context) {
        super(context,DB_NAME,null,1);
        this.mycontext=context;
        boolean dbexist = checkdatabase();
        createdatabase();
        opendatabase();
//        if (dbexist) {
//            Log.d(TAG,"exist");
//            opendatabase();
//        } else {
//            Log.d(TAG, "no exist");
//            createdatabase();
//            opendatabase();
//        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
    private boolean checkdatabase() {

        boolean checkdb = false;
        try {
            String myPath = DB_PATH + DB_NAME;
            File dbfile = new File(myPath);
            checkdb = dbfile.exists();
        } catch(SQLiteException e) {
            System.out.println("Database doesn't exist");
        }
        return checkdb;
    }
    public void opendatabase() throws SQLException {
        //Open the database
        String mypath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(mypath, null, SQLiteDatabase.OPEN_READWRITE);
    }
    public void createdatabase() {
        boolean dbexist = checkdatabase();
        this.getReadableDatabase();
        try {
            copydatabase();
        } catch(IOException e) {
            throw new Error("Error copying database");
        }
//        if(dbexist) {
//            System.out.println(" Database exists.");
//
//        } else {
//            this.getReadableDatabase();
//            try {
//                copydatabase();
//            } catch(IOException e) {
//                throw new Error("Error copying database");
//            }
//        }
    }
    private void copydatabase() throws IOException {
        //Open your local db as the input stream
        //Log.d(TAG,"copydatabase");
        InputStream myinput = mycontext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outfilename = DB_PATH + DB_NAME;
        //Log.d(TAG,"copydatabase"+outfilename);

        //Open the empty db as the output stream
        OutputStream myoutput = new FileOutputStream(outfilename);

        // transfer byte to inputfile to outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myinput.read(buffer))>0) {
            myoutput.write(buffer,0,length);
        }

        //Close the streams
        myoutput.flush();
        myoutput.close();
        myinput.close();
    }
    public String getVoiceResult(String voice){
        String finalResult=null;
        String where = VOICE_COLUMN + "='" + voice+"'";
        Cursor cursor = myDataBase.query(
                TABLE_NAME, null, where, null, null, null, null, null);
        cursor.moveToFirst();
        if(cursor!=null && cursor.getCount()>0){
            //Log.d("語音內建資料庫","成功");
            return  cursor.getString(1).toString();
        }
        //Log.d("語音內建資料庫","失敗");
        return  "finalResultIsNull";
    }
}
