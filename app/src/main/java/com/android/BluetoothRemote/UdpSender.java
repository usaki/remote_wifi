package com.android.BluetoothRemote;

import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by lee on 2016/5/11.
 */
public class UdpSender extends Thread{
    private static Handler handler;
    private String message_in="";
    private String ip_in="192.168.2.255";
    UdpSender(Handler handler){
         this.handler = handler;
    }
    public void start(String ip,String message){
        ip_in = ip;
        message_in =message;
        this.start();
    }
    public void run(){
        DatagramSocket socket = null;
        try
        {
            socket = new DatagramSocket();
            socket.setSoTimeout(10000);
            InetAddress serverAddress = InetAddress.getByName(ip_in);
            //Log.d("IP Address", serverAddress.toString());

            //創建一個用於發送的DatagramPacket對象
            DatagramPacket packet=new DatagramPacket(message_in.getBytes(),message_in.length(),serverAddress,16666);
            socket.send(packet);
            String txt;
            while(true){
                byte[] byte1024 = new byte[100];
                DatagramPacket dPacket = new DatagramPacket(byte1024, 100);
                socket.receive(dPacket);

                while(true)
                {
                    //印出來到螢幕上
                    txt = new String(byte1024, 0, dPacket.getLength());
                    //MainActivity.exHandler.sendMessage(MainActivity.exHandler.obtainMessage(1,txt));
                    //Log.d("User","Handler send Message "+txt);
                    UdpSender.handler.obtainMessage(HomeRegisterActivity.getMessage, txt).sendToTarget();
                    if(true) break;
                }


            }
//            }

        }
        catch(SocketException e)
        {
            e.printStackTrace();
            String error = e.toString();
            //Log.e("Error by Sender", error);
        }
        catch(UnknownHostException e)
        {
            e.printStackTrace();
            String error = e.toString();
            //Log.e("Error by Sender", error);
        } catch (SocketTimeoutException e){
            e.printStackTrace();
            String error = e.toString();
            //Log.e("Error by Sender", error);
            UdpSender.handler.obtainMessage(HomeRegisterActivity.socketTimeOut).sendToTarget();
        } catch(IOException e)
        {
            e.printStackTrace();
            String error = e.toString();
            //Log.e("Error by Sender", error);
        } catch(Exception e)
        {
            e.printStackTrace();
            String error = e.toString();
            //Log.e("Error by Sender", error);
        } finally{
            if(socket != null){
                socket.close();
            }

        }
    }
}
