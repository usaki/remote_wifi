package com.android.BluetoothRemote;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.android.RemoteWiFi.R;
import com.android.util.MySocketThread;
import com.bumptech.glide.Glide;

public class splash extends Activity{
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);

        Glide.with(this).load(R.drawable.splash).into((ImageView)findViewById(R.id.imageView1));
        new MySocketThread(new Handler(), splash.this, 0, "00");
        ThreadPoolUtil.getInstance().execute(() -> {
            try {
                Thread.sleep(500);
                startActivity(new Intent().setClass(splash.this, BluetoothChat.class));
                finish();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

    }
 

}