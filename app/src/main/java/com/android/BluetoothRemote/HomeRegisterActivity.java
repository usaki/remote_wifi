package com.android.BluetoothRemote;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.android.RemoteWiFi.R;

public class HomeRegisterActivity extends Activity implements Handler.Callback {
    public Handler handler = new Handler(this);
    public static final int  getMessage = 1;
    public static final int  socketTimeOut = 2;
    private SharedPreferences IPname;
    private static final String Ipdata = "IPDATA";
    private static final String gcmID = "gcn_ID";
    private static String token ="";

    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.activity_notify_sms);
        getFxIp();


    }
    private void getFxIp(){
        dialog = ProgressDialog.show(HomeRegisterActivity.this,
                "讀取中", "取得主機IP", true);
        UdpSender udpSender = new UdpSender(handler);
        //getNetSeg();
        String ip = getNetSeg();
        if(ip!="")
            udpSender.start(getNetSeg() + "255", "Get FX IP");
        else finish();
    }
    private String getNetSeg(){
        WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        if(wifiMgr.isWifiEnabled()){
            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            int ip = wifiInfo.getIpAddress();
            String ipAddress = Formatter.formatIpAddress(ip);
            String[] temp = ipAddress.split("\\.");
            //Log.d("TAG",temp[0]);
            //Log.d("TAG",temp[1]);
            return  temp[0]+"."+temp[1]+"."+temp[2]+".";
        }else{
            Toast.makeText(HomeRegisterActivity.this,"請連接主機所在的區域網路" , Toast.LENGTH_SHORT).show();
            return "";
        }


    }
    private void setFxWifi(String ip){
        dialog = ProgressDialog.show(HomeRegisterActivity.this,
                "讀取中", "請等待3秒...", true);
        UdpSender udpSender = new UdpSender(handler);
        udpSender.start(ip,"Set Wifi:");
    }
    private void setGcm(String ip){
        IPname = getSharedPreferences(Ipdata,0);
        token = IPname.getString(gcmID, "");
        dialog = ProgressDialog.show(HomeRegisterActivity.this,
                "讀取中", "註冊GCM", true);
        UdpSender udpSender = new UdpSender(handler);
        udpSender.start(ip,"GCM new ID:"+token);
    }
    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case  HomeRegisterActivity.getMessage:
                String message = msg.obj.toString();
                if(message.contains("-------------------------------") && message.contains("FX IP:")){
                    message = message.split("FX IP:")[1].split("\n")[0];
                    //Toast.makeText(HomeRegisterActivity.this,"已搜尋到家用主機" , Toast.LENGTH_SHORT).show();
                    //dialog.dismiss();
                    setGcm(message);
                }else if(message.contains("GCM new ID->")){
                    if(message.contains("OK"))
                        Toast.makeText(HomeRegisterActivity.this,"主機註冊成功" , Toast.LENGTH_SHORT).show();
                    else Toast.makeText(HomeRegisterActivity.this,"主機註冊失敗" , Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    Toast.makeText(HomeRegisterActivity.this,"連接失敗，請重試" , Toast.LENGTH_SHORT).show();
                    finish();
                }

                break;
            case HomeRegisterActivity.socketTimeOut:
                Toast.makeText(HomeRegisterActivity.this,"連線逾時，請確認主機已開啟" , Toast.LENGTH_LONG).show();
                finish();
                break;
        }
        return true;
    }
}
