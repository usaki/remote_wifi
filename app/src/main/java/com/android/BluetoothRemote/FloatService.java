package com.android.BluetoothRemote;

import java.util.ArrayList;
import java.util.List;

import com.android.RemoteWiFi.R;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

public class FloatService extends Service{
	List<ImageView> viewList = new ArrayList<ImageView>();
	WindowManager windowManager;
	PackageManager packageManager;
	
	Binder binder = new FloatBinder();
	static Handler handler_in;
	class IconHolder {
	        public ImageView view;
	        public Drawable defaultIcon;
	        public int id;
	        public float x_pos;
	        public float y_pos;

		IconHolder(ImageView view, Drawable baseIcon,int id) {
	            this.view = view;
	            this.defaultIcon = baseIcon;
	            this.id=id;
	           
	        }
	}
	 
	 public class FloatBinder extends Binder {
		 FloatService getService() {
 			//handler_in=handler;
 			return FloatService.this;
		 }
	 }
	 public void onCreate() {
		 super.onCreate();
		 //Log.e("TAG", "POPO");
	     windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
	     packageManager = getPackageManager();
	     
	 }
	 private void addIconToScreen(int x, int y,final int id) {	
			//Toast.makeText(getBaseContext(), "addIcon", Toast.LENGTH_SHORT).show();
			 
			 	final ImageView iconView = new ImageView(this);
			 	 viewList.add(iconView);
			  Drawable draw = getIcon(id);
			  iconView.setImageDrawable(draw);
			  
			  ViewConfiguration vc = ViewConfiguration.get(iconView.getContext());
			  final int mScaledTouchSlop = 60;
			  final int mLongPressTimeOut = 1000;//vc.getLongPressTimeout();
			  final int mTapTimeOut = 1000;//vc.getTapTimeout();
			 IconHolder iconHolder = new IconHolder(iconView, draw,id);

		        final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
		        setWindowParams(params, y, y);
		        windowManager.addView(iconView, params);
		        
		        iconView.setOnTouchListener(new View.OnTouchListener() {
		            private WindowManager.LayoutParams paramsF = params;
		            private int initialX;
		            private int initialY;
		            private float initialTouchX;
		            private float initialTouchY;

		            @Override
		            public boolean onTouch(View v, MotionEvent event) {
		                switch (event.getAction()) {
		                    case MotionEvent.ACTION_DOWN:
		                        //Log.d("AppFloat", "Action Down");
		                        initialX = paramsF.x;
		                        initialY = paramsF.y;
		                        initialTouchX = event.getRawX();
		                        initialTouchY = event.getRawY();
		                        return false;
		                    case MotionEvent.ACTION_MOVE:
		                        //Log.d("AppFloat", "Action Move");
		                        paramsF.x = initialX - (int) (event.getRawX() - initialTouchX);
		                        paramsF.y = initialY - (int) (event.getRawY() - initialTouchY);
		                        windowManager.updateViewLayout(iconView, paramsF);
		                        return false;
		                    case MotionEvent.ACTION_UP:
		                        //Log.d("AppFloat", "Action Up");
		                        //Log.d("AppFloat", "DistanceX: " + Math.abs(initialTouchX - event.getRawX()));
		                        //Log.d("AppFloat", "DistanceY: " + Math.abs(initialTouchY - event.getRawY()));
		                        //Log.d("AppFloat", "elapsed gesture time: " + (event.getEventTime() - event.getDownTime()));
		                        if((Math.abs(initialTouchX - event.getRawX()) <= mScaledTouchSlop) && (Math.abs(initialTouchY - event.getRawY()) <= mScaledTouchSlop)) {
		                            if((event.getEventTime() - event.getDownTime()) < mTapTimeOut ) {
		                                //Log.d("AppFloat", "Click Detected");
		                                
		                                startAppActivity(id);
		                                
		                            } else if((event.getEventTime() - event.getDownTime()) >= mLongPressTimeOut) {
		                                //Log.d("AppFloat", "Long Click Detected");
		                            }
		                        }
		                    default:
		                        //Log.d("AppFloat", "Action Default");
		                        break;
		                }
		                return false;
		            }

		        });
		    }
	 private void startAppActivity(int id) {
		 System.out.println("click: "+id);
		 
		 switch(id){
		 case 0:
			 Intent startMain = new Intent(Intent.ACTION_MAIN);
			 startMain.addCategory(Intent.CATEGORY_HOME);
			 startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			 startActivity(startMain);
			 Intent intent = packageManager.getLaunchIntentForPackage("com.feixin.smarthome");
			 if(intent != null)
				 startActivity(intent);
			 removeIconsFromScreen();
			 /*Intent intent = packageManager.getLaunchIntentForPackage("com.android.BluetoothRemote");
			 if(intent != null)
				 startActivity(intent);
			 amKillProcess("com.dlink.mydlink");
			 removeIconsFromScreen();*/
			 
			 break;
		 case 1:
			 //FloatService.handler_in.obtainMessage(MainActivity.SEND_MESSAGE, this).sendToTarget();
			 break;
		 }
		/* Intent intent = packageManager.getLaunchIntentForPackage("com.android.BluetoothRemote");
		 if(intent != null)
			 startActivity(intent);
	     //activityManager.killBackgroundProcesses("org.videolan.vlc.betav7neon");
	     //FloatService.handler_in.obtainMessage(MainActivity.OPENTHEDOOR, this).sendToTarget();
	     removeIconsFromScreen();*/
	       
	        
	 }
	 public void amKillProcess(String process)
	 {
	     ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
	     final List<RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();

	     for(RunningAppProcessInfo runningProcess : runningProcesses) 
	     {
	         if(runningProcess.processName.equals(process)) 
	         {               
	             android.os.Process.sendSignal(runningProcess.pid, android.os.Process.SIGNAL_KILL);   
	         }
	     }
	 }
	 private void setWindowParams(WindowManager.LayoutParams params, int x, int y) {
	 	params.height = WindowManager.LayoutParams.WRAP_CONTENT;
	 	params.width = WindowManager.LayoutParams.WRAP_CONTENT;
	    params.type = WindowManager.LayoutParams.TYPE_PHONE;
	    params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
	    params.format = PixelFormat.TRANSLUCENT;
	    params.gravity = Gravity.BOTTOM | Gravity.RIGHT;
	    if(x < 0 || y < 0) {
	    	params.y = dpToPx(100)  * viewList.size();
	    } else {
	    	params.x = (int) x;
	        params.y = (int) y;
	    }
	 }
	 	
	 private int dpToPx(int dp) {
		 return (int) (dp * getResources().getDisplayMetrics().density + 0.5f);
	 }
	private Drawable getIcon(int id) {
	 	Drawable icon = null;
	 		//Log.v("AppFloat", "Using default image for icon");
	 		switch (id){
	 		case 0:
	 			icon = getResources().getDrawable(R.drawable.return_back);
	 			break;
	 		
	 		}
	    	
		    return icon;
	}
	 public void floatApp(int id) {
	     addIconToScreen( -1, -1,id);
	 }
	 public void floatApp(int x ,int y ,int id){
		 addIconToScreen( x, y, id);
	 }
	 public void removeIconsFromScreen() {
		 for(ImageView view : viewList ) {
	            windowManager.removeView(view);
	        }
	        viewList.clear();
	        }
	        
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return binder;
	}
	
}
