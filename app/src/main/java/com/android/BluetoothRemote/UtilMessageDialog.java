package com.android.BluetoothRemote;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

import com.android.util.MyApplication;
import com.lee.gcm.QuickstartPreferences;
import com.mylhyl.circledialog.CircleDialog;
import com.mylhyl.circledialog.params.ProgressParams;

public class UtilMessageDialog {

    private static Toast toast;
    private static AlertDialog.Builder dialog;
    private static AlertDialog alert;
    private static DialogFragment dialogFragment;

    @SuppressLint("ShowToast")
    public static void showToast(Context context, String content) {
        if (toast == null) {
            toast = Toast.makeText(context, content, Toast.LENGTH_SHORT);
        } else {
            toast.setText(content);
        }
        toast.show();
    }

    public static void messageDialog(final Context context, String title, String message) {

        dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title);
        dialog.setPositiveButton("確定",
                (dialog, which) -> dialog.dismiss());

        dialog.setNegativeButton("開啟該功能",
                (dialog, which) -> PreferenceManager.getDefaultSharedPreferences(context).edit().putString(
                        QuickstartPreferences.IR_STATUS,"開啟").apply());

        alert = dialog.create();
        alert.setMessage(message);
        alert.setCancelable(false);
        alert.setCanceledOnTouchOutside(false);
        alert.show();

    }

    public static void dismissAlert() {
        try {
            alert.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setDialogFragment_progress(FragmentActivity activity, String title, String message) {
        dialogFragment = new CircleDialog.Builder(activity)
                .setProgressText(message)
                .setCancelable(true)
                .setCanceledOnTouchOutside(false)
                .setProgressStyle(ProgressParams.STYLE_SPINNER)
                .show();

    }

    public static void setDialogFragment_progress(FragmentActivity activity, String title, String message, boolean cancel, boolean touch_outside) {
        dialogFragment = new CircleDialog.Builder(activity)
                .setProgressText(message)
                .setCancelable(cancel)
                .setCanceledOnTouchOutside(touch_outside)
                .setProgressStyle(ProgressParams.STYLE_SPINNER)
                .show();

    }

    public static void dismissDialogFragment() {
        try {
            for (int i = 0; i <= 3; i++)
                dialogFragment.dismissAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setDialogFragment_progress(String title, String message) {
        if (MyApplication.currentActivity() != null) {
            try {
                if (MyApplication.currentActivity() != null) {
                    dialogFragment = new CircleDialog.Builder((FragmentActivity) MyApplication.currentActivity())
                            .setCanceledOnTouchOutside(false)
                            .setCancelable(false)
                            .setTitle(title)
                            .setText(message)
                            .setNegative("確定", null)
                            .show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


}