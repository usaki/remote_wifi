package com.android.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.BluetoothRemote.ThreadPoolUtil;
import com.android.BluetoothRemote.UtilMessageDialog;
import com.android.RemoteWiFi.R;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mylhyl.circledialog.BaseCircleDialog;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

//import okhttp3.FormBody;
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
import static com.android.BluetoothRemote.BluetoothChat.D;
import static com.android.util.MySocketThread.getCurentWifiSSID;
import static com.android.util.MySocketThread.getWiFiIP;

/**
 * Created by Feixin532 on 2017/10/6.
 */

public class APSettingDialog extends BaseCircleDialog implements View.OnClickListener {

    static APSettingDialog dialogFragment = new APSettingDialog();
    //private FormBody requestBody;
    private Context context;
    RequestQueue mQueue;

    @Override
    public View createView(Context context, LayoutInflater inflater, ViewGroup container) {
        this.context = context;
        return inflater.inflate(R.layout.dialog_ap, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().findViewById(R.id.btnAP_Cancel).setOnClickListener(this);
        getView().findViewById(R.id.btnAP_Setting).setOnClickListener(this);
        mQueue = Volley.newRequestQueue(context);
        String ssid = getCurentWifiSSID(getContext());
        if(D) Log.d("測試","ssid-->" + ssid);

        if(ssid.contains("eHome")){
            if(D) Log.d("測試","連線對");
        }else{
            UtilMessageDialog.showToast(getContext(),"請確認已連線至\n雲主機配對的無線分享器");
            dialogFragment.dismiss();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btnAP_Cancel:
               dialogFragment.dismiss();
                break;

            case R.id.btnAP_Setting:
                String strAP_Acc = String.valueOf(((EditText) getView().findViewById(R.id.et_AP_Acc)).getText());
                String strAP_Pw = String.valueOf(((EditText) getView().findViewById(R.id.et_AP_Pw)).getText());

                if(strAP_Acc!=null && strAP_Pw!=null)
                    AP_Setting(strAP_Acc,strAP_Pw);
                else
                    UtilMessageDialog.showToast(getContext(),"請輸入PPPOE撥號帳號及密碼");

                break;
        }

    }

    public static APSettingDialog getInstance() {
        dialogFragment.setCanceledBack(false);
        dialogFragment.setCanceledOnTouchOutside(false);
        return dialogFragment;
    }

    private void AP_Setting(final String acc, final String pw){

        //******************************//
        //判斷當前DHCP 分配到的IP是哪一種
        //根據IP切換host

        String ip = getWiFiIP(context);
        StringTokenizer st = new StringTokenizer(ip, ".");
        String[] tmp = new String[4];
        String url = "";
        int count = 0;
        while (st.hasMoreTokens()){
            tmp[count] = st.nextToken();
            count++;
        }


        if(tmp[2].equals("6")){
            url = "http://192.168.6.1/goform/formWanTcpipSetup";
        }else if(tmp[2].equals("2")){
            url = "http://192.168.2.1/goform/formWanTcpipSetup";
        }

        //*****************************//

        final String finalUrl = url;
        ThreadPoolUtil.getInstance().execute(new Runnable() {
            @Override
            public void run() {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, finalUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        UtilMessageDialog.showToast(context,"設定成功\\n請稍等約60秒，等待分享器重啟");
                        dialogFragment.dismiss();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        UtilMessageDialog.showToast(context,"分享器PPPOE設定失敗，請確認手機已正常連線至分享器(WiFi)");
                        dialogFragment.dismiss();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("pppUserName",acc);
                        map.put("pppPassword",pw);
                        map.put("macAddr", "000000000000");
                        map.put("DNSMode", "0");
                        map.put("httpProxyEnable", "0");
                        map.put("pppEnTtl", "0");
                        map.put("pppServName", "");
                        map.put("pppMTU", "1392");
                        map.put("ConnectType", "0");
                        map.put("DUAL_WAN_IGMP", "0");
                        map.put("duallAccessMode", "0");
                        map.put("dynIPHostName", "");
                        map.put("macAddr2", " ");
                        map.put("ip", "172.1.1.1");
                        map.put("mask", "255.255.0.0");
                        map.put("gateway", "172.1.1.254");
                        map.put("iqsetupclose", "1");
                        map.put("submit-url", "/inter_wan.asp");
                        map.put("wanMode", "2");
                        map.put("ipMode", "ppp");
                        map.put("pppConnect", "");
                        map.put("pppDisconnect", "");
                        map.put("pppConnectType", "0");
                        map.put("pppIdleTime", "10");
                        map.put("enableDuallAccess", "OFF");
                        map.put("isApply", "1");

                        return map;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", "Basic YWRtaW46Zng1MzI1MzI1Nw==");

                        return params;
                    };
                };

                mQueue.add(stringRequest);



                /*requestBody = new FormBody.Builder()
                        .add("pppUserName",acc)
                        .add("pppPassword",pw)
                        .add("macAddr", "000000000000")
                        .add("DNSMode", "0")
                        .add("httpProxyEnable", "0")
                        .add("pppEnTtl", "0")
                        .add("pppServName", "")
                        .add("pppMTU", "1392")
                        .add("ConnectType", "0")
                        .add("DUAL_WAN_IGMP", "0")
                        .add("duallAccessMode", "0")
                        .add("dynIPHostName", "")
                        .add("macAddr2", " ")
                        .add("ip", "172.1.1.1")
                        .add("mask", "255.255.0.0")
                        .add("gateway", "172.1.1.254")
                        .add("iqsetupclose", "1")
                        .add("submit-url", "/inter_wan.asp")
                        .add("wanMode", "2")
                        .add("ipMode", "ppp")
                        .add("pppConnect", "")
                        .add("pppDisconnect", "")
                        .add("pppConnectType", "0")
                        .add("pppIdleTime", "10")
                        .add("enableDuallAccess", "OFF")
                        .add("isApply", "1")
                        .build();

                OkHttpClient client = new OkHttpClient();

                Request request = new Request.Builder()
                        .url(finalUrl)
                        .addHeader("Authorization","Basic YWRtaW46Zng1MzI1MzI1Nw==")
                        .post(requestBody)
                        .build();

                String strAP_Setting_Result = "";
                okhttp3.Response response = null;
                try {
                    response = client.newCall(request).execute();
                    if (response != null && response.isSuccessful()) {
                        strAP_Setting_Result = "設定成功\n請稍等約60秒，等待分享器重啟";
                    }else{
                        strAP_Setting_Result = "分享器PPPOE設定失敗，請確認手機已正常連線至分享器(WiFi)";
                    }

                    UtilMessageDialog.setDialogFragment_progress((FragmentActivity) MyApplication.currentActivity(),"AP設定",strAP_Setting_Result);
                    response.body().close();
                    dialogFragment.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/








            }
        });

    }
}
