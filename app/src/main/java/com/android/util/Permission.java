package com.android.util;

import android.Manifest;
import android.util.Log;

import com.android.BluetoothRemote.BluetoothChat;
import com.yanzhenjie.permission.AndPermission;

/**
 * Created by Feixin532 on 2017/10/27.
 */

public class Permission {
    private static final String TAG = "Permission";
    private BluetoothChat bluetoothChat;
    private String[] colPermisson = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public Permission(BluetoothChat bluetoothChat) {
        this.bluetoothChat = bluetoothChat;
        PermissonCheck();
    }



    private void PermissonCheck(){

        //four Permission
        if(AndPermission.hasPermission(bluetoothChat,colPermisson))
            Log.d(TAG,"權限檢查:有權限");
        else {
            Log.d(TAG,"權限檢查:沒權限->詢問使用者授權");
            AndPermission.with(bluetoothChat)
                    .requestCode(100)
                    .permission(colPermisson)
                    .send();


        }


    }

}

