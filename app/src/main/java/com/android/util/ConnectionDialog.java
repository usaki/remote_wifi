package com.android.util;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.RemoteWiFi.R;
import com.mylhyl.circledialog.BaseCircleDialog;

import java.io.IOException;

import static com.android.BluetoothRemote.BluetoothChat.viewPager;
import static com.android.util.ResultCode.page_air;
import static com.android.util.ResultCode.page_custom1;
import static com.android.util.ResultCode.page_custom2;
import static com.android.util.ResultCode.page_custom3;
import static com.android.util.ResultCode.page_ip_config;
import static com.android.util.ResultCode.page_lamp;
import static com.android.util.ResultCode.page_macro;
import static com.android.util.ResultCode.page_tv;

/**
 * Created by Feixin532 on 2017/10/31.
 */

public class ConnectionDialog extends BaseCircleDialog implements View.OnClickListener {

    static ConnectionDialog dialogFragment = new ConnectionDialog();

    @Override
    public View createView(Context context, LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.activity_main, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void closeDialog() {
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

    }

    public static ConnectionDialog getInstance() {
        dialogFragment.setCanceledBack(true);
        dialogFragment.setCanceledOnTouchOutside(false);
        return dialogFragment;
    }




    }


