package com.android.util;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.text.format.Formatter;
import android.util.Log;


import com.android.BluetoothRemote.ThreadPoolUtil;
import com.lee.gcm.QuickstartPreferences;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Arrays;
import java.util.List;

import javax.net.SocketFactory;

import static android.content.Context.WIFI_SERVICE;
import static com.android.BluetoothRemote.BluetoothChat.D;
import static com.android.util.ResultCode.SEND_FAILED;
import static com.android.util.ResultCode.SEND_OK;
import static com.android.util.ResultCode.SEND_RECIEVE;

/**
 * Created by usaki on 2017/7/26.
 * Static Socket for all Application
 */

public class MySocketThread {
    private static final String TAG = "MySocketThread";
    private static boolean Receive = true;
    private static PowerManager pm = null;
    // 數據
    private String type;
    private String command;
    private String[] allAlert;
    private String alertOpenNow = "";

    // 網路連線
    private static Socket skt;
    private static String ip = "";
    private static int port;
    private static boolean reConnectionStatus = false;
    private static boolean connectionStatus = false;
    private static int errorCount = 0;

    // 執行緒
    private static Handler ht;
    private static Message ms;
    private static Bundle bundle = new Bundle();
    private int flag;
    private byte[] buffer;
    private static DataInputStream in;
    private static DataOutputStream out;
    private Context context;
    public SharedPreferences IPname;


    public MySocketThread(final Handler ht, Context context, int flag, final String command) {
        pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        reConnectionStatus = false;
        MySocketThread.ht = ht;
        this.flag = flag;
        this.command = command;
        this.context = context;
        newSocket();
    }

    private void newSocket() {
        if (pm.isScreenOn()) {
            ThreadPoolUtil.getInstance().execute(() -> {
                try {
                    if (netlink()) {
                        out = new DataOutputStream(skt.getOutputStream());

                        sendmessage(command);
                        ht.sendEmptyMessage(SEND_OK);
                    }

                } catch (Exception e1) {
                    if (D) Log.d(TAG, "送值錯誤\n");
                    SOcketExcetion(e1.getClass().toString());
                    e1.printStackTrace();
                    //待做 當前網路模式無法連接時-->判斷是否自動切換模式
                }
            });
        }
    }

    private void checkRecStatus() {
        if (D)
            Log.d(TAG, "連線中斷，重連判斷\t錯誤次數:\t" + errorCount + "\treConnectionStatus\t" + reConnectionStatus);
        if ((errorCount == 0)) {
            if (!reConnectionStatus) {
                if (D) Log.d(TAG, "連線中斷，嘗試重新連線");
                reConnection();
                ht.sendEmptyMessage(SEND_FAILED);
            }
        }
    }

    private void reConnection() {
        //測試 當斷線時，嘗試回復連線，錯誤閥值
        reConnectionStatus = false;
        ThreadPoolUtil.getInstance().execute(() -> {
            for (int i = 0; i < 3; i++) {
                if (!connectionStatus && errorCount < 3) {
                    if (D) Log.d(TAG, "重新嘗試連線 次數:" + errorCount + 1);
                    try {
                        Thread.sleep(500);
                        newSocket();
                        errorCount++;
                    } catch (InterruptedException e) {
                        SOcketExcetion(e.getClass().toString());
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private void sendmessage(String message) {
        if (D) Log.d(TAG, "傳值\t" + message);
        try {
            out.write(message.getBytes("GBK"));
        } catch (Exception e) {
            if (D) Log.d(TAG, "輸出串流異常，sendmessage error");
            StopSocket();
            checkRecStatus();
            e.printStackTrace();
        }
    }

    private static void ReceiveThread() {
        //接收
        ThreadPoolUtil.getInstance().execute(() -> {
            // TODO Auto-generated method stub
            try {
                if (D) Log.d(TAG, "接收串流Receive狀態-" + Receive);
                while (Receive) {
                    //偵測螢幕關閉-->中斷Socket連線
                    if (!pm.isScreenOn()) StopSocket();


                    byte[] buffer;
                    in = new DataInputStream(skt.getInputStream());
                    buffer = new byte[in.available()];
                    if (buffer.length != 0) {
                        in.read(buffer);
                        String msg = new String(buffer, "GBK");
                        Message m = new Message();
                        m.what = SEND_RECIEVE;
                        m.obj = msg;
                        ht.sendMessage(m);
                        if (D) Log.d(TAG, "接收串流--" + Arrays.toString(buffer));

                    }
                }
            } catch (Exception e) {
                if (D) Log.d(TAG, "接收串流發生錯誤");
                StopSocket();
                SOcketExcetion(e.getClass().toString());
                e.printStackTrace();
            }
        });
    }

    private boolean netlink() {
        if (null == skt) {
            try {
                getAddress();
                if (D) Log.d(TAG, "初始化連線:" + ip + "/" + port);
                skt = SocketFactory.getDefault().createSocket();
                SocketAddress remoteaddr = new InetSocketAddress(ip, port);
                skt.connect(remoteaddr, 8000);
                if (skt.isConnected()) {
                    connectionStatus = true;
                    Receive = true;
                    errorCount = 0;
                    if (D) Log.d(TAG, "連線建立完成");
                    ReceiveThread();
                    ht.sendEmptyMessage(SEND_OK);

                    return true;
                } else {
                    ht.sendEmptyMessage(SEND_FAILED);
                    StopSocket();
                    return false;
                }
            } catch (Exception e) {
                if (D) Log.d(TAG, "SOCKET初始化失敗\t");
                SOcketExcetion(e.getClass().toString());
                e.printStackTrace();
                return false;
            }
        } else {
            if (!skt.isConnected() && !skt.isClosed()) {
                if (D) Log.d(TAG, "!skt.isConnected() && !skt.isClosed()");
                StopSocket();
                ht.sendEmptyMessage(SEND_FAILED);
                return false;
            }
            return  true;
        }
    }


    private void getAddress() {
        //依據spinner取當前設定之IP、PORT
        String spinner = PreferenceManager.getDefaultSharedPreferences(this.context).getString(QuickstartPreferences.SPINNER_WHICH, "0");
        IPname = this.context.getSharedPreferences("IPDATA", 0);
        if (spinner.equals("0")) {
            //預設第一組
            ip = "192.168.2.66";
            port = 8899;
            if (D) Log.d(TAG, "第一組 192.168.2.66 8899");
        } else if (spinner.equals("1")) {
            // 預設第二組
            ip = "192.168.6.99";
            port = 8899;
            if (D) Log.d(TAG, "第二組 192.168.6.99 8899");
        } else {
            //其餘組別

            ip = IPname.getString("IP" + (Integer.valueOf(spinner)), "");

            if(IPname.getString("PORT" + (Integer.valueOf(spinner)), "").length() > 0){
                port = Integer.parseInt(IPname.getString("PORT" + (Integer.valueOf(spinner)), ""));
            }

            if (D) Log.d(TAG, "第" + spinner + "組 "  + ip + "'\t" + port);
        }
    }


    public static void StopSocket() {
        try {

            Receive = false;
            connectionStatus = false;


            if (in != null) {
                if (D) Log.d(TAG, "釋放InputSteam資源");
                in = null;
            }


            if (out != null) {
                if (D) Log.d(TAG, "釋放OutputSteam資源");
                out = null;
            }

            if (skt != null) {
                if (D) Log.d(TAG, "釋放Socket資源");
                skt = null;
            }


        } catch (Exception e) {
            if (D) Log.d(TAG, "釋放Socket失敗\t" + e.getMessage());
            SOcketExcetion(e.getClass().toString());
        }
    }

    private static void SOcketExcetion(String title) {

        if (D) Log.d(TAG, title);

        if (title.indexOf("SocketTimeou") > 0)
            title = "主機連線逾時";
        else
            title = "主機連線中斷";

        ms = new Message();
        bundle.putString("title", title);
        ms.what = SEND_FAILED;
        ms.setData(bundle);
        ht.sendMessage(ms);
    }

    /**
     * 判斷當前手機是否連上Wifi.
     *
     * @param context 上下文
     * @return boolean 是否連上網絡
     * <p>
     * *
     */
    static public boolean isWifiConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWiFiNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (mWiFiNetworkInfo != null) {
                return mWiFiNetworkInfo.isAvailable() && mWiFiNetworkInfo.isConnected();
            }
        }
        return false;
    }

    /**
     * 判斷當前手機的網絡是否可用.
     *
     * @param context 上下文
     * @return boolean 是否連上網絡
     * <p>
     * *
     */
    static public boolean isMobileConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobileNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (mMobileNetworkInfo != null) {
                return mMobileNetworkInfo.isAvailable() && mMobileNetworkInfo.isConnected();
            }
        }
        return false;
    }

    /**
     * 判斷當前網絡是手機網絡還是WIFI.
     *
     * @param context 上下文
     * @return ConnectedType 數據類型
     * <p>
     * *
     */
    public static int getConnectedType(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            // 獲取代表聯網狀態的NetWorkInfo對象
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            // 判斷NetWorkInfo對象是否為空；判斷當前的網絡連接是否可用
            if (mNetworkInfo != null && mNetworkInfo.isAvailable()) {
                return mNetworkInfo.getType();
            }
        }
        return -1;
    }

    /**
     * 獲取當前WIFI的SSID.
     *
     * @param context 上下文
     * @return ssid
     * <p>
     * *
     */
    public static String getCurentWifiSSID(Context context) {
        String ssid = "";
        if (context != null) {
            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();

            if (wifiInfo != null) {
                ssid = wifiInfo.getSSID();
                if (ssid.substring(0, 1).equals("\"") && ssid.substring(ssid.length() - 1).equals("\"")) {
                    ssid = ssid.substring(1, ssid.length() - 1);
                }
            }

        }
        return ssid;
    }

    /**
     * 用來獲得手機掃描到的所有wifi的信息.
     *
     * @param c 上下文
     * @return the current wifi scan result
     */
    static public List<ScanResult> getCurrentWifiScanResult(Context c) {
        WifiManager wifiManager = (WifiManager) c.getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiManager.startScan();
        return wifiManager.getScanResults();
    }

    static public String getConnectWifiSsid(Context c) {
        String ssid = "";
        WifiManager wifiManager = (WifiManager) c.getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo != null) {
            ssid = wifiInfo.getSSID();
        }
        return ssid;
    }

    // 以下是獲得版本信息的工具方法

    // 版本名
    public static String getVersionName(Context context) {
        return getPackageInfo(context).versionName;
    }

    // 版本號
    public static int getVersionCode(Context context) {
        return getPackageInfo(context).versionCode;
    }

    private static PackageInfo getPackageInfo(Context context) {
        PackageInfo pi = null;

        try {
            PackageManager pm = context.getPackageManager();
            pi = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_CONFIGURATIONS);

            return pi;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pi;
    }

    // 檢測android 應用在前台還是後台
    public static boolean isBackground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(context.getPackageName())) {
                /*
                 * BACKGROUND=400 EMPTY=500 FOREGROUND=100 GONE=1000
				 * PERCEPTIBLE=130 SERVICE=300 ISIBLE=200
				 */
                Log.i(context.getPackageName(), "此appimportace =" + appProcess.importance
                        + ",context.getClass().getName()=" + context.getClass().getName());
                if (appProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    Log.i(context.getPackageName(), "處於後台" + appProcess.processName);
                    return true;
                } else {
                    Log.i(context.getPackageName(), "處於前台" + appProcess.processName);
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean sktConnetion() {
        return skt != null && skt.isConnected();
    }

    /**
     *取得當前連線WiFi分配的IP
     *目前該方法不支援IPv6
     *
     * @return ip
     *
     *
     */
    static String getWiFiIP(Context context){
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        return Formatter.formatIpAddress(ip);
    }



}