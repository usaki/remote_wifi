package com.android.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.BluetoothRemote.BluetoothChat;
import com.android.RemoteWiFi.R;
import com.mylhyl.circledialog.BaseCircleDialog;

import java.io.IOException;

import static com.android.BluetoothRemote.BluetoothChat.viewPager;
import static com.android.util.ResultCode.page_air;
import static com.android.util.ResultCode.page_custom1;
import static com.android.util.ResultCode.page_custom2;
import static com.android.util.ResultCode.page_custom3;
import static com.android.util.ResultCode.page_ip_config;
import static com.android.util.ResultCode.page_lamp;
import static com.android.util.ResultCode.page_macro;
import static com.android.util.ResultCode.page_tv;

/**
 * Created by Feixin532 on 2017/10/30.
 */

public class MenuDialog extends BaseCircleDialog implements View.OnClickListener {

    static MenuDialog dialogFragment = new MenuDialog();
    private View dialog_menu;


    @Override
    public View createView(Context context, LayoutInflater inflater, ViewGroup container) {
        dialog_menu = inflater.inflate(R.layout.menu_dailog, null);
        return inflater.inflate(R.layout.menu_dailog, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().findViewById(R.id.cancel).setOnClickListener(v -> closeDialog());


        getView().findViewById(R.id.start1).setOnClickListener(v -> {

            viewPager.setCurrentItem(page_macro);
            closeDialog();
        });

        getView().findViewById(R.id.start4).setOnClickListener(v -> {
            viewPager.setCurrentItem(page_lamp);
            //sendmessage("W+j?u");
            closeDialog();
        });

        Button start7 = getView().findViewById(R.id.start7);
        start7.setOnClickListener(v -> {
            viewPager.setCurrentItem(page_air);
            closeDialog();
        });


        Button start2 = getView().findViewById(R.id.start2);
        start2.setOnClickListener(v -> {
            viewPager.setCurrentItem(page_tv);
            closeDialog();
        });

        Button start5 = getView().findViewById(R.id.start5);
        start5.setOnClickListener(v -> {
            viewPager.setCurrentItem(page_custom1);
            closeDialog();
        });

        Button start8 = getView().findViewById(R.id.start8);
        start8.setOnClickListener(v -> {
            viewPager.setCurrentItem(page_custom2);
            closeDialog();
        });


        Button start3 = getView().findViewById(R.id.start3);
        start3.setOnClickListener(v -> {
            viewPager.setCurrentItem(page_custom3);
            closeDialog();
        });

        Button start9 = getView().findViewById(R.id.start9);
        start9.setOnClickListener(v -> {
            viewPager.setCurrentItem(page_ip_config);
            closeDialog();
        });
//                Button start10 = (Button)dialog_menu.findViewById(R.id.start10);
//                start10.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        viewPager.setCurrentItem(page_window);
//                        Runtime runtime = Runtime.getRuntime();
//                        try {
//                            runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
//                        } catch (IOException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                        }
//                    }
//                });

    }

    private void closeDialog() {
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {


    }

    public static MenuDialog getInstance() {
        dialogFragment.setCanceledBack(true);
        dialogFragment.setCanceledOnTouchOutside(false);
        return dialogFragment;
    }
}
