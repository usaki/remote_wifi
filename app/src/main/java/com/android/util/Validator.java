package com.android.util;
import android.content.Context;
import android.util.Log;

import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import static com.android.BluetoothRemote.BluetoothChat.D;
import static com.android.util.AbsSuperApplication.getContext;
import static com.android.util.MySocketThread.getWiFiIP;

/**
 * Created by Feixin532 on 2017/11/3.
 */

public class Validator {

    public static boolean isValidIPAddr(String msg) {
        boolean result = true;

        StringTokenizer st = new StringTokenizer(msg, ".");
        String[] tmp = new String[4];
        int count = 0;
        while (st.hasMoreTokens()){
            tmp[count] = st.nextToken();
            count++;
        }

        try{
            if(D) Log.d("IP驗證","-內容:" + msg + "-長度:" + tmp.length);
            if (tmp.length == 4) {
                for (int i = 0; i < tmp.length; i++) {
                    if (Integer.parseInt(tmp[i]) > 255) {
                        result = false;
                    }
                }
            }else{
                result = false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }


        return result;
    }


    public static void TestTest(String str, Context context){
        StringTokenizer st = new StringTokenizer(getWiFiIP(context), ".");
        String[] tmp = new String[4];
        String url = "";
        int count = 0;
        while (st.hasMoreTokens()){
            tmp[count] = st.nextToken();
            count++;
        }


        if(tmp[2].equals("6")){
            url = "http://192.168.6.1/goform/formWanTcpipSetup";
        }else if(tmp[2].equals("2")){
            url = "http://192.168.2.1/goform/formWanTcpipSetup";
        }else {
            url = "?";
        }

        if(D) Log.d("WiFi IP 測試","url:" + url + "\t body:" + Arrays.toString(tmp));
    }


}
