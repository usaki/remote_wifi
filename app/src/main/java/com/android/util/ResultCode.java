package com.android.util;

/**
 * Created by Feixin532 on 2017/6/26.
 */

public class ResultCode {


    //MENU
    public static final int page_macro = 0;
    public static final int page_lamp = 1;
    public static final int page_tv = 2;
    public static final int page_air = 3;
    public static final int page_custom1 = 4;
    public static final int page_custom2 = 5;
    public static final int page_custom3 = 6;
    public static final int page_ip_config = 7;



    //SOCKET
    public static final byte SEND_OK = 11;
    public static final byte SEND_RECIEVE = 12;
    public static final byte SEND_FAILED = 13;
    public static final byte SEND_TIMEOUT = 14;





}