package com.lee.gcm;

/**
 * Created by lee on 2015/7/17.
 */
public class QuickstartPreferences {


    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String DEVICE_TOKEN = "DEVICE_TOKEN";
    public static final String USER_TOKEN = "USER_TOKEN";
    public static final String NETWORK_TYPE = "NETWORK_TYPE";
    public static final String NETWORK_DEVICE_IP = "NETWORK_DEVICE_IP";
    public static final String RTC_FROM = "RTC_FROM";
    public static final String DEVICE_TYPE = "DEVICE_TYPE";
    public static final String SPINNER_WHICH = "SPINNER_WHICH";
    public static final String IR_STATUS = "IR_STATUS";
    public static final String USER_USE_STATUS = "USER_USE_STATUS";

}